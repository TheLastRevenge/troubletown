
public ShowPostBox(Client)
{
	new String:SteamID[20], String:query[256];
	GetClientAuthString(Client, SteamID, sizeof(SteamID));
	Format(query, 512, "SELECT idx, count, text FROM postbox WHERE steamid = '%s';", SteamID);
	SQL_TQuery(databasehandle, Query_PostBox, query, Client);
}

public Query_PostBox(Handle:owner, Handle:hndl, const String:error[], any:Client)
{
	if(hndl == INVALID_HANDLE)
	{
		new Handle:menuhandle = g_player_postbox_Menu[Client];
		RemoveAllMenuItems(menuhandle);
		
		SetMenuExitButton(menuhandle, true);
		SetMenuExitBackButton(menuhandle, true);
		SetMenuTitle(menuhandle, "POST BOX - 0");
		for(new i; i<7; i++) AddMenuItem(menuhandle, "", "", ITEMDRAW_SPACER);
		DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
		return;
	}
	new Handle:menuhandle = g_player_postbox_Menu[Client];
	SetGlobalTransTarget(Client);
	RemoveAllMenuItems(menuhandle);
	
	SetMenuExitButton(menuhandle, true);
	SetMenuExitBackButton(menuhandle, true);
	new row_count = SQL_GetRowCount(hndl);
	SetMenuTitle(menuhandle, "POST BOX - %i", row_count);
	if(row_count)
	{
		decl String:s_data[16], String:text[160];
		while(SQL_FetchRow(hndl))
		{
			new index = SQL_FetchInt(hndl, 0);
			new count = SQL_FetchInt(hndl, 1);
			Format(s_data, sizeof(s_data), "%i/%i", index, count);
			SQL_FetchString(hndl, 2, text, 128);
			new String:print[256];
			if(index != 0) Format(print, sizeof(print), "%s x%i\n%s", ItemData[index][Name], count, text);
			else Format(print, sizeof(print), "%t x%i\n%s", "point", count, text);
			AddMenuItem(menuhandle, s_data, print);
		}
	}
	for(new i=GetMenuItemCount(menuhandle); i<7; i++)
	{
		AddMenuItem(menuhandle, "", "", ITEMDRAW_SPACER);
	}
	SetMenuExitButton(menuhandle, true);
	SetMenuExitBackButton(menuhandle, true);
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

stock SendPostToClient(Client, postIndex, postCount, const String:postText[])
{
	new String:sExcapedText[128];
	SQL_EscapeString(databasehandle, postText, sExcapedText, sizeof(sExcapedText));
	
	new String:SteamID[20], String:query[256];
	GetClientAuthString(Client, SteamID, sizeof(SteamID));
	Format(query, sizeof(query), "INSERT INTO postbox(steamid, idx, count, text) VALUES('%s', '%d', '%d', '%s');", SteamID, postIndex, postCount, sExcapedText);
	SQL_TQuery(databasehandle, save_info, query);
	
	if(IsClientInGame(Client))
	{
		PrintToChat(Client, "\x04[PostBox] \x01- \x08FF0000C8New postal matter arrived");
	}
}