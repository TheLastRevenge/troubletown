
public Command_Initialize()
{
	RegServerCmd("pointshop_player_info", Command_PointShop_Info, "ServerCommand : Show Player Info");
	RegServerCmd("pointshop_give_point", Command_PointShop_GivePoint, "ServerCommand : Give Command");
	RegServerCmd("pointshop_give_item", Command_PointShop_GiveItem, "ServerCommand : Give Command");
	RegServerCmd("pointshop_use_tag", Command_PointShop_Tag, "ServerCommand : Tag Command");
	RegServerCmd("pointshop_adjust_hat", Command_PointShop_AdjustHat, "ServerCommand : Adjust Hat Position Command");
	
	
	RegAdminCmd("sm_item", Command_Item, ADMFLAG_ROOT, "Add Item Command");
	RegAdminCmd("sm_point", Command_Point, ADMFLAG_ROOT, "Add Point Command");
	RegAdminCmd("sm_postbox", Command_PostBox, ADMFLAG_ROOT);
	
	AddCommandListener(Keyboard_F1_CallBack, "autobuy");
}

public Action:Command_PointShop_Info(Args)
{
	if(GetCmdArgs() != 2)
	{
		return Plugin_Handled;
	}
	
	new String:sClientUserId[8], String:sTargetUserId[8];
	GetCmdArg(1, sClientUserId, sizeof(sClientUserId));
	GetCmdArg(2, sTargetUserId, sizeof(sTargetUserId));
	
	new Client = GetClientOfUserId(StringToInt(sClientUserId));
	new Target = GetClientOfUserId(StringToInt(sTargetUserId));
	if(!Client || !Target)
		return Plugin_Handled;
	
	ShowClientInfo(Client, Target);
	return Plugin_Handled;
}

public Action:Command_PointShop_GivePoint(Args)
{
	if(GetCmdArgs() != 2)
	{
		return Plugin_Handled;
	}
	
	new String:sClientUserId[8], String:sTargetUserId[8];
	GetCmdArg(1, sClientUserId, sizeof(sClientUserId));
	GetCmdArg(2, sTargetUserId, sizeof(sTargetUserId));
	
	new Client = GetClientOfUserId(StringToInt(sClientUserId));
	new Target = GetClientOfUserId(StringToInt(sTargetUserId));
	if(!Client || !Target || !PlayerData[Client][DataLoaded] || !PlayerData[Target][DataLoaded])
		return Plugin_Handled;
	
	PlayerData[Client][SendPoint] = 0;
	SendPointMenu(Client, Target);
	return Plugin_Handled;
}

public Action:Command_PointShop_GiveItem(Args)
{
	if(GetCmdArgs() != 2)
	{
		return Plugin_Handled;
	}
	
	new String:sClientUserId[8], String:sTargetUserId[8];
	GetCmdArg(1, sClientUserId, sizeof(sClientUserId));
	GetCmdArg(2, sTargetUserId, sizeof(sTargetUserId));
	
	new Client = GetClientOfUserId(StringToInt(sClientUserId));
	new Target = GetClientOfUserId(StringToInt(sTargetUserId));
	if(!Client || !Target || !PlayerData[Client][DataLoaded] || !PlayerData[Target][DataLoaded])
		return Plugin_Handled;
	
	new String:SteamID[20], String:query[256];
	GetClientAuthString(Client, SteamID, sizeof(SteamID));
	Format(query, sizeof(query), "SELECT id,idx,strengthen FROM inventory WHERE steamid = '%s' ORDER BY idx ASC;", SteamID);
	
	new Handle:datapack = CreateDataPack();
	WritePackCell(datapack, Client);
	WritePackCell(datapack, Target);
	SQL_TQuery(databasehandle, SendItemMenu, query, datapack);
	return Plugin_Handled;
}

public Action:Command_PointShop_Tag(Args)
{
	if(GetCmdArgs() != 2)
	{
		return Plugin_Handled;
	}
	
	new String:sClientUserId[8], String:sTag[16], String:sEscapedTag[16];
	GetCmdArg(1, sClientUserId, sizeof(sClientUserId));
	GetCmdArg(2, sTag, sizeof(sTag));
	SQL_EscapeString(databasehandle, sTag, sEscapedTag, sizeof(sEscapedTag));
	
	new Client = GetClientOfUserId(StringToInt(sClientUserId));
	if(!Client || !PlayerData[Client][DataLoaded])
		return Plugin_Handled;
	
	/*if(태그 기간권 작동중)
	{
		LastCheckToUseTag(Client, sTag);
		return Plugin_Handled;
	}*/
	
	new Handle:datapack = CreateDataPack();
	WritePackCell(datapack, StringToInt(sClientUserId));
	WritePackString(datapack, sTag);
	
	new String:query[256], String:SteamID[20];
	GetClientAuthString(Client, SteamID, sizeof(SteamID));
	
	Format(query, sizeof(query), "SELECT id FROM inventory WHERE steamid = '%s' AND idx = '%d'", SteamID, DISPOSABLE_TAG_UTILIZATION);
	SQL_TQuery(databasehandle, CheckValid_TagUtilization, query, datapack);
	
	return Plugin_Handled;
}

public Action:Command_PointShop_AdjustHat(Args)
{
	if(GetCmdArgs() != 2)
	{
		return Plugin_Handled;
	}
	
	new String:sClientUserId[8], String:sSkinNumber[8];
	GetCmdArg(1, sClientUserId, sizeof(sClientUserId));
	GetCmdArg(2, sSkinNumber, sizeof(sSkinNumber));
	
	new Client = GetClientOfUserId(StringToInt(sClientUserId));
	if(!Client || !PlayerData[Client][DataLoaded])
		return Plugin_Handled;
	
	if(IsPlayerHavingHat(Client))
	{
		AdjustPlayerHat(Client, g_hat_number[Client], StringToInt(sSkinNumber), EntRefToEntIndex(g_hat_reference[Client]));
	}
	return Plugin_Handled;
}

public Action:Command_Item(Client, Args)
{
	if(Args != 2) {
		return Plugin_Handled;
	}
	new String:SteamID[20];
	GetClientAuthString(Client, SteamID, sizeof(SteamID));
	if(StrEqual(SteamID, ADMIN_STEAMID, false) == false || StrEqual(SteamID, SIRA_STEAMID, false) == false)
		return Plugin_Handled;
	
	new String:s_SearchName[32], String:s_number[12];
	GetCmdArg(1, s_SearchName, sizeof(s_SearchName));
	GetCmdArg(2, s_number, 12);
	
	new count = 0, Target = 0;
	for(new i=1; i <= MaxClients; i++)
	{
		if(IsClientInGame(i))
		{
			new String:Other[32];
			GetClientName(i, Other, sizeof(Other));
			if(StrContains(Other, s_SearchName, false) != -1)
			{
				Target = i;
				count++;
			}
		}
	}
	
	if(count == 0)
	{
		PrintToChat(Client, "%s님을 찾을수 없습니다.", s_SearchName);
		return Plugin_Handled;
	}
	if(count > 1)
	{
		PrintToChat(Client, "%s라는 이름을 소유한 사람이 %i명입니다.", s_SearchName, count);
		return Plugin_Handled;
	}
	
	new item_number = StringToInt(s_number);
	
	new String:query[256];
	GetClientAuthString(Target, SteamID, sizeof(SteamID));
	Format(query, 256, "INSERT INTO inventory(steamid, idx, type) values ('%s', '%d', '%d');", SteamID, item_number, ItemData[item_number][Type]);
	SQL_TQuery(databasehandle, save_info, query);
	
	PrintToChat(Client, "\x05%N님에게 %s를 주었습니다.", Target, ItemData[item_number][Name]);
	PrintToChat(Target, "\x05당신은 어드민에게 %s를 받았습니다.", ItemData[item_number][Name]);

	return Plugin_Handled;
}

public Action:Command_Point(Client, Args)
{
	if(Args != 2) {
		return Plugin_Handled;
	}
	new String:SteamID[20];
	GetClientAuthString(Client, SteamID, sizeof(SteamID));
	if(StrEqual(SteamID, ADMIN_STEAMID, false) == false)
		return Plugin_Handled;
	
	new String:s_SearchName[32], String:s_number[12];
	GetCmdArg(1, s_SearchName, sizeof(s_SearchName));
	GetCmdArg(2, s_number, 12);
	
	new count = 0, Target = 0;
	for(new i=1; i <= MaxClients; i++)
	{
		if(IsClientInGame(i))
		{
			new String:Other[32];
			GetClientName(i, Other, sizeof(Other));
			if(StrContains(Other, s_SearchName, false) != -1)
			{
				Target = i;
				count++;
			}
		}
	}
	
	if(count == 0)
	{
		PrintToChat(Client, "%s님을 찾을수 없습니다.", s_SearchName);
		return Plugin_Handled;
	}
	if(count > 1)
	{
		PrintToChat(Client, "%s라는 이름을 소유한 사람이 %i명입니다.", s_SearchName, count);
		return Plugin_Handled;
	}
	
	if(!PlayerData[Target][DataLoaded]) {
		PrintToChat(Client, "Player Data Not Loaded.");
		return Plugin_Handled;
	}
	
	new point = StringToInt(s_number);
	PlayerData[Target][Point] += point;
	SavePoint(Target);
	
	PrintToChat(Client, "\x05%N님에게 %d포인트를 주었습니다.", Target, point);
	PrintToChat(Target, "\x05당신은 어드민에게 %d포인트를 받았습니다.", point);

	return Plugin_Handled;
}

public Action:Command_PostBox(Client, Args)
{
	if(Args < 3)
	{
		return Plugin_Handled;
	}
	
	new String:Item[32], String:Amount[32], String:Text[128];
	GetCmdArg(1, Item, sizeof(Item));
	GetCmdArg(2, Amount, sizeof(Amount));
	GetCmdArg(3, Text, sizeof(Text));
	
	for(new i=1; i<=MaxClients; i++)
	{
		if(IsClientConnected(i) == true && IsFakeClient(i) == false)
		{
			SendPostToClient(i, StringToInt(Item), StringToInt(Amount), Text);
		}
	}
	new String:SteamID[20];
	GetClientAuthString(Client, SteamID, sizeof(SteamID));
	LogShopData("%N(%s) use command postbox %s(%s) %s counts \"%s\"", Client, SteamID, ItemData[StringToInt(Item)][Name], Item, Amount, Text);
	return Plugin_Handled;
}

public Action:Keyboard_F1_CallBack(Client, const String:command[], arg)
{
	ShowMainShop(Client);
}