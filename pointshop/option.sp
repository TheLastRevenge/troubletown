
public ShowOption(Client)
{
	new Handle:menuhandle = g_player_option_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	SetGlobalTransTarget(Client);
	
	SetMenuTitle(menuhandle, "--- TTT %t %t(%t) ---", "point", "shop", "option");
	for(new i = 1; i < MaxType+1; i++)
	{
		new String:s_number[4], String:sitemInfo[256];
		IntToString(i, s_number, sizeof(s_number));
		Format(sitemInfo, 256, "%t %t/%t", TypeName[i][0], "on", "off");
		AddMenuItem(menuhandle, s_number, sitemInfo);
	}
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}