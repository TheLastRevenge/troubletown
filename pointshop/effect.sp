
new hEffectEntity[MAXPLAYERS+1];

#define Youmu_Ghost 41
#define MagicCircle 42
#define Yuyuko_Butterfly 43

public CreatePlayerEffect(Client)
{
	new effect = PlayerData[Client][ItemID][EffectType];
	if(ItemData[effect][Type] != EffectType || !PlayerData[Client][ItemOn][EffectType])
	{
		return;
	}
	
	if(effect == Youmu_Ghost)
	{
		CreateGhost(Client);
	}
	if(effect == MagicCircle)
	{
		CreateTimer(0.1, CreateCircle, Client);
	}
	if(effect == Yuyuko_Butterfly)
	{
		CreateButterfly(Client);
	}
}

public RemovePlayerEffect(Client)
{
	new entity = EntRefToEntIndex(hEffectEntity[Client]);
	if(entity > 0)
	{
		AcceptEntityInput(entity, "Kill");
		RemoveEdict(entity);
	}
	hEffectEntity[Client] = INVALID_ENT_REFERENCE;
}

public CreateGhost(Client)
{
	RemovePlayerEffect(Client);
	new Float:vecOrigin[3];
	GetClientEyePosition(Client, vecOrigin);
	
	new projectile = CreateEntityByName("hegrenade_projectile");
	DispatchSpawn(projectile);
	
	new EntEffects = GetEntProp(projectile, Prop_Send, "m_fEffects");
	EntEffects |= 32;
	SetEntProp(projectile, Prop_Send, "m_fEffects", EntEffects);
	SetEntityRenderMode(projectile, RENDER_TRANSCOLOR);
	SetEntityRenderColor(projectile, 255, 255, 255, 0);
	SetEntityMoveType(projectile, MOVETYPE_NOCLIP);
	SetEntProp(projectile, Prop_Send, "m_CollisionGroup", 1);
	SetEntPropEnt(projectile, Prop_Send, "m_hOwnerEntity", Client);
	
	new sprite = CreateEntityByName("env_sprite");
	DispatchKeyValue(sprite, "model", "sprites/glow1.vmt");
	DispatchKeyValue(sprite, "renderamt", "255");
	DispatchKeyValue(sprite, "rendermode", "5");
	DispatchKeyValue(sprite, "spawnflags", "1");
	DispatchKeyValue(sprite, "scale", "1.1");
	DispatchSpawn(sprite);
	
	SetVariantString("!activator");
	AcceptEntityInput(sprite, "SetParent", projectile);
	
	new trail = CreateEntityByName("env_spritetrail");
	DispatchKeyValue(trail, "spritename", "sprites/laserbeam.vmt");
	DispatchKeyValue(trail, "startwidth", "16.0");
	DispatchKeyValue(trail, "endwidth", "0.5");
	DispatchKeyValue(trail, "renderamt", "255");
	DispatchKeyValue(trail, "rendermode", "5");
	DispatchKeyValue(trail, "lifetime", "0.4");
	DispatchSpawn(trail);
	
	SetVariantString("!activator");
	AcceptEntityInput(trail, "SetParent", sprite);
	
	TeleportEntity(projectile, vecOrigin, NULL_VECTOR, NULL_VECTOR);
	TeleportEntity(sprite, Float:{0.0, 0.0, 26.0}, NULL_VECTOR, NULL_VECTOR);
	
	hEffectEntity[Client] = EntIndexToEntRef(projectile);
	
	CreateTimer(0.1, SeekPose_Owner, hEffectEntity[Client], TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
}

public Action:CreateCircle(Handle:timer, any:Client)
{
	RemovePlayerEffect(Client);
	if(!IsClientInGame(Client) || !IsPlayerAlive(Client))
	{
		return;
	}
	
	new Float:vecOrigin[3];
	GetClientAbsOrigin(Client, vecOrigin);
	
	new prop = CreateEntityByName("prop_dynamic_override");
	DispatchKeyValue(prop, "model", ItemData[MagicCircle][Path]);
	DispatchKeyValue(prop, "DefaultAnim", "idle");
	DispatchKeyValue(prop, "skin", "1");
	DispatchSpawn(prop);
	AcceptEntityInput(prop, "TurnOn", Client);
	AcceptEntityInput(prop, "DisableShadow");
	
	SetEntPropFloat(prop, Prop_Send, "m_flModelScale", 0.32);
	SetEntProp(prop, Prop_Send, "m_CollisionGroup", 1);
	TeleportEntity(prop, vecOrigin, NULL_VECTOR, NULL_VECTOR);
	SetVariantString("!activator");
	AcceptEntityInput(prop, "SetParent", GetPlayerWeaponSlot(Client, 2));
	
	/*new func_rotating = CreateEntityByName("func_rotating");
	DispatchKeyValue(func_rotating, "targetname", ""); 
	DispatchKeyValue(func_rotating, "renderfx", "0"); 
	DispatchKeyValue(func_rotating, "rendermode", "0"); 
	DispatchKeyValue(func_rotating, "renderamt", "255"); 
	DispatchKeyValue(func_rotating, "rendercolor", "255 255 255"); 
	DispatchKeyValue(func_rotating, "maxspeed", "30"); 
	DispatchKeyValue(func_rotating, "friction", "20"); 
	DispatchKeyValue(func_rotating, "dmg", "0"); 
	DispatchKeyValue(func_rotating, "solid", "0"); 
	DispatchKeyValue(func_rotating, "spawnflags", "64"); 
	DispatchSpawn(func_rotating);
	TeleportEntity(func_rotating, vecOrigin, NULL_VECTOR, NULL_VECTOR);
	ParentToEntity(func_rotating, GetPlayerWeaponSlot(Client, 4));
	
	ParentToEntity(prop, func_rotating);
	AcceptEntityInput(func_rotating, "Start");*/
	
	TeleportEntity(prop, Float:{0.0, 0.0, -24.0}, NULL_VECTOR, NULL_VECTOR);
	
	//hEffectEntity[Client] = EntIndexToEntRef(func_rotating);
	hEffectEntity[Client] = EntIndexToEntRef(prop);
}

public CreateButterfly(Client)
{
	RemovePlayerEffect(Client);
	new Float:vecOrigin[3];
	GetClientEyePosition(Client, vecOrigin);
	
	new projectile = CreateEntityByName("hegrenade_projectile");
	DispatchSpawn(projectile);
	
	new EntEffects = GetEntProp(projectile, Prop_Send, "m_fEffects");
	EntEffects |= 32;
	SetEntProp(projectile, Prop_Send, "m_fEffects", EntEffects);
	SetEntityRenderMode(projectile, RENDER_TRANSCOLOR);
	SetEntityRenderColor(projectile, 255, 255, 255, 0);
	SetEntityMoveType(projectile, MOVETYPE_NOCLIP);
	SetEntProp(projectile, Prop_Send, "m_CollisionGroup", 1);
	SetEntPropEnt(projectile, Prop_Send, "m_hOwnerEntity", Client);
	
	new prop = CreateEntityByName("prop_dynamic_override");
	DispatchKeyValue(prop, "model", ItemData[Yuyuko_Butterfly][Path]);
	DispatchKeyValue(prop, "DefaultAnim", "idle");
	DispatchKeyValue(prop, "skin", "0");
	DispatchSpawn(prop);
	AcceptEntityInput(prop, "TurnOn", Client);
	AcceptEntityInput(prop, "DisableShadow");
	new color[3] = {255, 255, 255};
	for(new i; i<3; i++) color[i] = GetRandomInt(0, 255);
	SetEntityRenderMode(prop, RENDER_TRANSCOLOR);
	SetEntityRenderColor(prop, color[0], color[1], color[2], 255);
	
	SetVariantString("!activator");
	AcceptEntityInput(prop, "SetParent", projectile);
	
	new sprite = CreateEntityByName("env_sprite");
	DispatchKeyValue(sprite, "model", "sprites/glow1.vmt");
	DispatchSpawn(sprite);
	SetEntityRenderMode(sprite, RENDER_TRANSCOLOR);
	SetEntityRenderColor(sprite, color[0], color[1], color[2], 255);
	DispatchKeyValue(sprite, "renderamt", "150");
	DispatchKeyValue(sprite, "rendermode", "5");
	DispatchKeyValue(sprite, "spawnflags", "1");
	DispatchKeyValue(sprite, "scale", "0.6");
	
	SetVariantString("!activator");
	AcceptEntityInput(sprite, "SetParent", prop);
	
	TeleportEntity(projectile, vecOrigin, NULL_VECTOR, NULL_VECTOR);
	TeleportEntity(prop, Float:{0.0, 0.0, 26.0}, NULL_VECTOR, NULL_VECTOR);
	
	hEffectEntity[Client] = EntIndexToEntRef(projectile);
	
	CreateTimer(0.1, SeekPose_Owner, hEffectEntity[Client], TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
}

public Action:SeekPose_Owner(Handle:Timer, any:ref)
{
	new entity = EntRefToEntIndex(ref);
	if(entity > 0 && IsValidEntity(entity))
	{
		if(GetEntityMoveType(entity) == MOVETYPE_NOCLIP)
		{
			new owner = GetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity");
			if(owner > 0 && IsClientInGame(owner) && IsPlayerAlive(owner))
			{
				new Float:vecOrigin[3], Float:angle[3], Float:entityposition[3], Float:vector[3];
				GetClientEyePosition(owner, vecOrigin);
				GetEntPropVector(entity, Prop_Send, "m_vecOrigin", entityposition);
				vecOrigin[2] -= FloatSub(entityposition[2], vecOrigin[2]);
				MakeVectorFromPoints(entityposition, vecOrigin, vector);
				GetVectorAngles(vector, angle);
				new Float:dist = NormalizeVector(vector, vector);
				if(dist > 60.0)
				{
					dist -= 60.0;
					if(dist < 30) ScaleVector(vector, FloatMul(FloatDiv(1.0, 6.0), FloatMul(dist, dist)));
					else if(dist < 720.0) ScaleVector(vector, FloatMul(dist, 5.0));
					else ScaleVector(vector, 3600.0);
					TeleportEntity(entity, NULL_VECTOR, angle, vector);
				}
				else TeleportEntity(entity, NULL_VECTOR, angle, Float:{0.0, 0.0, 0.0});
			}
			else
			{
				AcceptEntityInput(entity, "Kill");
				RemoveEdict(entity);
			}
		}
	}
	else return Plugin_Stop;
	return Plugin_Continue;
}