
public ShowClientInfo(Client, Target)
{
#if defined DEBUG_PLUGIN
	if(!OwnerCheck(Client))
	{
		PrintToChat(Client, "\x04[TTT - ShowInfo] \x01- 현재 점검중입니다.");
		return;
	}
#endif
	new String:Formatting[256], String:SteamID[20];
	GetClientAuthString(Target, SteamID, sizeof(SteamID));
	
	new time = RoundToZero(PlayerData[Target][PlayingTime]);
	new second = time%60;
	new minute = (time/60)%60;
	new hour = time/3600;
	new String:sSecond[4], String:sMinute[4], String:sHour[6];
	Format(sSecond, 4, "%s%d", (second<10)?"0":"", second);
	Format(sMinute, 4, "%s%d", (minute<10)?"0":"", minute);
	Format(sHour, 6, "%s%d", (hour<10)?"0":"", hour);
	
	new Handle:menuhandle = g_player_info_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	SetGlobalTransTarget(Client);
	
	SetMenuTitle(menuhandle, "--- %N%t ---", Target, "person's info");
	Format(Formatting, 256, "%t : %N", "nick", Target);
	AddMenuItem(menuhandle, "", Formatting, ITEMDRAW_DISABLED);
	Format(Formatting, 256, "%t : %s", "steamid", SteamID);
	new String:OwnerSteamID[20];
	if(GetOwnerAuthString(Target, OwnerSteamID, sizeof(OwnerSteamID))) {
		if(StrEqual(SteamID, OwnerSteamID) == false) {
			Format(Formatting, 256, "%s\n└%t : %s", Formatting, "owner steamid", OwnerSteamID);
		}
	}
	AddMenuItem(menuhandle, "", Formatting, ITEMDRAW_DISABLED);
	
	Format(Formatting, 256, "%t : %i", "point", PlayerData[Target][Point]);
	AddMenuItem(menuhandle, "", Formatting, ITEMDRAW_DISABLED);
	Format(Formatting, 256, "%t : %s:%s:%s\n========================", "playing time",sHour,sMinute,sSecond);
	AddMenuItem(menuhandle, "", Formatting, ITEMDRAW_DISABLED);
	
	new String:s_number[5];
	for(new i = 1; i < MaxType+1; i++)
	{
		if(PlayerData[Target][ItemExist][i] == true)
		{
			new id = PlayerData[Target][ItemID][i];
			Format(Formatting, 256, "%t %t : %s", TypeName[i][0], "name", ItemData[id][Name]);
			if(PlayerData[Target][Strengthen][i]) Format(Formatting, 256, "%s +%d강", Formatting, PlayerData[Target][Strengthen][i]);
			IntToString(id, s_number, sizeof(s_number));
			AddMenuItem(menuhandle, s_number, Formatting, (Client==Target)?ITEMDRAW_DEFAULT:ITEMDRAW_DISABLED);
		}
		else
		{
			Format(Formatting, 256, "%t %t : %t", TypeName[i][0], "name", "none");
			AddMenuItem(menuhandle, "", Formatting, ITEMDRAW_DISABLED);
		}
	}
	
	if(Client == Target || GetUserAdmin(Client) != INVALID_ADMIN_ID)
	{
		Format(Formatting, 256, "Karma Point : %i", TTT_GetClientKarmaScore(Target));
		AddMenuItem(menuhandle, "", Formatting, ITEMDRAW_DISABLED);
		GetClientIP(Target, Formatting, sizeof(Formatting), true);
		Format(Formatting, 256, "IP : %s", Formatting);
		AddMenuItem(menuhandle, "", Formatting, ITEMDRAW_DISABLED);
		AddMenuItem(menuhandle, "", "", ITEMDRAW_SPACER);
	}
	
	SetMenuExitBackButton(menuhandle, (Client==Target)?true:false);
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

/*public MyInfo_CallBack(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		if(DataLoaded[Client])
		{
			new String:StringNumber[256], Number;
			GetMenuItem(menu, select, StringNumber, 256);
			Number = StringToInt(StringNumber);
			if(ItemAdmin[Number] == true)
			{
				PrintChat(Client, "\x04[TTT Item] \x01착용 해제 불가능한 아이템입니다.");
				return;
			}
			PrintChat(Client, "\x04[TTT Item] \x03%s\x01을/를 착용 해제하셨습니다.", ItemName[Number]);
			new String:query[512], String:SteamID[32];
			GetClientAuthString(Client, SteamID, sizeof(SteamID));
			ItemExist[Client][ItemType[Number]] = false;
			ItemID[Client][ItemType[Number]] = 0;
			Format(query, 512, "DELETE FROM %s WHERE steamid = '%s';", TypeName[ItemType[Number]][1], SteamID);
			SQL_TQuery(databasehandle, save_info, query, Client);
			Format(query, 512, "INSERT INTO inventory(steamid, idx, type) values ('%s', '%d', '%d');", SteamID, Number, ItemType[Number]);
			SQL_TQuery(databasehandle, save_info, query, Client);
			new String:Log[256], String:Time[30], String:ClientName[32];
			GetClientName(Client, ClientName, 32);
			while(StrContains(ClientName, "'") != -1) ReplaceStringEx(ClientName, 32, "'", "`");
			FormatTime(Time, 30, "%Y/%m/%d - %H:%M:%S", GetTime());
			Format(Log, 256, "%s unequip %s(%i)", ClientName, ItemName[Number], Number);
			Format(query, 512, "INSERT INTO shoplog(steamid, log, type, date) values ('%s', '%s', '%i', '%s');", SteamID, Log, 4, Time);
			SQL_TQuery(databasehandle, save_info, query);
		}
	}
	if(action == MenuAction_Cancel)
	{
		if(select == MenuCancel_ExitBack)
		{
			MainShop(Client);
		}
	}
	if(action == MenuAction_End)
	{
		CloseHandle(menu);
	}
}*/