
public ShowPointShop(Client)
{
	new Handle:menuhandle = g_pontshop_shopmain_Menu;
	RemoveAllMenuItems(menuhandle);
	SetGlobalTransTarget(Client);
	
	SetMenuTitle(menuhandle, "TTT %t %t", "point", "shop");
	for(new i =  1; i < MaxType+1; i++)
	{
		AddMenuItemTranslated(menuhandle, TypeName[i][0]);
	}
	AddMenuItem(menuhandle, "", "Tag");
	AddMenuItem(menuhandle, "", "Box");
	SetMenuExitButton(menuhandle, true);
	SetMenuExitBackButton(menuhandle, true);
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

public ShowPointShop_Buy(Client, type)
{
	new Handle:menuhandle = g_pontshop_shop_Menu[type];
	if(!GetMenuItemCount(menuhandle)) {
		PrintToChat(Client, "\x05%T", "no items available", Client);
		return;
	}
	SetGlobalTransTarget(Client);
	
	SetMenuTitle(menuhandle, "TTT %t %t", "point", "shop");
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}