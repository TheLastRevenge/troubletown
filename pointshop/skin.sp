
public WearPlayerSkin(Client)
{
	new skin = PlayerData[Client][ItemID][SkinType];
	if(ItemData[skin][Type] != SkinType || !PlayerData[Client][ItemOn][SkinType])
	{
		return;
	}
	
	SetEntityModel(Client, ItemData[skin][Path]);
}