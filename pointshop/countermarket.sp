
public CounterMarket(Client)
{
	new Handle:menuhandle = g_pointshop_countermakret_Menu;
	RemoveAllMenuItems(menuhandle);
	SetGlobalTransTarget(Client);
	
	SetMenuTitle(menuhandle, "%t", "flea market");
	AddMenuItemTranslated(menuhandle, "buy");
	AddMenuItemTranslated(menuhandle, "sell");
	AddMenuItemTranslated(menuhandle, "my selling item");
	AddMenuItem(menuhandle, "", "Profit");
	SetMenuExitButton(menuhandle, true);
	SetMenuExitBackButton(menuhandle, true);
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

/*TODO
public CounterMarket_Search(Client, const String:word[])
{
	new String:sEscapedWord[strlen(word)*2+1];
	SQL_EscapeString(databasehandle, word, sEscapedWord, sizeof(sEscapedWord));
	
	new String:query[256];
	Format(query, sizeof(query), "SELECT nick,idx,price FROM countermarket WHERE 
}*/

public CounterMarket_Buy(Handle:owner, Handle:hndl, const String:error[], any:Client)
{
	if(hndl == INVALID_HANDLE)
	{
		CounterMarket(Client);
		return;
	}
#if defined DEBUG_PLUGIN
	if(!OwnerCheck(Client))
	{
		PrintToChat(Client, "\x04[TTT - CounterMarket] \x01- 현재 점검중입니다.");
		return;
	}
#endif
	new Handle:menuhandle = g_pointshop_marketbuy_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	SetGlobalTransTarget(Client);
	
	
	SetMenuTitle(menuhandle, "%t (%t)", "flea market", "buy");
	new count;
	if(SQL_GetRowCount(hndl))
	{
		while(SQL_FetchRow(hndl))
		{
			new String:SteamID[32], String:Uploader[32];
			SQL_FetchString(hndl, 1, SteamID, sizeof(SteamID));
			SQL_FetchString(hndl, 2, Uploader, 32);
			new id = SQL_FetchInt(hndl, 0);
			new number = SQL_FetchInt(hndl, 3);
			new price = SQL_FetchInt(hndl, 4);
			new strengthen = SQL_FetchInt(hndl, 5);
			
			new String:s_number[8], String:sItemText[128];
			IntToString(id, s_number, sizeof(s_number));
			Format(sItemText, sizeof(sItemText), "%s", ItemData[number][Name]);
			if(strengthen) Format(sItemText, sizeof(sItemText), "%s +%d강", sItemText, strengthen);
			Format(sItemText, sizeof(sItemText), "%s\nPoints : %d / Uploader : %s", sItemText, price, Uploader);
			AddMenuItem(menuhandle, s_number, sItemText, (PlayerData[Client][Point] >= price) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
			count++;
			if(count%4) AddMenuItem(menuhandle, NULL_STRING, "", ITEMDRAW_SPACER);
		}
	}
	else
	{
		AddMenuItem(menuhandle, NULL_STRING, "Nothing~", ITEMDRAW_DISABLED);
	}
	SetMenuExitButton(menuhandle, true);
	SetMenuExitBackButton(menuhandle, true);
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

public CounterMarket_Check(Client, id)
{
	new String:query[128];
	Format(query, sizeof(query), "SELECT id,steamid,idx,price,strengthen FROM countermarket WHERE id = '%d';", id);
	SQL_TQuery(databasehandle, CounterMarket_CheckBuy, query, Client);
}

public CounterMarket_CheckBuy(Handle:owner, Handle:hndl, const String:error[], any:Client)
{
	if(hndl == INVALID_HANDLE)
	{
		CounterMarket(Client);
		return;
	}
	
	if(!SQL_GetRowCount(hndl)) {
		PrintToChat(Client, "\x04[TTT Counter-Market] \x01- Item was sold out!");
	}
	
	if(SQL_FetchRow(hndl))
	{
		new id = SQL_FetchInt(hndl, 0);
		new number = SQL_FetchInt(hndl, 2);
		new price = SQL_FetchInt(hndl, 3);
		new strengthen = SQL_FetchInt(hndl, 4);
		
		PlayerData[Client][Point] -= price;
		SavePoint(Client);
		
		new String:SteamID[20], String:query[512];
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		Format(query, 512, "INSERT INTO inventory(steamid, idx, type, strengthen) values ('%s', '%d', '%d', '%d');", SteamID, number, ItemData[number][Type], strengthen);
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		Format(query, 512, "DELETE FROM countermarket WHERE id = '%d';", id);
		SQL_TQuery(databasehandle, save_info, query);
		
		new String:UploaderSteamID[32];
		SQL_FetchString(hndl, 1, UploaderSteamID, sizeof(UploaderSteamID));
		Format(query, 512, "INSERT INTO countermarket_profit(steamid, number, profit) values ('%s', '%d', '%d');", UploaderSteamID, number, price);
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		PrintToChat(Client, "\x04[TTT Counter-Market] \x01-당신은 거래장터에서 \x03%s\x01를 \x07FFFFFF%d\x01Points에 구매하셨습니다.", ItemData[number][Name], price);
		
		new String:sName[32], String:sEscapedName[65];
		GetClientName(Client, sName, sizeof(sName));
		SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
		
		LogShopData("%s(%s) buy %s(%d) with %d points at counter-market from %s's", sEscapedName, SteamID, ItemData[number][Name], number, price, UploaderSteamID);
		CounterMarket(Client);
	}
}

public CounterMarket_Sell(Handle:owner, Handle:hndl, const String:error[], any:Client)
{
	if(hndl == INVALID_HANDLE)
	{
		CounterMarket(Client);
		return;
	}
	new Handle:menuhandle = g_pointshop_marketsell_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	SetGlobalTransTarget(Client);
	
	SetMenuTitle(menuhandle, "%t (%t)", "flea market", "sell");
	if(SQL_GetRowCount(hndl))
	{
		while(SQL_FetchRow(hndl))
		{
			new id = SQL_FetchInt(hndl, 0);
			new item = SQL_FetchInt(hndl, 1);
			new strengthen = SQL_FetchInt(hndl, 2);
			new String:s_ItemInfo[24], String:s_ItemText[256];
			Format(s_ItemInfo, sizeof(s_ItemInfo), "%d %d %d", id, item, strengthen);
			Format(s_ItemText, sizeof(s_ItemText), "%s", ItemData[item][Name]);
			if(IsBoxItem(item))
			{
				Format(s_ItemText, sizeof(s_ItemText), "%s\n%s", s_ItemText, ItemData[item][Path]);
			}
			else if(strengthen)
			{
				Format(s_ItemText, sizeof(s_ItemText), "%s +%d강", s_ItemText, strengthen);
			}
			AddMenuItem(menuhandle, s_ItemInfo, s_ItemText);
		}
	}
	else
	{
		AddMenuItem(menuhandle, NULL_STRING, "You don't have any item", ITEMDRAW_DISABLED);
	}
	
	SetMenuExitButton(menuhandle, true);
	SetMenuExitBackButton(menuhandle, true);
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

public CounterMarket_SellPoint(Client, id, item, strengthen)
{
	new Handle:menuhandle = g_pointshop_marketsellpoint_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	
	new String:s_ItemInfo[24];
	Format(s_ItemInfo, sizeof(s_ItemInfo), "%d %d %d", id, item, strengthen);
	SetMenuTitle(menuhandle, "Upload Item(%s) with %dPoints", ItemData[item][Name], PlayerData[Client][SendPoint]);
	AddMenuItem(menuhandle, s_ItemInfo, "Upload", PlayerData[Client][SendPoint] ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	AddMenuItem(menuhandle, s_ItemInfo, "+10");
	AddMenuItem(menuhandle, s_ItemInfo, "+100");
	AddMenuItem(menuhandle, s_ItemInfo, "+1000");
	AddMenuItem(menuhandle, s_ItemInfo, "+10000");
	AddMenuItem(menuhandle, s_ItemInfo, "-10", (PlayerData[Client][SendPoint] >= 10) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	AddMenuItem(menuhandle, s_ItemInfo, "-100", (PlayerData[Client][SendPoint] >= 100) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	AddMenuItem(menuhandle, s_ItemInfo, "-1000", (PlayerData[Client][SendPoint] >= 1000) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	AddMenuItem(menuhandle, s_ItemInfo, "-10000", (PlayerData[Client][SendPoint] >= 10000) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	SetMenuExitButton(menuhandle, true);
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

public CounterMarket_MyItem(Handle:owner, Handle:hndl, const String:error[], any:Client)
{
	if(hndl == INVALID_HANDLE)
	{
		CounterMarket(Client);
		return;
	}
	new Handle:menuhandle = g_pointshop_market_myitem_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	SetGlobalTransTarget(Client);
	
	SetMenuTitle(menuhandle, "%t (%t)", "flea market", "my selling item");
	new count = SQL_GetRowCount(hndl);
	if(count)
	{
		while(SQL_FetchRow(hndl))
		{
			new id = SQL_FetchInt(hndl, 0);
			new item = SQL_FetchInt(hndl, 1);
			new price = SQL_FetchInt(hndl, 2);
			new strengthen = SQL_FetchInt(hndl, 3);
			
			new String:sItemInfo[24], String:sItemText[256];
			Format(sItemInfo, sizeof(sItemInfo), "%d %d %d", id, item, strengthen);
			Format(sItemText, sizeof(sItemText), "%s", ItemData[item][Name]);
			if(strengthen) Format(sItemText, sizeof(sItemText), "%s +%d강", sItemText, strengthen);
			Format(sItemText, sizeof(sItemText), "%s - %d Points", sItemText, price);
			AddMenuItem(menuhandle, sItemInfo, sItemText);
		}
	}
	else
	{
		AddMenuItem(menuhandle, NULL_STRING, "Nothing~", ITEMDRAW_DISABLED);
	}
	SetMenuExitButton(menuhandle, true);
	SetMenuExitBackButton(menuhandle, true);
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

public CounterMarket_Profit(Handle:owner, Handle:hndl, const String:error[], any:Client)
{
	if(hndl == INVALID_HANDLE)
	{
		CounterMarket(Client);
		return;
	}
	if(SQL_GetRowCount(hndl))
	{
		while(SQL_FetchRow(hndl))
		{
			new id = SQL_FetchInt(hndl, 0);
			new number = SQL_FetchInt(hndl, 1);
			new price = SQL_FetchInt(hndl, 2);
			
			PlayerData[Client][Point] += price;
			PrintToChat(Client, "\x04[TTT Counter-Market] \x01- You got %dPoints by selling %s", price,  ItemData[number][Name]);
			
			new String:SteamID[20], String:query[512];
			GetClientAuthString(Client, SteamID, sizeof(SteamID));
			Format(query, 512, "DELETE FROM countermarket_profit WHERE steamid = '%s' AND id = '%i';", SteamID, id);
			SQL_TQuery(databasehandle, save_info, query, Client);
			
			new String:sName[32], String:sEscapedName[65];
			GetClientName(Client, sName, sizeof(sName));
			SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
			
			LogShopData("%s(%s) get profit %d point by selling %s(%i)", sEscapedName, SteamID, price, ItemData[number][Name], number);
		}
		SavePoint(Client);
	}
	else
	{
		PrintToChat(Client, "\x04[TTT Counter-Market] \x01- No profit found.");
	}
	CounterMarket(Client);
}