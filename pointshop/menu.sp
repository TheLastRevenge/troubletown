
new Handle:g_pointshop_main_Menu;
new Handle:g_pointshop_inventory_Menu;
new Handle:g_pointshop_countermakret_Menu;
new Handle:g_pontshop_shop_Menu[MaxType+3];
new Handle:g_pontshop_shopmain_Menu;

new Handle:g_player_info_Menu[MAXPLAYERS+1];

new Handle:g_player_inventory_Menu[MAXPLAYERS+1];
new Handle:g_player_garbagecan_Menu[MAXPLAYERS+1];
new Handle:g_player_strengthen_Menu[MAXPLAYERS+1];

new Handle:g_player_option_Menu[MAXPLAYERS+1];
new Handle:g_player_postbox_Menu[MAXPLAYERS+1];

new Handle:g_player_givepoint_Menu[MAXPLAYERS+1];
new Handle:g_player_giveitem_Menu[MAXPLAYERS+1];

new Handle:g_pointshop_marketbuy_Menu[MAXPLAYERS+1];
new Handle:g_pointshop_marketsell_Menu[MAXPLAYERS+1];
new Handle:g_pointshop_marketsellpoint_Menu[MAXPLAYERS+1];
new Handle:g_pointshop_market_myitem_Menu[MAXPLAYERS+1];

new Handle:g_pointshop_trade_Menu[MAXPLAYERS+1];

new Handle:g_vip_selectbox_Menu[MAXPLAYERS+1];
new Handle:g_selectbox_Menu[MAXPLAYERS+1];
new Handle:g_cirno_selectbox_Menu[MAXPLAYERS+1];

new Handle:g_tag_select_Menu[MAXPLAYERS+1];

public Menu_Initialize()
{
	for(new i=1; i<=MAXPLAYERS; i++)
	{
		g_player_info_Menu[i] = CreateMenu(g_PlayerInfo_Handler);
		SetMenuExitButton(g_player_info_Menu[i], true);
		
		g_player_inventory_Menu[i] = CreateMenu(g_PlayerInventory_Handler);
		SetMenuExitButton(g_player_inventory_Menu[i], true);
		SetMenuExitBackButton(g_player_inventory_Menu[i], true);
		
		g_player_garbagecan_Menu[i] = CreateMenu(g_PlayerGarbageCan_Handler);
		SetMenuExitButton(g_player_garbagecan_Menu[i], true);
		SetMenuExitBackButton(g_player_garbagecan_Menu[i], true);
		
		g_player_strengthen_Menu[i] = CreateMenu(g_PlayerStrengthen_Handler);
		SetMenuExitButton(g_player_strengthen_Menu[i], true);
		
		g_player_option_Menu[i] = CreateMenu(g_PlayerOption_Handler);
		SetMenuExitButton(g_player_option_Menu[i], true);
		SetMenuExitBackButton(g_player_option_Menu[i], true);
		
		g_player_postbox_Menu[i] = CreateMenu(g_PlayerPostBox_Handler);
		SetMenuExitButton(g_player_postbox_Menu[i], true);
		SetMenuExitBackButton(g_player_postbox_Menu[i], true);
		
		g_player_givepoint_Menu[i] = CreateMenu(g_PlayerGivePoint_Handler);
		SetMenuExitButton(g_player_givepoint_Menu[i], true);
		SetMenuPagination(g_player_givepoint_Menu[i], MENU_NO_PAGINATION);
		
		g_player_giveitem_Menu[i] = CreateMenu(g_PlayerGiveItem_Handler);
		SetMenuExitButton(g_player_giveitem_Menu[i], true);
		
		g_pointshop_marketbuy_Menu[i] = CreateMenu(g_CounterMarketBuy_Handler);
		SetMenuExitButton(g_player_giveitem_Menu[i], true);
		
		g_pointshop_marketsell_Menu[i] = CreateMenu(g_CounterMarketSell_Handler);
		SetMenuExitButton(g_pointshop_marketsell_Menu[i], true);
		
		g_pointshop_marketsellpoint_Menu[i] = CreateMenu(g_CounterMarketSellPoint_Handler);
		SetMenuExitButton(g_pointshop_marketsellpoint_Menu[i], true);
		SetMenuPagination(g_pointshop_marketsellpoint_Menu[i], MENU_NO_PAGINATION);
		
		g_pointshop_market_myitem_Menu[i] = CreateMenu(g_CounterMarketMyItem_Handler);
		SetMenuExitButton(g_pointshop_market_myitem_Menu[i], true);
		
		g_pointshop_trade_Menu[i] = CreateMenu(g_TradeMenu_Handler);
		SetMenuExitButton(g_pointshop_trade_Menu[i], true);
		
		g_vip_selectbox_Menu[i] = CreateMenu(g_VIPSelectBox_Handler);
		SetMenuExitButton(g_vip_selectbox_Menu[i], true);
		
		g_selectbox_Menu[i] = CreateMenu(g_SelectBox_Handler);
		SetMenuExitButton(g_selectbox_Menu[i], true);
		
		g_cirno_selectbox_Menu[i] = CreateMenu(g_CirnoSelectBox_Handler);
		SetMenuExitButton(g_cirno_selectbox_Menu[i], true);
		
		g_tag_select_Menu[i] = CreateMenu(g_TagSelect_Handler);
		SetMenuExitButton(g_tag_select_Menu[i], false);
	}
	
	g_pointshop_main_Menu = CreateMenu(g_PointShopMain_Handler);
	SetMenuPagination(g_pointshop_main_Menu, MENU_NO_PAGINATION);
	
	g_pointshop_inventory_Menu = CreateMenu(g_PointShopInventory_Handler);
	
	g_pointshop_countermakret_Menu = CreateMenu(g_PointShopMarket_Handler);
	
	g_pontshop_shopmain_Menu = CreateMenu(g_PointShopSelect_Handler);
	for(new i=1; i<=MaxType+2; i++)
	{
		g_pontshop_shop_Menu[i] = CreateMenu(g_PointShopBuy_Handler);
	}
	for(new i=1; i<=MAX_ITEMS; i++)
	{
		if(!ItemData[i][Price] || ItemData[i][forEvent] || ItemData[i][forAdmin]) {
			continue;
		}
		new String:s_number[8], String:sItemInfo[128];
		IntToString(i, s_number, sizeof(s_number));
		Format(sItemInfo, sizeof(sItemInfo), "%s %iPoints", ItemData[i][Name], ItemData[i][Price]);
		if(!IsBoxItem(i)) {
			AddMenuItem(g_pontshop_shop_Menu[ItemData[i][Type]], s_number, sItemInfo);
		} else if(ItemData[i][Type] == 10) {
			AddMenuItem(g_pontshop_shop_Menu[MaxType+2], s_number, sItemInfo);
		}
	}
}

public SendPointMenu(Client, Target)
{
	if(!IsClientInGame(Target)) {
		PrintToChat(Client, "\x04[TTT] \x01- Target Miss");
		return;
	}
#if defined DEBUG_PLUGIN
	if(!OwnerCheck(Client))
	{
		PrintToChat(Client, "\x04[TTT - SendPoint] \x01- 현재 점검중입니다.");
		return;
	}
#endif
	new Handle:menuhandle = g_player_givepoint_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	
	new String:s_number[4];
	IntToString(Target, s_number, sizeof(s_number));
	SetMenuTitle(menuhandle, "Send %d Points To %N", PlayerData[Client][SendPoint], Target);
	AddMenuItem(menuhandle, s_number, "Send", PlayerData[Client][SendPoint] ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	AddMenuItem(menuhandle, s_number, "+10", (PlayerData[Client][Point] >= PlayerData[Client][SendPoint]+10) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	AddMenuItem(menuhandle, s_number, "+100", (PlayerData[Client][Point] >= PlayerData[Client][SendPoint]+100) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	AddMenuItem(menuhandle, s_number, "+1000", (PlayerData[Client][Point] >= PlayerData[Client][SendPoint]+1000) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	AddMenuItem(menuhandle, s_number, "+10000", (PlayerData[Client][Point] >= PlayerData[Client][SendPoint]+10000) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	AddMenuItem(menuhandle, s_number, "-10", (PlayerData[Client][SendPoint] >= 10) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	AddMenuItem(menuhandle, s_number, "-100", (PlayerData[Client][SendPoint] >= 100) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	AddMenuItem(menuhandle, s_number, "-1000", (PlayerData[Client][SendPoint] >= 1000) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	AddMenuItem(menuhandle, s_number, "-10000", (PlayerData[Client][SendPoint] >= 10000) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	SetMenuExitButton(menuhandle, true);
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

public SendItemMenu(Handle:owner, Handle:hndl, const String:error[], any:datapack)
{
	ResetPack(datapack);
	new Client = ReadPackCell(datapack);
	new Target = ReadPackCell(datapack);
	CloseHandle(datapack);
	
	if(!IsClientInGame(Client) || !IsClientInGame(Target))
	{
		return;
	}
	if(hndl == INVALID_HANDLE)
	{
		PrintToChat(Client, "\x05%T", "no items available", Client);
		return;
	}
	
#if defined DEBUG_PLUGIN
	if(!OwnerCheck(Client))
	{
		PrintToChat(Client, "\x04[TTT - SendItem] \x01- 현재 점검중입니다.");
		return;
	}
#endif
	
	if(SQL_GetRowCount(hndl) == 0)
	{
		PrintToChat(Client, "\x05%T", "no items available", Client);
		return;
	}
	new Handle:menuhandle = g_player_giveitem_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	
	SetMenuTitle(menuhandle, "Send Item To %N", Target);
	if(SQL_GetRowCount(hndl))
	{
		while(SQL_FetchRow(hndl))
		{
			new id = SQL_FetchInt(hndl, 0);
			new item = SQL_FetchInt(hndl, 1);
			new strengthen = SQL_FetchInt(hndl, 2);
			new String:s_ItemInfo[32], String:s_ItemText[256];
			Format(s_ItemInfo, sizeof(s_ItemInfo), "%d %d %d %d", id, item, strengthen, Target);
			Format(s_ItemText, sizeof(s_ItemText), "%s", ItemData[item][Name]);
			if(strengthen)
			{
				Format(s_ItemText, sizeof(s_ItemText), "%s +%d강", s_ItemText, strengthen);
			}
			AddMenuItem(menuhandle, s_ItemInfo, s_ItemText);
		}
		DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
	}
}

public ShowVIPSelectBox(Client)
{
	new Handle:menuhandle = g_vip_selectbox_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	
	SetMenuTitle(menuhandle, "Select Item what you want");
	
	new String:s_number[4];
	for(new i=1; i<=MAX_ITEMS; i++)
	{
		if(ItemData[i][Type] < 10 && !ItemData[i][forAdmin] && ItemData[i][Type] != TagType) // don't select box & admin item
		{
			IntToString(i, s_number, sizeof(s_number));
			AddMenuItem(menuhandle, s_number, ItemData[i][Name]);
		}
	}
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

public ShowSelectBox(Client)
{
	new Handle:menuhandle = g_selectbox_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	
	SetMenuTitle(menuhandle, "Select Item what you want");
	
	new String:s_number[4];
	for(new i=1; i<=MAX_ITEMS; i++)
	{
		if(ItemData[i][Type] < 10 && !ItemData[i][forAdmin]) // don't select box & admin item
		{
			IntToString(i, s_number, sizeof(s_number));
			AddMenuItem(menuhandle, s_number, ItemData[i][Name]);
		}
	}
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

public ShowCirnoSelectBox(Client)
{
	new Handle:menuhandle = g_cirno_selectbox_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	
	SetMenuTitle(menuhandle, "Select Cirno what you want");
	
	for(new i=1; i<=MAX_ITEMS; i++)
	{
		if(ItemData[i][Type] < 10 && !ItemData[i][forAdmin]) // don't select box & admin item
		{
			AddMenuItem(menuhandle, "11", ItemData[11][Name]);
		}
	}
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

public NULL_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_End)
	{
		CloseHandle(menu);
	}
}

public g_PlayerInfo_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		if(PlayerData[Client][DataLoaded] == false)
			return;
		
		new type = select-3;
		new equiped_number = PlayerData[Client][ItemID][type];
		new equiped_strengthen = PlayerData[Client][Strengthen][type];
		if(ItemData[equiped_number][forAdmin] == true)
		{
			PrintToChatAll("\x04[TTT Item] \x01어드민 메뉴에서 어드민 스킨을 해제하시기 바랍니다.");
			return;
		}
		new String:SteamID[20], String:query[512];
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		Format(query, 512, "DELETE FROM %s WHERE steamid = '%s';", TypeName[type][1], SteamID);
		SQL_TQuery(databasehandle, save_info, query, Client);
		Format(query, 512, "INSERT INTO inventory(steamid, idx, type, strengthen) values ('%s', '%d', '%d', '%d');", SteamID, equiped_number, type, equiped_strengthen);
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		new String:sName[32], String:sEscapedName[65];
		GetClientName(Client, sName, sizeof(sName));
		SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
		LogShopData("%s(%s) unequip %s(%i)", sEscapedName, SteamID, ItemData[equiped_number][Name], equiped_number);
		
		PlayerData[Client][ItemExist][type] = false;
		PlayerData[Client][ItemID][type] = 0;
		if(equiped_strengthen) PrintToChat(Client, "\x04[TTT - Equip] \x01- You unequipped \x07FFFFFF%s +%d강", ItemData[equiped_number][Name], equiped_strengthen);
		else PrintToChat(Client, "\x04[TTT - Equip] \x01- You unequipped \x07FFFFFF%s", ItemData[equiped_number][Name]);
	}
	if(action == MenuAction_Cancel)
	{
		if(select == MenuCancel_ExitBack)
		{
			ShowMainShop(Client);
		}
	}
}

public g_PointShopMain_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		if(select == 0)
		{
			ShowClientInfo(Client, Client);
		}
		if(select == 1)
		{
			ShowInventory(Client);
		}
		if(select == 2)
		{
			ShowPointShop(Client);
		}
		if(select == 3)
		{
			ShowOption(Client);
		}
		if(select == 4)
		{
			CounterMarket(Client);
		}
		if(select == 5)
		{
			new String:SteamID[20], String:query[128];
			GetClientAuthString(Client, SteamID, sizeof(SteamID));
			Format(query, 128, "SELECT nick,point FROM info ORDER BY point DESC;");
			SQL_TQuery(databasehandle, query_wealth, query, Client);
		}
		if(select == 6)
		{
			ShowPostBox(Client);
		}
		if(select == 7)
		{
			
		}
	}
}

public g_PointShopInventory_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		new String:SteamID[20], String:query[256];
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		if(select == MaxType+2)
		{
			Format(query, sizeof(query), "SELECT id,idx,strengthen FROM garbagecan WHERE steamid = '%s';", SteamID);
			SQL_TQuery(databasehandle, garbagecan, query, Client);
		}
		else
		{
			if(select == 0) Format(query, sizeof(query), "SELECT id,idx,strengthen FROM inventory WHERE steamid = '%s' ORDER BY idx ASC, strengthen DESC;", SteamID);
			else if(select == MaxType+1) Format(query, sizeof(query), "SELECT id,idx,strengthen FROM inventory WHERE steamid = '%s' AND type >= '10' ORDER BY idx ASC, strengthen DESC;", SteamID);
			else Format(query, sizeof(query), "SELECT id,idx,strengthen FROM inventory WHERE steamid = '%s' AND type = '%i' ORDER BY idx ASC, strengthen DESC;", SteamID, select);
			SQL_TQuery(databasehandle, inventory, query, Client);
		}
	}
	if(action == MenuAction_Cancel)
	{
		if(select == MenuCancel_ExitBack)
		{
			ShowMainShop(Client);
		}
	}
}

public g_PlayerInventory_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		new String:sItemInfo[24], String:sExplodedInfo[3][8];
		GetMenuItem(menu, select, sItemInfo, sizeof(sItemInfo));
		ExplodeString(sItemInfo, " ", sExplodedInfo, 3, 8);
		Prompt(Client, StringToInt(sExplodedInfo[0]), StringToInt(sExplodedInfo[1]), StringToInt(sExplodedInfo[2]));
	}
	if(action == MenuAction_Cancel)
	{
		if(select == MenuCancel_ExitBack)
		{
			ShowInventory(Client);
		}
	}
}

public g_PlayerGarbageCan_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		new String:sItemInfo[24], String:sExplodedInfo[3][8];
		GetMenuItem(menu, select, sItemInfo, sizeof(sItemInfo));
		ExplodeString(sItemInfo, " ", sExplodedInfo, 3, 8);
		new id = StringToInt(sExplodedInfo[0]);
		new item = StringToInt(sExplodedInfo[1]);
		new strengthen = StringToInt(sExplodedInfo[2]);
		
		new String:SteamID[20], String:query[512];
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		
		Format(query, 512, "DELETE FROM garbagecan WHERE steamid = '%s' AND id = '%d';", SteamID, id);
		SQL_TQuery(databasehandle, save_info, query, Client);
		Format(query, 512, "INSERT INTO inventory(steamid, idx, type, strengthen) values ('%s', '%d', '%d','%d');", SteamID, item, ItemData[item][Type], strengthen);
		SQL_TQuery(databasehandle, save_info, query, Client);
		ShowInventory(Client);
		
		PrintToChat(Client, "\x04[TTT Item] \x01- you recycled item \x05%s", ItemData[item][Name]);
		
		new String:sName[32], String:sEscapedName[65];
		GetClientName(Client, sName, sizeof(sName));
		SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
		LogShopData("%s(%s) recycle %s+%d(%d)", sEscapedName, SteamID, ItemData[item][Name], strengthen, item);
	}
	if(action == MenuAction_Cancel)
	{
		if(select == MenuCancel_ExitBack)
		{
			ShowInventory(Client);
		}
	}
}

public g_PlayerStrengthen_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		new String:sItemInfo[32], String:sExplodedInfo[4][8];
		GetMenuItem(menu, select, sItemInfo, sizeof(sItemInfo));
		ExplodeString(sItemInfo, " ", sExplodedInfo, 4, 8);
		new id = StringToInt(sExplodedInfo[0]);
		new item = StringToInt(sExplodedInfo[1]);
		new strengthen = StringToInt(sExplodedInfo[2]);
		new id2 = StringToInt(sExplodedInfo[3]);
		
		new String:query[256], String:SteamID[20];
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		Format(query, sizeof(query), "DELETE FROM inventory WHERE steamid = '%s' AND id = '%d';", SteamID, id2);
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		new String:sName[32], String:sEscapedName[65];
		GetClientName(Client, sName, sizeof(sName));
		SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
		
		new bool:success = StrengthenChance(strengthen+1, ItemData[item][forEvent] || !ItemData[item][Price]);
		if(success)
		{
			Format(query, sizeof(query), "UPDATE inventory SET strengthen = strengthen+1 WHERE steamid = '%s' and id = '%d';", SteamID, id);
			SQL_TQuery(databasehandle, save_info, query, Client);
			
			PrintToChatAll("\x04[TTT - Strengthen] \x01- \x04%N \x01player \x070000FFsuccess \x01to strengthen \x07FFFFFF%s +%d강", Client, ItemData[item][Name], strengthen+1);
			
			LogShopData("%s(%s) success to strengthen %s +%d", sEscapedName, SteamID, ItemData[item][Name], strengthen+1);
		}
		else
		{
			PrintToChatAll("\x04[TTT - Strengthen] \x01- \x04%N \x01player \x07FF0000fail \x01to strengthen \x07FFFFFF%s +%d강", Client, ItemData[item][Name], strengthen+1);
			LogShopData("%s(%s) fail to strengthen %s +%d", sEscapedName, SteamID, ItemData[item][Name], strengthen+1);
		}
	}
}

public g_PlayerOption_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		new String:s_number[4];
		GetMenuItem(menu, select, s_number, sizeof(s_number));
		SetGlobalTransTarget(Client);
		new type = StringToInt(s_number);
		if(PlayerData[Client][ItemExist][type])
		{
			new String:SteamID[20], String:query[256];
			GetClientAuthString(Client, SteamID, sizeof(SteamID));
			
			PlayerData[Client][ItemOn][type] = !(PlayerData[Client][ItemOn][type]);
			Format(query, 256, "UPDATE %s SET options = '%i' WHERE steamid = '%s';", TypeName[type][1], PlayerData[Client][ItemOn][type], SteamID);
			SQL_TQuery(databasehandle, save_info, query, Client);
			
			if(PlayerData[Client][ItemOn][type])
			{
				PrintToChat(Client, "\x05%t %t", TypeName[type][0], "on");
				if(type == EffectType)
				{
					CreatePlayerEffect(Client);
				}
			}
			else
			{
				PrintToChat(Client, "\x05%t %t", TypeName[type][0], "off");
				if(type == EffectType)
				{
					RemovePlayerEffect(Client);
				}
			}
		}
		else
		{
			PrintToChat(Client, "\x05%t%t", TypeName[type][0], "not exist");
		}
		ShowOption(Client);
	}
	if(action == MenuAction_Cancel)
	{
		if(select == MenuCancel_ExitBack)
		{
			ShowMainShop(Client);
		}
	}
}

public g_PlayerPostBox_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		if(PlayerData[Client][DataLoaded] == false)
			return;
		
		new String:s_data[16], String:s_data2[2][16];
		GetMenuItem(menu, select, s_data, sizeof(s_data));
		ExplodeString(s_data, "/", s_data2, 2, 16);
		new index = StringToInt(s_data2[0]);
		new count = StringToInt(s_data2[1]);
		if(index != 0) PrintToChat(Client, "\x04[우편함] \x01- \x0800FF00C8%s 아이템을 수령하셨습니다.", ItemData[index][Name]);
		else PrintToChat(Client, "\x04[우편함] \x01- \x0800FF00C8%i 포인트를 수령하셨습니다.", count);
		new String:SteamID[20], String:query[256];
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		if(index != 0) Format(query, 256, "INSERT INTO inventory(steamid, idx, type) values ('%s', '%d', '%d');", SteamID, index, ItemData[index][Type]);
		else
		{
			PlayerData[Client][Point] += count;
			SavePoint(Client);
		}
		SQL_TQuery(databasehandle, save_info, query, Client);
		Format(query, 512, "DELETE FROM postbox WHERE steamid = '%s' and idx = '%i' limit 1;", SteamID, index);
		SQL_TQuery(databasehandle, save_info, query, Client);
	}
	if(action == MenuAction_Cancel)
	{
		if(select == MenuCancel_ExitBack)
		{
			ShowMainShop(Client);
		}
	}
}

public g_PlayerGivePoint_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		new String:s_number[4];
		GetMenuItem(menu, select, s_number, sizeof(s_number));
		new Target = StringToInt(s_number);
		
		if(select == 0) {
			new points = PlayerData[Client][SendPoint];
			PlayerData[Client][Point] -= points;
			SavePoint(Client);
			PlayerData[Target][Point] += points;
			SavePoint(Target);
			PrintToChat(Client, "\x04[TTT] \x01- %N님에게 %d포인트를 보내드렸습니다.", Target, points);
			PrintToChat(Target, "\x04[TTT] \x01- %N님으로부터 %d포인트를 받으셨습니다.", Client, points);
		
			new String:sName[32], String:STargetName[32], String:sEscapedName[65], String:sEscapedTargetName[65];
			GetClientName(Client, sName, sizeof(sName));
			GetClientName(Target, STargetName, sizeof(STargetName));
			SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
			SQL_EscapeString(databasehandle, STargetName, sEscapedTargetName, sizeof(sEscapedTargetName));
			
			new String:SteamID[20], String:TargetSteamID[20];
			GetClientAuthString(Client, SteamID, sizeof(SteamID));
			GetClientAuthString(Target, TargetSteamID, sizeof(TargetSteamID));
			LogShopData("%s(%s) send %d points to %s(%s)", sEscapedName, SteamID, points, sEscapedTargetName, TargetSteamID);
		} else {
			if(select == 1) PlayerData[Client][SendPoint] += 10;
			if(select == 2) PlayerData[Client][SendPoint] += 100;
			if(select == 3) PlayerData[Client][SendPoint] += 1000;
			if(select == 4) PlayerData[Client][SendPoint] += 10000;
			if(select == 5) PlayerData[Client][SendPoint] -= 10;
			if(select == 6) PlayerData[Client][SendPoint] -= 100;
			if(select == 7) PlayerData[Client][SendPoint] -= 1000;
			if(select == 8) PlayerData[Client][SendPoint] -= 10000;
			SendPointMenu(Client, Target);
		}
	}
}

public g_PlayerGiveItem_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		new String:sItemInfo[32], String:sExplodedInfo[4][8];
		GetMenuItem(menu, select, sItemInfo, sizeof(sItemInfo));
		ExplodeString(sItemInfo, " ", sExplodedInfo, 4, 8);
		new id = StringToInt(sExplodedInfo[0]);
		new item = StringToInt(sExplodedInfo[1]);
		new strengthen = StringToInt(sExplodedInfo[2]);
		new target = StringToInt(sExplodedInfo[3]);
		
		new String:SteamID[20], String:TargetSteamID[20], String:query[512];
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		GetClientAuthString(target, TargetSteamID, sizeof(TargetSteamID));
		Format(query, 512, "DELETE FROM inventory WHERE steamid = '%s' AND id = '%d';", SteamID, id);
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		Format(query, 512, "INSERT INTO inventory(steamid, idx, type, strengthen) values ('%s', '%d', '%d', '%d');", TargetSteamID, item, ItemData[item][Type], strengthen);
		SQL_TQuery(databasehandle, save_info, query, target);
		
		PrintToChat(Client, "\x04[TTT - SendItem] \x01- you sent \x07FFFFFF%s(+%d) \x01to \x04%N", ItemData[item][Name], strengthen, target);
		PrintToChat(target, "\x04[TTT - SendItem] \x01- you received \x07FFFFFF%s(+%d) \x01 from \x04%N", ItemData[item][Name], strengthen, Client);
		
		new String:sName[32], String:STargetName[32], String:sEscapedName[65], String:sEscapedTargetName[65];
		GetClientName(Client, sName, sizeof(sName));
		GetClientName(target, STargetName, sizeof(STargetName));
		SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
		SQL_EscapeString(databasehandle, STargetName, sEscapedTargetName, sizeof(sEscapedTargetName));
		
		LogShopData("%s(%s) send item to %s(%s) %s+%d(%d)", sEscapedName, SteamID, sEscapedTargetName, TargetSteamID, ItemData[item][Name], strengthen, item);
	}
}

public g_PointShopMarket_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		new String:SteamID[20], String:query[256];
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		if(select == 0)
		{
			Format(query, 256, "SELECT * FROM countermarket ORDER BY idx ASC, strengthen DESC, price ASC;");
			SQL_TQuery(databasehandle, CounterMarket_Buy, query, Client);
		}
		if(select == 1)
		{
			Format(query, 256, "SELECT id,idx,strengthen FROM inventory WHERE steamid = '%s' ORDER BY idx ASC, strengthen DESC;", SteamID);
			SQL_TQuery(databasehandle, CounterMarket_Sell, query, Client);
		}
		if(select == 2)
		{
			Format(query, 256, "SELECT id,idx,price,strengthen FROM countermarket WHERE steamid = '%s' ORDER BY idx ASC, strengthen DESC, price ASC;", SteamID);
			SQL_TQuery(databasehandle, CounterMarket_MyItem, query, Client);
		}
		if(select == 3)
		{
			Format(query, 256, "SELECT id,number,profit FROM countermarket_profit WHERE steamid = '%s';", SteamID);
			SQL_TQuery(databasehandle, CounterMarket_Profit, query, Client);
		}
	}
}

public g_CounterMarketBuy_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		if(!PlayerData[Client][DataLoaded]) {
			return;
		}
		
		new String:s_number[8];
		GetMenuItem(menu, select, s_number, sizeof(s_number));
		new id = StringToInt(s_number);
		CounterMarket_Check(Client, id);
	}
	if(action == MenuAction_Cancel)
	{
		if(select == MenuCancel_ExitBack)
		{
			CounterMarket(Client);
		}
	}
}

public g_CounterMarketSell_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		if(!PlayerData[Client][DataLoaded]) {
			return;
		}
		
		PlayerData[Client][SendPoint] = 0;
		
		new String:sItemInfo[24], String:sExplodedInfo[3][8];
		GetMenuItem(menu, select, sItemInfo, sizeof(sItemInfo));
		ExplodeString(sItemInfo, " ", sExplodedInfo, 3, 8);
		new id = StringToInt(sExplodedInfo[0]);
		new item = StringToInt(sExplodedInfo[1]);
		new strengthen = StringToInt(sExplodedInfo[2]);
		CounterMarket_SellPoint(Client, id, item, strengthen);
	}
	if(action == MenuAction_Cancel)
	{
		if(select == MenuCancel_ExitBack)
		{
			CounterMarket(Client);
		}
	}
}

public g_CounterMarketSellPoint_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		if(!PlayerData[Client][DataLoaded]) {
			return;
		}
		
		new String:sItemInfo[24], String:sExplodedInfo[3][8];
		GetMenuItem(menu, select, sItemInfo, sizeof(sItemInfo));
		ExplodeString(sItemInfo, " ", sExplodedInfo, 3, 8);
		new id = StringToInt(sExplodedInfo[0]);
		new item = StringToInt(sExplodedInfo[1]);
		new strengthen = StringToInt(sExplodedInfo[2]);
		if(select == 0)
		{
			new String:SteamID[20], String:query[512];
			GetClientAuthString(Client, SteamID, sizeof(SteamID));
			Format(query, 512, "DELETE FROM inventory WHERE steamid = '%s' AND id = '%i';", SteamID, id);
			SQL_TQuery(databasehandle, save_info, query, Client);
			
			new String:sName[32], String:sEscapedName[65];
			GetClientName(Client, sName, sizeof(sName));
			SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
			Format(query, 512, "INSERT INTO countermarket(steamid, nick, idx, price, strengthen) values ('%s', '%s', '%d', '%d', '%d');", SteamID, sEscapedName, item, PlayerData[Client][SendPoint], strengthen);
			SQL_TQuery(databasehandle, save_info, query);
			
			PrintToChat(Client, "\x04[TTT Counter-Market] \x01- Upload Item(%s +%d) with %dPoints", ItemData[item][Name], strengthen, PlayerData[Client][SendPoint]);
			
			LogShopData("%s(%s) upload %s(%i) with %dpoints at counter-market", sEscapedName, SteamID, ItemData[item][Name], item, PlayerData[Client][SendPoint]);
			CounterMarket(Client);
		}
		else
		{
			if(select == 1) PlayerData[Client][SendPoint] += 10;
			if(select == 2) PlayerData[Client][SendPoint] += 100;
			if(select == 3) PlayerData[Client][SendPoint] += 1000;
			if(select == 4) PlayerData[Client][SendPoint] += 10000;
			if(select == 5) PlayerData[Client][SendPoint] -= 10;
			if(select == 6) PlayerData[Client][SendPoint] -= 100;
			if(select == 7) PlayerData[Client][SendPoint] -= 1000;
			if(select == 8) PlayerData[Client][SendPoint] -= 10000;
			CounterMarket_SellPoint(Client, id, item, strengthen);
		}
	}
}

public g_CounterMarketMyItem_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		if(!PlayerData[Client][DataLoaded]) {
			return;
		}
		
		new String:sItemInfo[24], String:sExplodedInfo[3][8];
		GetMenuItem(menu, select, sItemInfo, sizeof(sItemInfo));
		ExplodeString(sItemInfo, " ", sExplodedInfo, 3, 8);
		new id = StringToInt(sExplodedInfo[0]);
		new item = StringToInt(sExplodedInfo[1]);
		new strengthen = StringToInt(sExplodedInfo[2]);
		
		new String:SteamID[20], String:query[512];
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		Format(query, 512, "INSERT INTO inventory(steamid, idx, type, strengthen) values ('%s', '%d', '%d', '%d');", SteamID, item, ItemData[item][Type], strengthen);
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		Format(query, 512, "DELETE FROM countermarket WHERE id = '%d';", id);
		SQL_TQuery(databasehandle, save_info, query);
		
		PrintToChat(Client, "\x04[TTT Counter-Market] \x01- Withdrew Item(%s +%d) from Counter-Market", ItemData[item][Name], strengthen);
		
		new String:sName[32], String:sEscapedName[65];
		GetClientName(Client, sName, sizeof(sName));
		SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
		
		LogShopData("%s(%s) refund %s +%d(%i) at counter-market", sEscapedName, SteamID, ItemData[item][Name], strengthen, item);
		CounterMarket(Client);
	}
	if(action == MenuAction_Cancel)
	{
		if(select == MenuCancel_ExitBack)
		{
			CounterMarket(Client);
		}
	}
}

//Need to optimize code
public g_TradeMenu_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		PlayerData[Client][selectedCount]++;
		
		new String:sItemInfo[24], String:sExplodedInfo[3][8], String:SteamID[20], String:query[512];
		GetMenuItem(menu, select, sItemInfo, sizeof(sItemInfo));
		ExplodeString(sItemInfo, " ", sExplodedInfo, 3, 8);
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		
		new String:sName[32], String:sEscapedName[65];
		GetClientName(Client, sName, sizeof(sName));
		SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
		
		new ItemIndex = StringToInt(sExplodedInfo[0]);
		new ItemNumber = StringToInt(sExplodedInfo[1]);
		new ItemStrengthen = StringToInt(sExplodedInfo[2]);
		new count = PlayerData[Client][selectedCount];
		
		PlayerData[Client][selectedIndex][count] = ItemIndex;
		PlayerData[Client][selectedNumber][count] = ItemNumber;
		PlayerData[Client][selectedStrengthen][count] = ItemStrengthen;
		
		PrintToChat(Client, "\x04[Trade] \x01- Selected %d item. %s(+%d)", count+1, ItemData[ItemNumber][Name], ItemStrengthen);
		
		if(count < 4)
		{
			if(count == 2)
			{
				new species = 2;
				for(new i; i<=count; i++)
				{
					//check all items are same number
					if(species == 2 && PlayerData[Client][selectedNumber][i] != ItemNumber)
					{
						species = 1;
						i = 0;
					}
					else if(ItemData[PlayerData[Client][selectedNumber][i]][Type] != ItemData[ItemNumber][Type])
					{
						species = 0;
						break;
					}
				}
				PlayerData[Client][selectedCase] = species;
			}
			RefreshTradeMenu(Client);
		}
		else
		{
			for(new i; i<4; i++) {
				Format(query, 512, "DELETE FROM inventory WHERE steamid = '%s' AND id = '%i';", SteamID, PlayerData[Client][selectedIndex][i]);
				SQL_TQuery(databasehandle, save_info, query, Client);
			}
			new resultItem;
			if(PlayerData[Client][selectedCase] == 2) {
				resultItem = 27;
			} else if(PlayerData[Client][selectedCase] == 1) {
				if(ItemData[ItemNumber][Type] == HatType) {
					resultItem = 38;
				} else if(ItemData[ItemNumber][Type] == SkinType) {
					resultItem = 39;
				} else if(ItemData[ItemNumber][Type] == EffectType) {
					resultItem = 67;
				} else {
					resultItem = 48;
				}
			} else {
				resultItem = 10;
			}
			SendPostToClient(Client, resultItem, 1, "Trading Box Result.");
			LogShopData("%s(%s) trade item %s(+%d) %s(+%d) %s(+%d) %s(+%d) %s(+%d) to %s", sEscapedName, SteamID, ItemData[PlayerData[Client][selectedNumber][0]][Name], PlayerData[Client][selectedStrengthen][0]
																																															, ItemData[PlayerData[Client][selectedNumber][1]][Name], PlayerData[Client][selectedStrengthen][1]
																																															, ItemData[PlayerData[Client][selectedNumber][2]][Name], PlayerData[Client][selectedStrengthen][2]
																																															, ItemData[PlayerData[Client][selectedNumber][3]][Name], PlayerData[Client][selectedStrengthen][3]
																																															, ItemData[PlayerData[Client][selectedNumber][4]][Name], PlayerData[Client][selectedStrengthen][4]
																																															, ItemData[resultItem][Name]);
		}
	}
}

public g_PointShopSelect_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		ShowPointShop_Buy(Client, select+1);
	}
}

public g_PointShopBuy_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		if(!PlayerData[Client][DataLoaded]) {
			return;
		}
		
		new String:s_number[4];
		GetMenuItem(menu, select, s_number, sizeof(s_number));
		new number = StringToInt(s_number);
		
		if(ItemData[number][Price] > PlayerData[Client][Point]) {
			PrintToChat(Client, "\x04[TTT Shop] \x01- Not enough point");
			return;
		}
		
		PlayerData[Client][Point] -= ItemData[number][Price];
		SavePoint(Client);
		
		new String:SteamID[20], String:query[512];
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		
		Format(query, 512, "INSERT INTO inventory(steamid, idx, type) values ('%s', '%d', '%d');", SteamID, number, ItemData[number][Type]);
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		PrintToChat(Client, "\x04[TTT Item] \x01당신은 \x03%s\x01아이템을 구매하셨습니다.", ItemData[number][Name]);
		
		new String:sName[32], String:sEscapedName[65];
		GetClientName(Client, sName, sizeof(sName));
		SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
		
		LogShopData("%s(%s) buy %s(%i) at shop", sEscapedName, SteamID, ItemData[number][Name], number);
		ShowInventory(Client);
	}
}

public g_VIPSelectBox_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		new String:s_number[4];
		GetMenuItem(menu, select, s_number, sizeof(s_number));
		new number = StringToInt(s_number);
		
		new String:SteamID[20], String:query[512];
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		
		Format(query, 512, "DELETE FROM inventory WHERE steamid = '%s' AND idx = '%i' limit 1;", SteamID, 47);
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		Format(query, 512, "INSERT INTO inventory(steamid, idx, type) values ('%s', '%d', '%d');", SteamID, number, ItemData[number][Type]);
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		PrintToChatAll("\x04[TTT Item] \x03%N\x01님이 VIP 감사 상자에서 \x05%s \x01스킨을 선택하셨습니다.", Client, ItemData[number][Name]);
		
		new String:sName[32], String:sEscapedName[65];
		GetClientName(Client, sName, sizeof(sName));
		SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
		
		LogShopData("%s(%s) select item with vip box %s(%i)", sEscapedName, SteamID, ItemData[number][Name], number);
		ShowInventory(Client);
	}
}

public g_SelectBox_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		new String:s_number[4];
		GetMenuItem(menu, select, s_number, sizeof(s_number));
		new number = StringToInt(s_number);
		
		new String:SteamID[20], String:query[512];
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		
		Format(query, 512, "DELETE FROM inventory WHERE steamid = '%s' AND idx = '%i' limit 1;", SteamID, 55);
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		Format(query, 512, "INSERT INTO inventory(steamid, idx, type) values ('%s', '%d', '%d');", SteamID, number, ItemData[number][Type]);
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		PrintToChatAll("\x04[TTT Item] \x03%N\x01님이 이벤트 한정 상자에서 \x05%s \x01스킨을 선택하셨습니다.", Client, ItemData[number][Name]);
		
		new String:sName[32], String:sEscapedName[65];
		GetClientName(Client, sName, sizeof(sName));
		SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
		
		LogShopData("%s(%s) select item with event box %s(%i)", sEscapedName, SteamID, ItemData[number][Name], number);
		ShowInventory(Client);
	}
}

public g_CirnoSelectBox_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		new String:s_number[4];
		GetMenuItem(menu, select, s_number, sizeof(s_number));
		new number = StringToInt(s_number);
		
		new String:SteamID[20], String:query[512];
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		
		Format(query, 512, "DELETE FROM inventory WHERE steamid = '%s' AND idx = '%i' limit 1;", SteamID, 61);
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		Format(query, 512, "INSERT INTO inventory(steamid, idx, type) values ('%s', '%d', '%d');", SteamID, number, ItemData[number][Type]);
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		PrintToChatAll("\x04[TTT Item] \x03%N\x01님이 ⑨l벤트 한정 상자에서 \x05%s \x01스킨을 선택하셨습니다.", Client, ItemData[number][Name]);
		
		new String:sName[32], String:sEscapedName[65];
		GetClientName(Client, sName, sizeof(sName));
		SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
		
		LogShopData("%s(%s) select item with cirno box %s(%i)", sEscapedName, SteamID, ItemData[number][Name], number);
		ShowInventory(Client);
	}
}

public g_TagSelect_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		if(select == 0)
		{
			new String:sTag[16];
			GetMenuItem(menu, select, sTag, sizeof(sTag));
			ApplyPlayerTag(Client, sTag);
		}
	}
}