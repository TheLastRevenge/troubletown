
public CheckValid_TagUtilization(Handle:owner, Handle:hndl, const String:error[], any:datapack)
{
	ResetPack(datapack);
	new Client = GetClientOfUserId(ReadPackCell(datapack));
	new String:sTag[16];
	ReadPackString(datapack, sTag, sizeof(sTag));
	CloseHandle(datapack);
	
	if(hndl == INVALID_HANDLE)
	{
		LogError("SQL ERROR: %s", error);
		return;
	}
	
	if(!Client || !SQL_GetRowCount(hndl)) {
		return;
	}
	
	LastCheckToUseTag(Client, sTag);
}

public LastCheckToUseTag(Client, String:sTag[16])
{
#if defined DEBUG_PLUGIN
	if(!OwnerCheck(Client))
	{
		PrintToChat(Client, "\x04[TTT - Tag] \x01- 현재 점검중입니다.");
		return;
	}
#endif
	new Handle:menuhandle = g_tag_select_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	
	SetMenuTitle(menuhandle, "Using Tag '%s'. Right?", sTag);
	AddMenuItem(menuhandle, sTag, "Yes");
	AddMenuItem(menuhandle, NULL_STRING, "No");
	SetMenuExitButton(menuhandle, false);
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

public ApplyPlayerTag(Client, String:sTag[16])
{
	new String:query[256], String:SteamID[20];
	GetClientAuthString(Client, SteamID, sizeof(SteamID));
	
	Format(PlayerData[Client][Tag], 16, sTag);
	Format(query, sizeof(query), "UPDATE info SET tag = '%s' WHERE steamid = '%s';", sTag, SteamID);
	SQL_TQuery(databasehandle, save_info, query, Client);
	
	/*if(태그 기간권 작동중)
	{
		return;
	}*/
	
	Format(query, sizeof(query), "DELETE FROM inventory WHERE steamid = '%s' AND idx = '%d' limit 1;", SteamID, DISPOSABLE_TAG_UTILIZATION);
	SQL_TQuery(databasehandle, save_info, query, Client);
	
	new String:sName[32], String:sEscapedName[65];
	GetClientName(Client, sName, sizeof(sName));
	SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
	
	LogShopData("%s(%s) use tag %s by disposable tag utilization", sEscapedName, SteamID, sTag);
}