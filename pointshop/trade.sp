
public RefreshTradeMenu(Client)
{
	new String:SteamID[20], String:query[512];
	GetClientAuthString(Client, SteamID, sizeof(SteamID));
	Format(query, sizeof(query), "SELECT id,idx,strengthen FROM inventory WHERE steamid = '%s' AND NOT idx in('2','3','4','5','6','10','18','27','38','39','47','48','49','54','55','58','59','60','61','62','63','67') AND NOT id in('%d'", SteamID, PlayerData[Client][selectedIndex][0]);
	for(new i=1; i<=PlayerData[Client][selectedCount]; i++)
	{
		Format(query, sizeof(query), "%s,'%d'", query, PlayerData[Client][selectedIndex][i]);
	}
	Format(query, sizeof(query), "%s) ORDER BY idx ASC, strengthen DESC;", query);
	SQL_TQuery(databasehandle, trading_inventory, query, Client);
}

public trading_inventory(Handle:owner, Handle:hndl, const String:error[], any:Client)
{
	if(hndl == INVALID_HANDLE)
	{
		PrintToChat(Client, "\x05%T", "no items available", Client);
		ShowInventory(Client);
		return;
	}
	if(SQL_GetRowCount(hndl) == 0)
	{
		PrintToChat(Client, "\x05%T", "no items available", Client);
		ShowInventory(Client);
		return;
	}
	
	new Handle:menuhandle = g_pointshop_trade_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	
	new String:sTitle[256];
	Format(sTitle, sizeof(sTitle), "%T", "select material for trading box", Client);
	for(new i; i<=PlayerData[Client][selectedCount]; i++)
	{
		new item = PlayerData[Client][selectedNumber][i];
		new strengthen = PlayerData[Client][selectedStrengthen][i];
		Format(sTitle, sizeof(sTitle), "%s\n%d. %s + %d", sTitle, i+1, ItemData[item][Name], strengthen);
	}
	SetMenuTitle(menuhandle, sTitle);
	if(SQL_GetRowCount(hndl))
	{
		new count
		while(SQL_FetchRow(hndl))
		{
			new item = SQL_FetchInt(hndl, 1);
			if(PlayerData[Client][selectedCase] == 2)
			{
				if(PlayerData[Client][selectedNumber][0] != item)
					continue;
			}
			if(PlayerData[Client][selectedCase] == 1)
			{
				if(ItemData[PlayerData[Client][selectedNumber][0]][Type] != ItemData[item][Type])
					continue;
			}
			new id = SQL_FetchInt(hndl, 0);
			new strengthen = SQL_FetchInt(hndl, 2);
			new String:s_ItemInfo[24], String:s_ItemText[256];
			Format(s_ItemInfo, sizeof(s_ItemInfo), "%d %d %d", id, item, strengthen);
			Format(s_ItemText, sizeof(s_ItemText), "%s", ItemData[item][Name]);
			if(strengthen)
			{
				Format(s_ItemText, sizeof(s_ItemText), "%s +%d", s_ItemText, strengthen);
			}
			AddMenuItem(menuhandle, s_ItemInfo, s_ItemText);
			count++;
		}
		if(count > 0) {
			DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
		} else {
			PrintToChat(Client, "\x05%T", "no items available", Client);
			ShowInventory(Client);
			return;
		}
	}
}