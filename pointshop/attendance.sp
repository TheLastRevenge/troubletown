
public AttendanceReward(Client, day_reward)
{
	new String:query[256], String:SteamID[20];
	GetClientAuthString(Client, SteamID, sizeof(SteamID));
	Format(query, sizeof(query), "UPDATE info SET attendance = '%d', attendance_reward = '%d' WHERE steamid = '%s';", DayToInteger(), day_reward, SteamID);
	SQL_TQuery(databasehandle, save_info, query, Client);
	
	//Check Family Sharing
	new String:OwnerSteamID[20];
	if(GetOwnerAuthString(Client, OwnerSteamID, sizeof(OwnerSteamID)))
	{
		if(StrEqual(SteamID, OwnerSteamID) == false)
		{
			return;
		}
	}
	
	new String:sRewardText[64];
	Format(sRewardText, sizeof(sRewardText), "%dday(s) attendance reward.", day_reward);
	new iRewardItem, iRewardCount;//item == 0 -> point
	
	if(day_reward == 4 || day_reward == 11 || day_reward == 18 || day_reward == 25)
	{
		iRewardCount = 1000;
	}
	else if(day_reward == 7 || day_reward == 8 || day_reward == 14 || day_reward == 21 || day_reward == 22 || day_reward == 28)
	{
		iRewardCount = 1;
		if(day_reward == 7) iRewardCount = 2500; // 2500 Points
		if(day_reward == 8) iRewardItem = 10; // Random Item Box
		if(day_reward == 14) iRewardItem = 38; // Random Hat Box
		if(day_reward == 21) iRewardItem = 48; // Random Crowbar Box
		if(day_reward == 22) iRewardItem = 27;// Beauty Shop New Box
		if(day_reward == 28) iRewardItem = 39; // Random Skin Box
	}
	else
	{
		iRewardCount = 300;
	}
	
	Format(query, sizeof(query), "INSERT INTO postbox(steamid, idx, count, text) VALUES('%s', '%d', '%d', '%s');", SteamID, iRewardItem, iRewardCount, sRewardText);
	SQL_TQuery(databasehandle, save_info, query, Client);
}

public CheckAnniversary(Client, bool:dataExist[MAX_ITEMS+1])
{
	new String:SteamID[20], String:sIP[16];
	GetClientAuthString(Client, SteamID, sizeof(SteamID));
	GetClientIP(Client, sIP, sizeof(sIP));
	
	new String:Time_Year[6], String:Time_Month[6], String:Time_Day[6];
	FormatTime(Time_Year, 6, "%Y", GetTime());
	FormatTime(Time_Month, 6, "%m", GetTime());
	FormatTime(Time_Day, 6, "%d", GetTime());
	new year = StringToInt(Time_Year), month = StringToInt(Time_Month), day = StringToInt(Time_Day);
	
	//Christmas Reward
	if(month == 12 && day == 25 && dataExist[54] == false)
	{
		dataExist[54] = true;
		
		new String:query[256];
		Format(query, 256, "INSERT INTO restrictitem(steamid, ip, idx, date) values ('%s', '%s', '54', '%d');", SteamID, sIP, DayToInteger());
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		SendPostToClient(Client, 54, 1, "Christmas attendance reward");
	}
	
	// 2015/02/19 - Lunar New Year's Day
	if(year == 2015 && month == 2 && (day == 18 || day == 20) && dataExist[27] == false)
	{
		dataExist[27] = true;
		
		new String:query[256];
		Format(query, 256, "INSERT INTO restrictitem(steamid, ip, idx, date) values ('%s', '%s', '27', '%d');", SteamID, sIP, DayToInteger());
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		SendPostToClient(Client, 27, 1, "Lunar New Year's Day attendance reward (Previous/Next)");
	}
	if(year == 2015 && month == 2 && day == 19 && dataExist[55] == false)
	{
		dataExist[55] = true;
		
		new String:query[256];
		Format(query, 256, "INSERT INTO restrictitem(steamid, ip, idx, date) values ('%s', '%s', '55', '%d');", SteamID, sIP, DayToInteger());
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		SendPostToClient(Client, 55, 1, "Lunar New Year's Day attendance reward");
	}
	if(year == 2015 && month == 2 && day == 19 && dataExist[63] == false)
	{
		dataExist[63] = true;
		
		new String:query[256];
		Format(query, 256, "INSERT INTO restrictitem(steamid, ip, idx, date) values ('%s', '%s', '63', '%d');", SteamID, sIP, DayToInteger());
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		SendPostToClient(Client, 63, 1, "Lunar New Year's Day attendance reward");
	}
	if(year == 2015 && month == 2 && day == 19 && dataExist[0] == false)
	{
		dataExist[0] = true;
		
		new String:query[256];
		Format(query, 256, "INSERT INTO restrictitem(steamid, ip, idx, date) values ('%s', '%s', '0', '%d');", SteamID, sIP, DayToInteger());
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		SendPostToClient(Client, 0, 2000, "Lunar New Year's Day attendance reward");
	}
}