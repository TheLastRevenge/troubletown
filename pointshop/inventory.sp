
public ShowInventory(Client)
{
	new Handle:menuhandle = g_pointshop_inventory_Menu;
	RemoveAllMenuItems(menuhandle);
	SetGlobalTransTarget(Client);
	
	SetMenuExitButton(menuhandle, true);
	SetMenuExitBackButton(menuhandle, true);
	SetMenuTitle(menuhandle, "--- %t ---", "my inventory");
	AddMenuItemTranslated(menuhandle, "all of type");
	for(new i =  1; i < MaxType+1; i++)
	{
		AddMenuItemTranslated(menuhandle, TypeName[i][0]);
	}
	AddMenuItem(menuhandle, "", "Box");
	AddMenuItem(menuhandle, "", "Garbage Can");
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

public inventory(Handle:owner, Handle:hndl, const String:error[], any:Client)
{
	if(hndl == INVALID_HANDLE)
	{
		PrintToChat(Client, "\x05%T", "no items available", Client);
		ShowInventory(Client);
		return;
	}
	if(SQL_GetRowCount(hndl) == 0)
	{
		PrintToChat(Client, "\x05%T", "no items available", Client);
		ShowInventory(Client);
		return;
	}
	new Handle:menuhandle = g_player_inventory_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	SetMenuTitle(menuhandle, "%T", "my inventory", Client);
	if(SQL_GetRowCount(hndl))
	{
		while(SQL_FetchRow(hndl))
		{
			new id = SQL_FetchInt(hndl, 0);
			new item = SQL_FetchInt(hndl, 1);
			new strengthen = SQL_FetchInt(hndl, 2);
			new String:s_ItemInfo[24], String:s_ItemText[256];
			Format(s_ItemInfo, sizeof(s_ItemInfo), "%d %d %d", id, item, strengthen);
			Format(s_ItemText, sizeof(s_ItemText), "%s", ItemData[item][Name]);
			if(strengthen)
			{
				Format(s_ItemText, sizeof(s_ItemText), "%s +%d강", s_ItemText, strengthen);
			}
			if(IsBoxItem(item))
			{
				Format(s_ItemText, sizeof(s_ItemText), "%s\n%s", s_ItemText, ItemData[item][Path]);
			}
			AddMenuItem(menuhandle, s_ItemInfo, s_ItemText);
		}
		DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
	}
}

public Prompt(Client, ItemIndex, ItemNumber, ItemStrengthen)
{
	new Handle:menuhandle = CreateMenu(Prompt_CallBack);
	SetGlobalTransTarget(Client);
	SetMenuTitle(menuhandle, "%t", "my inventory");
	
	new String:sItemInfo[24];
	Format(sItemInfo, sizeof(sItemInfo), "%d %d %d", ItemIndex, ItemNumber, ItemStrengthen);
	AddMenuItemTranslated2(menuhandle, sItemInfo, "use", ItemData[ItemNumber][Type] == TagType);
	//AddMenuItemTranslated2(menuhandle, s_number, "sell 1/2", Client, (ItemData[ItemNumber][forEvent] || !ItemData[ItemNumber][Price]));
	AddMenuItemTranslated2(menuhandle, sItemInfo, "sell 1/2", true);
	AddMenuItemTranslated2(menuhandle, sItemInfo, "remove");
	AddMenuItemTranslated2(menuhandle, sItemInfo, "strengthen", ItemData[ItemNumber][Type] != SkinType && ItemData[ItemNumber][Type] != VMSkinType);
	AddMenuItemTranslated2(menuhandle, sItemInfo, "trade", ItemData[ItemNumber][Type] >= TagType);
	SetMenuExitButton(menuhandle, true);
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

public Prompt_CallBack(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		if(PlayerData[Client][DataLoaded] == false)
			return;
		
		new String:sItemInfo[24], String:sExplodedInfo[3][8], String:SteamID[20], String:query[512];
		GetMenuItem(menu, select, sItemInfo, sizeof(sItemInfo));
		ExplodeString(sItemInfo, " ", sExplodedInfo, 3, 8);
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		
		new String:sName[32], String:sEscapedName[65];
		GetClientName(Client, sName, sizeof(sName));
		SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
		
		new ItemIndex = StringToInt(sExplodedInfo[0]);
		new ItemNumber = StringToInt(sExplodedInfo[1]);
		new ItemStrengthen = StringToInt(sExplodedInfo[2]);
		if(select == 0)
		{
			new bool:success = false;
			new type = ItemData[ItemNumber][Type];
			if(IsBoxItem(ItemNumber))
			{
				if(type == 10)
				{
					new item = GetRandomItem();
					Format(query, 512, "INSERT INTO inventory(steamid, idx, type) values ('%s', '%d', '%d');", SteamID, item, ItemData[item][Type]);
					SQL_TQuery(databasehandle, save_info, query, Client);
					
					PrintToChatAll("\x04[TTT Item] \x03%N\x01님이 랜덤 아이템 상자에서 \x05%s \x01스킨을 획득하셨습니다.", Client, ItemData[item][Name]);
					
					success = true;
				}
				if(type == 11)
				{
					new item = GetRandomNewestItem();
					Format(query, 512, "INSERT INTO inventory(steamid, idx, type) values ('%s', '%d', '%d');", SteamID, item, ItemData[item][Type]);
					SQL_TQuery(databasehandle, save_info, query, Client);
					
					PrintToChatAll("\x04[TTT Item] \x03%N\x01님이 신상품 상자에서 \x05%s \x01스킨을 획득하셨습니다.", Client, ItemData[item][Name]);
					
					success = true;
				}
				if(12 <= type <= 15)
				{
					new item = GetRandomTypeItem(type-11);
					Format(query, 512, "INSERT INTO inventory(steamid, idx, type) values ('%s', '%d', '%d');", SteamID, item, ItemData[item][Type]);
					SQL_TQuery(databasehandle, save_info, query, Client);
					
					PrintToChatAll("\x04[TTT Item] \x03%N\x01님이 %s에서 \x05%s \x01스킨을 획득하셨습니다.", Client, ItemData[ItemNumber][Name], ItemData[item][Name]);
					
					success = true;
				}
				if(type == 16)
				{
					ShowVIPSelectBox(Client);
					return;
				}
				if(type == 17)
				{
					new item = GetChristmasItem();
					Format(query, 512, "INSERT INTO inventory(steamid, idx, type) values ('%s', '%d', '%d');", SteamID, item, ItemData[item][Type]);
					SQL_TQuery(databasehandle, save_info, query, Client);
					
					PrintToChatAll("\x04[TTT Item] \x03%N\x01님이 크리스마스 상자에서 \x05%s \x01스킨을 획득하셨습니다.", Client, ItemData[item][Name]);
					
					success = true;
				}
				if(type == 18)
				{
					ShowSelectBox(Client);
					return;
				}
				if(type == 19)
				{
					new item = 11;
					if(ItemNumber == 58 && !GetRandomInt(0, 4)) {
						item = 59+GetRandomInt(0,2);
					}
					Format(query, 512, "INSERT INTO inventory(steamid, idx, type) values ('%s', '%d', '%d');", SteamID, item, ItemData[item][Type]);
					SQL_TQuery(databasehandle, save_info, query, Client);
					
					PrintToChatAll("\x04[TTT Item] \x03%N\x01님이 %s에서 \x05%s \x01스킨을 획득하셨습니다.", Client, ItemData[ItemNumber][Name], ItemData[item][Name]);
					
					success = true;
				}
				if(type == 20)
				{
					ShowCirnoSelectBox(Client);
					return;
				}
			}
			else
			{
				if(PlayerData[Client][ItemExist][type])
				{
					new equiped_number = PlayerData[Client][ItemID][type];
					new equiped_strengthen = PlayerData[Client][Strengthen][type];
					if(ItemData[equiped_number][forAdmin] == true)
					{
						PrintToChatAll("\x04[TTT Item] \x01어드민 메뉴에서 어드민 스킨을 해제하시기 바랍니다.");
						return;
					}
					Format(query, 512, "DELETE FROM %s WHERE steamid = '%s';", TypeName[type][1], SteamID);
					SQL_TQuery(databasehandle, save_info, query, Client);
					Format(query, 512, "INSERT INTO inventory(steamid, idx, type, strengthen) values ('%s', '%d', '%d', '%d');", SteamID, equiped_number, type, equiped_strengthen);
					SQL_TQuery(databasehandle, save_info, query, Client);
					LogShopData("%s(%s) unequip %s(%d)", sEscapedName, SteamID, ItemData[equiped_number][Name], equiped_number);
				}
				Format(query, 512, "INSERT INTO %s(steamid, idx, options, strengthen) values ('%s', '%d', '1', '%d');", TypeName[type][1], SteamID, ItemNumber, ItemStrengthen);
				SQL_TQuery(databasehandle, save_info, query, Client);
				PlayerData[Client][ItemID][type] = ItemNumber;
				PlayerData[Client][ItemExist][type] = true;
				PlayerData[Client][ItemOn][type] = true;
				PlayerData[Client][Strengthen][type] = ItemStrengthen;
				success = true;
				
				if(ItemStrengthen) PrintToChat(Client, "\x04[TTT - Equip] \x01- You Equipped \x07FFFFFF%s +%d강", ItemData[ItemNumber][Name], ItemStrengthen);
				else PrintToChat(Client, "\x04[TTT - Equip] \x01- You Equipped \x07FFFFFF%s", ItemData[ItemNumber][Name]);
			}
			
			if(success == true)
			{
				Format(query, 512, "DELETE FROM inventory WHERE steamid = '%s' AND id = '%d' limit 1;", SteamID, ItemIndex);
				SQL_TQuery(databasehandle, save_info, query, Client);
				
				LogShopData("%s(%s) use %s(%d)", sEscapedName, SteamID, ItemData[ItemNumber][Name], ItemNumber);
				
				ShowInventory(Client);
			}
		}
		/*if(select == 1)
		{
			Format(query, 512, "DELETE FROM inventory WHERE steamid = '%s' AND idx = '%d' limit 1;", SteamID, Number);
			SQL_TQuery(databasehandle, delete_inventory, query, Client);
			Point[Client] += RoundToNearest(float(ItemPrice[Number])/2.0);
			SavePoint(Client);
			PrintChat(Client, "\x04%s\x01을/를 %d포인트에 파셨습니다.", ItemName[Number], RoundToNearest(float(ItemPrice[Number])/2.0));
			new String:Log[256], String:Time[30], String:ClientName[32];
			GetClientName(Client, ClientName, 32);
			while(StrContains(ClientName, "'") != -1) ReplaceStringEx(ClientName, 32, "'", "`");
			FormatTime(Time, 30, "%Y/%m/%d - %H:%M:%S", GetTime());
			Format(Log, 256, "%s sell %s(%d)", ClientName, ItemName[Number], Number);
			Format(query, 512, "INSERT INTO shoplog(steamid, log, type, date) values ('%s', '%s', '%d', '%s');", SteamID, Log, 2, Time);
			SQL_TQuery(databasehandle, save_info, query);
		}*/
		if(select == 2)
		{
			Format(query, 512, "DELETE FROM inventory WHERE steamid = '%s' AND id = '%d' limit 1;", SteamID, ItemIndex);
			SQL_TQuery(databasehandle, save_info, query, Client);
			Format(query, 512, "INSERT INTO garbagecan(steamid, idx, type, strengthen) values ('%s', '%d', '%d', '%d');", SteamID, ItemNumber, ItemData[ItemNumber][Type], ItemStrengthen);
			SQL_TQuery(databasehandle, save_info, query, Client);
			ShowInventory(Client);
			
			PrintToChat(Client, "\x04[TTT Item] \x01- You dropped item \x05%s", ItemData[ItemNumber][Name]);
			
			LogShopData("%s(%s) drop %s(%d)", sEscapedName, SteamID, ItemData[ItemNumber][Name], ItemNumber);
		}
		if(select == 3)
		{
			Format(query, sizeof(query), "SELECT id,idx,strengthen FROM inventory WHERE steamid = '%s' AND idx in('%d','%d') AND id != '%d';", SteamID, ItemNumber, DISPOSABLE_STRENGTHEN_UTILIZATION, ItemIndex);
			new Handle:datapack = CreateDataPack();
			WritePackCell(datapack, Client);
			WritePackCell(datapack, ItemIndex);
			WritePackCell(datapack, ItemNumber);
			WritePackCell(datapack, ItemStrengthen);
			SQL_TQuery(databasehandle, strengthen_inventory, query, datapack);
		}
		if(select == 4)
		{
			if(ItemData[ItemNumber][Price]) {
				PrintToChat(Client, "\x04[Trade] \x01- This item can't be selected.");
				return;
			}
			PlayerData[Client][selectedIndex][0] = ItemIndex;
			PlayerData[Client][selectedNumber][0] = ItemNumber;
			PlayerData[Client][selectedStrengthen][0] = ItemStrengthen;
			PlayerData[Client][selectedCount] = 0;
			PlayerData[Client][selectedCase] = 0;
			PrintToChat(Client, "\x04[Trade] \x01- Selected 1 item. %s(+%d)", ItemData[ItemNumber][Name], ItemStrengthen);
			RefreshTradeMenu(Client);
		}
	}
	if(action == MenuAction_End)
	{
		CloseHandle(menu);
	}
}

public garbagecan(Handle:owner, Handle:hndl, const String:error[], any:Client)
{
	if(hndl == INVALID_HANDLE)
	{
		PrintToChat(Client, "\x05%T", "no items available", Client);
		ShowInventory(Client);
		return;
	}
	
	new Handle:menuhandle = g_player_garbagecan_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	SetMenuTitle(menuhandle, "Recycler");
	
	if(SQL_GetRowCount(hndl))
	{
		while(SQL_FetchRow(hndl))
		{
			new id = SQL_FetchInt(hndl, 0);
			new item = SQL_FetchInt(hndl, 1);
			new strengthen = SQL_FetchInt(hndl, 2);
			new String:s_ItemInfo[24], String:s_ItemText[256];
			Format(s_ItemInfo, sizeof(s_ItemInfo), "%d %d %d", id, item, strengthen);
			Format(s_ItemText, sizeof(s_ItemText), "%s", ItemData[item][Name]);
			if(IsBoxItem(item))
			{
				Format(s_ItemText, sizeof(s_ItemText), "%s\n%s", s_ItemText, ItemData[item][Path]);
			}
			else if(strengthen)
			{
				Format(s_ItemText, sizeof(s_ItemText), "%s +%d강", s_ItemText, strengthen);
			}
			AddMenuItem(menuhandle, s_ItemInfo, s_ItemText);
		}
	}
	else
	{
		PrintToChat(Client, "\x05%T", "no items available", Client);
		ShowInventory(Client);
		return;
	}
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}