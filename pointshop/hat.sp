
public CreatePlayerHat(Client)
{
	new hat = PlayerData[Client][ItemID][HatType];
	if(ItemData[hat][Type] != HatType || ItemData[PlayerData[Client][ItemID][SkinType]][Type] != SkinType || !PlayerData[Client][ItemOn][HatType])
	{
		return;
	}
	new skin = PlayerData[Client][ItemOn][SkinType] ? PlayerData[Client][ItemID][SkinType] : 0;
	
	new Float:vecPosition[3];
	GetClientAbsOrigin(Client, vecPosition);
	
	new prop = CreateEntityByName("prop_dynamic_override");
	DispatchKeyValue(prop, "classname", "hat_entity");
	DispatchKeyValue(prop, "model", ItemData[hat][Path]);
	DispatchKeyValue(prop, "solid", "0");
	DispatchSpawn(prop);
	AcceptEntityInput(prop, "TurnOn", Client);
	
	SetEntPropEnt(prop, Prop_Send, "m_hOwnerEntity", Client);
	
	TeleportEntity(prop, vecPosition, NULL_VECTOR, NULL_VECTOR);
	
	g_hat_reference[Client] = EntIndexToEntRef(prop);
	
	SDKHook(prop, SDKHook_SetTransmit, Hook_SetTransmit_Hat);
	
	SetVariantString("!activator");
	AcceptEntityInput(prop, "SetParent", Client);
	SetVariantString("forward");
	AcceptEntityInput(prop, "SetParentAttachment");
	
	g_hat_number[Client] = hat;
	AdjustPlayerHat(Client, hat, skin, prop);
}

stock AdjustPlayerHat(Client, HatNumber, SkinNumber, HatEntity)
{
	new Float:vecHatPosition[3], Float:vecHatAngle[3];
	new Float:vecTemp[3], Float:vecTemp2[3];
	for(new i; i<3; i++)
	{
		vecTemp[i] = ItemData[HatNumber][Position][i];
		vecTemp2[i] = ItemData[SkinNumber][Position][i];
	}
	AddVectors(vecTemp, vecTemp2, vecHatPosition);
	DetailPosition(HatNumber, SkinNumber, vecHatPosition);
	for(new i; i<3; i++)
	{
		vecTemp[i] = ItemData[HatNumber][Angle][i];
		vecTemp2[i] = ItemData[SkinNumber][Angle][i];
	}
	AddVectors(vecTemp, vecTemp2, vecHatAngle);
	DetailAngle(HatNumber, SkinNumber, vecHatAngle);
	if(ItemData[SkinNumber][Scale] > 0.1)
	{
		new Float:scale = ItemData[SkinNumber][Scale];
		DetailScale(HatNumber, SkinNumber, scale);
		SetEntPropFloat(HatEntity, Prop_Send, "m_flModelScale", scale);
	}
	else
	{
		SetEntPropFloat(HatEntity, Prop_Send, "m_flModelScale", 1.0);
	}
	
	TeleportEntity(HatEntity, vecHatPosition, vecHatAngle, NULL_VECTOR);
}

stock RemovePlayerHat(Client)
{
	if(IsPlayerHavingHat(Client))
	{
		AcceptEntityInput(EntRefToEntIndex(g_hat_reference[Client]), "Kill");
	}
}

stock DetailPosition(hat, skin, Float:hat_position[3])
{
	if(hat == 24)//우주인 헬멧
	{
		if(skin == 0)//게릴라
		{
			AddVectors(hat_position, Float:{0.0, 0.0, 1.0}, hat_position);
		}
		if(3<= skin <= 6)//텔레토비
		{
			AddVectors(hat_position, Float:{0.0, 0.0, -5.0}, hat_position);
		}
		if(skin == 7)//추석토끼
		{
			AddVectors(hat_position, Float:{0.0, 0.0, -3.0}, hat_position);
		}
		if(skin == 12 || skin == 13)//레밀리아 스칼렛, 플랑도르 스칼렛
		{
			AddVectors(hat_position, Float:{0.0, 0.0, 2.0}, hat_position);
		}
		if(18<= skin <= 22)//곰들
		{
			AddVectors(hat_position, Float:{0.0, 0.5, -7.0}, hat_position);
		}
		if(skin == 35)//이자요이 사쿠야
		{
			AddVectors(hat_position, Float:{0.0, 0.0, -0.5}, hat_position);
		}
	}
	if(hat == 28)//피카츄
	{
		if(3<= skin <= 6)//텔레토비
		{
			AddVectors(hat_position, Float:{-5.5, 0.0, -19.5}, hat_position);
		}
		if(skin == 7)//추석토끼
		{
			AddVectors(hat_position, Float:{-3.5, 0.0, -18.5}, hat_position);
		}
		if(skin == 8)//레이센 우동게인 이나바
		{
			AddVectors(hat_position, Float:{0.0, 0.0, 0.5}, hat_position);
		}
		if(skin == 11)//치르노
		{
			AddVectors(hat_position, Float:{-2.0, 0.0, -13.0}, hat_position);
		}
		if(skin == 12)//레밀리아 스칼렛
		{
			AddVectors(hat_position, Float:{0.0, 0.0, -14.5}, hat_position);
		}
		if(skin == 13)//플랑도르 스칼렛
		{
			AddVectors(hat_position, Float:{0.0, 0.0, -15.0}, hat_position);
		}
		if(skin == 14)//코치야 사나에
		{
			AddVectors(hat_position, Float:{-1.5, 0.0, -6.0}, hat_position);
		}
		if(skin == 15)//코메이지 사토리
		{
			AddVectors(hat_position, Float:{-1.5, 0.0, -6.5}, hat_position);
		}
		if(skin == 16)//콘파쿠 요우무
		{
			AddVectors(hat_position, Float:{-1.5, 0.0, 2.5}, hat_position);
		}
		if(18<= skin <= 22)//곰들
		{
			AddVectors(hat_position, Float:{-2.0, 0.5, -27.0}, hat_position);
		}
		if(skin == 32)//미쿠
		{
			AddVectors(hat_position, Float:{-2.5, 0.0, -1.5}, hat_position);
		}
		if(skin == 33 || skin == 34)//분홍미쿠, 파랑미쿠
		{
			AddVectors(hat_position, Float:{-1.0, 0.0, -3.5}, hat_position);
		}
		if(skin == 35)//이자요이 사쿠야
		{
			AddVectors(hat_position, Float:{0.0, 0.0, -4.0}, hat_position);
		}
	}
	if(hat == 29)//아프로
	{
		if(3<= skin <= 7)//텔레토비, 추석토끼
		{
			AddVectors(hat_position, Float:{-3.0, 0.0, 0.0}, hat_position);
		}
		if(skin == 8)//레이센 우동게인 이나바
		{
			AddVectors(hat_position, Float:{1.5, 0.0, -2.0}, hat_position);
		}
		if(skin == 11)//치르노
		{
			AddVectors(hat_position, Float:{1.0, 0.0, 0.0}, hat_position);
		}
		if(skin == 12 || skin == 13)//레밀리아 스칼렛, 플랑도르 스칼렛
		{
			AddVectors(hat_position, Float:{1.0, 0.0, -1.0}, hat_position);
		}
		if(18<= skin <= 22)//곰들
		{
			AddVectors(hat_position, Float:{0.0, 0.5, 0.5}, hat_position);
		}
		if(skin == 35)//이자요이 사쿠야
		{
			AddVectors(hat_position, Float:{1.5, 0.0, -3.0}, hat_position);
		}
	}
}

stock DetailAngle(hat, skin, Float:hat_angle[3])
{
	if(hat == 29)
	{
		if(18<= skin <= 22)//곰들
		{
			//AddVectors(hat_angle, Float:{0.0, 0.0, -20.0}, hat_angle);
		}
	}
}

stock DetailScale(hat, skin, &Float:scale)
{
	if(hat == 29)
	{
		if(skin == 11)//치르노
		{
			scale += 0.05;
		}
		if(18<= skin <= 22)//곰들
		{
			scale += 0.1;
		}
	}
}

public Action:Hook_SetTransmit_Hat(entity, viewer)
{
	if(viewer < 1 || viewer > MaxClients || !IsClientInGame(viewer))
	{
		return Plugin_Continue;
	}
	
	new HatOwner = GetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity");
	if(HatOwner == viewer && GetEntProp(viewer, Prop_Send, "m_iObserverMode") == 0)
	{
		return Plugin_Handled;
	}
	
	if(GetEntProp(viewer, Prop_Send, "m_iObserverMode") == 4 && GetEntPropEnt(viewer, Prop_Send, "m_hObserverTarget") == HatOwner)
	{
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

stock bool:IsPlayerHavingHat(Client)
{
	if(g_hat_reference[Client] == INVALID_ENT_REFERENCE)
	{
		return false;
	}
	new HatIndex = EntRefToEntIndex(g_hat_reference[Client]);
	if(IsValidEdict(HatIndex) == false)
	{
		return false;
	}
	new String:classname[32];
	GetEdictClassname(HatIndex, classname, sizeof(classname));
	if(StrEqual(classname, "hat_entity") == true)
	{
		return true;
	}
	return false;
}