
public strengthen_inventory(Handle:owner, Handle:hndl, const String:error[], any:datapack)
{
	ResetPack(datapack);
	new Client = ReadPackCell(datapack);
	new ItemIndex = ReadPackCell(datapack);
	new ItemNumber = ReadPackCell(datapack);
	new ItemStrengthen = ReadPackCell(datapack);
	CloseHandle(datapack);
	
	if(hndl == INVALID_HANDLE)
	{
		PrintToChat(Client, "\x05%T", "no items available", Client);
		ShowInventory(Client);
		return;
	}
	if(SQL_GetRowCount(hndl) == 0)
	{
		PrintToChat(Client, "\x05%T", "no items available", Client);
		ShowInventory(Client);
		return;
	}
	new Handle:menuhandle = g_player_strengthen_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	SetMenuTitle(menuhandle, "%T", "select material for strengthen", Client);
	if(SQL_GetRowCount(hndl))
	{
		while(SQL_FetchRow(hndl))
		{
			new id = SQL_FetchInt(hndl, 0);
			new item = SQL_FetchInt(hndl, 1);
			new strengthen = SQL_FetchInt(hndl, 2);
			new String:s_ItemInfo[32], String:s_ItemText[256];
			Format(s_ItemInfo, sizeof(s_ItemInfo), "%d %d %d %d", ItemIndex, ItemNumber, ItemStrengthen, id);
			Format(s_ItemText, sizeof(s_ItemText), "%s", ItemData[item][Name]);
			if(strengthen)
			{
				Format(s_ItemText, sizeof(s_ItemText), "%s +%d강", s_ItemText, strengthen);
			}
			AddMenuItem(menuhandle, s_ItemInfo, s_ItemText);
		}
		DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
	}
}

stock bool:StrengthenChance(strengthen, bool:bEventItem)
{
	new iRand1 = GetRandomInt(1, strengthen+3), bool:bRand1;
	new iRand2 = GetRandomInt(1, (strengthen/5)+1), bool:bRand2;
	if(bEventItem)
	{
		if(iRand1 <= 4) bRand1 = true;
		if(iRand2 <= 2) bRand2 = true;
	}
	else
	{
		iRand1 = GetRandomInt(1, strengthen);
		iRand2 = GetRandomInt(1, (strengthen/5)+1);
		if(iRand1 == 1) bRand1 = true;
		if(iRand2 == 1) bRand2 = true;
	}
	return (bRand1&&bRand2);
}