
enum COUPON_DATA {
	coupon_index,
	iType,
	iNumber,
	bool:bDuplicate,
	String:szNumber[32],
};
new CouponData[MAXPLAYERS+1][COUPON_DATA];

public Action:OnClientCommand(Client, args)
{
	new String:Command[64];
	GetCmdArg(0, Command, sizeof(Command));
	if(StrEqual(Command, "coupon", false))
	{
		static Float:flLastEngineTime[MAXPLAYERS+1];
		if(FloatSub(GetEngineTime(), flLastEngineTime[Client]) >= 5.0)
		{
			flLastEngineTime[Client] = GetEngineTime();
			
			new String:sCoupon[32];
			GetCmdArg(1, sCoupon, sizeof(sCoupon));
			if(strlen(sCoupon) == 21 && ReplaceString(sCoupon, strlen(sCoupon), "-", "-", false) == 3)
				Check_Coupon(Client, sCoupon);
			else
				PrintToChat(Client, "\x04[TTT Coupon] \x01- 잘못된 쿠폰번호입니다.");
		}
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public Create_CouponBox(Client)
{
#if defined DEBUG_PLUGIN
	if(!OwnerCheck(Client))
	{
		PrintToChat(Client, "\x04[TTT - Coupon] \x01- 현재 점검중입니다.");
		return;
	}
#endif
	new String:SteamID[20], String:OwnerSteamID[20];
	GetClientAuthString(Client, SteamID, sizeof(SteamID));
	if(GetOwnerAuthString(Client, OwnerSteamID, sizeof(OwnerSteamID)))
	{
		if(StrEqual(SteamID, OwnerSteamID) == false)
		{
			return;
		}
	}
	new Handle:Kv = CreateKeyValues("entry");
	KvSetNum(Kv, "time", 100);
	KvSetNum(Kv, "level", 1);
	KvSetString(Kv, "title", "Press ESC");
	KvSetString(Kv, "msg", "Enter your coupon here.\nex) 4444-4444-4444-444444");
	KvSetString(Kv, "command", "coupon");
	KvSetColor(Kv, "color", 255, 255, 0, 255);
	CreateDialog(Client, Kv, DialogType_Entry);
	CloseHandle(Kv);
}

public Check_Coupon(Client, String:sCoupon[32])
{
	new String:query[128];
	Format(query, 128, "SELECT * FROM coupon WHERE couponnumber = '%s';", sCoupon);
	SQL_TQuery(databasehandle, Check_Coupon1, query, Client);
	Format(CouponData[Client][szNumber], 32, sCoupon);
}

public Check_Coupon1(Handle:owner, Handle:handle, const String:error[], any:Client)
{
	new String:query[128];
	if(handle == INVALID_HANDLE)
	{
		LogError("coupon check ERROR: %s", error);
		return;
	}
	else if(SQL_GetRowCount(handle))
	{
		while(SQL_FetchRow(handle))
		{
			CouponData[Client][coupon_index] = SQL_FetchInt(handle, 0);
			CouponData[Client][iType] = SQL_FetchInt(handle, 2);
			CouponData[Client][iNumber] = SQL_FetchInt(handle, 3);
			CouponData[Client][bDuplicate] = bool:SQL_FetchInt(handle, 4);
		}
	}
	else
	{
		PrintToChat(Client, "\x04[TTT Coupon] \x01- 존재하지 않거나 누군가 사용하신 쿠폰번호입니다.");
		return;
	}
	
	new String:SteamID[32];
	GetClientAuthString(Client, SteamID, sizeof(SteamID));
	Format(query, 128, "SELECT * FROM used_coupon WHERE number = '%i' AND steamid = '%s';", CouponData[Client][coupon_index], SteamID);
	SQL_TQuery(databasehandle, Check_Coupon2, query, Client);
}

public Check_Coupon2(Handle:owner, Handle:handle, const String:error[], any:Client)
{
	new String:query[256], String:SteamID[32];
	GetClientAuthString(Client, SteamID, sizeof(SteamID));
	if(handle == INVALID_HANDLE)
	{
		LogError("used coupon check ERROR: %s", error);
		return;
	}
	else if(SQL_GetRowCount(handle))
	{
		PrintToChat(Client, "\x04[TTT Coupon] \x01- 이미 사용하신 쿠폰번호입니다.");
		return;
	}
	
	if(CouponData[Client][iType] == 1)
	{
		PlayerData[Client][Point] += CouponData[Client][iNumber];
		SavePoint(Client);
		PrintToChat(Client, "\x04[TTT Coupon] \x01- 쿠폰에서 \x07FFFFFF%d\x01포인트를 획득하셨습니다.", CouponData[Client][iNumber]);
	}
	else
	{
		PrintToChat(Client, "\x04[TTT Coupon] \x01- 쿠폰에서 \x07FFFFFF%s\x01아이템을 획득하셨습니다.", ItemData[CouponData[Client][iNumber]][Name]);
		Format(query, 128, "INSERT INTO inventory(steamid, idx, type) values ('%s', '%d', '%d');", SteamID, CouponData[Client][iNumber], ItemData[CouponData[Client][iNumber]][Type]);
		SQL_TQuery(databasehandle, save_info, query, Client);
	}
	
	if(!CouponData[Client][bDuplicate])
	{
		Format(query, 128, "DELETE FROM coupon WHERE couponnumber = '%s' limit 1;", CouponData[Client][iNumber]);
		SQL_TQuery(databasehandle, save_info, query);
		LogShopData("%N(%s) used duplicated coupon %i %i %s", Client, SteamID, CouponData[Client][iType], CouponData[Client][iNumber], CouponData[Client][szNumber]);
	}
	else
	{
		Format(query, 128, "INSERT INTO used_coupon(steamid, number) values ('%s', '%d');", SteamID, CouponData[Client][coupon_index]);
		SQL_TQuery(databasehandle, save_info, query);
		LogShopData("%N(%s) used coupon %i %i %s", Client, SteamID, CouponData[Client][iType], CouponData[Client][iNumber], CouponData[Client][szNumber]);
	}
}