
#include<sourcemod>
#include<sdktools>
#include<sdkhooks>

native bool:GetOwnerAuthString(client, String:authid[], maxlen);
native TTT_GetClientKarmaScore(client);

#define ADMIN_STEAMID "STEAM_0:0:77456544"
#define SIRA_STEAMID "STEAM_0:1:17803258"
//#define DEBUG_PLUGIN

#define DATABASE_NAME "troubletown"
new Handle:databasehandle = INVALID_HANDLE;

#define HatType 1
#define SkinType 2
#define EffectType 3
#define VMSkinType 4
#define TagType 5
#define MaxType 4
new String:TypeName[MaxType+1][2][256] = {
	{"아이템종류", "데이터베이스이름"},
	{"hat skin", "hat"},
	{"player skin", "skin"},
	{"effect skin", "effect"},
	{"crowbar skin", "vmskin"}
};

#define MAX_ITEMS 67
enum _ItemData {
	//data
	String:Name[64],
	String:Path[256],
	Type,
	Price,
	
	//fit for model
	Float:Position[3],
	Float:Angle[3],
	Float:Scale,
	
	m_nModelIndex,
	
	//check variable
	bool:forEvent,
	bool:forRestrict,
	bool:forAdmin,
};
new ItemData[MAX_ITEMS+1][_ItemData];

enum _PlayerData {
	//Base
	Point,
	Float:PlayingTime,
	Float:LastEngineTime,
	String:Tag[16],
	
	SendPoint,
	
	//Item
	ItemID[MaxType+1],
	bool:ItemExist[MaxType+1],
	bool:ItemOn[MaxType+1],
	Strengthen[MaxType+1],
	
	//trade to Box
	selectedIndex[5],//id
	selectedNumber[5],//idx
	selectedStrengthen[5],//strengthen
	selectedCount,
	selectedCase,//0=all, 1=species, 2=once
	
	//Data Loaded
	bool:DataLoaded,
};
new PlayerData[MAXPLAYERS+1][_PlayerData];

new String:g_shoplog_path[PLATFORM_MAX_PATH];

new g_hat_reference[MAXPLAYERS+1];
new g_hat_number[MAXPLAYERS+1];

public const Float:ZERO_VECTOR[3] = {0.0, 0.0, 0.0};

//Specific Item Name
#define DISPOSABLE_TAG_UTILIZATION 62
#define DISPOSABLE_STRENGTHEN_UTILIZATION 63

#include "pointshop/command.sp"
#include "pointshop/menu.sp"

#include "pointshop/attendance.sp"
#include "pointshop/countermarket.sp"
#include "pointshop/coupon.sp"
#include "pointshop/info.sp"
#include "pointshop/inventory.sp"
#include "pointshop/option.sp"
#include "pointshop/postbox.sp"
#include "pointshop/shop.sp"
#include "pointshop/strengthen.sp"
#include "pointshop/trade.sp"

#include "pointshop/hat.sp"
#include "pointshop/skin.sp"
#include "pointshop/effect.sp"
#include "pointshop/tag.sp"

public OnPluginStart()
{
	LoadTranslations("ttt.phrases");
	
	BuildPath(Path_SM, g_shoplog_path, PLATFORM_MAX_PATH, "logs/shop_log.log");
	
	HookEvent("player_disconnect", Hook_PlayerDisconnect);
	HookEvent("player_spawn", Hook_PlayerSpawn);
	HookEvent("player_death", Hook_PlayerDeath);
	HookEvent("round_start", Hook_RoundStart);
	
	CreateItem(1, "푸딩(Pudding)", HatType, "models/hats/flan/flan.mdl", 0, Float:{-4.0, 0.0, 1.0}, Float:{20.0, 0.0, 0.0});
	CreateItem(2, "오징어(Squid)", HatType, "models/hats/squid/squid.mdl", 1000, Float:{-3.5, 0.0, 1.7}, Float:{0.0, 90.0, -10.0});
	CreateItem(3, "보라돌이(Tinky Winky)", SkinType, "models/troubletown/teletubbies_v2/tinkywinky.mdl", 1000, Float:{0.8, 0.0, 0.0}, _, 1.3);
	CreateItem(4, "뚜비(Dipsy)", SkinType, "models/troubletown/teletubbies_v2/dipsy.mdl", 1000, Float:{0.9, 0.0, 0.0}, _, 1.3);
	CreateItem(5, "나나(Laa-Laa)", SkinType, "models/troubletown/teletubbies_v2/laalaa.mdl", 1000, Float:{0.9, 0.0, 0.0}, _, 1.3);
	CreateItem(6, "뽀(Po)", SkinType, "models/troubletown/teletubbies_v2/po.mdl", 1000, Float:{0.9, 0.0, 0.0}, _, 1.3);
	CreateItem(7, "추석 토끼(Easter Bunny)", SkinType, "models/troubletown/techknow/easter/e-bunny_fix.mdl", 0, Float:{2.0, 0.0, -4.0}, _, 1.3);
	CreateItem(8, "레이센 우동게인 이나바(Reisen Udongein Inaba)", SkinType, "models/troubletown/reisenudongeininaba/reisenudongeininaba_fix.mdl", 0, Float:{1.0, 0.0, -3.0});
	CreateItem(9, "데드마우스(Dead Mouse)", HatType, "models/hats/deadmau5/deadmau5.mdl", 0, Float:{-3.5, 0.0, 2.0}, Float:{0.0, -90.0, -90.0});
	CreateItem(10, "랜덤 아이템 상자(Random Item Box)", 10, "상점,도전과제를 제외한 모든 아이템중 하나가 나온다.", 3000, _, _, _, false);
	CreateItem(11, "치르노(Cirno)", SkinType, "models/troubletown/cirno/cirno_v2.mdl", 0, Float:{0.0, 0.0, -6.0}, _, 1.21, true);
	CreateItem(12, "레밀리아 스칼렛(Remilia Scarlet)", SkinType, "models/troubletown/remilia_scarlet/remilia_scarlet.mdl", 0, Float:{0.0, 0.0, 2.0}, _, 1.25, true);
	CreateItem(13, "플랑도르 스칼렛(Flandre Scarlet)", SkinType, "models/troubletown/flandre_scarlet/flandre_scarlet.mdl", 0, Float:{0.0, 0.0, 2.0}, _, 1.25, true);
	CreateItem(14, "코치야 사나에(Kochiya Sanae)", SkinType, "models/troubletown/kochiya_sanae/kochiya_sanae.mdl", 0, Float:{-1.75, 0.0, 2.0}, _, 1.1, true);
	CreateItem(15, "코메이지 사토리(Komiji Satori)", SkinType, "models/troubletown/komeiji_satori/komeiji_satori.mdl", 0, Float:{-0.5, 0.0, 3.0}, _, 1.1, true);
	CreateItem(16, "콘파쿠 요우무(Konpaku Youmu)", SkinType, "models/troubletown/konpaku_youmu/konpaku_youmu.mdl", 0, Float:{-0.5, 0.0, 1.5}, _, _, true);
	CreateItem(17, "마조라 마스크(Majora Mask)", HatType, "models/hats/majoramask/majoramask.mdl", 0, Float:{-2.0, 0.0, 1.5}, _, _, true);
	CreateItem(18, "페도베어(Pedo Bear)", SkinType, "models/troubletown/bear/pedobear/pedobear.mdl", 1000, Float:{1.0, 0.0, -6.0}, _, 1.4);
	CreateItem(19, "화이트베어(White Bear)", SkinType, "models/troubletown/bear/whitebear/whitebear.mdl", 0, Float:{1.0, 0.0, -6.0}, _, 1.4, true);
	CreateItem(20, "블랙베어(Black Bear)", SkinType, "models/troubletown/bear/blackbear/blackbear.mdl", 0, Float:{1.0, 0.0, -6.0}, _, 1.4, true);
	CreateItem(21, "스카이베어(Sky Bear)", SkinType, "models/troubletown/bear/skybear/skybear.mdl", 0, Float:{1.0, 0.0, -6.0}, _, 1.4, true);
	CreateItem(22, "핑크베어(Pink Bear)", SkinType, "models/troubletown/bear/pinkbear/pinkbear.mdl", 0, Float:{1.0, 0.0, -6.0}, _, 1.4, true);
	CreateItem(23, "헤드크랩(HeadCrab)", HatType, "models/hats/headcrab/w_headcrab.mdl", 0, Float:{-3.5, 0.0, 4.5}, Float:{90.0, 180.0, 0.0}, _, true);
	CreateItem(24, "우주인 헬멧(Astronaut Helmet)", HatType, "models/hats/astronauthelmet/astronauthelmet.mdl", 0, Float:{-3.0, 0.0, -4.0}, Float:{0.0, 90.0, 0.0}, _, true);
	CreateItem(25, "케이크(Cake)", HatType, "models/hats/cakehat/cakehat.mdl", 0, Float:{0.0, 0.0, 5.0}, Float:{0.0, 90.0, 0.0}, _, true);
	CreateItem(26, "천사링(Halo)", HatType, "models/hats/Halo/halo.mdl", 0, Float:{-4.0, 0.0, 15.0}, _, _, true);
	CreateItem(27, "뷰티샵의 신상품 상자(BeautyShop New Product Box)", 11, "최신 아이템 10개중 1개가 담긴 상자", 0, _, _, _, false);
	CreateItem(28, "피카츄(Pikachu)", HatType, "models/hats/ttg_max/ttg_max.mdl", 0, Float:{-7.0, 0.0, -71.0}, _, _, true);
	CreateItem(29, "아프로 머리(Afro Hair)", HatType, "models/hats/wtf/wtf.mdl", 0, Float:{-5.0, 0.0, 5.5}, _, _, true);
	CreateItem(30, "모자(어드민전용)", HatType, "models/hats/devteamhat/devteamhat.mdl", 0, Float:{-1.5, 0.0, 6.5}, Float:{0.0, -90.0, -90.0}, _, _, _, true);
	CreateItem(31, "스킨(어드민전용)", SkinType, "models/troubletown/admin/admin_v2.mdl", 0, _, _, _, _, _, true);
	CreateItem(32, "미쿠(Miku)", SkinType, "models/troubletown/miku/miku.mdl", 0, Float:{0.0, 0.0, 0.5}, _, 1.05, true);
	CreateItem(33, "분홍 미쿠(Spring Miku)", SkinType, "models/troubletown/spring_miku/spring_miku.mdl", 0, _, _, 1.05, true);
	CreateItem(34, "파랑 미쿠(Sea Miku)", SkinType, "models/troubletown/sea_miku/sea_miku.mdl", 0, _, _, 1.05, true);
	CreateItem(35, "이자요이 사쿠야(Izayoi Sakuya)", SkinType, "models/troubletown/izayoi_sakuya/izayoi_sakuya.mdl", 0, Float:{0.0, 0.0, -2.5}, _, 1.05, true);
	CreateItem(36, "사이교우지 유유코(Saigyouji Yuyuko)", SkinType, "models/troubletown/saigyouji_yuyuko/saigyouji_yuyuko_v2.mdl", 0, _, _, 1.0, true);
	CreateItem(37, "우사미 렌코(Usami Renko)", SkinType, "models/troubletown/usami_renko/usami_renko.mdl", 0, _, _, 1.0, true);
	CreateItem(38, "랜덤 모자 상자(Random Hat Box)", 11+HatType, "모자가 나오는 신기한 상자", 0, _, _, _, true);
	CreateItem(39, "랜덤 스킨 상자(Random Skin Box)", 11+SkinType, "스킨이 나오는 신기한 상자", 0, _, _, _, true);
	CreateItem(40, "앨리스 마가트로이드(Alice Margatroid)", SkinType, "models/troubletown/alice_margatroid/alice_margatroid.mdl", 0, _, _, 1.0, true);
	CreateItem(Youmu_Ghost, "요우무의 반령(Youmu's Half-Soul)", EffectType, "", 250, _, _, 1.0, true);
	CreateItem(MagicCircle, "매직서클(Magic Circle)", EffectType, "models/troubletown/magic_circle/magic_circle.mdl", 250, _, _, 1.0, true);
	CreateItem(Yuyuko_Butterfly, "유유코의 사접(Yuyuko's Deathful Butterfly)", EffectType, "models/troubletown/butterfly/butter.mdl", 250, _, _, 1.0, true);
	CreateItem(44, "호쥬 누에(Houjuu Nue)", SkinType, "models/troubletown/houjuu_nue/houjuu_nue.mdl", 0, _, _, 1.0, true);
	CreateItem(45, "하쿠레이 레이무(Hakurei Reimu)", SkinType, "models/troubletown/hakurei_reimu/hakurei_reimu.mdl", 0, _, _, 1.0, true);
	CreateItem(46, "키리사메 마리사(Kirisame Marisa)", SkinType, "models/troubletown/kirisame_marisa/kirisame_marisa.mdl", 0, _, _, 1.0, true);
	CreateItem(47, "VIP 감사 상자", 16, "시즌1을 위해 기부해주신 분들께 드리는 상자\n선택은 자신의 몫", 0, _, _, _, false);
	CreateItem(48, "랜덤 크로우바 상자(Random Crowbar Box)", 11+VMSkinType, "빠루 스킨이 나오는 신기한 상자", 0, _, _, _, true);
	CreateItem(49, "CS:S 기본 칼(CS:S Default Knife)", VMSkinType, "models/weapons/v_knife_t.mdl", 1000);
	CreateItem(50, "레바테인(Laevateinn)", VMSkinType, "models/troubletown/weapons/laevateinn/v_laevateinn.mdl", 0, _, _, _, true);
	CreateItem(51, "수박바(Cirno Knife)", VMSkinType, "models/troubletown/weapons/melonbar/v_melonbar.mdl", 0, _, _, _, true);
	CreateItem(52, "유유코의 부채(Yuyuko's Fan)", VMSkinType, "models/troubletown/weapons/yuyuko_fan/v_yuyuko_fan.mdl", 0, _, _, _, true);
	CreateItem(53, "마법봉(MoonStick)", VMSkinType, "models/troubletown/weapons/moonstick/v_moonstick.mdl", 0, _, _, _, true);
	CreateItem(54, "크리스마스 상자", 17, "아직 용도를 알 수없는 상자. 조금 더 기다려 보자", 0, _, _, _, false, true);
	CreateItem(55, "이벤트 한정 상자", 18, "무엇이든 고를수 있는 특별한 상자", 0, _, _, _, false);
	CreateItem(56, "궁그닐(Gungnir)", VMSkinType, "models/troubletown/weapons/gungnir/v_gungnir.mdl", 0, _, _, _, true);
	CreateItem(57, "뼈다귀(BoneClub)", VMSkinType, "models/troubletown/weapons/boneclub/v_boneclub.mdl", 0, _, _, _, true);
	CreateItem(58, "치르노 상자(Cirno Box)", 19, "치르노가 나오는 상자", 0, _, _, _, false);
	CreateItem(59, "랜덤 아이템 상자(Random Item Box)", 19, "상점,도전과제를 제외한 모든 아이템중 하나가 나온다.", 0, _, _, _, false);
	CreateItem(60, "랜덤 스킨 상자(Random SKin Box)", 19, "스킨이 나오는 신기한 상자", 0, _, _, _, false);
	CreateItem(61, "⑨l벤트 한정 상자", 20, "무엇이든 (치르노를) 고를수 있는 특별한 상자", 0, _, _, _, false);
	CreateItem(DISPOSABLE_TAG_UTILIZATION, "일회용 태그 이용권(Disposable Tag Utilization)", TagType, "일회용 태그 이용권", 1000, _, _, _, false);
	CreateItem(DISPOSABLE_STRENGTHEN_UTILIZATION, "일회용 강화 이용권(Disposable Strengthen Utilization)", TagType, "스킨 대신 사용할수 있는 강화 이용권", 0);
	CreateItem(64, "넵튠(Neptune)", SkinType, "models/troubletown/neptune/neptune.mdl", 0, _, _, 1.0, true);
	CreateItem(65, "히나나이 텐시(Hinanawi Tenshi)", SkinType, "models/troubletown/hinanawi_tenshi/hinanawi_tenshi.mdl", 0, _, _, 1.0, true);
	CreateItem(66, "미스티아 로렐라이(Mystia Lorelei)", SkinType, "models/troubletown/mystia_lorelei/mystia_lorelei.mdl", 0, _, _, 1.0, true);
	CreateItem(67, "랜덤 이펙트 상자(Random Effect Box)", 11+EffectType, "이펙트 스킨이 나오는 신기한 상자", 0, _, _, _, true);
	
	ConnectSQL();
	Command_Initialize();
	Menu_Initialize();
	PrecacheVMSkin();
}

public OnPluginEnd()
{
	for(new i=1; i<=MaxClients; i++)
	{
		if(!IsClientInGame(i) || GetClientTeam(i) < 2 || !IsPlayerAlive(i)) continue;
		
		RemovePlayerHat(i);
		RemovePlayerEffect(i);
	}
}

public APLRes:AskPluginLoad2(Handle:myself, bool:late, String:error[], err_max)
{
   CreateNative("Shop_GetViewModelIndex", Native_GetViewModelIndex);
   CreateNative("Shop_GetClientTag", Native_GetClientTag);
   CreateNative("Shop_GetExtraHealth", Native_GetExtraHealth);
   CreateNative("Shop_GetExtraKnifeDamage", Native_GetExtraKnifeDamage);
   CreateNative("Shop_CreateCouponBox", Native_CreateCouponBox);
   return APLRes_Success;
}

public PrecacheVMSkin()
{
	for(new i = 1; i <= MAX_ITEMS; i++)
	{
		if(ItemData[i][Type] == VMSkinType)
		{
			ItemData[i][m_nModelIndex] = PrecacheModel(ItemData[i][Path], true);
		}
	}
}

public OnMapStart()
{
	PrecacheVMSkin();
}

stock CreateItem(ItemNumber, String:Item_Name[64], Item_Type, String:Item_Path[256], Item_Price, Float:HatPosition[3] = {0.0,0.0,0.0}, Float:HatAngle[3] = {0.0,0.0,0.0}, Float:Item_Scale = 1.0, bool:onlyEvent=false, bool:Restrict=false, bool:onlyAdmin = false)
{
	Format(ItemData[ItemNumber][Name], 64, Item_Name);
	ItemData[ItemNumber][Type] = Item_Type;
	Format(ItemData[ItemNumber][Path], 256, Item_Path);
	ItemData[ItemNumber][Price] = Item_Price;
	for(new i; i<3; i++)
	{
		ItemData[ItemNumber][Position][i] = HatPosition[i];
		ItemData[ItemNumber][Angle][i] = HatAngle[i];
	}
	ItemData[ItemNumber][Scale] = Item_Scale;
	ItemData[ItemNumber][forEvent] = onlyEvent;
	ItemData[ItemNumber][forRestrict] = Restrict;
	ItemData[ItemNumber][forAdmin] = onlyAdmin;
}

stock GetRandomItem()
{
	new iList[MAX_ITEMS+1];
	new iCount;
	for(new i = 1; i <= MAX_ITEMS; i++)
	{
		if(ItemData[i][forEvent])
		{
			iList[iCount] = i;
			iCount++;
		}
	}
	if(iCount == 0)
	{
		return -1;
	}
	new iRandomIndex = GetRandomInt(0, iCount-1);
	return iList[iRandomIndex];
}

stock GetRandomNewestItem()
{
	new iList[10] = {46, 48, 50, 51, 52,56,57,64,65,66};
	new iRandomIndex = GetRandomInt(0, 9);
	return iList[iRandomIndex];
}

stock GetRandomTypeItem(type)
{
	new iList[MAX_ITEMS+1];
	new iCount;
	for(new i = 1; i <= MAX_ITEMS; i++)
	{
		if(ItemData[i][forEvent] && ItemData[i][Type] == type)
		{
			iList[iCount] = i;
			iCount++;
		}
	}
	if(iCount == 0)
	{
		return -1;
	}
	new iRandomIndex = GetRandomInt(0, iCount-1);
	return iList[iRandomIndex];
}

stock GetChristmasItem()
{
	if(GetRandomInt(0, 99) < 5) {
		return 55;
	}
	
	if(GetRandomInt(0, 99) < 15) {
		return 11;
	}
	
	new iList[7] = {10,27,38,39,48,56,57};
	new iRandomIndex = GetRandomInt(0, 6);
	return iList[iRandomIndex];
}

stock bool:IsBoxItem(ItemNumber)
{
	return (ItemData[ItemNumber][Type] >= 10);
}

public Native_GetViewModelIndex(Handle:plugin, numParams)
{
	new Client = GetNativeCell(1);
	if(Client < 1 || Client > MaxClients) {
		return ThrowNativeError(SP_ERROR_NATIVE, "Client index %d is invalid.", Client);
	}
	if(!PlayerData[Client][DataLoaded] || !PlayerData[Client][ItemExist][VMSkinType] || !PlayerData[Client][ItemOn][VMSkinType]) {
		return 0;
	}
	new vmskin = PlayerData[Client][ItemID][VMSkinType];
	return ItemData[vmskin][m_nModelIndex];
}

public Native_GetClientTag(Handle:plugin, numParams)
{
	new Client = GetNativeCell(1);
	if(Client < 1 || Client > MaxClients) {
		return ThrowNativeError(SP_ERROR_NATIVE, "Client index %d is invalid.", Client);
	}
	if(PlayerData[Client][DataLoaded])
	{
		new max_size = GetNativeCell(3);
		SetNativeString(2, PlayerData[Client][Tag], max_size, false);
	}
	return bool:strlen(PlayerData[Client][Tag]);
}

public Native_GetExtraHealth(Handle:plugin, numParams)
{
	new Client = GetNativeCell(1);
	if(Client < 1 || Client > MaxClients) {
		return ThrowNativeError(SP_ERROR_NATIVE, "Client index %d is invalid.", Client);
	}
	if(!PlayerData[Client][DataLoaded] || !PlayerData[Client][ItemExist][SkinType] || !PlayerData[Client][ItemOn][SkinType])
	{
		return 0;
	}
	return PlayerData[Client][Strengthen][SkinType]/2+PlayerData[Client][Strengthen][SkinType]%2;
}

public Native_GetExtraKnifeDamage(Handle:plugin, numParams)
{
	new Client = GetNativeCell(1);
	if(Client < 1 || Client > MaxClients) {
		return ThrowNativeError(SP_ERROR_NATIVE, "Client index %d is invalid.", Client);
	}
	if(!PlayerData[Client][DataLoaded] || !PlayerData[Client][ItemExist][VMSkinType] || !PlayerData[Client][ItemOn][VMSkinType])
	{
		return 0;
	}
	new strengthen = PlayerData[Client][Strengthen][VMSkinType];
	new temp = strengthen%3;
	return strengthen/3+temp/2+temp%2;
}

public Native_CreateCouponBox(Handle:plugin, numParams)
{
	new Client = GetNativeCell(1);
	if(Client < 1 || Client > MaxClients) {
		return ThrowNativeError(SP_ERROR_NATIVE, "Client index %d is invalid.", Client);
	}
	Create_CouponBox(Client);
	return 1;
}

stock LogShopData(const String:format[], any:...)
{
	new String:buffer[512];
	VFormat(buffer, sizeof(buffer), format, 2);
	LogToFileEx(g_shoplog_path, buffer);
}

stock CopyVector(Float:in[3], Float:out[3])
{
	for(new i; i<3; i++) out[i] = in[i];
}

public Hook_PlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast)
{
	new Client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(GetClientTeam(Client) < 2)
		return;
	
	WearPlayerSkin(Client);
	CreatePlayerHat(Client);
	CreatePlayerEffect(Client);
}

public Hook_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast)
{
	new Client = GetClientOfUserId(GetEventInt(event, "userid"));
	RemovePlayerHat(Client);
	RemovePlayerEffect(Client);
}

public Hook_PlayerDisconnect(Handle:event, const String:name[], bool:dontBroadcast)
{
	new Client = GetClientOfUserId(GetEventInt(event, "userid"));
	RemovePlayerHat(Client);
	RemovePlayerEffect(Client);
}

public Hook_RoundStart(Handle:event, const String:name[], bool:dontBroadcast)
{
	for(new i=1; i<=MaxClients; i++)
	{
		if(!IsClientInGame(i) || !IsPlayerAlive(i) || GetClientTeam(i) < 2) continue;
		
		WearPlayerSkin(i);
		CreatePlayerHat(i);
		CreatePlayerEffect(i);
	}
}

public ShowMainShop(Client)
{
#if defined DEBUG_PLUGIN
	if(!OwnerCheck(Client))
	{
		return;
	}
#endif
	new Handle:menuhandle = g_pointshop_main_Menu;
	RemoveAllMenuItems(menuhandle);
	SetGlobalTransTarget(Client);
	
	SetMenuExitButton(menuhandle, true);
	SetMenuTitle(menuhandle, "%t", "point shop");
	AddMenuItemTranslated(menuhandle, "my info");
	AddMenuItemTranslated(menuhandle, "my inventory");
	AddMenuItemTranslated(menuhandle, "buy item");
	AddMenuItemTranslated(menuhandle, "option");
	AddMenuItemTranslated(menuhandle, "flea market");
	AddMenuItemTranslated(menuhandle, "wealth");
	AddMenuItemTranslated(menuhandle, "post box", PlayerData[Client][PlayingTime] < 9000.0);
	
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

stock AddMenuItemTranslated(Handle:menuhandle, const String:words[], bool:disable = false)
{
	new String:sItemInfo[64];
	Format(sItemInfo, sizeof(sItemInfo), "%t", words);
	AddMenuItem(menuhandle, NULL_STRING, sItemInfo, disable ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
}

stock AddMenuItemTranslated2(Handle:menuhandle, const String:info[], const String:words[], bool:disable = false)
{
	new String:sItemInfo[64];
	Format(sItemInfo, sizeof(sItemInfo), "%t", words);
	AddMenuItem(menuhandle, info, sItemInfo, disable ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
}

stock DayToInteger()
{
	new String:sYear[4], String:sMonth[4], String:sDay[4];
	FormatTime(sYear, 30, "%Y", GetTime());
	FormatTime(sMonth, 30, "%m", GetTime());
	FormatTime(sDay, 30, "%d", GetTime());
	
	new String:sValue[32];
	Format(sValue, sizeof(sValue), "%04d%02d%02d", StringToInt(sYear), StringToInt(sMonth), StringToInt(sDay));
	return StringToInt(sValue);
}

stock bool:OwnerCheck(Client)
{
	new String:SteamID[20];
	GetClientAuthString(Client, SteamID, sizeof(SteamID));
	return StrEqual(SteamID, ADMIN_STEAMID);
}

public ConnectSQL()
{
	SQL_TConnect(CallBack_ConnectSQL, DATABASE_NAME);
}

public CallBack_ConnectSQL(Handle:owner, Handle:dbhandle, const String:error[], any:data)
{
	if(dbhandle == INVALID_HANDLE)
	{
		LogError("SQL Connecting Failed %s", error);
	}
	else
	{
		databasehandle = dbhandle;
		
		//글자 설정(UTF-8)
		SQL_TQuery(databasehandle, save_info, "SET NAMES 'UTF8'", 0, DBPrio_High);
		
		//플레이어 데이터 테이블 생성
		SQL_TQuery(databasehandle, CallBack_CreateTable, "create table if not exists info(id bigint primary key not null AUTO_INCREMENT, steamid varchar(20) CHARACTER SET utf8, nick varchar(32) CHARACTER SET utf8, point int DEFAULT 0, playingtime FLOAT DEFAULT '0.0', tag varchar(16) CHARACTER SET utf8 NOT NULL, attendance int DEFAULT 19990101, attendance_reward int DEFAULT 0);");
		SQL_TQuery(databasehandle, CallBack_CreateTable, "create table if not exists garbagecan(id bigint primary key not null AUTO_INCREMENT, steamid varchar(20) CHARACTER SET utf8, idx int, type bigint);");
		SQL_TQuery(databasehandle, CallBack_CreateTable, "create table if not exists inventory(id bigint primary key not null AUTO_INCREMENT, steamid varchar(20) CHARACTER SET utf8, idx int, type bigint, strengthen int DEFAULT 0);");
		SQL_TQuery(databasehandle, CallBack_CreateTable, "create table if not exists postbox(id bigint primary key not null AUTO_INCREMENT, steamid varchar(20) CHARACTER SET utf8, idx int, count bigint DEFAULT 1, text varchar(128) CHARACTER SET utf8);");
		SQL_TQuery(databasehandle, CallBack_CreateTable, "create table if not exists countermarket(id bigint primary key not null AUTO_INCREMENT, steamid varchar(20) CHARACTER SET utf8, nick varchar(32) CHARACTER SET utf8, idx int, price int, strengthen int DEFAULT 0);");
		SQL_TQuery(databasehandle, CallBack_CreateTable, "create table if not exists countermarket_profit(id bigint primary key not null AUTO_INCREMENT, steamid varchar(20) CHARACTER SET utf8, number int, profit int);");
		SQL_TQuery(databasehandle, CallBack_CreateTable, "create table if not exists admtag(id bigint primary key not null AUTO_INCREMENT, steamid varchar(20) CHARACTER SET utf8, red int DEFAULT 198, green int DEFAULT 198, blue int DEFAULT 198, alpha int DEFAULT 255);");
		SQL_TQuery(databasehandle, CallBack_CreateTable, "create table if not exists restrictitem(id bigint primary key not null AUTO_INCREMENT, steamid varchar(20) CHARACTER SET utf8, ip varchar(16) CHARACTER SET utf8, idx int, date int);");
		SQL_TQuery(databasehandle, CallBack_CreateTable, "create table if not exists setting_option(id bigint primary key not null AUTO_INCREMENT, steamid varchar(20) CHARACTER SET utf8, bgm int DEFAULT 1, autofire int DEFAULT 1);");
		SQL_TQuery(databasehandle, CallBack_CreateTable, "create table if not exists title(id bigint primary key not null AUTO_INCREMENT, steamid varchar(20) CHARACTER SET utf8, title int DEFAULT 0);");
		SQL_TQuery(databasehandle, CallBack_CreateTable, "create table if not exists coupon(id bigint primary key not null AUTO_INCREMENT, couponnumber varchar(32) CHARACTER SET utf8, type tinyint, number int, duplicate tinyint DEFAULT 0);");
		SQL_TQuery(databasehandle, CallBack_CreateTable, "create table if not exists used_coupon(id bigint primary key not null AUTO_INCREMENT, steamid varchar(20) CHARACTER SET utf8, number int);");
		SQL_TQuery(databasehandle, CallBack_CreateTable, "create table if not exists tag_duration(id bigint primary key not null AUTO_INCREMENT, steamid varchar(20) CHARACTER SET utf8, year int DEFAULT 0, month int DEFAULT 0, day int DEFAULT 0, hour int DEFAULT 0);");
		for(new i = 1; i < MaxType+1; i++)
		{
			new String:query[256];
			Format(query, sizeof(query), "create table if not exists %s(id bigint primary key not null AUTO_INCREMENT, steamid varchar(20) CHARACTER SET utf8, idx int, options int, strengthen int DEFAULT 0);", TypeName[i][1]);
			SQL_TQuery(databasehandle, CallBack_CreateTable, query, 0);
		}
		
		SQL_TQuery(databasehandle, save_info, "SET global event_scheduler = on;", 0);
		SQL_TQuery(databasehandle, CallBack_CreateEvent, "create event if not exists reset_garbagecan ON SCHEDULE EVERY 1 DAY STARTS '2015-01-01 00:00:00' do DELETE FROM garbagecan;", 0);
		SQL_TQuery(databasehandle, CallBack_CreateEvent, "create event if not exists reset_attendance ON SCHEDULE EVERY 1 MONTH STARTS '2015-01-01 00:00:00' do UPDATE info SET attendance_reward = 0;", 0);
		
		//load player data
		for(new i=1; i<=MaxClients; i++)
		{
			if(IsClientInGame(i) == false) continue;
			
			new String:SteamID[20];
			GetClientAuthString(i, SteamID, sizeof(SteamID));
			if(strlen(SteamID))
			{
				OnClientAuthorized(i, SteamID);
			}
			
			OnClientPutInServer(i);
		}
	}
}

public CallBack_CreateTable(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	if(hndl == INVALID_HANDLE)
	{
		LogError("Player Data Table Creating Failed. %s", error);
	}
}

public CallBack_CreateEvent(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	if(hndl == INVALID_HANDLE)
	{
		LogError("Event Creating Failed. %s", error);
	}
}

public save_info(Handle:owner, Handle:handle, const String:error[], any:data)
{
	if(handle == INVALID_HANDLE)
	{
		LogError("SQL ERROR: %s", error);
	}
}

public save_info_nolog(Handle:owner, Handle:handle, const String:error[], any:data)
{
}

public first_join_gift(Handle:owner, Handle:handle, const String:error[], any:Client)
{
	if(handle == INVALID_HANDLE)
	{
		LogError("SQL ERROR: %s", error);
		return;
	}
	
	new String:SteamID[20], String:OwnerSteamID[20];
	GetClientAuthString(Client, SteamID, sizeof(SteamID));
	if(GetOwnerAuthString(Client, OwnerSteamID, sizeof(OwnerSteamID)))
	{
		if(StrEqual(SteamID, OwnerSteamID))
		{
			new String:query[256];
			Format(query, sizeof(query), "INSERT INTO postbox(steamid, idx, count, text) VALUES('%s', '38', '1', 'First Join Reward');", SteamID);
			SQL_TQuery(databasehandle, save_info, query, Client);
			Format(query, sizeof(query), "INSERT INTO postbox(steamid, idx, count, text) VALUES('%s', '39', '1', 'First Join Reward');", SteamID);
			SQL_TQuery(databasehandle, save_info, query, Client);
			Format(query, sizeof(query), "INSERT INTO postbox(steamid, idx, count, text) VALUES('%s', '48', '1', 'First Join Reward');", SteamID);
			SQL_TQuery(databasehandle, save_info, query, Client);
		}
	}
}

public OnClientConnected(Client)
{
	ResetPlayerData(Client);
}

public OnClientPutInServer(Client)
{
	if(PlayerData[Client][DataLoaded])
	{
		new String:SteamID[20], String:query[256];
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		Format(query, sizeof(query), "SELECT id FROM postbox WHERE steamid = '%s';", SteamID);
		SQL_TQuery(databasehandle, Announce_postbox, query, Client);
	}
	SDKHook(Client, SDKHook_PostThinkPost, Hook_PostThinkPost);
}

public Hook_PostThinkPost(Client)
{
	if(!PlayerData[Client][DataLoaded] || GetClientTeam(Client) < 2) {
		return;
	}
	
	static saveCount[MAXPLAYERS+1];
	new Float:diff = FloatSub(GetEngineTime(), PlayerData[Client][LastEngineTime]);
	if(diff > 1.0)
	{
		PlayerData[Client][LastEngineTime] = GetEngineTime();
		if(diff > 3.0) { // bug
			return;
		}
		
		if(RoundToZero(PlayerData[Client][PlayingTime]) % 300 > RoundToZero(FloatAdd(PlayerData[Client][PlayingTime], diff)) % 300)
		{
			PlayerData[Client][Point] += 10;
			ServerCommand("zx2_add_zdollar %d 100", GetClientUserId(Client));
			SavePoint(Client);
			PrintToChat(Client, "\x04[TTT] \x01- You received 10 points and 100 Z-Dollar");
		}
		PlayerData[Client][PlayingTime] += diff;
		saveCount[Client]++;
		if(saveCount[Client]%30 == 0)
		{
			new String:SteamID[32], String:query[512];
			GetClientAuthString(Client ,SteamID, sizeof(SteamID));
			Format(query, 512, "UPDATE info SET playingtime = '%f' WHERE steamid = '%s';", PlayerData[Client][PlayingTime], SteamID);
			SQL_TQuery(databasehandle, save_info, query, Client);
		}
	}
}

public OnClientAuthorized(Client, const String:SteamID[])
{
	//db not loaded
	if(databasehandle == INVALID_HANDLE)
		return;
	
	//bot don't need to load data
	if(StrEqual(SteamID, "BOT") == true)
		return;
	
	Player_DataLoad(Client, SteamID);
}

public ResetPlayerData(Client)
{
	PlayerData[Client][Point] = 0;
	PlayerData[Client][PlayingTime] = 0.0;
	PlayerData[Client][LastEngineTime] = GetEngineTime();
	Format(PlayerData[Client][Tag], 16, NULL_STRING);
	
	for(new i=1; i<=MaxType; i++)
	{
		PlayerData[Client][ItemID][i] = 0;
		PlayerData[Client][ItemOn][i] = false;
		PlayerData[Client][ItemExist][i] = false;
		PlayerData[Client][Strengthen][i] = 0;
	}
	
	PlayerData[Client][DataLoaded] = false;
}

public Player_DataLoad(Client, const String:SteamID[])
{
	new String:query[256];
	Format(query, sizeof(query), "SELECT nick, point, playingtime, tag,attendance,attendance_reward FROM info WHERE steamid = '%s';", SteamID);
	SQL_TQuery(databasehandle, DataLoad_info, query, Client);
	Format(query, sizeof(query), "SELECT idx,options,strengthen FROM hat WHERE steamid = '%s';", SteamID);
	SQL_TQuery(databasehandle, DataLoad_hat, query, Client);
	Format(query, sizeof(query), "SELECT idx,options,strengthen FROM skin WHERE steamid = '%s';", SteamID);
	SQL_TQuery(databasehandle, DataLoad_skin, query, Client);
	Format(query, sizeof(query), "SELECT idx,options,strengthen FROM effect WHERE steamid = '%s';", SteamID);
	SQL_TQuery(databasehandle, DataLoad_effect, query, Client);
	Format(query, sizeof(query), "SELECT idx,options,strengthen FROM vmskin WHERE steamid = '%s';", SteamID);
	SQL_TQuery(databasehandle, DataLoad_vmskin, query, Client);
	new String:sIP[16];
	if(GetClientIP(Client, sIP, sizeof(sIP))) {
		Format(query, sizeof(query), "SELECT idx FROM restrictitem WHERE (steamid = '%s' or ip = '%s') AND date = '%d';", SteamID, sIP, DayToInteger());
		SQL_TQuery(databasehandle, DataLoad_restrictitem, query, Client);
	}
	/*Format(query, sizeof(query), "SELECT * FROM setting_option WHERE steamid = '%s';", SteamID);
	SQL_TQuery(databasehandle, setting_check, query, Client);
	*/
}

public DataLoad_info(Handle:owner, Handle:handle, const String:error[], any:Client)
{
	if(handle == INVALID_HANDLE)
	{
		LogError("SQL ERROR: %s", error);
		return;
	}
	
	if(!IsClientConnected(Client) || PlayerData[Client][DataLoaded])
		return;
	
	new String:query[256], String:SteamID[20];
	GetClientAuthString(Client, SteamID, sizeof(SteamID));
	if(SQL_GetRowCount(handle))
	{
		while(SQL_FetchRow(handle))
		{
			new String:db_Name[32], String:cur_Name[32];
			GetClientName(Client, cur_Name, sizeof(cur_Name));
			SQL_FetchString(handle, 0, db_Name, sizeof(db_Name));
			PlayerData[Client][Point] = SQL_FetchInt(handle, 1);
			PlayerData[Client][PlayingTime] = SQL_FetchFloat(handle, 2);
			SQL_FetchString(handle, 3, PlayerData[Client][Tag], 16);
			if(StrEqual(db_Name, cur_Name) == false)
			{
				new String:sEscapedName[65];
				if(SQL_EscapeString(databasehandle, cur_Name, sEscapedName, sizeof(sEscapedName)) == true)
				{
					Format(query, sizeof(query), "UPDATE info SET nick = '%s' WHERE steamid = '%s';", sEscapedName, SteamID);
					SQL_TQuery(databasehandle, save_info, query, Client);
				}
			}
			PlayerData[Client][DataLoaded] = true;
			if(SQL_FetchInt(handle, 4) < DayToInteger())
			{
				AttendanceReward(Client, SQL_FetchInt(handle, 5)+1);
			}
		}
		return;
	}
	else
	{
		new String:sName[32];
		GetClientName(Client, sName, sizeof(sName));
		new String:sEscapedName[65];
		SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
		Format(query, sizeof(query), "INSERT INTO info(steamid, nick) VALUES('%s', '%s');", SteamID, sEscapedName);
		SQL_TQuery(databasehandle, first_join_gift, query, Client);
		PlayerData[Client][DataLoaded] = true;
	}
}

public DataLoad_hat(Handle:owner, Handle:handle, const String:error[], any:Client)
{
	if(handle == INVALID_HANDLE)
	{
		LogError("SQL ERROR: %s", error);
		return;
	}
	
	if(SQL_GetRowCount(handle))
	{
		while(SQL_FetchRow(handle))
		{
			PlayerData[Client][ItemExist][HatType] = true;
			PlayerData[Client][ItemID][HatType] = SQL_FetchInt(handle, 0);
			PlayerData[Client][ItemOn][HatType] = bool:SQL_FetchInt(handle, 1);
			PlayerData[Client][Strengthen][HatType] = SQL_FetchInt(handle, 2);
			if(IsClientInGame(Client) && GetClientTeam(Client) > 1 && IsPlayerAlive(Client))
			{
				CreatePlayerHat(Client);
			}
			break;
		}
	}
}

public DataLoad_skin(Handle:owner, Handle:handle, const String:error[], any:Client)
{
	if(handle == INVALID_HANDLE)
	{
		LogError("SQL ERROR: %s", error);
		return;
	}
	
	if(SQL_GetRowCount(handle))
	{
		while(SQL_FetchRow(handle))
		{
			PlayerData[Client][ItemExist][SkinType] = true;
			PlayerData[Client][ItemID][SkinType] = SQL_FetchInt(handle, 0);
			PlayerData[Client][ItemOn][SkinType] = bool:SQL_FetchInt(handle, 1);
			PlayerData[Client][Strengthen][SkinType] = SQL_FetchInt(handle, 2);
			if(IsClientInGame(Client) && GetClientTeam(Client) > 1 && IsPlayerAlive(Client))
			{
				WearPlayerSkin(Client);
			}
			break;
		}
	}
}

public DataLoad_effect(Handle:owner, Handle:handle, const String:error[], any:Client)
{
	if(handle == INVALID_HANDLE)
	{
		LogError("SQL ERROR: %s", error);
		return;
	}
	
	if(SQL_GetRowCount(handle))
	{
		while(SQL_FetchRow(handle))
		{
			PlayerData[Client][ItemExist][EffectType] = true;
			PlayerData[Client][ItemID][EffectType] = SQL_FetchInt(handle, 0);
			PlayerData[Client][ItemOn][EffectType] = bool:SQL_FetchInt(handle, 1);
			PlayerData[Client][Strengthen][EffectType] = SQL_FetchInt(handle, 2);
			if(IsClientInGame(Client) && GetClientTeam(Client) > 1 && IsPlayerAlive(Client))
			{
				CreatePlayerEffect(Client);
			}
			break;
		}
	}
}

public DataLoad_vmskin(Handle:owner, Handle:handle, const String:error[], any:Client)
{
	if(handle == INVALID_HANDLE)
	{
		LogError("SQL ERROR: %s", error);
		return;
	}
	
	if(SQL_GetRowCount(handle))
	{
		while(SQL_FetchRow(handle))
		{
			PlayerData[Client][ItemExist][VMSkinType] = true;
			PlayerData[Client][ItemID][VMSkinType] = SQL_FetchInt(handle, 0);
			PlayerData[Client][ItemOn][VMSkinType] = bool:SQL_FetchInt(handle, 1);
			PlayerData[Client][Strengthen][VMSkinType] = SQL_FetchInt(handle, 2);
			break;
		}
	}
}

public DataLoad_restrictitem(Handle:owner, Handle:handle, const String:error[], any:Client)
{
	if(handle == INVALID_HANDLE)
	{
		LogError("SQL ERROR: %s", error);
		return;
	}
	
	new bool:dataExist[MAX_ITEMS+1];
	if(SQL_GetRowCount(handle))
	{
		while(SQL_FetchRow(handle))
		{
			new index = SQL_FetchInt(handle, 0);
			if(index <= MAX_ITEMS) {
				dataExist[index] = true;
			}
		}
	}
	
	CheckAnniversary(Client, dataExist);
}

public Announce_postbox(Handle:owner, Handle:handle, const String:error[], any:Client)
{
	if(handle == INVALID_HANDLE)
	{
		LogError("SQL ERROR: %s", error);
		return;
	}
	new count = SQL_GetRowCount(handle);
	if(count > 0)
	{
		PrintToChat(Client, "\x04[PostBox] \x01- \x08FF0000C8%d count(s) postal matter wasn't checked.", count);
	}
}

stock AddFolderToDownloadsTable(const String:sDirectory[], const String:contain_word[] = "")
{
	decl String:sFilename[128], String:sPath[256];
	new Handle:hDirectory = OpenDirectory(sDirectory);
	if(hDirectory != INVALID_HANDLE)
	{
		decl FileType:fileType;
		
		while(ReadDirEntry(hDirectory, sFilename, sizeof(sFilename), fileType))
		{
			if(fileType == FileType_Directory)
			{
				if(FindCharInString(sFilename, '.') == -1)
				{
					Format(sPath, sizeof(sPath), "%s/%s", sDirectory, sFilename);
					AddFolderToDownloadsTable(sPath, contain_word);
				}
			}
			else if(fileType == FileType_File)
			{
				if(StrContains(sFilename, contain_word, false) != -1)
				{
					Format(sPath, sizeof(sPath), "%s/%s", sDirectory, sFilename);
					AddFileToDownloadsTable(sPath);
					
					new iPos = FindCharInString(sPath, '.', true);
					
					if(iPos != -1)
					{			
						if(StrEqual(sPath[iPos], ".mdl"))
						{
							PrecacheModel(sPath, true);
						}
						else if(StrEqual(sPath[iPos], ".mp3") || StrEqual(sPath[iPos], ".wav"))
						{
							ReplaceStringEx(sPath, sizeof(sPath), "sound/", "");
							PrecacheSound(sPath, true);
						}
					}
				}
			}
		}
		CloseHandle(hDirectory);
	}
}

stock SavePoint(Client)
{
	if(PlayerData[Client][DataLoaded])
	{
		new String:query[512], String:SteamID[32];
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		Format(query, 512, "UPDATE info SET point = '%i' WHERE steamid = '%s';", PlayerData[Client][Point], SteamID);
		SQL_TQuery(databasehandle, save_info, query, Client);
	}
}

public query_wealth(Handle:owner, Handle:hndl, const String:error[], any:Client)
{
	if(hndl == INVALID_HANDLE)
	{
		return;
	}
	new counted = SQL_GetRowCount(hndl), Handle:menuhandle = CreateMenu(NULL_Handler), String:Formatting[256];
	SetMenuTitle(menuhandle, "-- Point Rank --");
	if(counted > 0)
	{
		if(SQL_HasResultSet(hndl))
		{
			while(SQL_FetchRow(hndl))
			{
				SQL_FetchString(hndl, 0, Formatting, 256);
				if(!StrEqual(Formatting, "")) Format(Formatting, 256, "%s - %i", Formatting, SQL_FetchInt(hndl, 1));
				else Format(Formatting, 256, "Unnamed - %i", SQL_FetchInt(hndl, 1));
				AddMenuItem(menuhandle, "", Formatting, ITEMDRAW_DISABLED);
			}
		}
	}
	SetMenuExitButton(menuhandle, true);
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}