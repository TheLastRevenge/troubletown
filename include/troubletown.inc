#if defined _troubletown_included
  #endinput
#endif
#define _troubletown_included

enum Activity {
	ACT_VM_DRAW = 171,
	ACT_VM_HOLSTER,
	ACT_VM_IDLE,
	ACT_VM_FIDGET,
	ACT_VM_PULLBACK,
	ACT_VM_PULLBACK_HIGH,
	ACT_VM_PULLBACK_LOW,
	ACT_VM_THROW,
	ACT_VM_PULLPIN,
	ACT_VM_PRIMARYATTACK,		// fire
	ACT_VM_SECONDARYATTACK,		// alt. fire
	ACT_VM_RELOAD,			
	ACT_VM_RELOAD_START,			
	ACT_VM_RELOAD_FINISH,			
	ACT_VM_DRYFIRE,				// fire with no ammo loaded.
	ACT_VM_HITLEFT,				// bludgeon, swing to left - hit (primary attk)
	ACT_VM_HITLEFT2,			// bludgeon, swing to left - hit (secondary attk)
	ACT_VM_HITRIGHT,			// bludgeon, swing to right - hit (primary attk)
	ACT_VM_HITRIGHT2,			// bludgeon, swing to right - hit (secondary attk)
	ACT_VM_HITCENTER,			// bludgeon, swing center - hit (primary attk)
	ACT_VM_HITCENTER2,			// bludgeon, swing center - hit (secondary attk)
	ACT_VM_MISSLEFT,			// bludgeon, swing to left - miss (primary attk)
	ACT_VM_MISSLEFT2,			// bludgeon, swing to left - miss (secondary attk)
	ACT_VM_MISSRIGHT,			// bludgeon, swing to right - miss (primary attk)
	ACT_VM_MISSRIGHT2,			// bludgeon, swing to right - miss (secondary attk)
	ACT_VM_MISSCENTER,			// bludgeon, swing center - miss (primary attk)
	ACT_VM_MISSCENTER2,			// bludgeon, swing center - miss (secondary attk)
	ACT_VM_HAULBACK,			// bludgeon, haul the weapon back for a hard strike (secondary attk)
	ACT_VM_SWINGHARD,			// bludgeon, release the hard strike (secondary attk)
	ACT_VM_SWINGMISS,
	ACT_VM_SWINGHIT,
	ACT_VM_IDLE_TO_LOWERED,
	ACT_VM_IDLE_LOWERED,
	ACT_VM_LOWERED_TO_IDLE,
	ACT_VM_RECOIL1,
	ACT_VM_RECOIL2,
	ACT_VM_RECOIL3,
	ACT_VM_PICKUP,
	ACT_VM_RELEASE,
};

forward GetPlayerMaxSpeed(client, &Float:MaxSpeed);
forward Action:SetPlayerAmmoCount(client, &ammo);

//Set ragdoll bone position,angle,velocity fit to client
native TranslateRagdoll(client, ragdoll);

//set physics can pick up by weapon_zm_carry
native SetPlayerCanPickup(physics, bool:pickup);

native bool:IsZMCarry(weapon);
native bool:IsCrossBow(weapon);
native bool:IsTVirus(weapon);
native bool:IsRPG(weapon);
native bool:IsNewtonLauncher(weapon);
native bool:IsHealthStation(weapon);
native bool:IsDeathStation(weapon);
native bool:IsTurret(weapon);

native SendWeaponAnim(weapon, Activity:anim);

// socket shop plugin
native Shop_GetViewModelIndex(client);
native bool:Shop_GetClientTag(client, String:tag[], maxlen);
native Shop_GetExtraHealth(client);
native Shop_GetExtraKnifeDamage(client);
native Shop_CreateCouponBox(client);

/**
 * Do not edit below this line!
 */
public Extension:__ext_troubletown = 
{
	name = "TroubleTown",
	file = "troubletown.ext",
	autoload = 1,
	#if defined REQUIRE_EXTENSIONS
		required = 1,
	#else
		required = 0,
	#endif
}