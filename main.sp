
#include<sourcemod>
#include<sdktools>
#include<sdkhooks>
#include<cstrike>
#include<troubletown>
#include<basecomm>

#include "troubletown/define.sp"
#include "troubletown/variable.sp"
#include "troubletown/database.sp"
#include "troubletown/dll_function.sp"

#include "troubletown/command.sp"
#include "troubletown/hook.sp"
#include "troubletown/menu.sp"
#include "troubletown/timer.sp"
#include "troubletown/offset.sp"
#include "troubletown/sendproxy.sp"
#include "troubletown/weapon.sp"

#include "troubletown/entity/item_nvgs.sp"
#include "troubletown/entity/ttt_health_station.sp"
#include "troubletown/entity/ttt_death_station.sp"
#include "troubletown/entity/weapon_disguise.sp"
#include "troubletown/entity/weapon_jihadbomb.sp"
#include "troubletown/entity/weapon_ttt_teleport.sp"
#include "troubletown/entity/weapon_ttt_tvirus.sp"
#include "troubletown/entity/weapon_ttt_wtester.sp"

#include "troubletown/maps/cs_office_sh.sp"
#include "troubletown/maps/ttt_anxiety.sp"
#include "troubletown/maps/ttt_minecraft_b4.sp"
#include "troubletown/maps/ttt_sola_night.sp"
#include "troubletown/maps/zm_roy_the_ship_64.sp"

#include "troubletown/tool.sp"

public Plugin:myinfo = 
{
	name = "Trouble in Terrorist Town",
	author = "TheLastRevenge",
	description = "Trouble in Terrorist Town in CS:S",
	url = "http://cafe.naver.com/cssttt"
}

public APLRes:AskPluginLoad2(Handle:myself, bool:late, String:error[], err_max)
{
   CreateNative("TTT_GetClientKarmaScore", Native_GetKarmaScore);
   return APLRes_Success;
}

public OnPluginStart()
{
	//load translation file
	LoadTranslations("ttt.phrases");
	
	//Log init
	BuildPath(Path_SM, g_log_path, PLATFORM_MAX_PATH, "logs/troubletown_log.log");
	
	//Initialize
	//Achievement_Init();
	//Title_Init();
	Hook_Initialize();
	Command_Initialize();
	Menu_Initialize();
	Offset_Initialize();
	for(new i=1; i<=MAXPLAYERS; i++)
	{
		g_damage_log[i][0] = CreateArray(128);
		g_damage_log[i][1] = CreateArray(128);
		g_attack_log[i][0] = CreateArray(128);
		g_attack_log[i][1] = CreateArray(128);
		g_traitor_kill_log[i] = CreateArray(128);
	}
	g_damage_all_log = CreateArray(128);
	
	//load dll function
	LoadOffset();
	LoadSignature();
	
	//connect to mysql
	ConnectSQL();
}

public OnPluginEnd()
{
	Hook_Shutdown();
	
	DisconnectSQL();
	
	for(new i=1; i<=MaxClients; i++)
	{
		CloseHandle(g_damage_log[i][0]);
		g_damage_log[i][0] = INVALID_HANDLE;
		CloseHandle(g_damage_log[i][1]);
		g_damage_log[i][1] = INVALID_HANDLE;
		CloseHandle(g_attack_log[i][0]);
		g_attack_log[i][0] = INVALID_HANDLE;
		CloseHandle(g_attack_log[i][1]);
		g_attack_log[i][1] = INVALID_HANDLE;
		if(!IsClientInGame(i)) continue;
		
		PlayerHook_Shutdown(i);
	}
	CloseHandle(g_damage_all_log);
	g_damage_all_log = INVALID_HANDLE;
	
	CloseTimer(g_ReadyTimer);
	g_ReadyTimer = INVALID_HANDLE;
}

public OnMapStart()
{
	//Download and Precache All Files
	AddFolderToDownloadsTable("sound");
	AddFolderToDownloadsTable("materials");
	AddFolderToDownloadsTable("models");
	
	PrecacheSound("items/medshot4.wav", true);
	PrecacheSound("items/medshotno1.wav", true);
	PrecacheSound("weapons/slam/throw.wav", true);
	PrecacheSound("buttons/blip2.wav", true);
	PrecacheSound("ambient/levels/labs/electric_explosion4.wav", true);
	PrecacheSound("ambient/levels/labs/electric_explosion2.wav", true);
	PrecacheSound("weapons/crossbow/fire1.wav", true);
	PrecacheSound("weapons/crossbow/hit1.wav", true);
	PrecacheSound("weapons/crossbow/bolt_fly4.wav", true);
	PrecacheSound("vo/npc/male01/overhere01.wav", true);
	PrecacheSound("weapons/sniper/sniper_zoomin.wav", true);
	PrecacheSound("weapons/sniper/sniper_zoomout.wav", true);
	PrecacheSound("troubletown/biohazard_detected.wav", true);
	
	for(new i=0; i<sizeof(g_deathsound_list); i++)
	{
		PrecacheSound(g_deathsound_list[i], true);
	}
	
	AdjustSpawnPoint();
	RemoveUselessEntity();
	
	Weapon_Initialize();
	SendProxy_OnMapStart();
	TTT_ExecuteMapFunction("MapStart");
}

public OnMapEnd()
{
	Weapon_Shutdown();
}

public OnConfigsExecuted()
{
	TTT_SetConfiguration();
}

public OnClientConnected(Client)
{
	ResetPlayerData(Client);
	TTT_ResetPlayerData(Client);
}

public OnClientAuthorized(Client, const String:SteamID[])
{
	//db not loaded
	if(databasehandle == INVALID_HANDLE)
		return;
	
	//bot don't need to load data
	if(StrEqual(SteamID, "BOT") == true)
		return;
	
	Player_DataLoad(Client, SteamID);
	
	if(IsClientInGame(Client))
	{
		PlayerHook_Initialize(Client);
	}
}

public OnClientPutInServer(Client)
{
	if(PlayerData[Client][DataLoaded] || IsFakeClient(Client))
	{
		PlayerHook_Initialize(Client);
		CheckKarmaScore(Client);
	}
	CreateTimer(0.2, g_HintTextTimer_Event, Client, TIMER_REPEAT);
}