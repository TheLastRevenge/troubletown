
public Menu_Initialize()
{
	for(new i=1; i<=MAXPLAYERS; i++)
	{
		g_fast_chat_Menu[i] = CreateMenu(g_FastChat_Handler, MENU_ACTIONS_ALL);
		SetMenuExitButton(g_fast_chat_Menu[i], true);
		SetMenuPagination(g_fast_chat_Menu[i], MENU_NO_PAGINATION);
		
		g_credit_shop_Menu[i] = CreateMenu(g_CreditShop_Handler);
		SetMenuExitButton(g_credit_shop_Menu[i], true);
		
		g_ragdoll_info_Menu[i] = CreateMenu(g_RagdollInfo_Handler);
		SetMenuExitButton(g_ragdoll_info_Menu[i], true);
		
		g_ragdoll_killlist_Menu[i] = CreateMenu(g_RagdollKillList_Handler);
		SetMenuExitButton(g_ragdoll_killlist_Menu[i], true);
		
		g_report_Menu[i] = CreateMenu(NULL_Handler);
		SetMenuExitButton(g_report_Menu[i], true);
		
		g_dnascan_Menu[i] = CreateMenu(g_DNAScan_Handler);
		SetMenuExitButton(g_dnascan_Menu[i], true);
		SetMenuPagination(g_dnascan_Menu[i], MENU_NO_PAGINATION);
		
		g_adminlist_Menu[i] = CreateMenu(g_AdminList_Handler);
		SetMenuExitButton(g_adminlist_Menu[i], true);
		
		g_disguise_nickname_Menu[i] = CreateMenu(g_Disguise_NickName_Handler);
		SetMenuExitButton(g_disguise_nickname_Menu[i], true);
	}
	g_disguise_Menu = CreateMenu(g_Disguise_Handler);
	SetMenuTitle(g_disguise_Menu, "Disguise Menu");
	AddMenuItem(g_disguise_Menu, "", "NickName");
	AddMenuItem(g_disguise_Menu, "", "Skin");
	SetMenuExitButton(g_disguise_Menu, true);
	
	g_disguise_skin_Menu = CreateMenu(g_Disguise_Skin_Handler);
	SetMenuTitle(g_disguise_skin_Menu, "Disguise Menu - Skin");
	for(new i; i<sizeof(g_skin_list); i++)
	{
		AddMenuItem(g_disguise_skin_Menu, "", g_skin_list[i][0]);
	}
	SetMenuExitButton(g_disguise_skin_Menu, true);
}

public ShowFastChat(Client)
{
	new Handle:menuhandle = g_fast_chat_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	SetGlobalTransTarget(Client);
	
	SetMenuTitle(menuhandle, "%t", "Quickchat keys");
	for(new i; i<9; i++)
	{
		AddMenuItemTranslated(menuhandle, g_fast_chat[i][0]);
	}
	SetMenuExitButton(menuhandle, true);
	DisplayMenu(menuhandle, Client, 1);
}

public ShowFastChat2(Client)
{
	
}

public ShowCreditShop(Client)
{
	new Handle:menuhandle = g_credit_shop_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	SetGlobalTransTarget(Client);
	
	SetMenuTitle(menuhandle, "-- %t --", "Credit Shop");
	TranslateCreditShop(menuhandle, "Body Armor", "Passive Equipment", "Armor Description", Client, bool:GetClientArmorValue(Client));
	TranslateCreditShop(menuhandle, "Health Station", "Active Equipment", "Health Station Description", Client);
	if(TTT_GetReaminCount(Knife)) {
		TranslateCreditShop(menuhandle, "Knife", "Weapon", "Knife Description", Client, !TTT_CanUseWeaponType(Client, true, WEAPONTYPE_KNIFE));
	} else {
		TranslateCreditShop_Block(menuhandle, "Knife", "Weapon", "Knife Description", "No Stock", Client);
	}
	TranslateCreditShop(menuhandle, "Speed Boots", "Active Equipment", "Speed Boots Description", Client, PlayerData[Client][HasSpeedBoots]);
	TranslateCreditShop(menuhandle, "Iron Shoes", "Passive Equipment", "Iron Shoes Description", Client, PlayerData[Client][HasIronShoes]);
	TranslateCreditShop(menuhandle, "Night Vision", "Active Equipment", "Night Vision Description", Client, PlayerData[Client][HasNightVision]);
	if(PlayerData[Client][Job] == detective)
	{
		if(TTT_GetReaminCount(GoldenGun) && TTT_GetClientRemainCount(Client, GoldenGun)) {
			TranslateCreditShop(menuhandle, "Golden Gun", "Weapon", "Golden Gun Description", Client, !TTT_CanUseWeaponType(Client, true, WEAPONTYPE_SECONDARY));
		} else {
			TranslateCreditShop_Block(menuhandle, "Golden Gun", "Weapon", "Golden Gun Description", "No Stock", Client);
		}
		TranslateCreditShop(menuhandle, "SG552", "Weapon", "SG552 Description", Client, !TTT_CanUseWeaponType(Client, false, WEAPONTYPE_PRIMARY));
		TranslateCreditShop(menuhandle, "Teleporter", "Weapon", "Teleporter Description", Client);
		TranslateCreditShop(menuhandle, "Crossbow", "Weapon", "Crossbow Description", Client, !TTT_CanUseWeaponType(Client, true, WEAPONTYPE_PRIMARY));
		TranslateCreditShop(menuhandle, "StunGun", "Weapon", "StunGun Description", Client, !TTT_CanUseWeaponType(Client, true, WEAPONTYPE_PRIMARY));
	}
	if(PlayerData[Client][Job] == traitor)
	{
		if(TTT_GetReaminCount(AWP)) {
			TranslateCreditShop(menuhandle, "AWP", "Weapon", "AWP Description", Client, !TTT_CanUseWeaponType(Client, false, WEAPONTYPE_PRIMARY));
		} else {
			TranslateCreditShop_Block(menuhandle, "AWP", "Weapon", "AWP Description", "No Stock", Client);
		}
		TranslateCreditShop(menuhandle, "Silenced Pistol", "Weapon", "Silenced Pistol Description", Client, !TTT_CanUseWeaponType(Client, true, WEAPONTYPE_SECONDARY));
		TranslateCreditShop(menuhandle, "Death Station", "Active Equipment", "Death Station Description", Client);
		if(TTT_GetReaminCount(SuperCrowbar)) {
			TranslateCreditShop(menuhandle, "Super Crowbar", "Weapon", "Super Crowbar Description", Client, !TTT_CanUseWeaponType(Client, true, WEAPONTYPE_KNIFE));
		} else {
			TranslateCreditShop_Block(menuhandle, "Super Crowbar", "Weapon", "Super Crowbar Description", "No Stock", Client);
		}
		TranslateCreditShop(menuhandle, "Teleporter", "Weapon", "Teleporter Description", Client);
		TranslateCreditShop(menuhandle, "Jihad", "Active Equipment", "Jihad Description", Client, !TTT_CanUseWeaponType(Client, true, WEAPONTYPE_C4));
		if(TTT_GetReaminCount(RPG)) {
			TranslateCreditShop(menuhandle, "RPG-7", "Weapon", "RPG-7 Description", Client, !TTT_CanUseWeaponType(Client, true, WEAPONTYPE_PRIMARY));
		} else {
			TranslateCreditShop_Block(menuhandle, "RPG-7", "Weapon", "RPG-7 Description", "No Stock", Client);
		}
		if(TTT_GetReaminCount(TVirus)) {
			TranslateCreditShop(menuhandle, "T-Virus", "Weapon", "T-Virus Description", Client, !TTT_CanUseWeaponType(Client, true, WEAPONTYPE_PRIMARY));
		} else {
			TranslateCreditShop_Block(menuhandle, "T-Virus", "Weapon", "T-Virus Description", "No Stock", Client);
		}
		TranslateCreditShop(menuhandle, "Turret", "Active Equipment", "Test Description", Client);
		TranslateCreditShop(menuhandle, "Flare Gun", "Weapon", "Flare Gun Description", Client, !TTT_CanUseWeaponType(Client, true, WEAPONTYPE_SECONDARY));
		TranslateCreditShop(menuhandle, "Newton Launcher", "Weapon", "Newton Launcher Description", Client, !TTT_CanUseWeaponType(Client, true, WEAPONTYPE_PRIMARY));
		TranslateCreditShop(menuhandle, "Disguise", "Weapon", "Disguise Description", Client, !TTT_CanUseWeaponType(Client, true, WEAPONTYPE_C4));
		if(TTT_GetReaminCount(HeadCrabLauncher)) {
			TranslateCreditShop(menuhandle, "HeadCrab Launcher", "Weapon", "HeadCrab Launcher Description", Client, !TTT_CanUseWeaponType(Client, true, WEAPONTYPE_SECONDARY));
		} else {
			TranslateCreditShop_Block(menuhandle, "HeadCrab Launcher", "Weapon", "HeadCrab Launcher Description", "No Stock", Client);
		}
	}
	DisplayMenu(menuhandle, Client, 10);
}

public ShowRagdollInfo(ragdoll, Client)
{
	new Handle:menuhandle = g_ragdoll_info_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	SetGlobalTransTarget(Client);
	
	new String:sMenuTitle[512];
	Format(sMenuTitle, sizeof(sMenuTitle), "-- TTT %t --", "Body Search Results");
	
	new String:ragdollName[32], String:jobName[16];
	TTT_GetRagdollPlayerName(ragdoll, ragdollName, sizeof(ragdollName));
	TTT_GetRagdollJobName(ragdoll, jobName, sizeof(jobName));
	Format(sMenuTitle, sizeof(sMenuTitle), "%s\n%t", sMenuTitle, "This is the body of person", ragdollName);
	Format(sMenuTitle, sizeof(sMenuTitle), "%s\n%t", sMenuTitle, "This person was a jobname", jobName);
	
	new time = RoundToZero(TTT_GetRagdollDeathTime(ragdoll));
	new second = time%60, minute = time/60;
	new String:sTime[8], String:sExplodedString[2][4];
	Format(sExplodedString[0], 4, "%s%d", (minute<10)?"0":"", minute);
	Format(sExplodedString[1], 4, "%s%d", (second<10)?"0":"", second);
	ImplodeStrings(sExplodedString, 2, ":", sTime, sizeof(sTime));
	Format(sMenuTitle, sizeof(sMenuTitle), "%s\n%t", sMenuTitle, "He died roughly before you conducted the search", sTime);
	
	new String:sReason[128], String:sReason2[128];
	TTT_GetRagdollReason(ragdoll, sReason, sizeof(sReason));
	Format(sMenuTitle, sizeof(sMenuTitle), "%s\n%t", sMenuTitle, sReason);
	if(TTT_GetRagdollReason2(ragdoll, sReason2, sizeof(sReason2)))
	{
		new String:sWeaponName[32];
		TTT_GetRagdollWeaponName(ragdoll, sWeaponName, sizeof(sWeaponName));
		Format(sMenuTitle, sizeof(sMenuTitle), "%s\n%t", sMenuTitle, sReason2, sWeaponName);
	}
	
	if(TTT_RagdollIsHeadshot(ragdoll))
	{
		Format(sMenuTitle, sizeof(sMenuTitle), "%s\n%t", sMenuTitle, "The fatal wound was a headshot. No time to scream");
	}
	
	SetMenuTitle(menuhandle, sMenuTitle);
	AddMenuItem(menuhandle, "Close", "Close");
	
	new String:sCallDetective[32];
	Format(sCallDetective, sizeof(sCallDetective), "%t", "Call Detective");
	AddMenuItem(menuhandle, "Close", sCallDetective, ITEMDRAW_DISABLED);
	
	new credit = TTT_GetRagdollCredit(ragdoll), String:sRagdollNumber[16];
	Format(sRagdollNumber, sizeof(sRagdollNumber), "%i", EntIndexToEntRef(ragdoll));
	AddMenuItem(menuhandle, sRagdollNumber, "Get Remain Credit", credit ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);
	
	new String:sKillList[128];
	if(TTT_GetRagdollKillList(ragdoll, sKillList, sizeof(sKillList))) {
		Format(sKillList, sizeof(sKillList), "%t", sKillList);
		AddMenuItem(menuhandle, sRagdollNumber, sKillList);
	}
	DisplayMenu(menuhandle, Client, 15);
}

public ShowRagdollKillList(ragdoll, Client)
{
	new Handle:menuhandle = g_ragdoll_killlist_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	SetGlobalTransTarget(Client);
	
	new String:sKillList[128];
	TTT_GetRagdollKillList(ragdoll, sKillList, sizeof(sKillList));
	SetMenuTitle(menuhandle, "%t", sKillList);
	
	new Handle:h_ragdoll_trie = TTT_GetRagdollKillListTrie(ragdoll);
	if(h_ragdoll_trie != INVALID_HANDLE)
	{
		new String:s_number[8], String:sName[32];
		for(new i; i<GetTrieSize(h_ragdoll_trie); i++)
		{
			IntToString(i, s_number, sizeof(s_number));
			GetTrieString(h_ragdoll_trie, s_number, sName, sizeof(sName));
			AddMenuItem(menuhandle, NULL_STRING, sName, ITEMDRAW_DISABLED);
		}
	}
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

public ShowAdminList(Client)
{
	new Handle:menuhandle = g_adminlist_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	
	SetMenuTitle(menuhandle, "Admin List");
	new String:s_number[4], String:s_name[32];
	for(new i=1; i<=MaxClients; i++)
	{
		if(IsClientInGame(i) && GetUserAdmin(i) != INVALID_ADMIN_ID)
		{
			IntToString(i, s_number, sizeof(s_number));
			GetClientName(i, s_name, sizeof(s_name));
			AddMenuItem(menuhandle, s_number, s_name);
		}
	}
	if(GetMenuItemCount(menuhandle))
	{
		SetMenuExitButton(menuhandle, true);
		DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
	}
	else
	{
		PrintToChat(Client, "\x05No admin found");
	}
}

public ShowAttackReport(Client)
{
	new Handle:hArray = (RoundType == RoundState_GamePlay) ? g_attack_log[Client][1] : g_attack_log[Client][0];
	new len = GetArraySize(hArray);
	if(len == 0) // don't have any log
	{
		return;
	}
	
	new Handle:menuhandle = g_report_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	SetMenuTitle(menuhandle, "Attack Report");
	
	new String:buffer[128];
	for(new i; i<len; i++)
	{
		GetArrayString(hArray, i, buffer, sizeof(buffer));
		AddMenuItem(menuhandle, "", buffer, ITEMDRAW_DISABLED);
	}
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

public ShowDamageReport(Client)
{
	new Handle:hArray = (RoundType == RoundState_GamePlay) ? g_damage_log[Client][1] : g_damage_log[Client][0];
	new len = GetArraySize(hArray);
	if(len == 0) // don't have any log
	{
		return;
	}
	
	new Handle:menuhandle = g_report_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	SetMenuTitle(menuhandle, "Damage Report");
	
	new String:buffer[128];
	for(new i; i<len; i++)
	{
		GetArrayString(hArray, i, buffer, sizeof(buffer));
		AddMenuItem(menuhandle, "", buffer, ITEMDRAW_DISABLED);
	}
	DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
}

public NULL_Handler(Handle:menu, MenuAction:action, Client, select)
{
}

public g_FastChat_Handler(Handle:menu, MenuAction:action, Client, select)
{
	new bool:bExitMenu = false;
	if(action == MenuAction_Select)
	{
		bExitMenu = true;
		if(IsPlayerAlive(Client))
		{
			//new bool:useSound = PlayerData[Client][SpecialFlag] & PLAYER_FLAG_DEVELOPER || PlayerData[Client][SpecialFlag] & PLAYER_FLAG_TESTER;
			if(select < 8) {
				TTT_FastChat(Client, select);
			}
			else {
				ShowFastChat2(Client);
			}
		}
		CancelClientMenu(Client);
	}
	if(action == MenuAction_DisplayItem)
	{
		SetGlobalTransTarget(Client);
		if(3 <= select <= 7)
		{
			new entity = TTT_GetEntityFromAim(Client, true);
			if(IsValidClient(entity))
			{
				PlayerData[Client][playerFastChat] = entity;
			}
			entity = PlayerData[Client][playerFastChat];
			
			new String:buffer[128];
			Format(buffer, sizeof(buffer), "%t", g_fast_chat[select][0]);
			if(IsValidClient(entity) && IsClientInGame(entity))
			{
				new String:sName[32];
				if(PlayerData[entity][bExecuteDisguise])
				{
					if(strlen(PlayerData[entity][sDisguisedName]))
					{
						Format(sName, sizeof(sName), PlayerData[entity][sDisguisedName]);
						if(GetDistanceEntityTo(Client, entity) > 500.0) Format(buffer, sizeof(buffer), "%t", g_fast_chat[select][2], sName);
						else Format(buffer, sizeof(buffer), "%t", g_fast_chat[select][1], sName);
					}
				}
				else
				{
					GetClientName(entity, sName, sizeof(sName));
					if(GetDistanceEntityTo(Client, entity) > 500.0) Format(buffer, sizeof(buffer), "%t", g_fast_chat[select][2], sName);
					else Format(buffer, sizeof(buffer), "%t", g_fast_chat[select][1], sName);
				}
			}
			if(g_survivor_count[0] == 1 && g_survivor_count[1] == 1 && PlayerData[Client][Job] == traitor)
			{
				Format(buffer, sizeof(buffer), "%t", g_fast_chat[select][3]);
			}
			RedrawMenuItem(buffer);
		}
	}
	if((action == MenuAction_Cancel && select == MenuCancel_Exit) || bExitMenu)
	{
		if(!IsValidClient(Client)){
			return;
		}
		
		if(g_FastChatTimer[Client] != INVALID_HANDLE)
		{
			KillTimer(g_FastChatTimer[Client]);
		}
		g_FastChatTimer[Client] = INVALID_HANDLE;
		PlayerData[Client][openFastChat] = false;
		PlayerData[Client][playerFastChat] = 0;
		ClientCommand(Client, "slot10");
	}
}
public g_CreditShop_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		if(RoundType != RoundState_GamePlay || !IsPlayerAlive(Client)) {
			return;
		}
		new String:info[32];
		GetMenuItem(menu, select, info, sizeof(info));
		TTT_BuyItem(Client, info);
	}
}
public g_RagdollInfo_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		if(RoundType != RoundState_GamePlay) {
			return;
		}
		new String:info[32];
		GetMenuItem(menu, select, info, sizeof(info));
		
		new entity = EntRefToEntIndex(StringToInt(info));
		if(TTT_GetRagdollTrie(entity) == INVALID_HANDLE) return;
		
		SetGlobalTransTarget(Client);
		
		if(select == 2)
		{
			if(!IsPlayerAlive(Client)) {
				return;
			}
			
			new credit = TTT_GetRagdollCredit(entity);
			TTT_SetRagdollCredit(entity, 0);
			ShowRagdollInfo(entity, Client);
			
			if(!credit) {
				return;
			}
			
			PlayerData[Client][Credit] += credit;
			PrintToChat(Client, "\x07C53AC5%t", "found credit", credit, "\x07FFFFFF");
		}
		if(select == 3)
		{
			ShowRagdollKillList(entity, Client);
		}
	}
}

public g_RagdollKillList_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
	}
}

public g_DNAScan_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		if(RoundType != RoundState_GamePlay || !IsPlayerAlive(Client)) {
			return;
		}
		
		DNAScan(Client, select);
	}
}

public g_AdminList_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		if(RoundType == RoundState_GamePlay && IsPlayerAlive(Client))
		{
			PrintToChat(Client, "\x04[TTT] \x01- can't see player info after game started");
			return;
		}
		
		new String:s_number[3];
		GetMenuItem(menu, select, s_number, sizeof(s_number));
		new target = StringToInt(s_number);
		ServerCommand("pointshop_player_info %i %i", GetClientUserId(Client), GetClientUserId(target));
	}
}

public g_Disguise_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		if(RoundType != RoundState_GamePlay || !IsPlayerAlive(Client)) {
			return;
		}
		
		new weapons = GetClientWeaponIndex(Client);
		if(!TTT_IsDisguise(weapons)) {
			return;
		}
		
		if(select == 0)
		{
			new Handle:menuhandle = g_disguise_nickname_Menu[Client];
			RemoveAllMenuItems(menuhandle);
			
			SetMenuTitle(menuhandle, "Disguise Menu - Nickname");
			AddMenuItem(menuhandle, "", "Hide Nickname.");
			for(new i=1; i<MaxClients; i++)
			{
				if(!IsClientInGame(i) || GetClientTeam(i) < 2 || IsClientSourceTV(i) || IsPlayerAlive(i)) continue;
				
				new ragdoll = TTT_GetPlayerRagdoll(i);
				if(!IsPlayerAlive(i) && ragdoll == -1) continue;//not have own ragdoll
				
				if(!IsPlayerAlive(i) && ragdoll != -1 && TTT_RagdollSearched(ragdoll)) continue;//already searched
				
				if(Client == i) continue;//not need to hide to original nickname
				
				if(PlayerData[i][Job] == detective) continue;//not need to hide to detective nickname
				
				new String:sName[32];
				GetClientName(i, sName, sizeof(sName));
				AddMenuItem(menuhandle, sName, sName);
			}
			SetMenuExitButton(menuhandle, true);
			DisplayMenu(menuhandle, Client, 10);
		}
		if(select == 1)
		{
			DisplayMenu(g_disguise_skin_Menu, Client, MENU_TIME_FOREVER);
		}
	}
}

public g_Disguise_NickName_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		if(RoundType != RoundState_GamePlay || !IsPlayerAlive(Client)) {
			return;
		}
		
		new weapons = GetClientWeaponIndex(Client);
		if(!TTT_IsDisguise(weapons)) {
			return;
		}
		
		if(g_DisguiseTimer[Client] != INVALID_HANDLE || g_DisguiseTimer2[Client] != INVALID_HANDLE)
		{
			return;
		}
		
		new String:info[32];
		GetMenuItem(menu, select, info, sizeof(info));
		Format(PlayerData[Client][sDisguisePrepareName], 32, info);
		
		if(PlayerData[Client][bExecuteDisguise])
		{
			TTT_SetProgressBar(Client, GetGameTime(), 1);
			SetEntityMoveType(Client, MOVETYPE_NONE);
			SetEntityRenderMode(Client, RENDER_TRANSCOLOR);
			
			SDKHook(Client, SDKHook_PostThinkPost, Hook_CheckDisguise);
			SDKHook(Client, SDKHook_WeaponSwitch, Hook_BlockWeaponSwitch);
			SDKHook(Client, SDKHook_WeaponDrop, Hook_BlockWeaponDrop);
			g_DisguiseTimer[Client] = CreateTimer(DISGUISE_NICK_DELAY/2, g_DisguiseTimer_Event, Client);
			Disguise[weapons][flDelay] = DISGUISE_NICK_DELAY;
			Disguise[weapons][fStartTime] = GetEngineTime();
		}
		
		if(select) SayText2Message(Client, Client, "\x04[TTT Disguise] \x01Selected nickname \x03%s\x01.", info);
		else SayText2Message(Client, Client, "\x04[TTT Disguise] \x01Selected \x03'hide nickname'\x01.");
	}
}

public g_Disguise_Skin_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		if(RoundType != RoundState_GamePlay || !IsPlayerAlive(Client)) {
			return;
		}
		
		new weapons = GetClientWeaponIndex(Client);
		if(!TTT_IsDisguise(weapons)) {
			return;
		}
		
		PlayerData[Client][iDisguisedSkin] = select;
		SayText2Message(Client, Client, "\x04[TTT Disguise] \x01Selected skin \x03%s\x01.", g_skin_list[select][0]);
	}
}

public SearchPlayer_Handler(Handle:menu, MenuAction:action, Client, select)
{
	if(action == MenuAction_Select)
	{
		new String:info[8];
		GetMenuItem(menu, select, info, sizeof(info));
		new String:data[2][4];
		ExplodeString(info, " ", data, 2, 4);
		new target = StringToInt(data[0]);
		new type = StringToInt(data[1]);
		if(type == SEARCHTYPE_PLAYERINFO)
			ServerCommand("pointshop_player_info %i %i", GetClientUserId(Client), GetClientUserId(target));
		if(type == SEARCHTYPE_GIFTPOINT)
			ServerCommand("pointshop_give_point %i %i", GetClientUserId(Client), GetClientUserId(target));
		if(type == SEARCHTYPE_GIFTITEM)
			ServerCommand("pointshop_give_item %i %i", GetClientUserId(Client), GetClientUserId(target));
		if(type == SEARCHTYPE_SPECTATOR)
		{
			CommitSuicide(target, true);
			ChangeClientTeam(target, 1);
		}
		if(type == SEARCHTYPE_KARMA)
		{
			AddKarmaScore(Client, target);
			CheckKarmaScore(target);
		}
	}
	if(action == MenuAction_End)
	{
		CloseHandle(menu);
	}
}

stock TranslateCreditShop(Handle:menuhandle, const String:ItemName[], const String:ItemType[], const String:ItemDescription[], Client, bool:disable = false)
{
	new String:sItemInfo[256];
	Format(sItemInfo, sizeof(sItemInfo), "%t - %t\n%t", ItemName, ItemType, ItemDescription);
	while(1 <= GetMenuItemCount(menuhandle)%7 <= 2 || 4 <= GetMenuItemCount(menuhandle)%7 <= 5)
	{
		AddMenuItem(menuhandle, NULL_STRING, NULL_STRING, ITEMDRAW_SPACER);
	}
	AddMenuItem(menuhandle, ItemName, sItemInfo, (disable || !PlayerData[Client][Credit]) ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
}

stock TranslateCreditShop_Block(Handle:menuhandle, const String:ItemName[], const String:ItemType[], const String:ItemDescription[], const String:Reason[], Client)
{
	new String:sItemInfo[256];
	Format(sItemInfo, sizeof(sItemInfo), "%t - %t\n%t\n%t", ItemName, ItemType, ItemDescription, Reason);
	while(1 <= GetMenuItemCount(menuhandle)%7 <= 2 || 4 <= GetMenuItemCount(menuhandle)%7 <= 5)
	{
		AddMenuItem(menuhandle, NULL_STRING, NULL_STRING, ITEMDRAW_SPACER);
	}
	AddMenuItem(menuhandle, ItemName, sItemInfo, ITEMDRAW_DISABLED);
}

stock AddMenuItemTranslated(Handle:menuhandle, const String:words[], bool:disable = false)
{
	new String:sItemInfo[128];
	Format(sItemInfo, sizeof(sItemInfo), "%t", words);
	AddMenuItem(menuhandle, NULL_STRING, sItemInfo, disable ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
}

