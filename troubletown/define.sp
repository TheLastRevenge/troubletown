
#define ADMIN_STEAMID "STEAM_0:0:77456544"

#define PLAYER_FLAG_DEVELOPER (1<<0)
#define PLAYER_FLAG_TESTER (1<<0)

#define RAGDOLL_SEARCH_DIST 150.0

#define START_GAME_TIME 240
#define CAN_BUY_TIME 30

#define CROWBAR_KNOCKBACK 300.0

//Teleporter
#define TELEPORT_DELAY 0.8
#define TELEPORT_MAX_COUNT 8

#define TELEPORT_ATTACKDELAY 0.5

//Disguise
#define DISGUISE_ALL_DELAY 1.0
#define DISGUISE_NICK_DELAY 0.2

#define Score_Ban_Bully 2

#define TEAM_PLAYER 2
#define TEAM_ENEMY 3

#define EF_NODRAW (1<<5)

//Item Define
#define HatType		1
#define SkinType		2
#define EffectType	3
#define MaxType		3

#define MAX_ITEMS 44

//search type
#define SEARCHTYPE_PLAYERINFO	1
#define SEARCHTYPE_GIFTPOINT	2
#define SEARCHTYPE_GIFTITEM		3
#define SEARCHTYPE_SPECTATOR	4
#define SEARCHTYPE_KARMA			5

//will type
#define WILLTYPE_FASTCHAT										 0
#define WILLTYPE_FASTCHAT_PLAYER						1
#define WILLTYPE_FASTCHAT_LONGDISTANCE	2
#define WILLTYPE_CHAT													3

//hud msg
#define MSGTYPE_PLAYERINFO 1
#define MSGTYPE_NOTICE 2

//job info
#define unselected	0
#define innocent		1
#define traitor			2
#define detective		3
new const String:JobName[4][16] = {"unknown","innocent","traitor","detective"};
new const String:JobTag[4][8] = {"[U]", "[I]", "[T]", "[D]"};

//translate string
new const String:g_player_health[5][16] = {"Near Death", "Badly Wounded", "Wounded", "Hurt", "Healthy"};
new const String:g_fast_chat[9][4][128] = {
	{"Yes", "Yes", "Yes", "Yes"},
	{"No", "No", "No", "No"},
	{"Help", "Help", "Help", "Help"},
	{"I'm with someone", "I'm with person", "I see person", "I see you"},
	{"someone is a Traitor", "person is a Traitor", "person is a Traitor", "come out wherever you are"},
	{"someone acts suspicious", "person acts suspicious", "person acts suspicious", "someone acts suspicious"},
	{"someone is innocent", "person is innocent", "person is innocent", "someone is innocent"},
	{"Anyone still alive", "Anyone still alive", "Anyone still alive", "Where are you hiding"},
	{"I bought something", "I bought something", "I bought something", "I bought something"}
}

enum TTT_RoundState {
	RoundState_GameReady,
	RoundState_GamePlay,
	RoundState_GameEnd
};
new TTT_RoundState:RoundType;

enum _PlayerData {
	//TTT Data
	Job,
	Credit,
	Float:NextFastChatSound,
	
	bool:AllowDrop,
	bool:BuyDrop,
	h_Sprite_Player, // Reference
	hRagdoll, //Reference
	ExtraHealth,
	
	//socket
	vmIndex,
	
	String:sOriginalModel[128],
	iOriginalSkin,
	
	bool:openFastChat,
	playerFastChat,
	bool:blockFlash,
	bool:blockSwitch, // block to switch weapon when they are picked up
	keyValue,
	
	bool:HasIronShoes,
	bool:HasSpeedBoots,
	bool:HasNightVision,
	
	bool:bExecuteJihad,
	bool:bExecuteDisguise,
	String:sDisguisedName[32],
	String:sDisguisePrepareName[32],
	iDisguisedSkin,
	
	bool:bIsInfected,
	Float:flLastInfectTime,
	
	bool:bBlockFallDamage,
	
	Karma,
	String:RegisterCode[10],
	
	//check bariable
	bool:DataLoaded,
	SpecialFlag
};
new PlayerData[MAXPLAYERS+1][_PlayerData];

enum TELEPORT_DATA {
	Float:vecTeleportOrigin[3],
	Float:vecTeleportAngle[3],
	iRemainCount,
	bool:savedLocation,
	Float:fStartTime,
};
new Teleporter[2048][TELEPORT_DATA];

enum DISGUISE_DATA {
	iRemainEnergy,
	Float:fStartTime,
	Float:flDelay,
};
new Disguise[2048][DISGUISE_DATA];

enum _Offset {
	m_hMyWeapons,
	m_hViewModel,
	m_nModelIndex,
	m_iWorldModelIndex,
	m_fEffects,
	m_nSequence,
	m_flPlaybackRate,
	m_flCycle,
	m_hWeapon,
	m_hGroundEntity,
	m_CollisionGroup,
	m_vecPunchAngle,
};

new Offset[_Offset];

#define WEAPONTYPE_PRIMARY				0
#define WEAPONTYPE_SECONDARY		1
#define WEAPONTYPE_KNIFE					2
#define WEAPONTYPE_C4							4

enum _WeaponData {
	bool:IsSilencedPistol,
	bool:IsGoldenGun,
	bool:IsKnife,
	bool:IsSuperCrowbar,
	bool:IsFlareGun,
	bool:IsJihad,
	bool:IsDisguise,
	bool:IsHeadCrabLauncher,
	
	bool:IsTraitorWeapon,
	bool:IsDetectiveWeapon,
	bool:IsUsingViewmodel,
	bool:IsUsingSpecialSlot,
	bool:IsUsingSendProxy,
	
	String:szName[32],
};
new WeaponData[2048][_WeaponData];

enum RemainData {
	Knife,
	SuperCrowbar,
	GoldenGun,
	AWP,
	RPG,
	TVirus,
	HeadCrabLauncher,
};
new g_remain_data[RemainData];
new g_max_weapon_limit[MAXPLAYERS+1][RemainData];

new Handle:g_weapon_trie = INVALID_HANDLE;
new String:g_log_path[PLATFORM_MAX_PATH];

//Global Timer
new Handle:g_ReadyTimer = INVALID_HANDLE;
new Handle:g_GameTimer = INVALID_HANDLE;
//Player Timer
new Handle:g_FastChatTimer[MAXPLAYERS+1];
new Handle:g_BlindTimer[MAXPLAYERS+1];
new Handle:g_WillTimer[MAXPLAYERS+1];
new Handle:g_DNAScanTimer[MAXPLAYERS+1];
new Handle:g_TeleportTimer[MAXPLAYERS+1];
new Handle:g_TVirusTimer[MAXPLAYERS+1];
new Handle:g_DisguiseTimer[MAXPLAYERS+1];
new Handle:g_DisguiseTimer2[MAXPLAYERS+1];

//DamageLog 0 = current, 1 = old
new Handle:g_damage_log[MAXPLAYERS+1][2];
new Handle:g_attack_log[MAXPLAYERS+1][2];
new Handle:g_damage_all_log;
new Handle:g_traitor_kill_log[MAXPLAYERS+1];
new Handle:g_tester_list;

//Menu
new Handle:g_fast_chat_Menu[MAXPLAYERS+1];
new Handle:g_credit_shop_Menu[MAXPLAYERS+1];
new Handle:g_ragdoll_info_Menu[MAXPLAYERS+1];
new Handle:g_ragdoll_killlist_Menu[MAXPLAYERS+1];
new Handle:g_report_Menu[MAXPLAYERS+1];
new Handle:g_dnascan_Menu[MAXPLAYERS+1];
new Handle:g_adminlist_Menu[MAXPLAYERS+1];
new Handle:g_disguise_Menu;
new Handle:g_disguise_nickname_Menu[MAXPLAYERS+1];
new Handle:g_disguise_skin_Menu;

//DNA
new g_Entity_DNA[2048];

//specific mode
new bool:g_isJailMode;
new String:g_sJailCommand[200];
new bool:g_isOpendDoor;
new Handle:g_DoorTimer;

//Easter Egg
new g_survivor_count[2];
new Float:g_next_fastchat_sound;

//Death Sound
new const String:g_deathsound_list[][128] = {
	"vo/npc/male01/pain07.wav",
	"vo/npc/male01/pain08.wav",
	"vo/npc/male01/pain09.wav"
};

new const String:g_skin_list[34][3][128] = {
	{"TTT 기본 스킨(Guerilla)", "models/player/t_guerilla.mdl", "0"},
	{"TTT 기본 스킨(Leet)", "models/player/t_leet.mdl", "0"},
	{"TTT 기본 스킨(Pheonix)", "models/player/t_phoenix.mdl", "0"},
	{"TTT 기본 스킨(Arctic)", "models/player/t_arctic.mdl", "0"},
	{"보라돌이(Tinky Winky)", "models/troubletown/teletubbies_v2/tinkywinky.mdl", "3"},
	{"뚜비(Dipsy)", "models/troubletown/teletubbies_v2/dipsy.mdl", "4"},
	{"나나(Laa-Laa)", "models/troubletown/teletubbies_v2/laalaa.mdl", "5"},
	{"뽀(Po)", "models/troubletown/teletubbies_v2/po.mdl", "6"},
	{"추석 토끼(Easter Bunny)", "models/troubletown/techknow/easter/e-bunny_fix.mdl", "7"},
	{"레이센 우동게인 이나바(Reisen Udongein Inaba)", "models/troubletown/reisenudongeininaba/reisenudongeininaba_fix.mdl", "8"},
	{"치르노(Cirno)", "models/troubletown/cirno/cirno_v2.mdl", "11"},
	{"레밀리아 스칼렛(Remilia Scarlet)", "models/troubletown/remilia_scarlet/remilia_scarlet.mdl", "12"},
	{"플랑도르 스칼렛(Flandre Scarlet)", "models/troubletown/flandre_scarlet/flandre_scarlet.mdl", "13"},
	{"코치야 사나에(Kochiya Sanae)", "models/troubletown/kochiya_sanae/kochiya_sanae.mdl", "14"},
	{"코메이지 사토리(Komiji Satori)", "models/troubletown/komeiji_satori/komeiji_satori.mdl", "15"},
	{"콘파쿠 요우무(Konpaku Youmu)", "models/troubletown/konpaku_youmu/konpaku_youmu.mdl", "16"},
	{"페도베어(Pedo Bear)", "models/troubletown/bear/pedobear/pedobear.mdl", "18"},
	{"화이트베어(White Bear)", "models/troubletown/bear/whitebear/whitebear.mdl", "19"},
	{"블랙베어(Black Bear)", "models/troubletown/bear/blackbear/blackbear.mdl", "20"},
	{"스카이베어(Sky Bear)", "models/troubletown/bear/skybear/skybear.mdl", "21"},
	{"핑크베어(Pink Bear)", "models/troubletown/bear/pinkbear/pinkbear.mdl", "22"},
	{"미쿠(Miku)", "models/troubletown/miku/miku.mdl", "32"},
	{"분홍 미쿠(Spring Miku)", "models/troubletown/spring_miku/spring_miku.mdl", "33"},
	{"파랑 미쿠(Sea Miku)", "models/troubletown/sea_miku/sea_miku.mdl", "34"},
	{"이자요이 사쿠야(Izayoi Sakuya)", "models/troubletown/izayoi_sakuya/izayoi_sakuya.mdl", "35"},
	{"사이교우지 유유코(Saigyouji Yuyuko)", "models/troubletown/saigyouji_yuyuko/saigyouji_yuyuko_v2.mdl", "36"},
	{"우사미 렌코(Usami Renko)", "models/troubletown/usami_renko/usami_renko.mdl", "37"},
	{"앨리스 마가트로이드(Alice Margatroid)", "models/troubletown/alice_margatroid/alice_margatroid.mdl", "40"},
	{"호쥬 누에(Houjuu Nue)", "models/troubletown/houjuu_nue/houjuu_nue.mdl", "44"},
	{"하쿠레이 레이무(Hakurei Reimu)", "models/troubletown/hakurei_reimu/hakurei_reimu.mdl", "45"},
	{"키리사메 마리사(Kirisame Marisa)", "models/troubletown/kirisame_marisa/kirisame_marisa.mdl", "46"},
	{"넵튠(Neptune)", "models/troubletown/neptune/neptune.mdl", "64"},
	{"히나나이 텐시(Hinanawi Tenshi)", "models/troubletown/hinanawi_tenshi/hinanawi_tenshi.mdl", "65"},
	{"미스티아 로렐라이(Mystia Lorelei)", "models/troubletown/mystia_lorelei/mystia_lorelei.mdl", "66"}
};