
public Hook_Initialize()
{
	HookEvent("player_connect", Hook_PlayerConnect);
	HookEvent("player_disconnect", Hook_PlayerDisconnect);
	HookEvent("player_spawn", Hook_PlayerSpawn);
	HookEvent("player_death", Hook_PlayerDeath, EventHookMode_Pre);
	HookEvent("player_hurt", Hook_PlayerHurt);
	HookEvent("player_blind", Hook_PlayerBlind);
	HookEvent("player_changename", Hook_PlayerChangeName);
	HookEvent("round_start", Hook_RoundStart);
	HookEvent("round_end", Hook_RoundEnd);
	HookEvent("weapon_fire", Hook_WeaponFire);
	
	HookUserMessage(GetUserMessageId("TextMsg"), MsgHook_TextMsg, true);
	HookUserMessage(GetUserMessageId("SayText2"), MsgHook_SayText2, true);
	
	AddNormalSoundHook(NormalSoundHook);
}

public Hook_Shutdown()
{
	UnhookEvent("player_connect", Hook_PlayerConnect);
	UnhookEvent("player_disconnect", Hook_PlayerDisconnect);
	UnhookEvent("player_spawn", Hook_PlayerSpawn);
	UnhookEvent("player_death", Hook_PlayerDeath, EventHookMode_Pre);
	UnhookEvent("player_hurt", Hook_PlayerHurt);
	UnhookEvent("player_blind", Hook_PlayerBlind);
	UnhookEvent("player_changename", Hook_PlayerChangeName);
	UnhookEvent("round_start", Hook_RoundStart);
	UnhookEvent("round_end", Hook_RoundEnd);
	UnhookEvent("weapon_fire", Hook_WeaponFire);
	
	UnhookUserMessage(GetUserMessageId("TextMsg"), MsgHook_TextMsg, true);
	UnhookUserMessage(GetUserMessageId("SayText2"), MsgHook_SayText2, true);
	
	RemoveNormalSoundHook(NormalSoundHook);
}

public PlayerHook_Initialize(Client)
{
	SDKHook(Client, SDKHook_WeaponDrop, Hook_WeaponDrop);
	SDKHook(Client, SDKHook_TraceAttack, Hook_TraceAttack);
	SDKHook(Client, SDKHook_OnTakeDamage, Hook_OnTakeDamage);
	SDKHook(Client, SDKHook_PostThinkPost, Hook_PostThinkPost);
	SDKHook(Client, SDKHook_PostThinkPost, Hook_SetWeaponModel);
	SDKHook(Client, SDKHook_PostThink, Hook_SetWeaponFireratePre);
	SDKHook(Client, SDKHook_PostThinkPost, Hook_SetWeaponFirerate);
	SDKHook(Client, SDKHook_WeaponEquipPost, Hook_WeaponEquipPost);
	SDKHook(Client, SDKHook_WeaponCanUse, Hook_WeaponCanUse);
	SDKHook(Client, SDKHook_WeaponSwitch, Hook_WeaponSwitch);
	
	SendProxy_HookClient(Client);
}

public PlayerHook_Shutdown(Client)
{
	SDKUnhook(Client, SDKHook_WeaponDrop, Hook_WeaponDrop);
	SDKUnhook(Client, SDKHook_TraceAttack, Hook_TraceAttack);
	SDKUnhook(Client, SDKHook_OnTakeDamage, Hook_OnTakeDamage);
	SDKUnhook(Client, SDKHook_PostThinkPost, Hook_PostThinkPost);
	SDKUnhook(Client, SDKHook_PostThinkPost, Hook_SetWeaponModel);
	SDKUnhook(Client, SDKHook_PostThink, Hook_SetWeaponFireratePre);
	SDKUnhook(Client, SDKHook_PostThinkPost, Hook_SetWeaponFirerate);
	SDKUnhook(Client, SDKHook_WeaponEquipPost, Hook_WeaponEquipPost);
	SDKUnhook(Client, SDKHook_WeaponCanUse, Hook_WeaponCanUse);
	SDKUnhook(Client, SDKHook_WeaponSwitch, Hook_WeaponSwitch);
}

public Action:CS_OnBuyCommand(Client, const String:classname[])
{
	/*if(StrEqual(classname, "usp") || StrEqual(classname, "glock") || StrEqual(classname, "fiveseven") || StrEqual(classname, "p228") || StrEqual(classname, "elite") || StrEqual(classname, "deagle") || (RoundType == RoundState_GamePlay && (StrEqual(classname, "smokegrenade") || StrEqual(classname, "flashbang"))))
	{
		return Plugin_Continue;
	}*/
	return Plugin_Handled;
}

public Action:CS_OnGetWeaponPrice(Client, const String:classname[], &price)
{
	/*if(StrEqual(classname, "weapon_usp") || StrEqual(classname, "weapon_glock") || StrEqual(classname, "weapon_fiveseven") || StrEqual(classname, "weapon_p228") || StrEqual(classname, "weapon_elite") || StrEqual(classname, "weapon_deagle") || StrEqual(classname, "weapon_smokegrenade") || StrEqual(classname, "weapon_flashbang"))
	{
		price = 800;
		SetAllowDrop(Client, true, true);
	}
	else
	{
		price = 16001;
	}*/
	price = 16001;
	return Plugin_Changed;
}

public Action:OnPlayerRunCmd(Client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon, &subtype, &cmdnum, &tickcount, &seed, mouse[2])
{
	PlayerData[Client][keyValue] = buttons;
	if(IsPlayerAlive(Client) && buttons & IN_SPEED)
	{
		buttons &= ~IN_SPEED;
	}
	if(impulse == 100)
	{
		impulse = 0;
		PlayerData[Client][openFastChat] = true;
		PlayerData[Client][playerFastChat] = 0;
		CloseTimer(g_FastChatTimer[Client]);
		g_FastChatTimer[Client] = CreateTimer(10.0, g_FastChatTimer_Event, Client);
	}
	if(weapon)
	{
		Weapon_SetList(weapon);
	}
}

public GetPlayerMaxSpeed(Client, &Float:MaxSpeed)
{
	if(IsClientInGame(Client) && IsPlayerAlive(Client))
	{
		if(PlayerData[Client][HasSpeedBoots] && PlayerData[Client][keyValue] & IN_SPEED)
		{
			MaxSpeed *= 1.4;
		}
	}
}

public Action:SetPlayerAmmoCount(Client, &Ammo)
{
	if(RoundType == RoundState_GameReady || !Ammo) {
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public Action:NormalSoundHook(clients[64], &numClients, String:sample[PLATFORM_MAX_PATH], &entity, &channel, &Float:volume, &level, &pitch, &flags)
{
	if(StrEqual(sample, "items/itempickup.wav")) {
		return Plugin_Handled;
	}
	if(StrContains(sample, "items/nvg_") == 0) {
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

/****************************************************************************************************
 * Event Hook
****************************************************************************************************/

public Hook_PlayerConnect(Handle:event, const String:name[], bool:dontBroadcast)
{
	new String:Name[32], String:SteamID[20];
	GetEventString(event, "name", Name, sizeof(Name));
	GetEventString(event, "networkid", SteamID, sizeof(SteamID));
	
	new String:sEscapedName[65];
	SQL_EscapeString(databasehandle, Name, sEscapedName, sizeof(sEscapedName));
	SaveEventLog(0, "▲ %s(%s) connected.", sEscapedName, SteamID);
}

public Hook_PlayerDisconnect(Handle:event, const String:name[], bool:dontBroadcast)
{
	new Client = GetClientFromEvent(event);
	PlayerHook_Shutdown(Client);
	TTT_ResetPlayerData(Client);
	TTT_RoundEndCheck();
	TTT_ClearPlayerLog(Client);
	DropTester(Client);
	TTT_RemovePlayerZMCarry(Client);
	RemoveAllItems(Client);
	ResetPlayerData(Client);
	
	new String:Name[32], String:SteamID[20], String:sReason[32];
	GetEventString(event, "name", Name, sizeof(Name));
	GetEventString(event, "networkid", SteamID, sizeof(SteamID));
	GetEventString(event, "reason", sReason, sizeof(sReason));
	
	new String:sEscapedName[65];
	SQL_EscapeString(databasehandle, Name, sEscapedName, sizeof(sEscapedName));
	SaveEventLog(0, "▼ %s(%s) disconnected. - %s", sEscapedName, SteamID, sReason);
}

public Hook_PlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast)
{
	new Client = GetClientFromEvent(event);
	if(Client == 0 || GetClientTeam(Client) < 2)
		return;
	
	if(g_isJailMode && PlayerData[Client][Job] == detective) // jail mode
	{
		CS_SwitchTeam(Client, 2);
		return;
	}
	
	TTT_ExecuteMapFunction_ParameterInt("PlayerSpawn", Client);
	
	TTT_OnPlayerRespawned(Client);
}

public Action:Hook_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast)
{
	new Client = GetClientFromEvent(event);
	new Attacker = GetAttackerFromEvent(event);
	RemovePlayerRagdoll(Client);
	IncrementDeathCount(Client, -1);
	if(IsValidClient(Attacker))
	{
		new String:sName[32];
		GetClientName(Client, sName, sizeof(sName));
		PushArrayString(g_traitor_kill_log[Attacker], sName);
		IncrementFragCount(Attacker, 2-GetClientFrags(Attacker));
	}
	else
	{
		IncrementFragCount(Client, 1);
	}
	
	if(RoundType != RoundState_GameReady)
	{
		new String:clientModel[128];
		GetClientModel(Client, clientModel, sizeof(clientModel));
		new ragdoll = CreateEntityByName("prop_ragdoll");
		DispatchKeyValue(ragdoll, "model", clientModel);
		DispatchSpawn(ragdoll);
		TranslateRagdoll(Client, ragdoll);
		SDKHook(ragdoll, SDKHook_OnTakeDamage, Hook_RagdollTakeDamage);
		
		PlayerData[Client][hRagdoll] = EntIndexToEntRef(ragdoll);
		TTT_SetRagdollData(ragdoll, event);
		TTT_SetPlayerDeadMark(Client);
		TTT_CheckCreditAward(PlayerData[Attacker][Job], PlayerData[Client][Job]);
		TTT_EmitDeathSound(Client);
	}
	
	if(RoundType == RoundState_GamePlay)
	{
		// suicide, killed traitor don't increase traitor count
		if(Attacker != Client && PlayerData[Client][Job] != traitor)
		{
			AddTraitorCount(30);
		}
	}
	
	TTT_RemovePlayerZMCarry(Client);
	RemoveReference(PlayerData[Client][h_Sprite_Player]);
	NightVision_Remove(Client);
	TTT_RoundEndCheck();
	return Plugin_Handled; // hide player, can't know attacker.
}

public Hook_PlayerHurt(Handle:event, const String:name[], bool:dontBroadcast)
{
	new Client = GetClientFromEvent(event);
	new Attacker = GetAttackerFromEvent(event);
	new damage = GetEventInt(event, "dmg_health");
	
	new String:buffer[128];
	if(!IsValidClient(Attacker)) // not client
	{
		new String:classname[32];
		GetEdictClassname(Attacker, classname, sizeof(classname));
		Format(buffer, sizeof(buffer), "%s->%s%N : %d", classname, JobTag[PlayerData[Client][Job]], Client, damage);
	}
	else
	{
		new String:sWeaponName[32];
		GetEventString(event, "weapon", sWeaponName, sizeof(sWeaponName));
		Format(buffer, sizeof(buffer), "%s%N->%s%N : %d(%s)", JobTag[PlayerData[Attacker][Job]], Attacker, JobTag[PlayerData[Client][Job]], Client, damage, sWeaponName);
		if(StrEqual(sWeaponName, "ump45", false))
		{
			new Float:vecPunch[3];
			GetEntDataVector(Client, Offset[m_vecPunchAngle], vecPunch);
			vecPunch[0] = fclamp(vecPunch[0]+GetRandomFloat(-10.0, 10.0), -90.0, 90.0);
			vecPunch[1] = fclamp(vecPunch[1]+GetRandomFloat(-10.0, 10.0), -90.0, 90.0);
			SetEntDataVector(Client, Offset[m_vecPunchAngle], vecPunch);
		}
		if(StrEqual(sWeaponName, "syringe_bolt", false))
		{
			ActiveTVirus(Client);
		}
	}
	TTT_AddPlayerLog(Attacker, Client, buffer);
}

public Hook_PlayerBlind(Handle:event, const String:name[], bool:dontBroadcast)
{
	new Client = GetClientFromEvent(event);
	
	PlayerData[Client][blockFlash] = true;
	
	new Float:blindtime = GetEntPropFloat(Client, Prop_Send, "m_flFlashDuration");
	CloseTimer(g_BlindTimer[Client]);
	g_BlindTimer[Client] = CreateTimer(fmax(0.1, blindtime-0.1), g_BlindTimer_Event, Client);
}

public Hook_PlayerChangeName(Handle:event, const String:name[], bool:dontBroadcast)
{
	new String:OldName[256], String:NewName[256];
	GetEventString(event, "oldname", OldName, 256);
	GetEventString(event, "newname", NewName, 256);
	
	new Client = GetClientFromEvent(event);
	if(RoundType == RoundState_GamePlay && IsPlayerAlive(Client)) {
		KickClient(Client, "%T", "Dont change nickname", Client);
	} else {
		for(new i=1; i<=MaxClients; i++)
		{
			if(IsClientInGame(i) == false) continue;
			
			if(i == Client) PrintToChat(Client, "\x03Your nickname was changed. \x07C5C5C5(%s \x01-> \x07C5C5C5%s)", OldName, NewName);
			
			if(GetUserAdmin(i) != INVALID_ADMIN_ID) PrintToChat(i, "\x04%s \x01nickname was changed to \x04%s\x01.", OldName, NewName);
		}
	}
}

public Hook_RoundStart(Handle:event, const String:name[], bool:Broadcast)
{
	TTT_ResetGameData();
	TTT_ResetDeadMark();
	RemoveWeaponEntity();
	RoundType = RoundState_GameReady;
	SetReadyCount(20);
	
	for(new i=1; i<=MaxClients; i++)
	{
		TTT_ResetPlayerData(i);
		
		if(!IsClientInGame(i) || GetClientTeam(i) < 2 || !IsPlayerAlive(i)) continue;
		
		TTT_OnPlayerRespawned(i);
	}
	TTT_ExecuteMapFunction("RoundStart");
	
	CloseTimer(g_ReadyTimer);
	g_ReadyTimer = CreateTimer(1.0, g_ReadyTimer_Event,0,TIMER_REPEAT);
}

public Hook_RoundEnd(Handle:event, const String:name[], bool:Broadcast)
{
	ChangeSourceTVTeam(1);
	g_ReadyTimer = INVALID_HANDLE;
	g_GameTimer = INVALID_HANDLE;
	RoundType = RoundState_GameEnd;
	TTT_PrintAllLog();
	
	for(new i=1; i<=MaxClients; i++)
	{
		if(!IsClientInGame(i) || GetClientTeam(i) < 2) continue;
		
		if(GetClientTeam(i) == 3) CS_SwitchTeam(i, 2);
	}
}

public Hook_WeaponFire(Handle:event, const String:name[], bool:Broadcast)
{
	new Client = GetClientFromEvent(event);
	
	new weapons = GetClientWeaponIndex(Client);
	if(TTT_IsHeadCrabLauncher(weapons) && GetEntProp(weapons, Prop_Send, "m_iClip1") > 0)
	{
		new Float:vecEyePosition[3], Float:vecAngles[3], Float:vecForward[3];
		GetClientEyePosition(Client, vecEyePosition);
		GetClientEyeAngles(Client, vecAngles);
		GetAngleVectors(vecAngles, vecForward, NULL_VECTOR, NULL_VECTOR);
		TR_TraceRayFilter(vecEyePosition, vecAngles, MASK_SOLID, RayType_Infinite, TraceFilter_OneEntity, Client);
		
		if(TR_DidHit() == false)
		{
			PrintToChat(Client, "\x04[TTT Error]HeadCrabLauncher Fire Failed.");
			return;
		}
		
		new Float:vecEndPosition[3], Float:vecNormalVector[3], Float:vecNormalAngle[3];
		TR_GetEndPosition(vecEndPosition);
		TR_GetPlaneNormal(INVALID_HANDLE, vecNormalVector);
		GetVectorAngles(vecNormalVector, vecNormalAngle);
		
		ScaleVector(vecForward, -5000.0);
		AddVectors(vecEyePosition, vecForward, vecEyePosition);
		vecEyePosition[2] += 10000.0;
		new info_target = CreateEntityByName("info_target");
		TeleportEntity(info_target, vecEyePosition, NULL_VECTOR, NULL_VECTOR);
		DispatchKeyValue(info_target, "targetname", "headcrab_target");
		DispatchSpawn(info_target);
		
		new canister = CreateEntityByName("env_headcrabcanister");
		if(canister != -1)
		{
			TeleportEntity(canister, vecEndPosition, vecNormalAngle, NULL_VECTOR);
			
			DispatchKeyValue(canister, "LaunchPositionName", "headcrab_target");
			
			new String:s_HeadcrabType[4];
			Format(s_HeadcrabType, sizeof(s_HeadcrabType), "%i", GetRandomInt(0, 2));
			DispatchKeyValue(canister, "HeadcrabType", s_HeadcrabType);
			
			new String:s_HeadcrabCount[4];
			Format(s_HeadcrabCount, sizeof(s_HeadcrabCount), "%i", GetRandomInt(4, 10));
			DispatchKeyValue(canister, "HeadcrabCount", s_HeadcrabCount);
			
			new String:s_FlightSpeed[4];
			Format(s_FlightSpeed, sizeof(s_FlightSpeed), "%i", GetRandomInt(2500, 6000));
			DispatchKeyValue(canister, "FlightSpeed", s_FlightSpeed);
			
			new String:s_FlightTime[4];
			Format(s_FlightTime, sizeof(s_FlightTime), "%i", GetRandomInt(2, 5));
			DispatchKeyValue(canister, "FlightTime", s_FlightTime);
			
			new String:s_Damage[4];
			Format(s_Damage, sizeof(s_Damage), "%i", GetRandomInt(50, 90));
			DispatchKeyValue(canister, "Damage", s_Damage);
			
			new String:s_DamageRadius[4];
			Format(s_DamageRadius, sizeof(s_DamageRadius), "%i", GetRandomInt(300, 512));
			DispatchKeyValue(canister, "DamageRadius", s_DamageRadius);
			
			new String:s_SmokeLifetime[4];
			Format(s_SmokeLifetime, sizeof(s_SmokeLifetime), "%i", GetRandomInt(5, 10));
			DispatchKeyValue(canister, "SmokeLifetime", s_SmokeLifetime);
			
			DispatchKeyValue(canister, "spawnflags","8192");
			DispatchSpawn(canister);
			
			AcceptEntityInput(canister, "FireCanister", Client, Client);
			SetVariantString("OnImpacted !self:OpenCanister");
			AcceptEntityInput(canister, "AddOutput");
			
			SetOwnerEntity(canister, Client);
			
			new parent = CreateEntityByName("info_target");
			TeleportEntity(parent, vecEndPosition, NULL_VECTOR, NULL_VECTOR);
			DispatchSpawn(parent);
			
			new sprite = CreateSprite(vecEndPosition, "sprites/c4.vmt", 1.5, true, parent);
			SDKHook(sprite, SDKHook_SetTransmit, Hook_SetTransmit_TraitorSprite);
			SetOwnerEntity(sprite, Client);
			CreateTimer(StringToFloat(s_FlightTime), g_RemoveReference_Event, EntIndexToEntRef(parent), TIMER_FLAG_NO_MAPCHANGE);
		}
	}
}

/****************************************************************************************************
 * UserMessage Hook
****************************************************************************************************/

public Action:MsgHook_TextMsg(UserMsg:msg_id, Handle:bf, const players[], playersNum, bool:reliable, bool:init)
{
	decl String:buffer[256];
	
	buffer[0] = '\0';
	BfReadString(bf, buffer, sizeof(buffer), false);
	
	if(StrContains(buffer, "Game_teammate_attack") != -1)
	{
		return Plugin_Handled;
	}
	if(StrContains(buffer, "Killed_Teammate") != -1)
	{
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public Action:MsgHook_SayText2(UserMsg:msg_id, Handle:bf, const players[], playersNum, bool:reliable, bool:init)
{
	decl String:buffer[256], String:buffer2[256];
	
	buffer[0] = '\0';
	BfReadString(bf, buffer, sizeof(buffer), false);
	BfReadString(bf, buffer2, sizeof(buffer2), false);
	
	if(StrContains(buffer2, "#Cstrike_Name_Change") != -1)
	{
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

/****************************************************************************************************
 * SDK Hook
****************************************************************************************************/

public OnEntityCreated(entity, const String:classname[])
{
	TTT_ResetWeaponData(entity);
	if(StrEqual(classname, "predicted_viewmodel"))
	{
		SDKHook(entity, SDKHook_Spawn, Hook_SpawnViewModel);
	}
	if(StrEqual(classname, "cycler"))
	{
		SDKHook(entity, SDKHook_Spawn, Hook_SpawnNPC);
	}
}

//UpdateOnRemove
public OnEntityDestroyed(entity)
{
	if(entity < 0 || entity >= 2048)
	{
		return;
	}
	g_Entity_DNA[entity] = 0;
	TTT_ResetWeaponData(entity);
}

public Action:Hook_TraceAttack(Client, &attacker, &inflictor, &Float:damage, &damagetype, &ammotype, hitbox, hitgroup)
{
	if(damagetype & DMG_BLAST)
		return Plugin_Continue;
	
	//couldn't damaged player by shoot before game started.
	if(RoundType == RoundState_GameReady)
	{
		return Plugin_Handled;
	}
	
	new bool:changed = false;
	if(attacker > 0 && attacker <= MaxClients && IsClientInGame(attacker)) // attacker == player
	{
		new weapons = GetClientWeaponIndex(attacker);
		if(weapons != -1)
		{
			new String:classname[32];
			GetEdictClassname(weapons, classname, sizeof(classname));
			if(StrEqual(classname, "weapon_knife") && damage > 50) // crowbar knockback
			{
				new Float:vecOrigin[3], Float:vecTargetOrigin[3], Float:vector[3];
				GetClientAbsOrigin(attacker, vecOrigin);
				GetClientAbsOrigin(Client, vecTargetOrigin);
				MakeVectorFromPoints(vecOrigin, vecTargetOrigin, vector);
				NormalizeVector(vector, vector);
				ScaleVector(vector, CROWBAR_KNOCKBACK*(WeaponData[weapons][IsSuperCrowbar] ? 4 : 1));
				TeleportEntity(Client, NULL_VECTOR, NULL_VECTOR, vector);
				return Plugin_Handled;
			}
			if(StrEqual(classname, "weapon_glock") && GetEntProp(weapons, Prop_Send, "m_bBurstMode")) // reduce glock burst head damage
			{
				if(hitgroup == 1)
				{
					damage *= 0.6;
					changed = true;
				}
			}
			if(StrEqual(classname, "weapon_ump45")) // increase stungun head damage
			{
				if(hitgroup == 1)
				{
					damage *= 1.5;
					changed = true;
				}
			}
			if(IsNewtonLauncher(weapons)) // newton launcher knockback
			{
				if(hitgroup == 1)
				{
					damage *= 0.5;
					changed = true;
				}
			}
		}
	}
	
	if(changed) return Plugin_Changed;
	return Plugin_Continue;
}

public Action:Hook_OnTakeDamage(Client, &attacker, &inflictor, &Float:damage, &damagetype, &weapon, Float:damageForce[3], Float:damagePosition[3], damagecustom)
{
	if(damagetype & DMG_BLAST)
		return Plugin_Continue;
	
	if(damagetype & DMG_BURN)
	{
		new entityflame = GetEntPropEnt(Client, Prop_Data, "m_hEffectEntity");
		if(IsValidEdict(entityflame))
		{
			attacker = GetOwnerEntity(entityflame);
		}
		damage *= 2.0;
		return Plugin_Changed;
	}
	
	new bool:changed = false;
	if(attacker > 0 && attacker <= MaxClients && IsClientInGame(attacker)) // attacker == player
	{
		if(GetClientTeam(attacker) == GetClientTeam(Client))
		{
			damage *= 1.65; // multiplier for same team.
		}
		
		new weapons = GetClientWeaponIndex(attacker);
		if(weapons != -1)
		{
			new String:classname[32];
			GetEdictClassname(weapons, classname, sizeof(classname));
			if(StrEqual(classname, "weapon_awp")) // increase awp damage
			{
				damage *= 1.45;
			}
			if(StrEqual(classname, "weapon_ump45")) // reduce stungun damage
			{
				damage *= 0.5;
			}
			if(StrEqual(classname, "weapon_elite")) // reduce elite damage
			{
				damage *= 0.45;
			}
			if(StrEqual(classname, "weapon_glock")) // increase glock damage
			{
				damage *= 1.1;
			}
			if(StrEqual(classname, "weapon_usp")) // reduce usp damage
			{
				damage *= 0.7;
				if(WeaponData[weapons][IsSilencedPistol]) {
					damage *= 1.1;
				}
			}
			if(StrEqual(classname, "weapon_p228")) // reduce p228 damage
			{
				damage *= 0.5;
				damage *= (GetDistanceEntityTo(attacker, Client)  / 1024) * 0.375 + 0.75; // damage reduce 0.75~distance
			}
			if(StrEqual(classname, "weapon_deagle")) // reduce deagle damage
			{
				damage *= 0.75;
				if(WeaponData[weapons][IsGoldenGun])
				{
					new cweapon = GetClientWeaponIndex(Client);
					if(PlayerData[Client][Job] == traitor && (GetArraySize(g_traitor_kill_log[Client]) || (IsWeapon(cweapon) && WeaponData[cweapon][IsTraitorWeapon]))) {
						damage = 999.0;
					} else {
						damage = 0.0;
					}
				}
				if(WeaponData[weapons][IsFlareGun])
				{
					damage = 0.0;
					IgniteEntity(Client, 5.0, false, 10.0, true);
					new entityflame = GetEntPropEnt(Client, Prop_Data, "m_hEffectEntity");
					if(IsValidEdict(entityflame)) SetOwnerEntity(entityflame, attacker);
				}
			}
			if(StrEqual(classname, "weapon_knife")) // set knife damage
			{
				damage = 57.142857; // set damage to 20
				if(WeaponData[weapons][IsKnife])
				{
					damage *= 10.0;
				}
				if(WeaponData[weapons][IsSuperCrowbar])
				{
					damage *= 2.0;
				}
				//damage += float(Shop_GetExtraKnifeDamage(attacker))*2.857143;
			}
			if(IsNewtonLauncher(weapons)) // newton launcher knockback
			{
				new Float:vecOrigin[3], Float:vecTargetOrigin[3], Float:vector[3];
				GetClientAbsOrigin(attacker, vecOrigin);
				GetClientEyePosition(Client, vecTargetOrigin);
				MakeVectorFromPoints(vecOrigin, vecTargetOrigin, vector);
				NormalizeVector(vector, vector);
				ScaleVector(vector, CROWBAR_KNOCKBACK*1.5);
				if(vector[2] > 0.0)
				{
					vector[2] += 30.0;
					if(GetGroundEntity(Client) != -1)
					{
						vector[2] += 150.0;
					}
				}
				TeleportEntity(Client, NULL_VECTOR, NULL_VECTOR, vector);
				damage /= 20.0;
				if(damage < 7.0) damage = 7.0;
			}
			
			new victim_weapons = GetClientWeaponIndex(Client);
			if(victim_weapons != -1 && WeaponData[victim_weapons][IsKnife])
			{
				damage *= 1.6;
			}
		}
		changed = true;
	}
	
	if(attacker == 0) // world
	{
		if(PlayerData[Client][HasIronShoes]) // player has 'iron shoes'
		{
			damage *= 0.3; // reduce 70% of damage
			changed = true;
		}
		if(PlayerData[Client][bBlockFallDamage]) // block fall damage
		{
			damage = 0.0;
			changed = true;
		}
	}
	
	if(IsValidEdict(attacker))
	{
		new String:classname[32];
		GetEdictClassname(attacker, classname, sizeof(classname));
		if(StrEqual(classname, "npc_turret_floor"))
		{
			new String:sUserId[8];
			GetTargetName(attacker, sUserId, sizeof(sUserId));
			new owner = GetClientOfUserId(StringToInt(sUserId));
			if(IsValidClient(owner) && IsClientInGame(owner)) {
				attacker = owner;
			}
			damage = PlayerData[Client][Job] != traitor ? GetConVarFloat(FindConVar("sk_npc_turret_floor_bullet_damage")) : 0.0;
			changed = true;
		}
		if(StrContains(classname, "headcrab") != -1)
		{
			new owner = GetOwnerEntity(attacker);
			if(IsValidClient(owner) && IsClientInGame(owner)) {
				attacker = owner;
			}
			changed = true;
		}
		if(StrEqual(classname, "env_headcrabcanister"))
		{
			new owner = GetOwnerEntity(attacker);
			if(IsValidClient(owner) && IsClientInGame(owner)) {
				attacker = owner;
			}
			changed = true;
		}
	}
	
	if(changed) return Plugin_Changed;
	return Plugin_Continue;
}

public Action:Hook_RagdollTakeDamage(ragdoll, &attacker, &inflictor, &Float:damage, &damagetype, &weapon, Float:damageForce[3], Float:damagePosition[3], damagecustom)
{
	if(damagetype & DMG_BLAST)
		return Plugin_Continue;
	
	if(attacker > 0 && attacker <= MaxClients && IsClientInGame(attacker)) // attacker == player
	{
		new weapons = GetClientWeaponIndex(attacker);
		if(IsSecondaryWeapon(weapons) && WeaponData[weapons][IsFlareGun])
		{
			IgniteEntity(ragdoll, 3.0, false, 5.0, true);
			CreateTimer(3.0, g_TeleportRagdoll_Event, EntIndexToEntRef(ragdoll), TIMER_FLAG_NO_MAPCHANGE);
			SDKUnhook(ragdoll, SDKHook_OnTakeDamage, Hook_RagdollTakeDamage);
		}
	}
	return Plugin_Handled;
}

public Hook_PostThinkPost(Client)
{
	new entity = TTT_GetEntityFromAim(Client);
	if(entity != -1)
	{
		new String:sHudMsg[255];
		if(IsRagdollEntity(entity))
		{
			if(TTT_RagdollSearched(entity))
			{
				new String:ragdollName[32];
				TTT_GetRagdollPlayerName(entity, ragdollName, sizeof(ragdollName));
				Format(sHudMsg, sizeof(sHudMsg), "%T", "person's corpse", Client, ragdollName);
			}
			else
			{
				Format(sHudMsg, sizeof(sHudMsg), "%T", "unidentified body", Client);
			}
		}
		else if(IsHealthStationEntity(entity)) // health station
		{
			HealthStation_ShowHud(entity, Client);
		}
		else if(IsDeathStationEntity(entity)) // fake health station
		{
			DeathStation_ShowHud(entity, Client);
		}
		else if(IsValidClient(entity)) // player
		{
			new health = GetClientHealth(entity) -1;
			if(health >= 100) health = 99;
			new String:sName[32];
			if(PlayerData[entity][bExecuteDisguise])
			{
				if(strlen(PlayerData[entity][sDisguisedName])) Format(sName, sizeof(sName), PlayerData[entity][sDisguisedName]);
			}
			else GetClientName(entity, sName, sizeof(sName));
			
			if(strlen(sName)) Format(sHudMsg, sizeof(sHudMsg), "%s\n%T", sName, g_player_health[health/20], Client);
		}
		new weapons = GetClientWeaponIndex(Client);
		if(IsWeapon(weapons) && TTT_IsTester(weapons))
		{
			if((IsRagdollEntity(entity) && TTT_GetRagdollDNA(entity)) || g_Entity_DNA[entity])
			{
				Format(sHudMsg, sizeof(sHudMsg), "%s\nThis entity have DNA", sHudMsg);
			}
		}
		if(strlen(sHudMsg)) HudMsg(Client, MSGTYPE_PLAYERINFO, -1.0, 0.55, {255, 255, 255, 255}, {255, 255, 255, 255}, 2, 0.01, 1.0, 1.0, 0.0, sHudMsg);
	}
	
	static delay3Times[MAXPLAYERS+1];
	if(PlayerData[Client][openFastChat])
	{
		if(delay3Times[Client]%3 == 0) {
			ShowFastChat(Client); // too fast -> player disconnect
		}
	}
	
	if(PlayerData[Client][bIsInfected])
	{
		if(IsPlayerAlive(Client) && g_TVirusTimer[Client] != INVALID_HANDLE)
		{
			new String:szMsg[200]; 
			Format(szMsg, sizeof(szMsg), "%T", "progress zombie", Client);
			
			new Handle:kv = CreateKeyValues("data");
			KvSetString(kv, "msg", szMsg);
			KvSetNum(kv, "total", 300); 
			KvSetNum(kv, "current", RoundToZero(FloatSub(GetGameTime(), PlayerData[Client][flLastInfectTime])*100));  
			ShowVGUIPanel(Client, "nav_progress", kv, true);
			CloseHandle(kv);
		}
		else
		{
			PlayerData[Client][bIsInfected] = false;
			ShowVGUIPanel(Client, "nav_progress", INVALID_HANDLE, false);
		}
	}
	
	static OldButtons[MAXPLAYERS+1];
	new buttons = GetClientButtons(Client);
	if(buttons & IN_USE && !(OldButtons[Client] & IN_USE))
	{
		entity = TTT_GetEntityFromAim(Client, true);
		if(IsRagdollEntity(entity)) TTT_RagdollSearch(entity, Client);
	}
	if(buttons & IN_ATTACK && !(OldButtons[Client] & IN_ATTACK))
	{
		new weapons = GetClientWeaponIndex(Client);
		PerformJihad(Client, weapons);
		if(IsValidEdict(weapons) && IsValidEntity(weapons))
		{
			if(IsTurret(weapons))
			{
				new Float:vecEyePosition[3], Float:vecAngles[3];
				GetClientEyePosition(Client, vecEyePosition);
				GetClientEyeAngles(Client, vecAngles);
				
				TR_TraceRayFilter(vecEyePosition, vecAngles, MASK_SOLID, RayType_Infinite, TraceFilter_OneEntity, Client);
				
				
				if(TR_DidHit())
				{
					new Float:vecResultPosition[3];
					TR_GetEndPosition(vecResultPosition);
					if(GetVectorDistance(vecEyePosition, vecResultPosition) < 250.0)
					{
						ServerCommand("spawn_turret %i %f %f %f %f %f %f", GetClientUserId(Client), vecResultPosition[0], vecResultPosition[1], vecResultPosition[2], 0.0, vecAngles[1], 0.0);
						if(IsValidEdict(weapons))
						{
							RemovePlayerItem(Client, weapons);
							RemoveEdict(weapons);
						}
					}
					else
					{
						PrintToChat(Client, "\x03Too far from spawing point!");
					}
				}
			}
			if(IsHealthStation(weapons))
			{
				SpawnHealthStation(Client);
				RemovePlayerItem(Client, weapons);
				RemoveEdict(weapons);
			}
			if(IsDeathStation(weapons))
			{
				SpawnDeathStation(Client);
				RemovePlayerItem(Client, weapons);
				RemoveEdict(weapons);
			}
		}
	}
	if(buttons & IN_ATTACK2 && !(OldButtons[Client] & IN_ATTACK2))
	{
		new weapons = GetClientWeaponIndex(Client);
		PerformJihad(Client, weapons, true);
		/*if(IsKnifeWeapon(weapons) && WeaponData[weapons][IsKnife])
		{
			new entity = CreateEntityByName("ttt_knife_proj")
			DispatchSpawn(entity);
			
			RemovePlayerItem(Client, weapons);
			RemoveEdict(weapons);
		}*/
	}
	PlayerRunCmd_HealthStation(Client, buttons);
	PlayerRunCmd_DeathStation(Client, buttons);
	PlayerRunCmd_DNAScanner(Client, buttons, OldButtons[Client]);
	PlayerRunCmd_Teleporter(Client, buttons, OldButtons[Client]);
	PlayerRunCmd_Disguise(Client, buttons, OldButtons[Client]);
	OldButtons[Client] = buttons;
	
	delay3Times[Client]++;
	
	//dead player can't execute after this line
	if(!IsPlayerAlive(Client)) return;
	
	if(TTT_CanPlayerBuy()) {
		SetEntProp(Client, Prop_Send, "m_bInBuyZone", true);
	}
}

public Hook_SpawnViewModel(entity)
{
	new owner = GetEntPropEnt(entity, Prop_Send, "m_hOwner");
	if(!IsValidClient(owner) || GetEntProp(entity, Prop_Send, "m_nViewModelIndex")) {
		return;
	}
	
	SDKHook(entity, SDKHook_SetTransmit, Hook_SetTransmit_ViewModel);
}

public Hook_SpawnNPC(entity)
{
	if(IsValidEdict(entity) == false) return;
	
	new String:classname[32];
	GetEdictClassname(entity, classname, sizeof(classname));
	if(StrContains(classname, "headcrab") != -1)
	{
		new canister = GetOwnerEntity(entity);
		if(IsValidEdict(canister))
		{
			SetOwnerEntity(entity, GetOwnerEntity(canister));
		}
	}
}

public Action:Hook_SetTransmit_TraitorSprite(entity, Client)
{
	if(!IsValidClient(Client) || !IsClientInGame(Client) || IsClientSourceTV(Client)) return Plugin_Continue;
	
	new owner = GetOwnerEntity(entity);
	if(!IsValidClient(owner)) return Plugin_Continue;
	
	if(owner == Client || PlayerData[Client][Job] != traitor) return Plugin_Handled;
	return Plugin_Continue;
}

public Action:Hook_SetTransmit_DetectiveSprite(entity, Client)
{
	if(!IsValidClient(Client) || !IsClientInGame(Client) || !IsPlayerAlive(Client)) return Plugin_Continue;
	
	new owner = GetOwnerEntity(entity);
	if(!IsValidClient(owner)) return Plugin_Continue;
	
	if(owner == Client) return Plugin_Handled;
	
	new target = TTT_GetEntityFromAim(Client, true);
	if(target == owner) return Plugin_Continue;
	return Plugin_Handled;
}

public Action:Hook_SetTransmit_DNASprite(entity, Client)
{
	if(!IsValidClient(Client) || !IsClientInGame(Client)) return Plugin_Continue;
	
	new owner = GetOwnerEntity(entity);
	if(!IsValidClient(owner)) return Plugin_Handled;
	
	if(owner == Client) return Plugin_Continue;
	return Plugin_Handled;
}

public Action:Hook_SetTransmit_ViewModel(entity, Client)
{
	if(!IsValidClient(Client) || !IsClientInGame(Client)) return Plugin_Continue;
	
	new owner = GetEntPropEnt(entity, Prop_Send, "m_hOwner");
	if(!IsValidClient(owner)) return Plugin_Continue;
	
	if(owner == Client)
	{
		new weapons = GetClientWeaponIndex(Client);
		if(IsWeapon(weapons) && WeaponData[weapons][IsUsingViewmodel]) {
			return Plugin_Handled;
		}
	}
	return Plugin_Continue;
}