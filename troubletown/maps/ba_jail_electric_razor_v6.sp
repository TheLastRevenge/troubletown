
public ba_jail_electric_razor_v6_RoundStart()
{
	Format(g_sJailCommand, 199, "");
	g_isOpendDoor = false;
	RemoveEntityByClassname("weapon_awp");
	RemoveEntityByClassname("weapon_hegrenade");
	RemoveEntityByClassname("weapon_sg550");
	RemoveEntityByClassname("weapon_g3sg1");
	RemoveEntityByClassname("weapon_ak47");
	RemoveEntityByClassname("weapon_m4a1");
	
	if(g_DoorTimer != INVALID_HANDLE)
	{
		KillTimer(g_DoorTimer);
		g_DoorTimer = INVALID_HANDLE;
	}
}

public ba_jail_electric_razor_v6_GameStart()
{
	for(new i=1; i<=MaxClients; i++)
	{
		if(!IsClientInGame(i) || !IsPlayerAlive(i) || GetClientTeam(i) < 2 || !PlayerData[i][DataLoaded]) continue;
		
		if(PlayerData[i][Job] != detective)
		{
			SetEntityMoveType(i, MOVETYPE_WALK);
			continue;
		}
		
		PlayerData[i][Credit]++;
		TTT_BuyItem(i, "Super Crowbar");
	}
	
	PrintToChatAll("\x04[TTT - Jail] \x01- 명령이 없을 경우, 30.0초후 문이 자동으로 열립니다.");
	g_DoorTimer = CreateTimer(30.0, g_OpenTheDoor_PreEvent, _, TIMER_FLAG_NO_MAPCHANGE);
}

public ba_jail_electric_razor_v6_PlayerSpawn(Client)
{
	SetEntityMoveType(Client, MOVETYPE_NONE);
	PrintToChat(Client, "\x04[TTT - Jail] \x01- 탐정이 선택될 때까지 움직일 수 없습니다.")
}

public ba_jail_electric_razor_v6_Say(const String:Msg[])
{
	Format(g_sJailCommand, 199, "%s", Msg);
	for(new i=1; i<=MaxClients; i++)
	{
		if(!IsClientInGame(i) || !PlayerData[i][DataLoaded]) continue;
		
		PrintCenterText(i, "%T", "detective new command", i);
		ClientCommand(i, "play buttons/button9");
	}
	if(g_isOpendDoor == false && g_DoorTimer != INVALID_HANDLE)
	{
		KillTimer(g_DoorTimer);
		g_DoorTimer = CreateTimer(5.0, g_OpenTheDoor_Event, _, TIMER_FLAG_NO_MAPCHANGE);
		g_isOpendDoor = true;
		PrintToChatAll("\x04[TTT - Jail] \x01- 명령이 내려졌습니다. 5초후 문이 자동으로 열립니다.");
	}
}