
new iDiaCount;

public ttt_minecraft_b4_MapStart()
{
	HookEntityOutput("trigger_multiple", "OnStartTouch", ttt_minecraft_b4_trigger_StartTouch);
	HookEntityOutput("trigger_once", "OnStartTouch", ttt_minecraft_b4_trigger_StartTouch);
}

public ttt_minecraft_b4_RoundStart()
{
	iDiaCount = 0;
	
	new ent, String:iName[32];
	while((ent = FindEntityByClassname(ent, "func_brush")) != -1)
	{
		GetTargetName(ent, iName, sizeof(iName));
		if(StrEqual(iName, "I2_Door1_Clip") || StrEqual(iName, "Ttest_Frontdoors_Clip")) {
			SetEntData(ent, Offset[m_CollisionGroup], 2, 4, true);
		}
	}
	while((ent = FindEntityByClassname(ent, "func_door_rotating")) != -1)
	{
		GetTargetName(ent, iName, sizeof(iName));
		if(StrEqual(iName, "I2_Door1") || StrEqual(iName, "Ttest_Frontdoors")) {
			SetEntData(ent, Offset[m_CollisionGroup], 2, 4, true);
		}
	}
	while((ent = FindEntityByClassname(ent, "func_physbox")) != -1)
	{
		GetTargetName(ent, iName, sizeof(iName));
		if(StrContains(iName, "DiaOre_") == 0) {
			SetEntityRenderMode(ent, RENDER_TRANSCOLOR);
			SetEntityRenderColor(ent, 255, 255, 255, 0);
		}
	}
	PrintToChatAll("\x04[TTT - Furnace] \x01- You have placed 0 In the furnace You need 9 more.");
}

public ttt_minecraft_b4_GameStart()
{
	new ent, String:iName[32];
	while((ent = FindEntityByClassname(ent, "func_brush")) != -1)
	{
		GetTargetName(ent, iName, sizeof(iName));
		if(StrEqual(iName, "T1_Door1_Clip") || StrEqual(iName, "T1_Door2_Clip")) {
			SetEntData(ent, Offset[m_CollisionGroup], 2, 4, true);
		}
	}
	while((ent = FindEntityByClassname(ent, "func_door_rotating")) != -1)
	{
		GetTargetName(ent, iName, sizeof(iName));
		if(StrEqual(iName, "T1_Door1") || StrEqual(iName, "T1_Door2")) {
			SetEntData(ent, Offset[m_CollisionGroup], 2, 4, true);
		}
	}
	while((ent = FindEntityByClassname(ent, "func_physbox")) != -1)
	{
		GetTargetName(ent, iName, sizeof(iName));
		if(StrContains(iName, "DiaOre_") == 0) {
			SetEntityRenderColor(ent, 255, 255, 255, 255);
		}
	}
	while((ent = FindEntityByClassname(ent, "trigger_multiple")) != -1)
	{
		GetTargetName(ent, iName, sizeof(iName));
		if(!StrEqual(iName, "T1_Door2_Trigger")) {
			AcceptEntityInput(ent, "Enable");
		}
	}
}

public ttt_minecraft_b4_trigger_StartTouch(const String:output[], caller, activator, Float:delay)
{
	new Float:pos[3];
	GetEntPropVector(caller, Prop_Send, "m_vecOrigin", pos);
	if(RoundToNearest(pos[0]) == -952 && RoundToNearest(pos[1]) == -270 && RoundToNearest(pos[2]) == 72)
	{
		if(!TTT_IsTraitor(activator))
		{
			makeDamage(caller, activator, 999, (1<<17), 1.0, pos, "ttt_traitor_trap");
		}
	}
	
	new String:iName[32];
	GetTargetName(caller, iName, sizeof(iName));
	if(StrEqual(iName, "Furnace_Trigger"))
	{
		iDiaCount++;
		if(iDiaCount < 9)
		{
			PrintToChatAll("\x04[TTT - Furnace] \x01- You have placed %i In the furnace You need %i more.", iDiaCount, 9-iDiaCount);
		}
		else if(iDiaCount == 9)
		{
			PrintToChatAll("\x04[TTT - Furnace] \x01- Diamond is made");
		}
	}
	
	if(RoundToNearest(pos[0]) == -1808 && RoundToNearest(pos[1]) == 592 && RoundToNearest(pos[2]) == 738)
	{
		CS_TerminateRound(10.0, CSRoundEnd_TargetSaved);
	}
}