
public surf_10x_reloaded_MapStart()
{
	ServerCommand("sv_airaccelerate 9999;");
	ServerCommand("sv_enablebunnyhopping 1;");
}

public surf_10x_reloaded_GameStart()
{
	for(new i=1; i<=MaxClients; i++)
	{
		if(!IsClientInGame(i) || !IsPlayerAlive(i) || GetClientTeam(i) < 2 || !PlayerData[i][DataLoaded]) continue;
		
		PlayerData[i][Credit]++;
		PlayerData[i][bBlockFallDamage] = true;
		TTT_BuyItem(i, "Iron Shoes");
	}
}

public surf_10x_reloaded_RoundStart()
{
	RemoveEntityByClassname("weapon_awp");
	RemoveEntityByClassname("weapon_hegrenade");
	RemoveEntityByClassname("weapon_sg550");
	RemoveEntityByClassname("weapon_g3sg1");
	RemoveEntityByClassname("weapon_ak47");
	RemoveEntityByClassname("weapon_m4a1");
}