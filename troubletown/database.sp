
#define DATABASE_NAME "troubletown"
new Handle:databasehandle = INVALID_HANDLE;

public ConnectSQL()
{
	SQL_TConnect(CallBack_ConnectSQL, DATABASE_NAME);
}

public DisconnectSQL()
{
	if(databasehandle != INVALID_HANDLE)
	{
		CloseHandle(databasehandle);
	}
	databasehandle = INVALID_HANDLE;
}

public CallBack_ConnectSQL(Handle:owner, Handle:dbhandle, const String:error[], any:data)
{
	if(dbhandle == INVALID_HANDLE)
	{
		LogError("SQL Connecting Failed %s", error);
	}
	else
	{
		databasehandle = dbhandle;
		
		//set character
		SQL_TQuery(databasehandle, save_info, "SET NAMES 'UTF8'", 0, DBPrio_High);
		
		//create table
		SQL_TQuery(databasehandle, CallBack_CreateTable, "create table if not exists karma(id bigint primary key not null AUTO_INCREMENT, steamid varchar(20) CHARACTER SET utf8, year int, month int, day int);");
		SQL_TQuery(databasehandle, CallBack_CreateTable, "create table if not exists chatlog(id bigint primary key not null AUTO_INCREMENT, steamid varchar(20) CHARACTER SET utf8, nick varchar(32) CHARACTER SET utf8, chat varchar(256) CHARACTER SET utf8, alive tinyint(1) DEFAULT 1, event tinyint(1) DEFAULT 0);");
		/*
		for(new i=1; i<=MAX_ACHIEVEMENTS; i++)
		{
			new String:Formatting[512];
			Format(Formatting, 512, "create table if not exists achievement_%i(id bigint primary key not null AUTO_INCREMENT, steamid varchar(256) CHARACTER SET utf8, earned tinyint DEFAULT 0", i);
			for(new j=1; j<=MAX_ACHIEVEMENT_ELEMENTS; j++)
			{
				Format(Formatting, 512, "%s, quest%i int DEFAULT 0", Formatting, j);
			}
			Format(Formatting, 512, "%s);", Formatting);
			SQL_TQuery(databasehandle, CallBack_CreateTable, Formatting, 0);
		}
		for(new i=1; i<=MAX_TITLES; i++)
		{
			new String:Formatting[512];
			Format(Formatting, 512, "create table if not exists title_%i(id bigint primary key not null AUTO_INCREMENT, steamid varchar(256) CHARACTER SET utf8, earned tinyint DEFAULT 0", i);
			for(new j=1; j<=MAX_TITLE_ELEMENTS; j++)
			{
				Format(Formatting, 512, "%s, quest%i int DEFAULT 0", Formatting, j);
			}
			Format(Formatting, 512, "%s);", Formatting);
			SQL_TQuery(databasehandle, CallBack_CreateTable, Formatting, 0);
		}*/
		
		//load player data
		for(new i=1; i<=MaxClients; i++)
		{
			if(IsClientInGame(i) == false) continue;
			
			new String:SteamID[20];
			GetClientAuthString(i, SteamID, sizeof(SteamID));
			if(strlen(SteamID) == 0) continue;
			
			OnClientAuthorized(i, SteamID);
		}
	}
}

public CallBack_CreateTable(Handle:owner, Handle:hndl, const String:error[], any:data)
{
	if(hndl == INVALID_HANDLE)
	{
		LogError("Player Data Table Creating Failed. %s", error);
	}
}

public save_info(Handle:owner, Handle:handle, const String:error[], any:data)
{
	if(handle == INVALID_HANDLE)
	{
		LogError("SQL ERROR: %s", error);
	}
}

public save_info_nolog(Handle:owner, Handle:handle, const String:error[], any:data)
{
}

public ResetPlayerData(Client)
{
	PlayerData[Client][Karma] = 0;
	PlayerData[Client][DataLoaded] = false;
	
	PlayerData[Client][vmIndex] = 0;
}

public Player_DataLoad(Client, const String:SteamID[])
{
	new String:query[256];
	Format(query, sizeof(query), "SELECT id,year,month,day FROM karma WHERE steamid = '%s';", SteamID);
	SQL_TQuery(databasehandle, DataLoad_karma, query, Client);
	
	Format(PlayerData[Client][RegisterCode], 10, "%09d", GetRandomInt(0,999999999));
	Format(query, sizeof(query), "UPDATE info SET registerValue = '%s' WHERE steamid = '%s';", PlayerData[Client][RegisterCode], SteamID);
	SQL_TQuery(databasehandle, save_info, query, Client);
	
	PlayerData[Client][SpecialFlag] = 0;
	if(StrEqual(SteamID, ADMIN_STEAMID))
	{
		PlayerData[Client][SpecialFlag] |= PLAYER_FLAG_DEVELOPER;
	}
	new String:buffer[20];
	for(new i; i<GetArraySize(g_tester_list); i++)
	{
		GetArrayString(g_tester_list, i, buffer, sizeof(buffer));
		if(StrEqual(buffer, SteamID))
		{
			PlayerData[Client][SpecialFlag] |= PLAYER_FLAG_TESTER;
			break;
		}
	}
}

public DataLoad_karma(Handle:owner, Handle:handle, const String:error[], any:Client)
{
	if(handle == INVALID_HANDLE)
	{
		LogError("SQL ERROR: %s", error);
		return;
	}
	PlayerData[Client][Karma] = SQL_GetRowCount(handle);
	PlayerData[Client][DataLoaded] = true;
	
	if(SQL_GetRowCount(handle))
	{
		while(SQL_FetchRow(handle))
		{
			new String:Time_Year[4], String:Time_Month[4], String:Time_Day[4], year, month, day;
			FormatTime(Time_Year, 30, "%Y", GetTime());
			FormatTime(Time_Month, 30, "%m", GetTime());
			FormatTime(Time_Day, 30, "%d", GetTime());
			year = SQL_FetchInt(handle, 1);
			month = SQL_FetchInt(handle, 2);
			day = SQL_FetchInt(handle, 3);
			if(StringToInt(Time_Year) >= year && StringToInt(Time_Month) >= month && StringToInt(Time_Day) >= day)
			{
				PlayerData[Client][Karma]--;
				
				new String:query[256];
				Format(query, sizeof(query), "DELETE FROM karma WHERE id = '%d';", SQL_FetchInt(handle, 0));
				SQL_TQuery(databasehandle, save_info, query);
			}
		}
	}
	
	if(IsClientInGame(Client))
		OnClientPutInServer(Client);
}