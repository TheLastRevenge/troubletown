
public Command_Initialize()
{
	RegConsoleCmd("say_team", SayCommand_CallBack);
	RegConsoleCmd("say", SayCommand_CallBack);
	
	RegConsoleCmd("say", SayCommand_Remake);
	RegConsoleCmd("say_team", SayTeamCommand_Remake);
	
	RegConsoleCmd("sm_lar", Command_LastAttackReport);
	RegConsoleCmd("sm_ldr", Command_LastDamageReport);
	RegConsoleCmd("ttt_radio", TroubleTownRadio_CallBack, "radio for troubletown");
	
	RegConsoleCmd("jointeam", JoinTeam_CallBack, "control jointeam for troubletown");
	RegConsoleCmd("joinclass", JoinClass_CallBack, "control joinclass for troubletown");
	
	RegServerCmd("socket_viewmodel", Socket_ViewModel_CallBack, "socket troubletown - shop");
	
	RegAdminCmd("sm_addbully", Command_AddBully, ADMFLAG_ROOT, "Karma command for troubletown");
	RegAdminCmd("sm_bully", Command_Bully, ADMFLAG_ROOT, "Karma command for troubletown");
	
	AddCommandListener(Keyboard_G_CallBack, "drop");
	AddCommandListener(Keyboard_N_CallBack, "nightvision");
}

public Action:SayCommand_CallBack(Client, Args)
{
	new String:Msg[256];
	GetCmdArgString(Msg, sizeof(Msg));
	StripQuotes(Msg);
	
	if(Client == 0)
		return;
	
	RemoveSpecialSay(Msg);
	
	if(StrEqual(Msg, "!info", false) || StrEqual(Msg, "!정보"))
	{
		if(IsPlayerAlive(Client) && RoundType == RoundState_GamePlay)
		{
			PrintToChat(Client, "\x04[TTT] \x01- can't see player info after game started");
			return;
		}
		SearchPlayerByWords(Client, NULL_STRING, SEARCHTYPE_PLAYERINFO);
	}
	if(StrEqual(Msg, "!gift", false) || StrEqual(Msg, "!선물"))
	{
		PrintToChat(Client, "\x04[TTT] \x01- Usage : \"%s (point, item, 포인트, 아이템) nickname\"", Msg);
	}
	if(StrEqual(Msg, "!spec", false))
	{
		if(GetUserAdmin(Client) != INVALID_ADMIN_ID) {
			SearchPlayerByWords(Client, NULL_STRING, SEARCHTYPE_SPECTATOR);
		}
	}
	if(StrEqual(Msg, "!coupon", false))
	{
		//Shop_CreateCouponBox(Client);
	}
	
	new String:sExploded[3][200];
	ExplodeString(Msg, " ", sExploded, sizeof(sExploded), 200);
	if(StrContains(Msg, "!info ", false) == 0 || StrContains(Msg, "!정보 ") == 0)
	{
		if(IsPlayerAlive(Client) && RoundType == RoundState_GamePlay)
		{
			PrintToChat(Client, "\x04[TTT] \x01- can't see player info after game started");
			return;
		}
		SearchPlayerByWords(Client, sExploded[1], SEARCHTYPE_PLAYERINFO);
	}
	if(StrContains(Msg, "!gift ", false) == 0 || StrContains(Msg, "!선물 ") == 0)
	{
		if(StrEqual(sExploded[1], "point", false) || StrEqual(sExploded[1], "포인트"))
		{
			SearchPlayerByWords(Client, sExploded[2], SEARCHTYPE_GIFTPOINT);
		}
		if(StrEqual(sExploded[1], "item", false) || StrEqual(sExploded[1], "아이템"))
		{
			SearchPlayerByWords(Client, sExploded[2], SEARCHTYPE_GIFTITEM);
		}
	}
	if(StrContains(Msg, "!tag ") == 0)
	{
		ReplaceStringEx(Msg, sizeof(Msg), "!tag ", "");
		ServerCommand("pointshop_use_tag %i \"%s\"", GetClientUserId(Client), Msg);
	}
	if(StrContains(Msg, "!태그 ") == 0)
	{
		ReplaceStringEx(Msg, sizeof(Msg), "!태그 ", "");
		ServerCommand("pointshop_use_tag %i \"%s\"", GetClientUserId(Client), Msg);
	}
	
	if(StrContains(Msg, "!spec ", false) == 0)
	{
		if(GetUserAdmin(Client) != INVALID_ADMIN_ID) {
			SearchPlayerByWords(Client, sExploded[1], SEARCHTYPE_SPECTATOR);
		}
	}
	
	if(!StrContains(Msg, "!p ", false))
	{
		new count = 0, target = 0;
		for(new i = 1; i <= MaxClients; i++)
		{
			if(IsClientInGame(i))
			{
				new String:s_name[32];
				GetClientName(i, s_name, 32);
				if(StrContains(s_name, sExploded[1], false) != -1)
				{
					count += 1;
					target = i;
				}
			}
		}
		if(count < 1)
		{
			PrintToChat(Client, "\x05Couldn't find %s", sExploded[1]);
		}
		else if(count > 1)
		{
			PrintToChat(Client, "\x05Duplicated name (%s) %d times", sExploded[1], count);
		}
		else
		{
			new String:msg[255];
			strcopy(msg, sizeof(msg), Msg);
			ReplaceStringEx(msg, sizeof(msg), sExploded[0], "");
			ReplaceStringEx(msg, sizeof(msg), " ", "");
			ReplaceStringEx(msg, sizeof(msg), sExploded[1], "");
			PrintToChat(Client, "\x07CA3AA0[To]\x07FF3A3A%N\x01 :\x07FFFFFF %s", target, msg);
			PrintToChat(target, "\x07CA3AA0[From]\x07FF3A3A%N\x01 :\x07FFFFFF %s", Client, msg);
			ClientCommand(target, "play buttons/button9");
			
			new sourceTV = GetSourceTVIndex();
			if(sourceTV != -1) PrintToChat(sourceTV, "\x07CA3AA0[\x07FF3A3A%N\x07CA3AA0->\x07FF3A3A%N\x07CA3AA0] \x07FF3A3A%N\x01 :\x07FFFFFF %s", Client, target, Client, msg);
			for(new i = 1; i <= MaxClients; i++)
			{
				if(IsClientInGame(i))
				{
					if(GetUserAdmin(i) != INVALID_ADMIN_ID && i != target && i != Client)
					{
						PrintToChat(i, "\x07CA3AA0[\x07FF3A3A%N\x07CA3AA0->\x07FF3A3A%N\x07CA3AA0] \x07FF3A3A%N\x01 :\x07FFFFFF %s", Client, target, Client, msg);
					}
				}
			}
		}
	}
	
	if(!StrContains(Msg, "!tdmin ", false))
	{
		new String:SteamID[20];
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		if(PlayerData[Client][SpecialFlag] & PLAYER_FLAG_DEVELOPER || PlayerData[Client][SpecialFlag] & PLAYER_FLAG_TESTER)
		{
			if(StrEqual(sExploded[1], "class") == true)
			{
				TTT_SetPlayerJob(Client, StringToInt(sExploded[2]));
			}
			if(StrEqual(sExploded[1], "ammo") == true)
			{
				SetEntProp(GetClientWeaponIndex(Client), Prop_Send, "m_iClip1", StringToInt(sExploded[2]));
			}
			if(StrEqual(sExploded[1], "bot_move") == true)
			{
				new Float:vecEyePosition[3], Float:vecAngles[3];
				GetClientEyePosition(Client, vecEyePosition);
				GetClientEyeAngles(Client, vecAngles);
				
				new pass_entity = (GetEntProp(Client, Prop_Send, "m_iObserverMode") == 4) ? GetEntPropEnt(Client, Prop_Send, "m_hObserverTarget") : Client;
				TR_TraceRayFilter(vecEyePosition, vecAngles, MASK_SOLID, RayType_Infinite, TraceFilter_OneEntity, pass_entity);
				
				if(TR_DidHit() == false)
				{
					return;
				}
				
				new Float:vecEndPosition[3];
				TR_GetEndPosition(vecEndPosition);
				
				for(new bot=1; bot<=MaxClients; bot++)
					Bot_MoveTo(bot, vecEndPosition, FASTEST_ROUTE);
			}
			if(StrEqual(sExploded[1], "bot_freeze") == true)
			{
				for(new bot=1; bot<=MaxClients; bot++)
				{
					if(!IsValidClient(bot) || !IsClientInGame(bot) || !IsFakeClient(bot) || !IsPlayerAlive(bot)) {
						continue;
					}
					
					ServerCommand("sm_freeze #%d 9999", GetClientUserId(bot));
				}
			}
		}
	}
	
	if(StrEqual(Msg, "!adminlist"))
	{
		ShowAdminList(Client);
	}
	
	if(StrEqual(Msg, "!registercode"))
	{
		if(PlayerData[Client][DataLoaded])
		{
			PrintToChat(Client, "\x01당신의 Register 코드는 \x04%s \x01입니다.", PlayerData[Client][RegisterCode]);
			PrintToChat(Client, "\x07FF0000※절대로 다른사람에게 알려주셔서는 안됩니다.");
		}
	}
	
	if(!StrContains(Msg, "@@@"))
	{
		if(GetUserAdmin(Client) != INVALID_ADMIN_ID)
		{
			ReplaceStringEx(Msg, 256, "@@@", "");
			PrintCenterTextAll("%N : %s", Client, Msg);
		}
	}
	else if(!StrContains(Msg, "@"))
	{
		if(GetUserAdmin(Client) != INVALID_ADMIN_ID)
		{
			ReplaceStringEx(Msg, 256, "@", "");
			PrintToChatAll("\x0788FF88(ADM) %N :\x01 %s", Client, Msg);
		}
	}
	else if(g_isJailMode && !StrContains(Msg, "#"))
	{
		if(PlayerData[Client][Job] == detective && IsPlayerAlive(Client))
		{
			ReplaceStringEx(Msg, 256, "#", "");
			TTT_ExecuteMapFunction_ParameterString("Say", Msg);
		}
	}
}

public Action:SayCommand_Remake(Client, Args)
{
	new String:Msg[256];
	GetCmdArgString(Msg, sizeof(Msg));
	new bool:isConsole = Msg[0] == '"' ? false : true;
	StripQuotes(Msg);
	
	if(Client == 0) {
		return Plugin_Handled;
	}
	
	if(BaseComm_IsClientGagged(Client)) {
		return Plugin_Handled;
	}
	
	if(BlockCommand(Msg, "!music") || BlockCommand(Msg, "!stop") || BlockCommand(Msg, "!mr") || BlockCommand(Msg, "!p") || BlockCommand2(Msg, "@")) {
		return Plugin_Handled;
	}
	
	if(StrEqual(Msg, "mycp")) { // this command for zx2
		return Plugin_Continue;
	}
	
	if(isConsole && IsPlayerAlive(Client) && PlayerData[Client][Job] != detective && PlayerData[Client][Job] != unselected) {
		return Plugin_Handled;
	}
	
	RemoveSpecialSay(Msg);
	new String:buffer[255], String:sTag[32];
	/*if(Shop_GetClientTag(Client, sTag, 16)) {
		Format(sTag, sizeof(sTag), "\x04%s \x01- ", sTag);
	}*/
	Format(buffer, sizeof(buffer), "%s\x03%N \x01:  %s", sTag, Client, Msg);
	if(isConsole && IsPlayerAlive(Client)) {
		Format(buffer, sizeof(buffer), "\x07FF8800(%t) %s", "Quickchat", buffer);
	}
	SendCustomSayText2Message(Client, buffer);
	SaveChatMessage(Client, 0, Msg);
	
	if(IsPlayerAlive(Client))
	{
		RemoveSpecialSay(buffer);
		CloseTimer(g_WillTimer[Client]);
		new Handle:datapack;
		g_WillTimer[Client] = CreateDataTimer(1.0, g_WillTimer_Event, datapack);
		WritePackCell(datapack, Client);
		WritePackCell(datapack, WILLTYPE_CHAT);
		WritePackString(datapack, Msg);
		WritePackString(datapack, "");
	}
	return Plugin_Handled;
}

public Action:SayTeamCommand_Remake(Client, Args)
{
	new String:Msg[256];
	GetCmdArgString(Msg, sizeof(Msg));
	StripQuotes(Msg);
	
	if(Client == 0)
		return Plugin_Handled;
	
	if(IsPlayerAlive(Client) && PlayerData[Client][Job] == traitor)
	{
		for(new i = 1; i <= MaxClients; i++)
		{
			if(!IsClientInGame(i) || PlayerData[i][Job] != traitor) continue;
			
			new String:sMessage[200];
			Format(sMessage, sizeof(sMessage), "\x04[\x05%T\x04]\x07FF3A3A%N \x01: %s", "traitor", i, Client, Msg);
			SayText2Message(Client, i, sMessage);
		}
	}
	return Plugin_Handled;
}

public Action:Command_LastAttackReport(Client, Args)
{
	ShowAttackReport(Client);
	return Plugin_Handled;
}

public Action:Command_LastDamageReport(Client, Args)
{
	ShowDamageReport(Client);
	return Plugin_Handled;
}

public Action:TroubleTownRadio_CallBack(Client, Args)
{
	if(Args != 1 || !IsPlayerAlive(Client)) {
		return Plugin_Handled;
	}
	
	
	new String:sArg[16];
	GetCmdArg(1, sArg, sizeof(sArg));
	
	new fastchat;
	if(StrEqual(sArg, "yes", false)) fastchat = 0;
	if(StrEqual(sArg, "no", false)) fastchat = 1;
	if(StrEqual(sArg, "help", false)) fastchat = 2;
	if(StrEqual(sArg, "with", false)) fastchat = 3;
	if(StrEqual(sArg, "traitor", false)) fastchat = 4;
	if(StrEqual(sArg, "suspicious", false)) fastchat = 5;
	if(StrEqual(sArg, "innocent", false)) fastchat = 6;
	if(StrEqual(sArg, "alive", false)) fastchat = 7;
	
	TTT_FastChat(Client, fastchat);
	return Plugin_Handled;
}

public Action:JoinTeam_CallBack(Client, Args)
{
	new String:sArg[4];
	GetCmdArg(1, sArg, sizeof(sArg));
	new team = StringToInt(sArg);
	
	if(IsPlayerAlive(Client)) { // block suicide
		return Plugin_Handled;
	}
	
	if(team == 1) { // allow to go to spectate
		return Plugin_Continue;
	}
	
	if(!PlayerData[Client][DataLoaded]) {
		return Plugin_Handled;
	}
	
	ChangeClientTeam(Client, 2);
	return Plugin_Handled;
}

public Action:JoinClass_CallBack(Client, Args)
{
	if(IsPlayerAlive(Client)) { // block suicide
		return Plugin_Handled;
	}
	
	if(!PlayerData[Client][DataLoaded]) {
		return Plugin_Handled;
	}
	
	return Plugin_Continue;
}

public Action:Socket_ViewModel_CallBack(Args)
{
	if(Args != 2)
	{
		return Plugin_Handled;
	}
	
	new String:sClientUserId[8], String:sViewModelIndex[8];
	GetCmdArg(1, sClientUserId, sizeof(sClientUserId));
	GetCmdArg(2, sViewModelIndex, sizeof(sViewModelIndex));
	
	new Client = GetClientOfUserId(StringToInt(sClientUserId));
	if(!Client || !PlayerData[Client][DataLoaded])
		return Plugin_Handled;
	
	PlayerData[Client][vmIndex] = StringToInt(sViewModelIndex);
	
	return Plugin_Handled;
}

public Action:Command_AddBully(Client, Args)
{
	new String:SteamID[32];
	GetCmdArg(1, SteamID, sizeof(SteamID));
	PrintToChat(Client, "\x04[TTT Karma] \x01Add bully score to %s", SteamID);
	
	new String:Time_Year[4], String:Time_Month[4], String:Time_Day[4], year, month, day;
	FormatTime(Time_Year, 30, "%Y", GetTime());
	FormatTime(Time_Month, 30, "%m", GetTime());
	FormatTime(Time_Day, 30, "%d", GetTime());
	CalculateDays(StringToInt(Time_Year), StringToInt(Time_Month), StringToInt(Time_Day), 31, year, month, day);
	
	new String:query[512];
	Format(query, 512, "INSERT INTO karma(steamid, year, month, day) values ('%s', '%d', '%d', '%d');", SteamID, year, month, day);
	SQL_TQuery(databasehandle, save_info, query, Client);
	
	new String:sName[32], String:sEscapedName[65];
	GetClientName(Client, sName, sizeof(sName));
	SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
	LogData("%s add bully %s", sEscapedName, SteamID);
}

public Action:Command_Bully(Client, Args)
{
	new String:sPlayerName[32];
	GetCmdArg(1, sPlayerName, sizeof(sPlayerName));
	SearchPlayerByWords(Client, sPlayerName, SEARCHTYPE_KARMA);
	return Plugin_Handled;
}

public Action:Keyboard_G_CallBack(Client, const String:command[], arg)
{
	new weapons = GetClientWeaponIndex(Client);
	if(!IsWeapon(weapons))
		return;
	
	if(IsZMCarry(weapons))
		return;
	
	//detective can't drop wtester with himself
	if(TTT_IsTester(weapons) && PlayerData[Client][Job] == detective)
		return;
	
	SetAllowDrop(Client);
	
	if(TTT_IsTeleporter(weapons)) {
		CS_DropWeapon(Client, weapons, false);
	}
}

public Action:Keyboard_N_CallBack(Client, const String:command[], arg)
{
	new buttons = PlayerData[Client][keyValue];
	if(buttons & IN_SPEED) {
		NightVision_Delay(Client);
		return Plugin_Continue;
	}
	ShowCreditShop(Client);
	return Plugin_Handled;
}