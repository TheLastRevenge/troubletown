
#include<sendproxy>

public SendProxy_OnMapStart()
{
	new ent = FindEntityByClassname(-1, "cs_player_manager");
	for(new i = 1; i <= MaxClients; i++)
	{
		SendProxy_HookArrayProp(ent, "m_bAlive", i, Prop_Int, SendProxy_bAlive);
		SendProxy_HookArrayProp(ent, "m_iTeam", i, Prop_Int, SendProxy_iTeam);
		SendProxy_Hook(ent, "m_iPlayerC4", Prop_Int, SendProxy_iPlayerC4);
	}
}

public SendProxy_HookClient(Client)
{
	for(new i; i<=10; i++)
	{
		SendProxy_HookArrayProp(Client, "m_iAmmo", i, Prop_Int, SendProxy_Ammo);
	}
}

public Action:SendProxy_bAlive(entity, const String:propname[], &iValue, element) 
{
	if(!IsClientInGame(element) || GetClientTeam(element) < 2)
		return Plugin_Continue;
	
	new ragdoll = TTT_GetPlayerRagdoll(element);
	if(ragdoll == -1)
		return Plugin_Continue;
	
	new bool:searched = TTT_RagdollSearched(ragdoll);
	iValue = searched ? 0 : 1;
	return Plugin_Changed;
}

public Action:SendProxy_iTeam(entity, const String:propname[], &iValue, element) 
{
	if(!IsClientInGame(element))
		return Plugin_Continue;
	
	if(GetClientTeam(element) < 2)
	{
		new ragdoll = TTT_GetPlayerRagdoll(element);
		if(ragdoll == -1)
			return Plugin_Continue;
		
		if(!TTT_RagdollSearched(ragdoll))
		{
			iValue = (PlayerData[element][Job] == detective) ? 3 : 2;
			return Plugin_Changed;
		}
	}
	
	if(IsClientSourceTV(element))
	{
		iValue = 1;
		return Plugin_Changed;
	}
	
	iValue = (PlayerData[element][Job] == detective) ? 3 : 2;
	return Plugin_Changed;
}

public Action:SendProxy_iPlayerC4(entity, const String:propname[], &iValue, element) 
{
	iValue = -1;
	return Plugin_Changed;
}

public Action:SendProxy_Weapon(entity, const String:propname[], &iValue, element) 
{
	new owner = GetOwnerEntity(entity);
	if(!IsValidClient(owner) ||!IsClientInGame(owner) || !IsPlayerAlive(owner)) {
		return Plugin_Continue;
	}
	new iClip1 = GetEntProp(entity, Prop_Send, "m_iClip1");
	new iPrimaryAmmoType = GetEntProp(entity, Prop_Send, "m_iPrimaryAmmoType");
	if(iClip1 || (iPrimaryAmmoType > 0 && GetEntProp(owner, Prop_Data, "m_iAmmo", _, iPrimaryAmmoType)))
	{
		iValue = 255;
		return Plugin_Changed;
	}
	return Plugin_Continue; 
}

public Action:SendProxy_Ammo(entity, const String:propname[], &iValue, element) 
{ 
	if(!IsValidClient(entity) ||!IsClientInGame(entity) || !IsPlayerAlive(entity)) {
		return Plugin_Continue;
	}
	
	new weapons = GetClientWeaponIndex(entity);
	if(!IsWeapon(weapons) || !WeaponData[weapons][IsUsingSendProxy]) {
		return Plugin_Continue;
	}
	
	new iClip1 = GetEntProp(weapons, Prop_Send, "m_iClip1");
	new iPrimaryAmmoType = GetEntProp(weapons, Prop_Send, "m_iPrimaryAmmoType");
	if(element != iPrimaryAmmoType) {
		return Plugin_Continue;
	}
	
	if(iPrimaryAmmoType == 0) {
		iValue = iClip1;
		return Plugin_Changed;
	}
	
	new ammo = GetEntProp(entity, Prop_Data, "m_iAmmo", _, iPrimaryAmmoType);
	if(iClip1 || ammo) {
		iValue = iClip1+ammo;
		return Plugin_Changed;
	}
	
	return Plugin_Continue; 
}