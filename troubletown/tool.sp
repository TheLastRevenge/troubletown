
new g_ready_count;
new g_game_count;
new g_traitor_count;
new g_ragdoll_count;
new Handle:g_RagdollData_Trie[MAXPLAYERS+1];
new Handle:g_RagdollKillList_Trie[MAXPLAYERS+1];
new g_ragdoll_trie_number[2048];

//Load SourceBan native
native SBBanPlayer(client, target, time, String:reason[]);

//0 = chatting, 1 = program, 2 = FastChat, 3 = Will
stock SaveChatMessage(Client, Type, const String:msg[], any:...)
{
	if(IsClientInGame(Client) == false)
	{
		return;
	}
	SetGlobalTransTarget(LANG_SERVER);
	new String:txt[255], String:sEscapedChatting[512];
	VFormat(txt, sizeof(txt), msg, 4);
	
	new String:SteamID[20], String:sName[32], String:sEscapedName[65];
	GetClientAuthString(Client, SteamID, sizeof(SteamID));
	GetClientName(Client, sName, sizeof(sName));
	
	SQL_EscapeString(databasehandle, txt, sEscapedChatting, sizeof(sEscapedChatting));
	SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
	
	new String:sDate[32];
	Date_MakeString(sDate, sizeof(sDate));
	
	new String:query[512];
	Format(query, sizeof(query), "INSERT INTO chatlog(steamid, nick, chat, date, type, alive) values ('%s', '%s', '%s', '%s', '%d', '%d');", SteamID, sEscapedName, sEscapedChatting, sDate, Type, IsPlayerAlive(Client));
	SQL_TQuery(databasehandle, save_info, query, Client);
}

//0 = Connect, 1 = Disconnect, 2 = Search
stock SaveEventLog(Type, const String:msg[], any:...)
{
	SetGlobalTransTarget(LANG_SERVER);
	new String:txt[255], String:sEscapedMessage[512];
	VFormat(txt, sizeof(txt), msg, 3);
	
	SQL_EscapeString(databasehandle, txt, sEscapedMessage, sizeof(sEscapedMessage));
	
	new String:sDate[32];
	Date_MakeString(sDate, sizeof(sDate));
	
	new String:query[512];
	Format(query, sizeof(query), "INSERT INTO chatlog(chat, date, type, event) values ('%s', '%s', '%d', '1');", sEscapedMessage, sDate, Type);
	SQL_TQuery(databasehandle, save_info, query);
}

stock HudMsg(client, channel, Float:x, Float:y, const firstcolor[4], const secondcolor[4], effect, Float:fadein, Float:fadeout, Float:holdtime, Float:fxtime, const String:msg[], any:...)
{
	new Handle:hudhandle = INVALID_HANDLE;
	
	if(client == 0) hudhandle = StartMessageAll("HudMsg", USERMSG_RELIABLE);
	else
	{
		hudhandle = StartMessageOne("HudMsg", client, USERMSG_RELIABLE);
		SetGlobalTransTarget(client);
	}
	
	new String:txt[255];
	VFormat(txt, sizeof(txt), msg, 13);
	
	if(hudhandle != INVALID_HANDLE)
	{
		BfWriteByte(hudhandle, channel);
		BfWriteFloat(hudhandle, x);
		BfWriteFloat(hudhandle, y);
		
		BfWriteByte(hudhandle, firstcolor[0]);
		BfWriteByte(hudhandle, firstcolor[1]);
		BfWriteByte(hudhandle, firstcolor[2]);
		BfWriteByte(hudhandle, firstcolor[3]);
		
		BfWriteByte(hudhandle, secondcolor[0]);
		BfWriteByte(hudhandle, secondcolor[1]);
		BfWriteByte(hudhandle, secondcolor[2]);
		BfWriteByte(hudhandle, secondcolor[3]);
		BfWriteByte(hudhandle, effect);
		BfWriteFloat(hudhandle, fadein);
		BfWriteFloat(hudhandle, fadeout);
		BfWriteFloat(hudhandle, holdtime);
		BfWriteFloat(hudhandle, fxtime);
		BfWriteString(hudhandle, txt);
		EndMessage();
	}
}

stock KeyHintText(client, String:msg[], any:...)
{
	new Handle:hudhandle = INVALID_HANDLE;
	hudhandle = StartMessageOne("KeyHintText", client, USERMSG_RELIABLE);
	SetGlobalTransTarget(client);
	
	if(hudhandle != INVALID_HANDLE)
	{
		new String:txt[255];
		VFormat(txt, sizeof(txt), msg, 3);
		BfWriteByte(hudhandle, 1);
		BfWriteString(hudhandle, txt);
		EndMessage();
	}
}

public ChangeSourceTVTeam(team)
{
	new SourceTV = GetSourceTVIndex();
	if(SourceTV == -1)
		return;
	
	ChangeClientTeam(SourceTV,team);
}

public GetSourceTVIndex()
{
	new index = -1;
	for(new i=1; i<=MaxClients; i++)
	{
		if(IsClientSourceTV(i))
		{
			index = i;
			break;
		}
	}
	return index;
}

public bool:TraceFilter_OneEntity(entity, mask, any:data)
{
	if(entity != data)
		return true;
	else
		return false;
}

public CloseTimer(&Handle:hTimer)
{
	if(hTimer != INVALID_HANDLE)
	{
		KillTimer(hTimer);
	}
	hTimer = INVALID_HANDLE;
}

/******************************************************************************************
 * TTT Function
******************************************************************************************/

stock TTT_FastChat(Client, chatIndex, bool:useSound = false)
{
	if(!IsValidClient(Client) || !IsClientInGame(Client) || !IsPlayerAlive(Client))
	{
		return;
	}
	
	if(chatIndex < 8)
	{
		new target = PlayerData[Client][playerFastChat];
		if(!IsValidClient(target) || !IsClientInGame(target))
		{
			target = TTT_GetEntityFromAim(Client, true);
		}
		
		new String:sTag[32], String:sName[32];
		new type = 0;
		if(IsValidClient(target) && IsClientInGame(target))
		{
			type = 1;
			if(PlayerData[target][bExecuteDisguise])
			{
				if(strlen(PlayerData[target][sDisguisedName])) Format(sName, sizeof(sName), PlayerData[target][sDisguisedName]);
				else type = 0;
			}
			else GetClientName(target, sName, sizeof(sName));
			if(GetDistanceEntityTo(Client, target) > 500.0 && type) type = 2;
		}
		if(PlayerData[Client][SpecialFlag] & PLAYER_FLAG_DEVELOPER || PlayerData[Client][SpecialFlag] & PLAYER_FLAG_TESTER)
		{
			PrintToConsole(Client, "[DEBUG] g_survivor_count[0] : %d, g_survivor_count[1] : %d", g_survivor_count[0], g_survivor_count[1]);
		}
		if(g_survivor_count[0] == 1 && g_survivor_count[1] == 1 && PlayerData[Client][Job] == traitor)
		{
			type = 3;
		}
		if(PlayerData[Client][Job] == detective)
		{
			Format(sTag, sizeof(sTag), "[%t]", "detective");
		}
		SendCustomSayText2Message(Client, "\x07FF8800(%t) \x03%s%N\x01 :  \x07FFFFFF%t", "Quickchat", sTag, Client, g_fast_chat[chatIndex][type], sName);
		SaveChatMessage(Client, 2, "%T", g_fast_chat[chatIndex][type], LANG_SERVER, sName);
		
		CloseTimer(g_WillTimer[Client]);
		new Handle:datapack;
		g_WillTimer[Client] = CreateDataTimer(1.0, g_WillTimer_Event, datapack);
		WritePackCell(datapack, Client);
		WritePackCell(datapack, type);
		WritePackString(datapack, g_fast_chat[chatIndex][type]);
		WritePackString(datapack, sName);
		
		if(useSound || ((chatIndex == 7 || chatIndex == 4) && type == 3))
		{
			new Float:nowtime = GetEngineTime();
			if(PlayerData[Client][NextFastChatSound] > nowtime || g_next_fastchat_sound > nowtime)
			{
				return;
			}
			StopSound(SOUND_FROM_PLAYER, SNDCHAN_AUTO, "bot/yea_ok.wav");
			StopSound(SOUND_FROM_PLAYER, SNDCHAN_AUTO, "bot/no_sir.wav");
			StopSound(SOUND_FROM_PLAYER, SNDCHAN_AUTO, "bot/need_help.wav");
			StopSound(SOUND_FROM_PLAYER, SNDCHAN_AUTO, "bot/im_with_you.wav");
			StopSound(SOUND_FROM_PLAYER, SNDCHAN_AUTO, "bot/i_see_the_bomber.wav");
			StopSound(SOUND_FROM_PLAYER, SNDCHAN_AUTO, "bot/what_have_you_done.wav");
			switch(chatIndex)
			{
				case 0:
				{
					EmitSoundToAll("bot/yea_ok.wav");
					PlayerData[Client][NextFastChatSound] = nowtime + 1.2;
				}
				case 1:
				{
					EmitSoundToAll("bot/no_sir.wav");
					PlayerData[Client][NextFastChatSound] = nowtime + 1.5;
				}
				case 2:
				{
					EmitSoundToAll("bot/need_help.wav");
					PlayerData[Client][NextFastChatSound] = nowtime + 1.2;
				}
				case 3:
				{
					if(type == 1)
					{
						EmitSoundToAll("bot/im_with_you.wav");
						PlayerData[Client][NextFastChatSound] = nowtime + 1.6;
					}
					if(type == 2)
					{
						EmitSoundToAll("bot/i_see_the_bomber.wav");
						PlayerData[Client][NextFastChatSound] = nowtime + 1.4;
					}
					if(type == 3)
					{
						EmitSoundToAll("bot/i_see_our_target.wav");
						PlayerData[Client][NextFastChatSound] = nowtime + 2.1;
						g_next_fastchat_sound = nowtime + 2.1;
					}
				}
				case 4:
				{
					if(type == 1 || type == 2)
					{
						EmitSoundToAll("bot/i_could_use_some_help_over_here.wav");
						PlayerData[Client][NextFastChatSound] = nowtime + 2.1;
						g_next_fastchat_sound = nowtime + 2.1;
					}
					if(type == 3)
					{
						EmitSoundToAll("bot/come_out_wherever_you_are.wav");
						PlayerData[Client][NextFastChatSound] = nowtime + 2.1;
						g_next_fastchat_sound = nowtime + 2.1;
					}
				}
				case 5:
				{
					if(type == 1 || type == 2)
					{
						EmitSoundToAll("bot/what_have_you_done.wav");
						PlayerData[Client][NextFastChatSound] = nowtime + 1.6;
					}
				}
				case 6:
				{
					if(type == 1 || type == 2)
					{
						EmitSoundToAll("bot/nothing_here.wav");
						PlayerData[Client][NextFastChatSound] = nowtime + 1.2;
						g_next_fastchat_sound = nowtime + 1.2;
					}
				}
				case 7:
				{
					if(type == 3)
					{
						EmitSoundToAll("bot/where_are_you_hiding.wav");
						PlayerData[Client][NextFastChatSound] = nowtime + 2.9;
						g_next_fastchat_sound = nowtime + 2.9;
					}
					else
					{
						EmitSoundToAll("bot/anyone_see_them.wav");
						PlayerData[Client][NextFastChatSound] = nowtime + 1.5;
						g_next_fastchat_sound = nowtime + 1.5;
					}
				}
			}
		}
	}
	else {
		ShowFastChat2(Client);
	}
}

stock TTT_EmitDeathSound(entity)
{
	new Float:vecOrigin[3];
	GetEntPropVector(entity, Prop_Send, "m_vecOrigin", vecOrigin);
	EmitSoundToAll(g_deathsound_list[GetRandomInt(0, sizeof(g_deathsound_list)-1)], 0, SNDCHAN_AUTO, ATTN_TO_SNDLEVEL(0.7), _, _, _, _, vecOrigin);
}

stock TTT_RemainDNA(entity, attacker)
{
	if(entity < 0 || entity > 2048 || !IsValidClient(attacker))
	{
		return;
	}
	g_Entity_DNA[entity] = attacker;
}

public Native_GetKarmaScore(Handle:plugin, numParams)
{
	new Client = GetNativeCell(1);
	if(Client < 1 || Client > MaxClients) {
		return ThrowNativeError(SP_ERROR_NATIVE, "Client index %d is invalid.", Client);
	}
	if(!PlayerData[Client][DataLoaded]) {
		return 0;
	}
	return PlayerData[Client][Karma];
}

stock AddKarmaScore(Client, target)
{
	PlayerData[target][Karma] += 1;
	
	new String:SteamID[20], String:query[256];
	GetClientAuthString(target, SteamID, sizeof(SteamID));
	
	new String:Time_Year[4], String:Time_Month[4], String:Time_Day[4], year, month, day;
	FormatTime(Time_Year, 30, "%Y", GetTime());
	FormatTime(Time_Month, 30, "%m", GetTime());
	FormatTime(Time_Day, 30, "%d", GetTime());
	CalculateDays(StringToInt(Time_Year), StringToInt(Time_Month), StringToInt(Time_Day), 31, year, month, day);
	
	Format(query, sizeof(query), "INSERT INTO karma(steamid, year, month, day) values ('%s', '%d', '%d', '%d');", SteamID, year, month, day);
	SQL_TQuery(databasehandle, save_info, query, target);
	
	PrintToChat(Client, "\x04[TTT Karma] \x01Add bully score to %N", target);
	
	new String:sName[32], String:sName2[32]
	GetClientName(Client, sName, sizeof(sName));
	GetClientName(target, sName2, sizeof(sName2));
	
	new String:sEscapedName[65], String:sEscapedName2[65];
	SQL_EscapeString(databasehandle, sName, sEscapedName, sizeof(sEscapedName));
	SQL_EscapeString(databasehandle, sName2, sEscapedName2, sizeof(sEscapedName2));
	
	LogData("%s add bully %s", sEscapedName, sEscapedName2);
	
	CheckKarmaScore(target);
}

stock CheckKarmaScore(Client)
{
	if(!IsClientInGame(Client) || !PlayerData[Client][DataLoaded]) {
		return;
	}
	
	if(PlayerData[Client][Karma] >= Score_Ban_Bully)
	{
		new String:SteamID[20];
		GetClientAuthString(Client, SteamID, sizeof(SteamID));
		
		new String:query[128];
		Format(query, 128, "DELETE FROM karma WHERE steamid = '%s' limit %d;", SteamID, Score_Ban_Bully);
		SQL_TQuery(databasehandle, save_info, query, Client);
		
		if(GetFeatureStatus(FeatureType_Native, "SBBanPlayer") == FeatureStatus_Available) {
			SBBanPlayer(0, Client, 10800, "Karma Ban [ Duration : 10800mins ]");
		} else BanClient(Client, 10800, BANFLAG_AUTHID, "Karma Ban [ Duration : 10800mins ]", "You have been banned from this server\n[ Reason : Karma Ban, Duration : 10800mins ]");
	}
}

stock TTT_RemovePlayerZMCarry(Client)
{
	new ent, carry;
	while((ent = FindEntityByClassname(ent, "weapon_c4")) != -1 && IsValidEdict(ent))
	{
		if(IsZMCarry(ent))
		{
			new owner = GetOwnerEntity(ent);
			if(owner == Client)
			{
				carry = ent;
			}
		}
	}
	if(carry)
	{
		RemoveEdict(carry);
	}
}

stock TTT_ExecuteMapFunction(const String:word[])
{
	new String:sMapName[128];
	GetCurrentMap(sMapName, sizeof(sMapName));
	new String:sFunctionName[64];
	Format(sFunctionName, sizeof(sFunctionName), "%s_%s", sMapName, word);
	new Function:exectueFunction = GetFunctionByName(INVALID_HANDLE, sFunctionName);
	if(exectueFunction != INVALID_FUNCTION)
	{
		Call_StartFunction(INVALID_HANDLE, exectueFunction);
		Call_Finish();
	}
}

stock TTT_ExecuteMapFunction_ParameterInt(const String:word[], parameter)
{
	new String:sMapName[128];
	GetCurrentMap(sMapName, sizeof(sMapName));
	new String:sFunctionName[64];
	Format(sFunctionName, sizeof(sFunctionName), "%s_%s", sMapName, word);
	new Function:exectueFunction = GetFunctionByName(INVALID_HANDLE, sFunctionName);
	if(exectueFunction != INVALID_FUNCTION)
	{
		Call_StartFunction(INVALID_HANDLE, exectueFunction);
		Call_PushCell(parameter);
		Call_Finish();
	}
}

stock TTT_ExecuteMapFunction_ParameterString(const String:word[], const String:parameter[])
{
	new String:sMapName[128];
	GetCurrentMap(sMapName, sizeof(sMapName));
	new String:sFunctionName[64];
	Format(sFunctionName, sizeof(sFunctionName), "%s_%s", sMapName, word);
	new Function:exectueFunction = GetFunctionByName(INVALID_HANDLE, sFunctionName);
	if(exectueFunction != INVALID_FUNCTION)
	{
		Call_StartFunction(INVALID_HANDLE, exectueFunction);
		Call_PushString(parameter);
		Call_Finish();
	}
}

stock bool:TTT_IsTraitor(Client)
{
	return (IsValidClient(Client) && IsClientInGame(Client) && IsPlayerAlive(Client) && PlayerData[Client][DataLoaded] && PlayerData[Client][Job] == traitor);
}

stock bool:TTT_IsDetective(Client)
{
	return (IsValidClient(Client) && IsClientInGame(Client) && IsPlayerAlive(Client) && PlayerData[Client][DataLoaded] && PlayerData[Client][Job] == detective);
}

stock bool:TTT_IsJihad(weapons)
{
	return (IsC4Weapon(weapons) && !IsZMCarry(weapons) && WeaponData[weapons][IsJihad]);
}

stock bool:TTT_IsTester(weapons)
{
	return (IsC4Weapon(weapons) && !IsZMCarry(weapons) && !WeaponData[weapons][IsTraitorWeapon]);
}

stock bool:TTT_IsTeleporter(weapons)
{
	if(!IsWeapon(weapons)) {
		return false;
	}
	new String:classname[32];
	GetEdictClassname(weapons, classname, sizeof(classname));
	return StrEqual(classname, "weapon_hegrenade");
}

stock bool:TTT_IsDisguise(weapons)
{
	return (IsC4Weapon(weapons) && !IsZMCarry(weapons) && WeaponData[weapons][IsDisguise]);
}

stock bool:TTT_IsHeadCrabLauncher(weapons)
{
	return (IsSecondaryWeapon(weapons) && WeaponData[weapons][IsHeadCrabLauncher]);
}

stock TTT_CheckCreditAward(job_attacker, job_victim)
{
	new players[MAXPLAYERS], count;
	if(job_attacker == traitor && job_victim == detective) {
		for(new i=1; i<=MaxClients; i++) {
			if(!IsClientInGame(i) || !IsPlayerAlive(i)) continue;
			
			if(PlayerData[i][Job] == traitor) {
				players[count] = i;
				count++;
			}
		}
	}
	if(job_attacker == detective && job_victim == traitor) {
		for(new i=1; i<=MaxClients; i++) {
			if(!IsClientInGame(i) || !IsPlayerAlive(i)) continue;
			
			if(PlayerData[i][Job] == detective) {
				players[count] = i;
				count++;
			}
		}
	}
	for(new i; i<count; i++) {
		new client = players[i];
		PlayerData[client][Credit]++;
		SetGlobalTransTarget(client);
		PrintToChat(client, "\x04[TTT Credit] \x01-%t", "received credit for killing", JobName[job_victim]);
	}
}

stock TTT_SetProgressBar(Client, Float:fStartTime2, iDuration = -1)
{
	SetEntPropFloat(Client, Prop_Send, "m_flProgressBarStartTime", fStartTime2);
	if(iDuration != -1) {
		SetEntProp(Client, Prop_Send, "m_iProgressBarDuration", iDuration);
	}
}

stock TTT_ResetDeadMark()
{
	for(new Client=1; Client<=MaxClients; Client++)
	{
		if(!IsClientInGame(Client)) continue;
		for(new i=1; i<=MaxClients; i++)
		{
			if(!IsClientInGame(i)) {
				continue;
			}
			SetEntProp(Client, Prop_Send, "m_bPlayerDominated", false, _, i);
			SetEntProp(i, Prop_Send, "m_bPlayerDominated", false, _, Client);
		}
	}
}

stock TTT_SetPlayerDeadMark(Client)
{
	for(new i=1; i<=MaxClients; i++)
	{
		if(!IsClientInGame(i)) {
			continue;
		}
		
		// player spawned (reset mark)
		if(IsPlayerAlive(Client)) {
			SetEntProp(Client, Prop_Send, "m_bPlayerDominated", false, _, i);
			SetEntProp(i, Prop_Send, "m_bPlayerDominated", false, _, Client);
			continue;
		}
		
		// dead player or traitor can see dead mark
		if(IsPlayerAlive(i) && PlayerData[i][Job] != traitor) {
			continue;
		}
		
		new ragdoll = TTT_GetPlayerRagdoll(i);
		if(!IsPlayerAlive(i) && ragdoll != -1 && !TTT_RagdollSearched(ragdoll)) {
			SetEntProp(Client, Prop_Send, "m_bPlayerDominated", true, _, i);
		}
		
		ragdoll = TTT_GetPlayerRagdoll(Client);
		if(ragdoll != -1 && !TTT_RagdollSearched(ragdoll)) {
			SetEntProp(i, Prop_Send, "m_bPlayerDominated", true, _, Client);
		} else {
			SetEntProp(i, Prop_Send, "m_bPlayerDominated", false, _, Client);
		}
	}
}

public TTT_GetWeaponName(weapons, String:dest[], length)
{
	if(IsWeapon(weapons) == false)
	{
		Format(dest, length, "None");
		return;
	}
	
	TTT_GetWeaponName2(weapons, dest, length);
	if(strlen(dest) == 0 || StrEqual(dest, "UMP"))
	{
		GetEdictClassname(weapons, dest, length);
		ReplaceWeaponName(weapons, dest, length);
	}
	else
	{
		Format(dest, length, "%t", dest);
	}
}

stock ReplaceWeaponName(weapons, String:dest[], length)
{
	ReplaceString(dest, length, "weapon_", "");
	ReplaceString(dest, length, "knife", "Crowbar");
	ReplaceString(dest, length, "fiveseven", "FiveSeven");
	ReplaceString(dest, length, "deagle", "Deagle");
	ReplaceString(dest, length, "glock", "Glock");
	if(IsZMCarry(weapons)) {
		ReplaceString(dest, length, "c4", "Magneto-Stick");
	}
	if(TTT_IsTester(weapons)) {
		ReplaceString(dest, length, "c4", "DNA Scanner");
	}
}

stock TTT_GetWeaponName2(weapon, String:dest[], length)
{
	if(!IsWeapon(weapon)) {
		return;
	}
	Format(dest, length, WeaponData[weapon][szName]);
}

stock TTT_SetWeaponName(weapon, const String:sName[])
{
	if(!IsWeapon(weapon)) {
		return;
	}
	Format(WeaponData[weapon][szName], 32, sName);
}

public TTT_SetClientRemainCount(Client, traitor_count)
{
	g_max_weapon_limit[Client][GoldenGun] = 1;
}

stock TTT_GetClientRemainCount(Client, RemainData:type)
{
	return g_max_weapon_limit[Client][type];
}

stock TTT_IncreaseClientRemainCount(Client, RemainData:type, value)
{
	g_max_weapon_limit[Client][type] += value;
}

stock TTT_ReduceClientRemainCount(Client, RemainData:type)
{
	g_max_weapon_limit[Client][type]--;
}

public TTT_SetWeaponRemainCount(traitor_count, detective_count)
{
	g_remain_data[GoldenGun] = detective_count;
	g_remain_data[Knife] = 1+traitor_count*2/3;
	g_remain_data[AWP] = traitor_count*3/2;
	g_remain_data[SuperCrowbar] = 1;
	g_remain_data[RPG] = traitor_count;
	g_remain_data[TVirus] = 1+traitor_count/3;
	g_remain_data[HeadCrabLauncher] = 1+traitor_count/2;
}

stock TTT_GetReaminCount(RemainData:type)
{
	return g_remain_data[type];
}

stock TTT_IncreaseRemainCount(RemainData:type, value)
{
	g_remain_data[type] += value;
}

stock TTT_ReduceRemainCount(RemainData:type)
{
	g_remain_data[type]--;
}

stock TTT_GetNextWeapon(Client, iType)
{
	new hActiveWeapon = GetClientWeaponIndex(Client);
	
	new weapon = -1;
	if(iType == WEAPONTYPE_PRIMARY) {
		weapon = TTT_GetWeaponFromList(Client, WEAPONSLOT_PRIMARY_2);
		if(weapon == -1 || weapon == hActiveWeapon) {
			weapon = TTT_GetWeaponFromList(Client, WEAPONSLOT_PRIMARY_1);
		}
	}
	if(iType == WEAPONTYPE_SECONDARY) {
		weapon = TTT_GetWeaponFromList(Client, WEAPONSLOT_SECONDARY_2);
		if(weapon == -1 || weapon == hActiveWeapon) {
			weapon = TTT_GetWeaponFromList(Client, WEAPONSLOT_SECONDARY_1);
		}
	}
	if(iType == WEAPONTYPE_KNIFE) {
		weapon = TTT_GetWeaponFromList(Client, WEAPONSLOT_KNIFE_2);
		if(weapon == -1 || weapon == hActiveWeapon) {
			weapon = TTT_GetWeaponFromList(Client, WEAPONSLOT_KNIFE_1);
		}
	}
	if(iType == WEAPONTYPE_C4) {
		weapon = TTT_GetWeaponFromList(Client, WEAPONSLOT_C4_2);
		if(weapon == -1 || weapon == hActiveWeapon) {
			weapon = TTT_GetWeaponFromList(Client, WEAPONSLOT_C4_1);
		}
	}
	return weapon;
}

stock TTT_SetWeaponToList(weapons, list1, list2)
{
	if(!IsWeapon(weapons) || IsGrenadeWeapon(weapons)) {
		return;
	}
	
	new owner = GetOwnerEntity(weapons);
	if(!IsValidClient(owner) || !IsClientInGame(owner)) {
		return;
	}
	
	new weapon = TTT_GetWeaponFromList(owner, list1);
	new weapon2 = TTT_GetWeaponFromList(owner, list2);
	if(IsWeapon(weapon) && IsWeapon(weapon2))
	{
		// swap weapons
		if(weapons == weapon || weapons == weapon2)
		{
			SetEntDataEnt2(owner, Offset[m_hMyWeapons]+(list1*4), weapon2);
			SetEntDataEnt2(owner, Offset[m_hMyWeapons]+(list2*4), weapon);
		}
		// otherwise, we can't set weapon to list
		return;
	}
	
	new offset = Offset[m_hMyWeapons] + ( (!IsWeapon(weapon) || weapon == weapons) ? list1*4 : list2*4 );
	if(offset == Offset[m_hMyWeapons]+(list1*4) && weapon2 == weapons) { // list2 weapon move to list1
		SetEntDataEnt2(owner, Offset[m_hMyWeapons]+(list2*4), -1);
	}
	else // remove original weapon list
	{
		new original_list = TTT_GetListFromWeapon(owner, weapons);
		if(original_list != -1) {
			SetEntDataEnt2(owner, Offset[m_hMyWeapons]+(original_list*4), -1);
		}
	}
	SetEntDataEnt2(owner, offset, weapons);
}

stock TTT_GetListFromWeapon(owner, weapons)
{
	new return_value = -1;
	for(new i; i<48; i++)
	{
		if(GetEntDataEnt2(owner, Offset[m_hMyWeapons]+(i*4)) == weapons)
		{
			return_value = i;
			break;
		}
	}
	return return_value;
}

stock TTT_GetWeaponFromList(Client, list)
{
	return GetEntDataEnt2(Client, Offset[m_hMyWeapons]+(list*4));
}

stock bool:TTT_AutoPickupWeapon(Client, weapons)
{
	if(!IsWeapon(weapons)) {
		return false;
	}
	
	if(IsGrenadeWeapon(weapons)) {
		return true;
	}
	
	if(IsC4Weapon(weapons) && GetPlayerWeaponSlot(Client, WEAPONTYPE_C4) == -1) {
		return true;
	}
	
	if(IsKnifeWeapon(weapons) && GetPlayerWeaponSlot(Client, WEAPONTYPE_KNIFE) == -1) {
		return true;
	}
	
	if(IsSecondaryWeapon(weapons) && GetPlayerWeaponSlot(Client, WEAPONTYPE_SECONDARY) == -1) {
		return true;
	}
	
	if(IsPrimaryWeapon(weapons) && GetPlayerWeaponSlot(Client, WEAPONTYPE_PRIMARY) == -1) {
		return true;
	}
	
	return false;
}

stock bool:TTT_CanUseWeapon(Client, weapons) // if return true, weapon should be pickuped forcely.
{
	if(!IsWeapon(weapons) || IsGrenadeWeapon(weapons)) {
		return false;
	}
	
	if(IsC4Weapon(weapons))
	{
		new c4 = GetPlayerWeaponSlot(Client, WEAPONTYPE_C4);
		if(c4 == -1) {
			return true;
		}
		return TTT_CanUseWeaponType(Client, WeaponData[weapons][IsUsingSpecialSlot], WEAPONTYPE_C4);
	}
	
	if(IsKnifeWeapon(weapons))
	{
		new knife = GetPlayerWeaponSlot(Client, WEAPONTYPE_KNIFE);
		if(knife == -1) {
			return true;
		}
		return TTT_CanUseWeaponType(Client, WeaponData[weapons][IsUsingSpecialSlot], WEAPONTYPE_KNIFE);
	}
	
	if(IsSecondaryWeapon(weapons))
	{
		new pistol = GetPlayerWeaponSlot(Client, WEAPONTYPE_SECONDARY);
		if(pistol == -1) {
			return true;
		}
		return TTT_CanUseWeaponType(Client, WeaponData[weapons][IsUsingSpecialSlot], WEAPONTYPE_SECONDARY);
	}
	
	new gun = GetPlayerWeaponSlot(Client, WEAPONTYPE_PRIMARY);
	if(gun == -1) {
		return true;
	}
	return TTT_CanUseWeaponType(Client, WeaponData[weapons][IsUsingSpecialSlot], WEAPONTYPE_PRIMARY);
}

stock bool:TTT_CanUseWeaponType(Client, bool:bSpecial, iType)
{
	new weapon, weapon2;
	if(iType == WEAPONTYPE_PRIMARY) {
		weapon = TTT_GetWeaponFromList(Client, WEAPONSLOT_PRIMARY_1);
		weapon2 = TTT_GetWeaponFromList(Client, WEAPONSLOT_PRIMARY_2);
	}
	if(iType == WEAPONTYPE_SECONDARY) {
		weapon = TTT_GetWeaponFromList(Client, WEAPONSLOT_SECONDARY_1);
		weapon2 = TTT_GetWeaponFromList(Client, WEAPONSLOT_SECONDARY_2);
	}
	if(iType == WEAPONTYPE_KNIFE) {
		weapon = TTT_GetWeaponFromList(Client, WEAPONSLOT_KNIFE_1);
		weapon2 = TTT_GetWeaponFromList(Client, WEAPONSLOT_KNIFE_2);
	}
	if(iType == WEAPONTYPE_C4) {
		weapon = TTT_GetWeaponFromList(Client, WEAPONSLOT_C4_1);
		weapon2 = TTT_GetWeaponFromList(Client, WEAPONSLOT_C4_2);
	}
	
	if(weapon != -1 && weapon2 != -1) { // fulled
		return false;
	}
	
	// check already have same slot
	if(weapon != -1 && WeaponData[weapon][IsUsingSpecialSlot] == bSpecial) {
		return false;
	}
	if(weapon2 != -1 && WeaponData[weapon2][IsUsingSpecialSlot] == bSpecial) {
		return false;
	}
	
	return true;
}

public TTT_OnPlayerRespawned(Client)
{
	RemoveAllItems(Client, true);
	GivePlayerItem(Client, "weapon_fiveseven");
	GivePlayerItem(Client, "weapon_knife");
	GivePlayerItem(Client, "weapon_zm_carry");
	SetEntProp(Client, Prop_Send, "m_bHasHelmet", 1);
	SetEntProp(Client, Prop_Send, "m_iAccount", 800);
	//PlayerData[Client][ExtraHealth] = Shop_GetExtraHealth(Client);
	SetEntityHealth(Client, 100+PlayerData[Client][ExtraHealth]);
	SetEntityRenderColor(Client, 255, 255, 255, 255);
	TTT_SetPlayerDeadMark(Client);
	IncrementFragCount(Client, 1-GetClientFrags(Client));
}

public TTT_BuyItem(Client, const String:info[])
{
	SetGlobalTransTarget(Client);
	
	if(!PlayerData[Client][Credit])
	{
		PrintToChat(Client, "\x0748D1CC%T", "Not Enough Credit", Client);
		return;
	}
	
	SetAllowDrop(Client, false);
	
	if(StrEqual(info, "Body Armor", false))
	{
		SetClientArmorValue(Client, 100);
	}
	
	if(StrEqual(info, "Iron Shoes", false))
	{
		PlayerData[Client][HasIronShoes] = true;
	}
	
	if(StrEqual(info, "Knife", false))
	{
		if(!TTT_CanUseWeaponType(Client, true, WEAPONTYPE_KNIFE)) {
			return;
		}
		new weapons = GivePlayerItem(Client, "weapon_knife");
		WeaponData[weapons][IsKnife] = true;
		WeaponData[weapons][IsUsingSpecialSlot] = true;
		EquipPlayerWeapon(Client, weapons);
		TTT_SetWeaponName(weapons, info);
		
		TTT_ReduceRemainCount(Knife);
		TTT_ReduceClientRemainCount(Client, Knife);
	}
	
	if(StrEqual(info, "Health Station", false))
	{
		GivePlayerItem(Client, "weapon_ttt_healthstation");
	}
	
	if(StrEqual(info, "Speed Boots", false))
	{
		PlayerData[Client][HasSpeedBoots] = true;
	}
	
	if(StrEqual(info, "Night Vision", false))
	{
		PlayerData[Client][HasNightVision] = true;
		SpawnNightVision(Client);
		PrintToChat(Client, "\x04[TTT Help] \x01You got NightVision. it can switch by \x07FFFFFFShift+N.");
	}
	
	if(StrEqual(info, "Death Station", false))
	{
		GivePlayerItem(Client, "weapon_ttt_deathstation");
	}
	
	if(StrEqual(info, "Super Crowbar", false))
	{
		if(!TTT_CanUseWeaponType(Client, true, WEAPONTYPE_KNIFE)) {
			return;
		}
		new weapons = GivePlayerItem(Client, "weapon_knife");
		WeaponData[weapons][IsSuperCrowbar] = true;
		WeaponData[weapons][IsUsingSpecialSlot] = true;
		EquipPlayerWeapon(Client, weapons);
		TTT_SetWeaponName(weapons, info);
		
		TTT_ReduceRemainCount(SuperCrowbar);
		TTT_ReduceClientRemainCount(Client, SuperCrowbar);
	}
	
	if(StrEqual(info, "Golden Gun", false))
	{
		if(!TTT_CanUseWeaponType(Client, true, WEAPONTYPE_SECONDARY)) {
			return;
		}
		SetAllowDrop(Client, false);
		
		new weapons = GivePlayerItem(Client, "weapon_deagle");
		WeaponData[weapons][IsGoldenGun] = true;
		WeaponData[weapons][IsDetectiveWeapon] = true;
		WeaponData[weapons][IsUsingSpecialSlot] = true;
		SetEntProp(weapons, Prop_Send, "m_iClip1", 1);
		SetEntProp(weapons, Prop_Send, "m_iPrimaryAmmoType", 0);
		TTT_SetWeaponName(weapons, info);
		
		SetAllowDrop(Client, false);
		EquipPlayerWeapon(Client, weapons);
		
		TTT_ReduceRemainCount(GoldenGun);
		TTT_ReduceClientRemainCount(Client, GoldenGun);
	}
	
	if(StrEqual(info, "SG552", false))
	{
		if(!TTT_CanUseWeaponType(Client, false, WEAPONTYPE_PRIMARY)) {
			return;
		}
		SetBlockSwtich(Client, true);
		
		new weapons = GivePlayerItem(Client, "weapon_sg552");
		WeaponData[weapons][IsDetectiveWeapon] = true;
		
		SetAllowDrop(Client, false);
		SetBlockSwtich(Client, true);
		EquipPlayerWeapon(Client, weapons);
	}
	
	if(StrEqual(info, "Teleporter", false))
	{
		new weapons = GivePlayerItem(Client, "weapon_ttt_teleport");
		TTT_SetWeaponName(weapons, info);
	}
	
	if(StrEqual(info, "Crossbow", false))
	{
		if(!TTT_CanUseWeaponType(Client, true, WEAPONTYPE_PRIMARY)) {
			return;
		}
		SetBlockSwtich(Client, true);
		
		new weapons = GivePlayerItem(Client, "weapon_crossbow");
		WeaponData[weapons][IsDetectiveWeapon] = true;
		WeaponData[weapons][IsUsingSpecialSlot] = true;
		Weapon_SendProxy(weapons);
		EquipPlayerWeapon(Client, weapons);
		SetClientAmmo(weapons, Client, 24);
		TTT_SetWeaponName(weapons, info);
		SetAllowDrop(Client, false);
	}
	
	if(StrEqual(info, "StunGun", false))
	{
		if(!TTT_CanUseWeaponType(Client, true, WEAPONTYPE_PRIMARY)) {
			return;
		}
		SetBlockSwtich(Client, true);
		
		new weapons = GivePlayerItem(Client, "weapon_ttt_stungun");
		WeaponData[weapons][IsDetectiveWeapon] = true;
		WeaponData[weapons][IsUsingSpecialSlot] = true;
		EquipPlayerWeapon(Client, weapons);
		TTT_SetWeaponName(weapons, info);
		SetAllowDrop(Client, false);
	}
	
	if(StrEqual(info, "AWP", false))
	{
		if(!TTT_CanUseWeaponType(Client, false, WEAPONTYPE_PRIMARY)) {
			return;
		}
		SetBlockSwtich(Client, true);
		
		new weapons = GivePlayerItem(Client, "weapon_awp");
		WeaponData[weapons][IsTraitorWeapon] = true;
		SetEntProp(weapons, Prop_Send, "m_iClip1", 4);
		SetEntProp(weapons, Prop_Send, "m_iPrimaryAmmoType", 0);
		TTT_RemainDNA(weapons, Client);
		
		SetAllowDrop(Client, false);
		SetBlockSwtich(Client, true);
		EquipPlayerWeapon(Client, weapons);
		
		TTT_ReduceRemainCount(AWP);
		TTT_ReduceClientRemainCount(Client, AWP);
	}
	
	if(StrEqual(info, "Silenced Pistol", false))
	{
		if(!TTT_CanUseWeaponType(Client, true, WEAPONTYPE_SECONDARY)) {
			return;
		}
		SetBlockSwtich(Client, true);
		
		new weapons = GivePlayerItem(Client, "weapon_usp");
		WeaponData[weapons][IsSilencedPistol] = true;
		WeaponData[weapons][IsTraitorWeapon] = true;
		WeaponData[weapons][IsUsingSpecialSlot] = true;
		TTT_RemainDNA(weapons, Client);
		TTT_SetWeaponName(weapons, info);
		
		SetAllowDrop(Client, false);
		SetBlockSwtich(Client, true);
		EquipPlayerWeapon(Client, weapons);
	}
	
	if(StrEqual(info, "Turret", false))
	{
		GivePlayerItem(Client, "weapon_ttt_turret");
	}
	
	if(StrEqual(info, "Jihad", false))
	{
		if(!TTT_CanUseWeaponType(Client, true, WEAPONTYPE_C4)) {
			return;
		}
		SetBlockSwtich(Client, true);
		
		new weapons = GivePlayerItem(Client, "weapon_c4");
		WeaponData[weapons][IsJihad] = true;
		WeaponData[weapons][IsTraitorWeapon] = true;
		WeaponData[weapons][IsUsingSpecialSlot] = true;
		Weapon_ResetModel(weapons);
		TTT_RemainDNA(weapons, Client);
		TTT_SetWeaponName(weapons, info);
		
		SetAllowDrop(Client, false);
		SetBlockSwtich(Client, true);
		EquipPlayerWeapon(Client, weapons);
	}
	
	if(StrEqual(info, "RPG-7", false))
	{
		if(!TTT_CanUseWeaponType(Client, true, WEAPONTYPE_PRIMARY)) {
			return;
		}
		SetBlockSwtich(Client, true);
		
		new weapons = GivePlayerItem(Client, "weapon_rpg");
		WeaponData[weapons][IsTraitorWeapon] = true;
		WeaponData[weapons][IsUsingSpecialSlot] = true;
		Weapon_SendProxy(weapons);
		SetEntProp(weapons, Prop_Send, "m_iPrimaryAmmoType", 0);
		TTT_RemainDNA(weapons, Client);
		TTT_SetWeaponName(weapons, info);
		
		SetAllowDrop(Client, false);
		SetBlockSwtich(Client, true);
		EquipPlayerWeapon(Client, weapons);
		
		TTT_ReduceRemainCount(RPG);
		TTT_ReduceClientRemainCount(Client, RPG);
	}
	
	if(StrEqual(info, "T-Virus", false))
	{
		if(!TTT_CanUseWeaponType(Client, true, WEAPONTYPE_PRIMARY)) {
			return;
		}
		SetBlockSwtich(Client, true);
		
		new weapons = GivePlayerItem(Client, "weapon_ttt_tvirus");
		WeaponData[weapons][IsTraitorWeapon] = true;
		WeaponData[weapons][IsUsingSpecialSlot] = true;
		Weapon_SendProxy(weapons);
		SetEntProp(weapons, Prop_Send, "m_iClip1", 1);
		SetEntProp(weapons, Prop_Send, "m_iPrimaryAmmoType", 0);
		TTT_RemainDNA(weapons, Client);
		TTT_SetWeaponName(weapons, info);
		
		SetAllowDrop(Client, false);
		SetBlockSwtich(Client, true);
		EquipPlayerWeapon(Client, weapons);
		
		TTT_ReduceRemainCount(TVirus);
		TTT_ReduceClientRemainCount(Client, TVirus);
	}
	
	if(StrEqual(info, "Flare Gun", false))
	{
		if(!TTT_CanUseWeaponType(Client, true, WEAPONTYPE_SECONDARY)) {
			return;
		}
		SetBlockSwtich(Client, true);
		
		new weapons = GivePlayerItem(Client, "weapon_deagle");
		WeaponData[weapons][IsFlareGun] = true;
		WeaponData[weapons][IsTraitorWeapon] = true;
		WeaponData[weapons][IsUsingSpecialSlot] = true;
		SetEntProp(weapons, Prop_Send, "m_iClip1", 4);
		SetEntProp(weapons, Prop_Send, "m_iPrimaryAmmoType", 0);
		TTT_RemainDNA(weapons, Client);
		TTT_SetWeaponName(weapons, info);
		
		SetAllowDrop(Client, false);
		SetBlockSwtich(Client, true);
		EquipPlayerWeapon(Client, weapons);
	}
	
	if(StrEqual(info, "Newton Launcher", false))
	{
		if(!TTT_CanUseWeaponType(Client, true, WEAPONTYPE_PRIMARY)) {
			return;
		}
		SetBlockSwtich(Client, true);
		
		new weapons = GivePlayerItem(Client, "weapon_ttt_push");
		WeaponData[weapons][IsTraitorWeapon] = true;
		WeaponData[weapons][IsUsingSpecialSlot] = true;
		TTT_RemainDNA(weapons, Client);
		SetEntProp(weapons, Prop_Send, "m_iClip1", 120);
		SetEntProp(weapons, Prop_Send, "m_iPrimaryAmmoType", 0);
		TTT_SetWeaponName(weapons, info);
		
		SetAllowDrop(Client, false);
		SetBlockSwtich(Client, true);
		EquipPlayerWeapon(Client, weapons);
	}
	
	if(StrEqual(info, "Disguise", false))
	{
		if(!TTT_CanUseWeaponType(Client, true, WEAPONTYPE_C4)) {
			return;
		}
		SetBlockSwtich(Client, true);
		
		new weapons = GivePlayerItem(Client, "weapon_c4");
		WeaponData[weapons][IsDisguise] = true;
		WeaponData[weapons][IsTraitorWeapon] = true;
		WeaponData[weapons][IsUsingSpecialSlot] = true;
		Weapon_ResetModel(weapons);
		TTT_RemainDNA(weapons, Client);
		TTT_SetWeaponName(weapons, info);
		
		SetAllowDrop(Client, false);
		SetBlockSwtich(Client, true);
		EquipPlayerWeapon(Client, weapons);
		
		Disguise_AddRemainEnergy(Client, 1500);
	}
	
	if(StrEqual(info, "HeadCrab Launcher", false))
	{
		if(!TTT_CanUseWeaponType(Client, true, WEAPONTYPE_SECONDARY)) {
			return;
		}
		SetBlockSwtich(Client, true);
		
		new weapons = GivePlayerItem(Client, "weapon_usp");
		WeaponData[weapons][IsHeadCrabLauncher] = true;
		WeaponData[weapons][IsTraitorWeapon] = true;
		WeaponData[weapons][IsUsingSpecialSlot] = true;
		TTT_RemainDNA(weapons, Client);
		SetEntProp(weapons, Prop_Send, "m_iClip1", 1);
		SetEntProp(weapons, Prop_Send, "m_iPrimaryAmmoType", 0);
		TTT_SetWeaponName(weapons, info);
		
		SetAllowDrop(Client, false);
		SetBlockSwtich(Client, true);
		EquipPlayerWeapon(Client, weapons);
		
		TTT_ReduceRemainCount(HeadCrabLauncher);
	}
	
	new String:sName[32];
	GetClientName(Client, sName, sizeof(sName));
	
	PlayerData[Client][Credit]--;
	PrintToChat(Client, "\x0748D1CC%t", "Credit_Buy", info, 1, "Credit", sName);
	ClientCommand(Client, "play ambient/levels/labs/coinslot1");
	
	new sourceTV = GetSourceTVIndex();
	if(sourceTV == -1)
		return;
	
	SetGlobalTransTarget(sourceTV);
	PrintToChat(sourceTV, "\x0748D1CC%t", "Credit_Buy", info, 1, "Credit", sName);
}

public TTT_SetPlayerJob(Client, job)
{
	PlayerData[Client][Job] = job;
	RemoveReference(PlayerData[Client][h_Sprite_Player]);
	
	//Show Player Sprite
	new Float:vecSpritePosition[3];
	GetClientAbsOrigin(Client, vecSpritePosition);
	vecSpritePosition[2] += 25.0;
	if(job == traitor)
	{
		ClientCommand(Client, "r_screenoverlay \"%s\"", "troubletown/icon_traitor_overlay.vmt");
		PrintHintText(Client, "You are traitor.\nKill innocents and detectives with other traitors.");
		new sprite = CreateSprite(vecSpritePosition, "troubletown/icon_traitor.vmt", 0.3, true, Client);
		SetEntPropEnt(sprite, Prop_Send, "m_hOwnerEntity", Client);
		PlayerData[Client][h_Sprite_Player] = EntIndexToEntRef(sprite);
		SDKHook(sprite, SDKHook_SetTransmit, Hook_SetTransmit_TraitorSprite);
		//PerformFade(Client, 5.0, 255, 0, 0, 32);
		
		PlayerData[Client][Credit] = 3;
	}
	if(job == detective)
	{
		ClientCommand(Client, "r_screenoverlay \"%s\"", "troubletown/icon_detective_overlay.vmt");
		PrintHintText(Client, "You are detective.\nFind traitors and Kill them.");
		new sprite = CreateSprite(vecSpritePosition, "troubletown/icon_detective.vmt", 0.2, true, Client);
		SetEntPropEnt(sprite, Prop_Send, "m_hOwnerEntity", Client);
		PlayerData[Client][h_Sprite_Player] = EntIndexToEntRef(sprite);
		SDKHook(sprite, SDKHook_SetTransmit, Hook_SetTransmit_DetectiveSprite);
		//PerformFade(Client, 5.0, 0, 0, 255, 32);
		
		new weapons = GivePlayerItem(Client, "weapon_ttt_wtester");
		WeaponData[weapons][IsUsingSpecialSlot] = true;
		
		PlayerData[Client][Credit] = 5;
		TTT_BuyItem(Client, "Body Armor");
		TTT_BuyItem(Client, "Iron Shoes");
	}
	if(job == innocent)
	{
		ClientCommand(Client, "r_screenoverlay \"%s\"", "troubletown/icon_innocent_overlay.vmt");
		//PerformFade(Client, 5.0, 0, 255, 0, 32);
	}
}

stock TTT_GetEntityFromAim(Client, bool:force = false)
{
	static target[MAXPLAYERS+1], Float:showTime[MAXPLAYERS+1];
	
	new Float:vecEyePosition[3], Float:vecAngles[3];
	GetClientEyePosition(Client, vecEyePosition);
	GetClientEyeAngles(Client, vecAngles);
	
	new pass_entity = (GetEntProp(Client, Prop_Send, "m_iObserverMode") == 4) ? GetEntPropEnt(Client, Prop_Send, "m_hObserverTarget") : Client;
	TR_TraceRayFilter(vecEyePosition, vecAngles, MASK_SOLID, RayType_Infinite, TraceFilter_OneEntity, pass_entity);
	
	if(TR_DidHit() == false)
	{
		target[Client] = -1;
		return -1;
	}
	
	new entity = TR_GetEntityIndex();
	if((entity >= 1 && entity <= MaxClients) || IsRagdollEntity(entity) || IsHealthStationEntity(entity) || IsDeathStationEntity(entity) || g_Entity_DNA[entity])
	{
		if(target[Client] != entity || FloatSub(GetEngineTime(), showTime[Client]) > 1.0 || force)
		{
			target[Client] = entity;
			if(!force) showTime[Client] = GetEngineTime();
			return entity;
		}
		else
		{
			return -1;
		}
	}
	
	new Float:vecEndPosition[3];
	TR_GetEndPosition(vecEndPosition);
	
	TR_TraceHullFilter(vecEyePosition, vecEndPosition, Float:{-4.0, -4.0, -8.0}, Float:{4.0, 4.0, 0.0}, MASK_SOLID, TraceFilter_OneEntity, pass_entity);
	if(TR_DidHit() == false)
	{
		target[Client] = entity;
		return -1;
	}
	
	new entity2 = TR_GetEntityIndex();
	if(entity2 >= 1 && entity2 <= MaxClients)
	{
		if(target[Client] != entity2 || FloatSub(GetEngineTime(), showTime[Client]) > 1.0 || force)
		{
			target[Client] = entity2;
			if(!force) showTime[Client] = GetEngineTime();
			return entity2;
		}
	}
	target[Client] = entity;
	return -1;
}

stock bool:TTT_GetRagdollPlayerName(ragdoll, String:dest[], length)
{
	new Handle:h_trie = TTT_GetRagdollTrie(ragdoll);
	if(h_trie == INVALID_HANDLE)
		return false;
	
	return GetTrieString(h_trie, "playername", dest, length);
}

stock bool:TTT_GetRagdollJobName(ragdoll, String:dest[], length)
{
	new Handle:h_trie = TTT_GetRagdollTrie(ragdoll);
	if(h_trie == INVALID_HANDLE)
		return false;
	
	return GetTrieString(h_trie, "jobname", dest, length);
}

stock bool:TTT_GetRagdollReason(ragdoll, String:dest[], length)
{
	new Handle:h_trie = TTT_GetRagdollTrie(ragdoll);
	if(h_trie == INVALID_HANDLE)
		return false;
	
	return GetTrieString(h_trie, "reason", dest, length);
}

stock bool:TTT_GetRagdollReason2(ragdoll, String:dest[], length)
{
	new Handle:h_trie = TTT_GetRagdollTrie(ragdoll);
	if(h_trie == INVALID_HANDLE)
		return false;
	
	return GetTrieString(h_trie, "reason2", dest, length);
}

stock bool:TTT_GetRagdollWeaponName(ragdoll, String:dest[], length)
{
	new Handle:h_trie = TTT_GetRagdollTrie(ragdoll);
	if(h_trie == INVALID_HANDLE)
		return false;
	
	return GetTrieString(h_trie, "weaponname", dest, length);
}

stock Float:TTT_GetRagdollDeathTime(ragdoll)
{
	new Handle:h_trie = TTT_GetRagdollTrie(ragdoll);
	if(h_trie == INVALID_HANDLE)
		return 0.0;
	
	new Float:time;
	if(GetTrieValue(h_trie, "deathtime", time) == true)
		return GetEngineTime()-time;
	return 0.0;
}

stock TTT_GetRagdollCredit(ragdoll)
{
	new Handle:h_trie = TTT_GetRagdollTrie(ragdoll);
	if(h_trie == INVALID_HANDLE)
		return 0;
	
	new credit;
	if(GetTrieValue(h_trie, "credit", credit) == true)
		return credit;
	return 0;
}

stock TTT_SetRagdollCredit(ragdoll, credit)
{
	new Handle:h_trie = TTT_GetRagdollTrie(ragdoll);
	if(h_trie == INVALID_HANDLE)
		return;
	
	SetTrieValue(h_trie, "credit", credit);
}

stock TTT_GetRagdollDNA(ragdoll)
{
	new Handle:h_trie = TTT_GetRagdollTrie(ragdoll);
	if(h_trie == INVALID_HANDLE)
		return 0;
	
	// dna was death...
	if(TTT_GetRagdollDeathTime(ragdoll) > 180.0)
		return 0;
	
	new dna;
	if(GetTrieValue(h_trie, "dna", dna) == true)
		return dna;
	return 0;
}

stock TTT_RemoveRagdollDNA(ragdoll)
{
	new Handle:h_trie = TTT_GetRagdollTrie(ragdoll);
	if(h_trie == INVALID_HANDLE)
		return;
	
	RemoveFromTrie(h_trie, "dna");
}

stock bool:TTT_GetRagdollKillList(ragdoll, String:dest[], length)
{
	new Handle:h_trie = TTT_GetRagdollTrie(ragdoll);
	if(h_trie == INVALID_HANDLE)
		return false;
	
	return GetTrieString(h_trie, "list", dest, length);
}

stock bool:TTT_RagdollSearched(ragdoll)
{
	new Handle:h_trie = TTT_GetRagdollTrie(ragdoll);
	if(h_trie == INVALID_HANDLE)
		return false;
	
	new search;
	if(GetTrieValue(h_trie, "search", search) == true)
		return bool:search;
	return false;
}

stock bool:TTT_RagdollIsHeadshot(ragdoll)
{
	new Handle:h_trie = TTT_GetRagdollTrie(ragdoll);
	if(h_trie == INVALID_HANDLE)
		return false;
	
	new isHeadshot;
	if(GetTrieValue(h_trie, "headshot", isHeadshot) == true)
		return bool:isHeadshot;
	return false;
}

stock TTT_RagdollSearch(ragdoll, Client)
{
	//to show ragdoll info menu need ragdoll data
	new Handle:h_trie = TTT_GetRagdollTrie(ragdoll);
	if(h_trie == INVALID_HANDLE)
		return;
	
	new Float:vecPlayerOrigin[3], Float:vecRagdollOrigin[3];
	GetClientAbsOrigin(Client, vecPlayerOrigin);
	GetEntPropVector(ragdoll, Prop_Send, "m_vecOrigin", vecRagdollOrigin);
	
	//too far from ragdoll
	if(GetVectorDistance(vecPlayerOrigin, vecRagdollOrigin) > RAGDOLL_SEARCH_DIST && Client != GetSourceTVIndex())
		return;
	
	ShowRagdollInfo(ragdoll, Client);
	
	//already searched or player is dead
	if(TTT_RagdollSearched(ragdoll) == true || (!IsPlayerAlive(Client) && Client != GetSourceTVIndex()))
		return;
	
	new buttons = GetClientButtons(Client);
	if(buttons & IN_DUCK) // search ragdoll silently
		return;
	
	new String:ragdollName[32], String:jobName[16];
	TTT_GetRagdollPlayerName(ragdoll, ragdollName, sizeof(ragdollName));
	TTT_GetRagdollJobName(ragdoll, jobName, sizeof(jobName));
	PrintToChatAll("\x07FF3A3A%N%t", Client, "found the body", ragdollName, jobName, "\x07FFFF3A", "\x07FF3A3A", "\x07FFFF3A", "\x07FFFFFF", "\x07FFFF3A");
	SaveEventLog(2, "%N%t", Client, "found the body", ragdollName, jobName, "", "", "", "", "");
	SetTrieValue(h_trie, "search", true);
	
	TTT_RoundEndCheck();
	
	for(new i=1; i<= MaxClients; i++)
	{
		if(!IsClientInGame(i) || GetClientTeam(i) < 2 || IsPlayerAlive(i)) continue;
		
		if(ragdoll == TTT_GetPlayerRagdoll(i))
		{
			IncrementFragCount(i, 0-GetClientFrags(i)); // decrease score
			TTT_SetPlayerDeadMark(i);
		}
		if(StrEqual(ragdollName, PlayerData[i][sDisguisedName]))
		{
			PlayerData[i][sDisguisedName] = '\0';
			SayText2Message(i, i, "\x04[TTT Disguise] \x01Ragdoll Searched. Change Nickname to '\x03Hide NickName\x01'.")
		}
	}
}

public Handle:TTT_GetRagdollTrie(ragdoll)
{
	if(ragdoll <= 0 || IsRagdollEntity(ragdoll) == false)
		return INVALID_HANDLE;
	
	new number = g_ragdoll_trie_number[ragdoll];
	return g_RagdollData_Trie[number];
}

public Handle:TTT_GetRagdollKillListTrie(ragdoll)
{
	if(ragdoll <= 0 || IsRagdollEntity(ragdoll) == false)
		return INVALID_HANDLE;
	
	new number = g_ragdoll_trie_number[ragdoll];
	return g_RagdollKillList_Trie[number];
}

public TTT_SetRagdollData(ragdoll, Handle:event)
{
	new number = g_ragdoll_count+1, Handle:h_new_trie = CreateTrie();
	new Client = GetClientFromEvent(event), Attacker = GetAttackerFromEvent(event), bool:isHeadshot = GetEventBool(event,"headshot"), String:sName[256], bool:auto_search;
	GetClientName(Client, sName, sizeof(sName));
	SetTrieString(h_new_trie, "playername", sName);
	SetTrieString(h_new_trie, "jobname", JobName[PlayerData[Client][Job]]);
	SetTrieValue(h_new_trie, "deathtime", GetEngineTime());
	SetTrieValue(h_new_trie, "credit", PlayerData[Client][Credit]);
	new String:sWeaponName[32];
	GetEventString(event, "weapon", sWeaponName, sizeof(sWeaponName));
	SetTrieString(h_new_trie, "weaponname", sWeaponName);
	SetTrieValue(h_new_trie, "dna", Attacker);
	if(!Attacker || StrEqual(sWeaponName, "worldspawn")) // fall damaged
	{
		SetTrieString(h_new_trie, "reason", "He fell to his death");
		RemoveFromTrie(h_new_trie, "dna");
	}
	else if(StrEqual(sWeaponName, "knife")) // knife damaged
	{
		SetTrieString(h_new_trie, "reason", "The body is bruised and battered. Clearly he was clubbed to death");
		new weapons = GetClientWeaponIndex(Attacker);
		if(weapons != -1 && WeaponData[weapons][IsKnife]) {
			RemoveFromTrie(h_new_trie, "dna");
			RemovePlayerItem(Attacker, weapons);
			RemoveEdict(weapons);
		}
	}
	else if(StrEqual(sWeaponName, "rpg_missile", false) || StrEqual(sWeaponName, "jihadbomb", false) || StrEqual(sWeaponName, "Arrow's Explosion", false)) // exploded
	{
		SetTrieString(h_new_trie, "reason", "Smells like roasted terrorist around here");
		SetTrieString(h_new_trie, "reason2", "It appears a weapon was used to kill him");
		RemoveFromTrie(h_new_trie, "dna");
		IgniteEntity(ragdoll, 5.0, _, 5.0);
		SetVariantString("40 30 10");
		AcceptEntityInput(ragdoll, "Color");
	}
	else if(Attacker == Client || StrEqual(sWeaponName, "ttt_death_station", false)) // unknown
	{
		SetTrieString(h_new_trie, "reason", "You cannot find a specific cause of this terrorist's death");
		RemoveFromTrie(h_new_trie, "dna");
		if(Attacker == Client)
		{
			PrintToChatAll("\x04[Trouble Town] \x03%N\x01 is suicided.", Client);
			auto_search = true;
			RemoveFromTrie(h_new_trie, "credit");
		}
	}
	else // shoted
	{
		SetTrieString(h_new_trie, "reason", "It is obvious he was shot to death");
		SetTrieString(h_new_trie, "reason2", "It appears a weapon was used to kill him");
		if(GetDistanceEntityTo(Attacker, Client) >= 1666.6) // too long to remain dna.
		{
			RemoveFromTrie(h_new_trie, "dna");
		}
		
		//change weapon name
		if(StrEqual(sWeaponName, "famas", false))
		{
			SetTrieString(h_new_trie, "weaponname", "Newton Launcher");
		}
	}
	if(PlayerData[Client][Job] == traitor)
	{
		RemoveFromTrie(h_new_trie, "dna");
	}
	SetTrieValue(h_new_trie, "headshot", isHeadshot);
	SetTrieValue(h_new_trie, "search", false);
	
	if(PlayerData[Client][Job] == traitor && GetArraySize(g_traitor_kill_log[Client])) {
		SetTrieString(h_new_trie, "list", "You found a list of kills with these names");
		g_RagdollKillList_Trie[number] = CreateTrie();
		new String:s_number[8], String:buffer[32];
		for(new i; i<GetArraySize(g_traitor_kill_log[Client]); i++)
		{
			GetArrayString(g_traitor_kill_log[Client], i, buffer, sizeof(buffer));
			IntToString(i, s_number, sizeof(s_number));
			SetTrieString(g_RagdollKillList_Trie[number], s_number, buffer);
		}
	}
	
	g_RagdollData_Trie[number] = h_new_trie;
	g_ragdoll_trie_number[ragdoll] = number;
	g_ragdoll_count++;
	
	if(auto_search)
	{
		TTT_RagdollSearch(ragdoll, GetSourceTVIndex());
	}
}

public TTT_GetPlayerRagdoll(Client)
{
	if(!IsValidClient(Client) || !IsClientInGame(Client) || GetClientTeam(Client) < 2 || IsPlayerAlive(Client))
		return -1;
	
	new ragdoll = EntRefToEntIndex(PlayerData[Client][hRagdoll]);
	if(TTT_GetRagdollTrie(ragdoll) == INVALID_HANDLE)
		return -1;
	
	return ragdoll;
}

public TTT_PrintAllLog()
{
	PrintToChatAll("\x04[Trouble Town] \x01Can see All Log in Console.");
	new String:buffer[128];
	for(new i; i<GetArraySize(g_damage_all_log); i++)
	{
		GetArrayString(g_damage_all_log, i, buffer, sizeof(buffer));
		for(new j=1; j<=MaxClients; j++)
		{
			if(!IsClientInGame(j)) continue;
			
			PrintToConsole(j, buffer);
		}
	}
	for(new j=1; j<=MaxClients; j++)
	{
		if(!IsClientInGame(j)) continue;
		
		ShowDamageReport(j);
	}
}

public TTT_AddPlayerLog(Attacker, Client, const String:Log[128])
{
	if(RoundType == RoundState_GameReady)
		return;
	
	if(IsValidClient(Attacker)) PushArrayString(g_attack_log[Attacker][0], Log);
	PushArrayString(g_damage_log[Client][0], Log);
	PushArrayString(g_damage_all_log, Log);
}

public TTT_UpdateLog(Client)
{
	ClearArray(g_damage_log[Client][1]);
	ClearArray(g_attack_log[Client][1]);
	ClearArray(g_traitor_kill_log[Client]);
	
	new String:buffer[128];
	for(new i; i<GetArraySize(g_attack_log[Client][0]); i++)
	{
		GetArrayString(g_attack_log[Client][0], i, buffer, sizeof(buffer));
		PushArrayString(g_attack_log[Client][1], buffer);
	}
	for(new i; i<GetArraySize(g_damage_log[Client][0]); i++)
	{
		GetArrayString(g_damage_log[Client][0], i, buffer, sizeof(buffer));
		PushArrayString(g_damage_log[Client][1], buffer);
	}
	
	ClearArray(g_damage_log[Client][0]);
	ClearArray(g_attack_log[Client][0]);
}

stock TTT_ClearPlayerLog(Client)
{
	for(new i; i<2; i++)
	{
		if(g_damage_log[Client][i] != INVALID_HANDLE) {
			ClearArray(g_damage_log[Client][i]);
		}
		if(g_attack_log[Client][i] != INVALID_HANDLE) {
			ClearArray(g_attack_log[Client][i]);
		}
	}
}

stock bool:TTT_CanPlayerBuy()
{
	if(RoundType == RoundState_GameReady) {
		return true;
	}
	
	new time = GetGameCount();
	if(time < START_GAME_TIME-CAN_BUY_TIME) { // buytime elapsed
		return false;
	}
	return true;
}

stock TTT_RoundEndCheck()
{
	//game wasn't started, or already finished.
	if(RoundType != RoundState_GamePlay) {
		return;
	}
	
	new endtype = 0;
	new temp_count[4];//0 == innocent & detective, 1 == traitor)
	for(new i=1; i<=MaxClients; i++)
	{
		if(!IsClientInGame(i) || GetClientTeam(i) < 2) continue;
		if(PlayerData[i][Job] == unselected) continue;
		
		new ragdoll = TTT_GetPlayerRagdoll(i);
		if(IsPlayerAlive(i))
		{
			if(PlayerData[i][Job] == traitor)
			{
				temp_count[1]++;
			}
			else
			{
				temp_count[0]++;
			}
		}
		else if(ragdoll != -1 && !TTT_RagdollSearched(ragdoll))
		{
			if(PlayerData[i][Job] == traitor)
			{
				temp_count[3]++;
			}
			else
			{
				temp_count[2]++;
			}
		}
	}
	for(new x; x<2; x++) g_survivor_count[x] = temp_count[x] + temp_count[x+2];
	
	if(!temp_count[0])
	{
		if(temp_count[1])
			endtype = 3; //traitor win
		else
			endtype = 1; //round draw, can't be occured.
	}
	else if(!temp_count[1])
		endtype = 2; //innocent win
	
	switch (endtype)
	{
		case 1: //round draw
		{
			CS_TerminateRound(10.0, CSRoundEnd_Draw);
		}
		case 2: //innocent win
		{
			CS_TerminateRound(10.0, CSRoundEnd_TargetSaved);
		}
		case 3: //traitor win
		{
			CS_TerminateRound(10.0, CSRoundEnd_TargetBombed);
		}
	}
}

stock TTT_ResetGameData()
{
	for(new i=1; i<=g_ragdoll_count; i++)
	{
		CloseHandle(g_RagdollData_Trie[i]);
		g_RagdollData_Trie[i] = INVALID_HANDLE;
		if(g_RagdollKillList_Trie[i] != INVALID_HANDLE)
		{
			CloseHandle(g_RagdollKillList_Trie[i]);
			g_RagdollKillList_Trie[i] = INVALID_HANDLE;
		}
	}
	g_ragdoll_count = 0;
	g_survivor_count[0] = g_survivor_count[1] = 0;
}

stock TTT_ResetPlayerData(Client)
{
	PlayerData[Client][Job] = unselected;
	PlayerData[Client][Credit] = 0;
	PlayerData[Client][AllowDrop] = false;
	PlayerData[Client][BuyDrop] = false;
	PlayerData[Client][HasIronShoes] = false;
	PlayerData[Client][HasSpeedBoots] = false;
	PlayerData[Client][HasNightVision] = false;
	PlayerData[Client][bExecuteJihad] = false;
	PlayerData[Client][bExecuteDisguise] = false;
	PlayerData[Client][hRagdoll] = INVALID_ENT_REFERENCE;
	PlayerData[Client][blockFlash] = false;
	PlayerData[Client][sDisguisedName] = '\0';
	PlayerData[Client][bBlockFallDamage] = false;
	if(IsValidClient(Client) && IsClientInGame(Client)) {
		ClientCommand(Client, "r_screenoverlay \"\"");
		RemoveReference(PlayerData[Client][h_Sprite_Player]);
	}
	CloseTimer(g_TVirusTimer[Client]);
	CloseTimer(g_DNAScanTimer[Client]);
}

stock TTT_ResetWeaponData(weapon)
{
	if(!IsWeapon(weapon)) {
		return;
	}
	
	WeaponData[weapon][IsSilencedPistol] = false;
	WeaponData[weapon][IsGoldenGun] = false;
	WeaponData[weapon][IsKnife] = false;
	WeaponData[weapon][IsSuperCrowbar] = false;
	WeaponData[weapon][IsFlareGun] = false;
	WeaponData[weapon][IsJihad] = false;
	WeaponData[weapon][IsDisguise] = false;
	WeaponData[weapon][IsHeadCrabLauncher] = false;
	WeaponData[weapon][IsTraitorWeapon] = false;
	WeaponData[weapon][IsDetectiveWeapon] = false;
	WeaponData[weapon][IsUsingViewmodel] = false;
	WeaponData[weapon][IsUsingSpecialSlot] = false;
	WeaponData[weapon][IsUsingSendProxy] = false;
	Format(WeaponData[weapon][szName], 32, NULL_STRING);
	Weapon_ResetModelData(weapon);
	Teleporter_ResetData(weapon);
	RemoveDNA(weapon);
}

stock TTT_SetConfiguration()
{
	//PrintToServer("/**************************************************\n * Load TTT Configuration\n**************************************************/");
	ServerCommand("mp_ignore_round_win_conditions 1");
	ServerCommand("mp_friendlyfire 1");
	ServerCommand("sv_nomvp 1");
	ServerCommand("mp_buytime 10000");
	ServerCommand("mp_flashlight 0");
	ServerCommand("mp_hostagepenalty 0");
	//PrintToServer("/**************************************************\n * Loaded\n**************************************************/");
}

stock bool:GetAllowDrop(Client)
{
	return PlayerData[Client][AllowDrop];
}

stock bool:GetBuyDrop(Client)
{
	return PlayerData[Client][BuyDrop];
}

stock SetAllowDrop(Client, bool:allow = true, bool:buy = false)
{
	PlayerData[Client][AllowDrop] = allow;
	PlayerData[Client][BuyDrop] = buy;
}

stock bool:GetBlockSwtich(Client)
{
	return PlayerData[Client][blockSwitch];
}

stock SetBlockSwtich(Client, bool:bBlock)
{
	PlayerData[Client][blockSwitch] = bBlock;
}

public SearchPlayerByWords(Client, const String:words[], type)
{
	new Handle:menuhandle = INVALID_HANDLE;
	menuhandle = CreateMenu(SearchPlayer_Handler);
	
	if(menuhandle == INVALID_HANDLE)
	{
		PrintToChat(Client, "\x04[Error] Unknown Search Type %d", type);
		LogError("\x04[Error] Unknown Search Type %d", type);
	}
	
	new count = 0, target, String:s_Number[8];
	SetMenuTitle(menuhandle, "Search Player - words : %s", words);
	for(new i=1; i<=MaxClients; i++)
	{
		if(!IsClientInGame(i) || IsFakeClient(i)) continue;
		
		new String:s_Name[32];
		GetClientName(i, s_Name, sizeof(s_Name));
		if(StrContains(s_Name, words, false) == -1) continue;
		
		IntToString(i, s_Number, sizeof(s_Number));
		Format(s_Number, sizeof(s_Number), "%s %d", s_Number, type);
		AddMenuItem(menuhandle, s_Number, s_Name);
		count++;
		target = i;
	}
	
	if(count == 0)
	{
		PrintToChat(Client, "\x04[Search]\x03Couldn't Find Player %s", words);
		CloseHandle(menuhandle);
	}
	if(count == 1)
	{
		if(type == SEARCHTYPE_PLAYERINFO)
			ServerCommand("pointshop_player_info %i %i", GetClientUserId(Client), GetClientUserId(target));
		if(type == SEARCHTYPE_GIFTPOINT)
			ServerCommand("pointshop_give_point %i %i", GetClientUserId(Client), GetClientUserId(target));
		if(type == SEARCHTYPE_GIFTITEM)
			ServerCommand("pointshop_give_item %i %i", GetClientUserId(Client), GetClientUserId(target));
		if(type == SEARCHTYPE_SPECTATOR)
		{
			CommitSuicide(target, true);
			ChangeClientTeam(target, 1);
		}
		if(type == SEARCHTYPE_KARMA)
		{
			AddKarmaScore(Client, target);
			CheckKarmaScore(target);
		}
		CloseHandle(menuhandle);
	}
	if(count > 1)
	{
		DisplayMenu(menuhandle, Client, MENU_TIME_FOREVER);
	}
}

public SetTraitorCount(count)
{
	g_traitor_count = count;
}

public GetTraitorCount()
{
	return g_traitor_count;
}

public AddTraitorCount(count)
{
	g_traitor_count += count;
	return GetTraitorCount();
}

public SetGameCount(count)
{
	g_game_count = count;
}

public GetGameCount()
{
	return g_game_count;
}

public AddGameCount(count)
{
	g_game_count += count;
	return GetGameCount();
}

public SetReadyCount(count)
{
	g_ready_count = count;
}

public GetReadyCount()
{
	return g_ready_count;
}

public AddReadyCount(count)
{
	g_ready_count += count;
	return GetReadyCount();
}

/******************************************************************************************
 * Common Function
******************************************************************************************/

stock Date_MakeString(String:dest[], len)
{
	new String:sYear[4], String:sMonth[4], String:sDay[4], String:sHour[4], String:sMinute[4], String:sSecond[4];
	FormatTime(sYear, 30, "%Y", GetTime());
	FormatTime(sMonth, 30, "%m", GetTime());
	FormatTime(sDay, 30, "%d", GetTime());
	FormatTime(sHour, 30, "%H", GetTime());
	FormatTime(sMinute, 30, "%M", GetTime());
	FormatTime(sSecond, 30, "%S", GetTime());
	
	Format(dest, len, "%04d%02d%02d%02d%02d%02d", StringToInt(sYear), StringToInt(sMonth), StringToInt(sDay), StringToInt(sHour), StringToInt(sMinute), StringToInt(sSecond));
}

stock min(val1, val2)
{
	return val1 < val2 ? val1 : val2;
}

stock max(val1, val2)
{
	return val1 > val2 ? val1 : val2;
}

stock Float:fmin(Float:value1, Float:value2)
{
	return (value1 < value2) ? value1 : value2;
}

stock Float:fmax(Float:value1, Float:value2)
{
	return (value1 > value2) ? value1 : value2;
}

stock Float:fclamp(Float:value, Float:min_value, Float:max_value)
{
	if(min_value > max_value) return value;
	if(value > max_value) return max_value;
	if(value < min_value) return min_value;
	return value;
}

stock LogData(const String:format[], any:...)
{
	new String:buffer[512];
	VFormat(buffer, sizeof(buffer), format, 2);
	LogToFileEx(g_log_path, buffer);
}

stock Float:GetDistanceEntityTo(from, to)
{
	if(!IsValidEntity(from) || !IsValidEntity(to)) {
		return 0.0;
	}
	
	new Float:vecFromOrigin[3], Float:vecToOrigin[3];
	GetEntPropVector(from, Prop_Send, "m_vecOrigin", vecFromOrigin);
	GetEntPropVector(to, Prop_Send, "m_vecOrigin", vecToOrigin);
	return GetVectorDistance(vecFromOrigin, vecToOrigin);
}

stock SetClientAmmo(weapon, Client, ammo)
{
	if(!IsWeapon(weapon)) {
		return;
	}
	
	new ammotype = GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType");
	if(ammotype < 1) {
		return;
	}
	SetEntProp(Client, Prop_Data, "m_iAmmo", ammo, _, ammotype);
}

stock GetClientAmmo(weapon, Client)
{
	if(!IsWeapon(weapon)) {
		return -1;
	}
	
	new ammotype = GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType");
	if(ammotype < 1) {
		return -1;
	}
	return GetEntProp(Client, Prop_Data, "m_iAmmo", _, ammotype);
}

stock bool:CanWeaponAttack(weapons, bool:bPrimary = true)
{
	if(!IsWeapon(weapons)) {
		return false;
	}
	new owner = GetOwnerEntity(weapons);
	if(!IsValidClient(owner)) {
		return false;
	}
	if(!IsC4Weapon(weapons) && !IsGrenadeWeapon(weapons)) {
		new Float:flNextAttack = GetEntPropFloat(owner, Prop_Send, "m_flNextAttack");
		if(GetGameTime() < flNextAttack) {
			return false;
		}
	}
	new Float:flNextPrimaryAttack = GetEntPropFloat(weapons, Prop_Send, "m_flNextPrimaryAttack");
	new Float:flNextSecondaryAttack = GetEntPropFloat(weapons, Prop_Send, "m_flNextSecondaryAttack");
	if(bPrimary ? (GetGameTime() < flNextPrimaryAttack) : (GetGameTime() < flNextSecondaryAttack)) {
		return false;
	}
	new iShotsFired = GetEntProp(owner, Prop_Send, "m_iShotsFired");
	if(IsSecondaryWeapon(weapons) && iShotsFired) {
		return false;
	}
	return true;
}

stock SetWeaponNextAttack(weapons, Float:fDelay)
{
	SetEntPropFloat(weapons, Prop_Send, "m_flNextPrimaryAttack", FloatAdd(GetGameTime(), fDelay));
	SetEntPropFloat(weapons, Prop_Send, "m_flNextSecondaryAttack", FloatAdd(GetGameTime(), fDelay));
}

stock Float:GetNextPrimaryFire(weapons)
{
	return GetEntPropFloat(weapons, Prop_Send, "m_flNextPrimaryAttack")-GetGameTime();
}

stock SetNextPrimaryFire(weapons, Float:fDelay)
{
	SetEntPropFloat(weapons, Prop_Send, "m_flNextPrimaryAttack", FloatAdd(GetGameTime(), fDelay));
}

stock Float:GetNextSecondaryFire(weapons)
{
	return GetEntPropFloat(weapons, Prop_Send, "m_flNextSecondaryAttack")-GetGameTime();
}

stock SetNextSecondaryFire(weapons, Float:fDelay)
{
	SetEntPropFloat(weapons, Prop_Send, "m_flNextSecondaryAttack", FloatAdd(GetGameTime(), fDelay));
}

stock GetGroundEntity(entity)
{
	return GetEntDataEnt2(entity, Offset[m_hGroundEntity]);
}

stock bool:HaveEffectFlags(entity, flag)
{
	return bool:(GetEntData(entity, Offset[m_fEffects]) & flag);
}

stock AddEffectFlags(entity, flag)
{
	new flags = GetEntData(entity, Offset[m_fEffects]);
	flags |= flag;
	SetEntData(entity, Offset[m_fEffects], flags, 4, true);
}

stock RemoveEffectFlags(entity, flag)
{
	new flags = GetEntData(entity, Offset[m_fEffects]);
	flags &= ~flag;
	SetEntData(entity, Offset[m_fEffects], flags, 4, true);
}

stock GetTargetName(entity, String:dest[], length)
{
	GetEntPropString(entity, Prop_Data, "m_iName", dest, length);
}

stock SetTargetName(entity, const String:sName[])
{
	DispatchKeyValue(entity, "targetname", sName);
}

stock GetClientArmorValue(Client)
{
	if(!IsValidClient(Client) || !IsClientInGame(Client) || !IsPlayerAlive(Client)) return 0;
	return GetEntProp(Client, Prop_Data, "m_ArmorValue");
}

stock SetClientArmorValue(Client, value)
{
	if(!IsValidClient(Client) || !IsClientInGame(Client) || !IsPlayerAlive(Client)) return;
	SetEntProp(Client, Prop_Data, "m_ArmorValue", value);
}

stock bool:IsValidClient(client)
{
	return (client >= 1 && client <= MaxClients);
}

stock CreateSprite(Float:vecSpritePosition[3], const String:sEffect[], Float:size, bool:Follow = false, hFollwEntity = 0)
{
	new sprite = CreateEntityByName("env_sprite");
	DispatchKeyValue(sprite, "model", sEffect);
	DispatchKeyValue(sprite, "rendercolor", "255 255 255");
	DispatchKeyValue(sprite, "renderamt", "255");
	DispatchKeyValue(sprite, "spawnflags", "1");
	DispatchKeyValue(sprite, "rendermode", "5");
	DispatchKeyValue(sprite, "framerate", "0");
	DispatchKeyValueFloat(sprite, "scale", size);
	DispatchSpawn(sprite);
	TeleportEntity(sprite, vecSpritePosition, NULL_VECTOR, NULL_VECTOR);
	if(Follow)
	{
		SetVariantString("!activator");
		AcceptEntityInput(sprite, "SetParent", hFollwEntity, sprite, 0);
	}
	return sprite;
}

stock RemoveC4(Client)
{
	new weapons = GetPlayerWeaponSlot(Client, 4);
	if(IsValidEdict(weapons)) {
		RemovePlayerItem(Client, weapons);
		RemoveEdict(weapons);
	}
}

stock RemoveReference(&Reference)
{
	if(IsValidReference(Reference))
	{
		new entity = EntRefToEntIndex(Reference);
		AcceptEntityInput(entity, "Kill");
	}
	Reference = INVALID_ENT_REFERENCE;
}

stock bool:IsValidReference(Reference)
{
	if(Reference == INVALID_ENT_REFERENCE)
		return false;
	
	new entity = EntRefToEntIndex(Reference);
	if(!IsValidEdict(entity) || entity == 0)
		return false;
	
	return true;
}

public Action:RemoveEntity_Timer(Handle:timer, any:entity)
{
	entity = EntRefToEntIndex(entity);
	if(IsValidEntity(entity) && IsValidEdict(entity))
	{
		AcceptEntityInput(entity, "Kill");
	}
}

stock PerformFade(client, Float:time, r, g, b, a)
{
	new Handle:hFadeClient=StartMessageOne("Fade",client,USERMSG_RELIABLE);
	BfWriteShort(hFadeClient,RoundToNearest(time*500));	// FIXED 16 bit, with SCREENFADE_FRACBITS fractional, seconds duration
	BfWriteShort(hFadeClient,500);		// FIXED 16 bit, with SCREENFADE_FRACBITS fractional, seconds duration until reset (fade & hold)
	BfWriteShort(hFadeClient,1); // fade type (in / out)
	BfWriteByte(hFadeClient,r);	// fade red
	BfWriteByte(hFadeClient,g);	// fade green
	BfWriteByte(hFadeClient,b);	// fade blue
	BfWriteByte(hFadeClient,a);	// fade alpha
	EndMessage();
}

public SayText2Message(Client, target, const String:Msg[], any:...)
{
	new String:txt[255];
	VFormat(txt, sizeof(txt), Msg, 4);
	
	new Handle:bf = StartMessageOne("SayText2",target,USERMSG_RELIABLE);
	if(bf != INVALID_HANDLE)
	{
		BfWriteByte(bf, Client);
		BfWriteByte(bf, 0);
		BfWriteString(bf, txt);
		EndMessage();
	}
}

public SayText2MessageAll(Client, const String:Msg[], any:...)
{
	new String:txt[255];
	VFormat(txt, sizeof(txt), Msg, 4);
	
	new Handle:bf = StartMessageAll("SayText2",USERMSG_RELIABLE);
	if(bf != INVALID_HANDLE)
	{
		BfWriteByte(bf, Client);
		BfWriteByte(bf, 0);
		BfWriteString(bf, txt);
		EndMessage();
	}
}

public SendCustomSayText2Message(Client, const String:Msg[], any:...)
{
	new String:sTag[16];
	if(IsPlayerAlive(Client) == false)
	{
		if(GetClientTeam(Client) < 2)
			Format(sTag, sizeof(sTag), "\x01*SPEC* ");
		else
			Format(sTag, sizeof(sTag), "\x01*DEAD* ");
	}
	
	new String:sBuffer[255];
	for(new i=1; i<=MaxClients; i++)
	{
		if(IsClientInGame(i) == false || (IsFakeClient(i) && !IsClientSourceTV(i))) continue;
		
		//alive player couldn't listen death player's chat
		if(!IsPlayerAlive(Client) && IsPlayerAlive(i)) continue;
		
		SetGlobalTransTarget(i);
		new String:txt[255];
		VFormat(txt, sizeof(txt), Msg, 3);
		Format(sBuffer, sizeof(sBuffer), "%s%s", sTag, txt);
		SayText2Message(Client, i, sBuffer);
	}
}

public RemoveSpecialSay(String:Msg[256])
{
	ReplaceString(Msg, sizeof(Msg), "\x03", "\x01");
	ReplaceString(Msg, sizeof(Msg), "\x04", "\x01");
	ReplaceString(Msg, sizeof(Msg), "\x05", "\x01");
	ReplaceString(Msg, sizeof(Msg), "\x07", "\x01");
	ReplaceString(Msg, sizeof(Msg), "\x08", "\x01");
}

stock bool:BlockCommand(const String:Msg[], const String:words[], const String:split[] = " ")
{
	if(StrEqual(Msg, words, false) == true)
		return true;
	else
	{
		new String:sCompared[32];
		Format(sCompared, sizeof(sCompared), "%s%s",words,split);
		if(StrContains(Msg, sCompared, false) == 0)
			return true;
	}
	return false;
}

stock bool:BlockCommand2(const String:Msg[], const String:words[])
{
	if(StrContains(Msg, words, false) == 0)
		return true;
	return false;
}

stock SetOwnerEntity(entity, owner)
{
	SetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity", owner);
}

stock GetOwnerEntity(entity)
{
	return GetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity");
}

stock GetClientWeaponIndex(Client)
{
	if(Client < 1 || Client > MaxClients) return -1;
	if(!IsClientInGame(Client) || !IsPlayerAlive(Client)) return -1;
	return GetEntPropEnt(Client, Prop_Send, "m_hActiveWeapon");
}

stock bool:IsPrimaryWeapon(weapons)
{
	if(IsValidEdict(weapons) == false)
	{
		return false;
	}
	if(IsSecondaryWeapon(weapons) == true)
	{
		return false;
	}
	if(IsKnifeWeapon(weapons) == true)
	{
		return false;
	}
	if(IsGrenadeWeapon(weapons) == true)
	{
		return false;
	}
	if(IsC4Weapon(weapons) == true)
	{
		return false;
	}
	new String:classname[32];
	GetEdictClassname(weapons, classname, sizeof(classname));
	if(StrContains(classname, "weapon_"))
	{
		return false;
	}
	return true;
}

stock bool:IsSecondaryWeapon(weapons)
{
	if(IsValidEdict(weapons) == false)
	{
		return false;
	}
	
	new String:classname[32];
	GetEdictClassname(weapons, classname, sizeof(classname));
	if(StrEqual(classname, "weapon_usp") || StrEqual(classname, "weapon_glock") || StrEqual(classname, "weapon_fiveseven") || StrEqual(classname, "weapon_p228") || StrEqual(classname, "weapon_elite") || StrEqual(classname, "weapon_deagle"))
	{
		return true;
	}
	return false;
}

stock bool:IsKnifeWeapon(weapons)
{
	if(IsValidEdict(weapons) == false)
	{
		return false;
	}
	
	new String:classname[32];
	GetEdictClassname(weapons, classname, sizeof(classname));
	if(StrEqual(classname, "weapon_knife"))
	{
		return true;
	}
	return false;
}

stock bool:IsGrenadeWeapon(weapons)
{
	if(IsValidEdict(weapons) == false)
	{
		return false;
	}
	
	new String:classname[32];
	GetEdictClassname(weapons, classname, sizeof(classname));
	if(StrEqual(classname, "weapon_hegrenade") || StrEqual(classname, "weapon_smokegrenade") || StrEqual(classname, "weapon_flashbang"))
	{
		return true;
	}
	return false;
}

stock bool:IsC4Weapon(weapons)
{
	if(IsValidEdict(weapons) == false)
	{
		return false;
	}
	
	new String:classname[32];
	GetEdictClassname(weapons, classname, sizeof(classname));
	if(StrEqual(classname, "weapon_c4"))
	{
		return true;
	}
	return false;
}

stock bool:IsWeapon(weapons)
{
	if(IsValidEdict(weapons) == false)
	{
		return false;
	}
	
	new String:classname[32];
	GetEdictClassname(weapons, classname, sizeof(classname));
	if(StrContains(classname, "weapon_") == 0)
	{
		return true;
	}
	return false;
}

stock bool:IsRagdollEntity(entity)
{
	if(IsValidEdict(entity) == false)
	{
		return false;
	}
	
	new String:classname[32];
	GetEdictClassname(entity, classname, sizeof(classname));
	if(StrContains(classname, "prop_ragdoll") != -1)
	{
		return true;
	}
	return false;
}

stock CopyVector(Float:in[3], Float:out[3])
{
	for(new i; i<3; i++) out[i] = in[i];
}

public GetRandomClients(clients[], count)
{		
	new index = GetRandomInt(0,GetURandomInt())%count;
	new client = clients[index];
	while(client == 0)
	{
		index = GetRandomInt(0,GetURandomInt())%count;
		client = clients[index];
	}
	return index;
}

public GetClientFromEvent(Handle:event)
{
	return GetClientOfUserId(GetEventInt(event, "userid"));
}

public GetAttackerFromEvent(Handle:event)
{
	return GetClientOfUserId(GetEventInt(event, "attacker"));
}

public GetPlayerRagdoll(player)
{
	return GetEntPropEnt(player, Prop_Send, "m_hRagdoll");
}

public RemovePlayerRagdoll(player)
{
	new ragdoll = GetEntPropEnt(player, Prop_Send, "m_hRagdoll");
	if(ragdoll == -1 || IsValidEdict(ragdoll) == false)
	{
		return;
	}
	AcceptEntityInput(ragdoll, "Kill");
}

public AdjustSpawnPoint()
{
	//CT Spawn Point convert to T
	if(g_isJailMode == false) // if jailmode ct will detective
		ChangeSpawnPointTo("info_player_counterterrorist", "info_player_terrorist");
	//HL2 Spawn Point convert to T
	ChangeSpawnPointTo("info_player_start", "info_player_terrorist");
	ChangeSpawnPointTo("info_player_deathmatch", "info_player_terrorist");
}

public ChangeSpawnPointTo(const String:before[], const String:after[])
{
	new ent, prev;
	while((ent = FindEntityByClassname(ent, before)) != -1)
	{
		if(prev)
		{
			new Float:temp_pos[3], Float:temp_ang[3];
			GetEntPropVector(prev, Prop_Send, "m_vecOrigin", temp_pos);
			temp_pos[2] += 4.0;
			GetEntPropVector(prev, Prop_Send, "m_angRotation", temp_ang);
			RemoveEdict(prev);
			for(new i; i<2; i++)
			{
				new spawn = CreateEntityByName(after);
				if(DispatchSpawn(spawn))
				{
					TeleportEntity(spawn, temp_pos, temp_ang, NULL_VECTOR);
				}
			}
		}
		prev = ent;
	}
	if(prev)
	{
		new Float:temp_pos[3], Float:temp_ang[3];
		GetEntPropVector(prev, Prop_Send, "m_vecOrigin", temp_pos);
		temp_pos[2] += 4.0;
		GetEntPropVector(prev, Prop_Send, "m_angRotation", temp_ang);
		RemoveEdict(prev);
		for(new i; i<2; i++)
		{
			new spawn = CreateEntityByName(after);
			if(DispatchSpawn(spawn))
			{
				TeleportEntity(spawn, temp_pos, temp_ang, NULL_VECTOR);
			}
		}
	}
}

new const String:s_useless_entity[][32] = {"func_bomb_target", "func_buyzone"};
new const String:s_weapon_entity[][32] = {"weapon_tmp", "weapon_mac10", "prop_ragdoll"};
public RemoveUselessEntity()
{
	for(new i; i<sizeof(s_useless_entity); i++)
	{
		RemoveEntityByClassname(s_useless_entity[i]);
	}
}

public RemoveWeaponEntity()
{
	for(new i; i<sizeof(s_weapon_entity); i++)
	{
		RemoveEntityByClassname(s_weapon_entity[i]);
	}
}

public RemoveEntityByClassname(const String:classname[])
{
	new ent, prev;
	while((ent = FindEntityByClassname(ent, classname)) != -1)
	{
		if(prev) RemoveEdict(prev);
		prev = ent;
	}
	if(prev) RemoveEdict(prev);
}

stock CalculateDays(year, month, day, plusdays, &returnyear, &returnmonth, &returnday)
{
	returnyear = year;
	returnmonth = month;
	returnday = day + plusdays;
	
	new bool:loop = true;
	while(loop)
	{
		loop = false;
		if(month == 2)
		{
			if((returnyear % 400 == 0 && returnyear % 100 == 0) || (returnyear % 100 != 0 && returnyear % 4 == 0))//윤년일때
			{
				if(returnday > 29)
				{
					returnmonth += 1;
					returnday -= 29;
					loop = true;
				}
			}
			else//윤년이 아닐때
			{
				if(returnday > 28)
				{
					returnmonth += 1;
					returnday -= 28;
					loop = true;
				}
			}
		}
		else if((returnmonth <= 7 && returnmonth % 2 == 1) || (returnmonth > 7 && returnmonth % 2 == 0))//31일이면
		{
			if(returnday > 31)
			{
				returnmonth += 1;
				returnday -= 31;
				loop = true;
			}
		}
		else//30일이면
		{
			if(returnday > 30)
			{
				returnmonth += 1;
				returnday -= 30;
				loop = true;
			}
		}
		if(returnmonth > 12)
		{
			returnyear += 1;
			returnmonth -= 12;
			loop = true;
		}
	}
}

stock makeExplosion(attacker = 0, inflictor = -1, const Float:attackposition[3], const String:weaponname[] = "", magnitude = 100, radiusoverride = 0, Float:damageforce = 0.0, flags = 0)
{
	new explosion = CreateEntityByName("env_explosion");
	if(explosion != -1)
	{
		DispatchKeyValueVector(explosion, "Origin", attackposition);
		decl String:intbuffer[64];
		IntToString(magnitude, intbuffer, 64);
		DispatchKeyValue(explosion,"iMagnitude", intbuffer);
		if(radiusoverride > 0)
		{
			IntToString(radiusoverride, intbuffer, 64);
			DispatchKeyValue(explosion,"iRadiusOverride", intbuffer);
		}
		if(damageforce > 0.0)
		{
			DispatchKeyValueFloat(explosion,"DamageForce", damageforce);
		}
		if(flags != 0)
		{
			IntToString(flags, intbuffer, 64);
			DispatchKeyValue(explosion,"spawnflags", intbuffer);
		}
		//웨폰네임 오버라이드
		if(!StrEqual(weaponname, "", false))
		{
			DispatchKeyValue(explosion,"classname", weaponname);
			if(inflictor != -1)
			{
				DispatchKeyValue(inflictor,"classname", weaponname);
			}
		}
		DispatchSpawn(explosion);
		if(attacker != -1)
		{
			SetEntPropEnt(explosion, Prop_Send, "m_hOwnerEntity", attacker);
		}
		if(inflictor != -1)
		{
			SetEntPropEnt(explosion, Prop_Data, "m_hInflictor", inflictor);
		}
		AcceptEntityInput(explosion, "Explode");
		if(~flags & 0x00000002)
		{
			AcceptEntityInput(explosion, "Kill");
		}
		return explosion;
	}
	else
	{
		return -1;
	}
}

stock bool:makeDamage(attacker, target, damage, damagetype, Float:damageradius, const Float:attackposition[3], const String:weaponname[] = "")
{
	new pointhurt = CreateEntityByName("point_hurt");
	if(pointhurt != -1)
	{
		if(target != -1)
		{
			decl String:targetname[64];
			Format(targetname, 128, "%f%f", GetEngineTime(), GetRandomFloat());
			DispatchKeyValue(target,"TargetName", targetname);
			DispatchKeyValue(pointhurt,"DamageTarget", targetname);
		}
		DispatchKeyValueVector(pointhurt, "Origin", attackposition);
		decl String:number[64];
		IntToString(damage, number, 64);
		DispatchKeyValue(pointhurt,"Damage", number);
		IntToString(damagetype, number, 64);
		DispatchKeyValue(pointhurt,"DamageType", number);
		DispatchKeyValueFloat(pointhurt, "DamageRadius", damageradius);
		if(!StrEqual(weaponname, "", false))
		{
			DispatchKeyValue(pointhurt,"classname", weaponname);
		}
		DispatchSpawn(pointhurt);
		AcceptEntityInput(pointhurt, "Hurt", attacker != -1 ? attacker : 0);
		AcceptEntityInput(pointhurt, "Kill");
		return true;
	}
	else
	{
		return false;
	}
}

stock AddFolderToDownloadsTable(const String:sDirectory[], const String:contain_word[] = "")
{
	decl String:sFilename[128], String:sPath[256];
	new Handle:hDirectory = OpenDirectory(sDirectory);
	if(hDirectory != INVALID_HANDLE)
	{
		decl FileType:fileType;
		
		while(ReadDirEntry(hDirectory, sFilename, sizeof(sFilename), fileType))
		{
			if(fileType == FileType_Directory)
			{
				if(FindCharInString(sFilename, '.') == -1)
				{
					Format(sPath, sizeof(sPath), "%s/%s", sDirectory, sFilename);
					AddFolderToDownloadsTable(sPath, contain_word);
				}
			}
			else if(fileType == FileType_File)
			{
				if(StrContains(sFilename, contain_word, false) != -1)
				{
					Format(sPath, sizeof(sPath), "%s/%s", sDirectory, sFilename);
					AddFileToDownloadsTable(sPath);
					
					new iPos = FindCharInString(sPath, '.', true);
					
					if(iPos != -1)
					{			
						if(StrEqual(sPath[iPos], ".mdl"))
						{
							PrecacheModel(sPath, true);
						}
						else if(StrEqual(sPath[iPos], ".mp3") || StrEqual(sPath[iPos], ".wav"))
						{
							ReplaceStringEx(sPath, sizeof(sPath), "sound/", "");
							PrecacheSound(sPath, true);
						}
					}
				}
			}
		}
		CloseHandle(hDirectory);
	}
}