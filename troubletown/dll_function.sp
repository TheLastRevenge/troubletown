
enum TakeDamageInfo
{
	Float:vecDamageForce[3],
	Float:vecDamagePosition[3],
	Float:vecReportedPosition[3],
	Address:hInflictor,
	Address:hAttacker,
	Address:hWeapon,
	Float:fDamage,
	Float:fMaxDamage,
	Float:fBaseDamage,
	iBitsDamageType,
	iDamageCustom,
	iDamageStats,
	iAmmoType,
	iDamagedOtherPlayers,
	iPlayerPenetrationCount,
	Float:flDamageBonus
};
enum RouteType
{
	FASTEST_ROUTE,
	SAFEST_ROUTE
};
new Handle:g_hSDKCall_CreateServerRagdoll = INVALID_HANDLE;
new Handle:g_hSDKCall_BumpWeapon = INVALID_HANDLE;
new Handle:g_hSDKCall_IncrementFragCount = INVALID_HANDLE;
new Handle:g_hSDKCall_ResetFragCount = INVALID_HANDLE;
new Handle:g_hSDKCall_IncrementDeathCount = INVALID_HANDLE;
new Handle:g_hSDKCall_ResetDeathCount = INVALID_HANDLE;
new Handle:g_hSDKCall_BotMoveTo = INVALID_HANDLE;

new Handle:g_hSDKCall_RemoveAllItems = INVALID_HANDLE;
new Handle:g_hSDKCall_CommitSuicide = INVALID_HANDLE;

public LoadSignature()
{
	StartPrepSDKCall(SDKCall_Static);
	PrepSDKCall_SetSignature(SDKLibrary_Server, "\x53\x8B\xDC\x83\xEC\x2A\x83\x2A\x2A\x83\x2A\x2A\x55\x8B\x2A\x2A\x89\x2A\x2A\x2A\x8B\x2A\xB8\x2A\x2A\x2A\x2A\xE8\x2A\x2A\x2A\x2A\x8B", 33);
	PrepSDKCall_AddParameter(SDKType_CBaseEntity, SDKPass_Pointer);
	PrepSDKCall_AddParameter(SDKType_PlainOldData, SDKPass_Plain);
	PrepSDKCall_AddParameter(SDKType_PlainOldData, SDKPass_ByRef);
	PrepSDKCall_AddParameter(SDKType_PlainOldData, SDKPass_Plain);
	PrepSDKCall_AddParameter(SDKType_Bool, SDKPass_Plain);
	PrepSDKCall_SetReturnInfo(SDKType_CBaseEntity, SDKPass_Pointer);
	
	if((g_hSDKCall_CreateServerRagdoll = EndPrepSDKCall()) == INVALID_HANDLE)
		LogError("CreateServerRagdolll Call Failed.");
	/********************************************************************************************************************************************************************************************************/
	StartPrepSDKCall(SDKCall_Player);
	PrepSDKCall_SetSignature(SDKLibrary_Server, "\x55\x8B\xEC\x83\x2A\x2A\x89\x2A\x2A\x6A\x00\x68\x2A\x2A\x2A\x2A\x68\x2A\x2A\x2A\x2A\x6A\x00\x8B\x2A\x2A\x50\xE8\x2A\x2A\x2A\x2A\x83\x2A\x2A\x89\x2A\x2A\x83\x2A\x2A\x2A\x75", 43);
	PrepSDKCall_AddParameter(SDKType_CBaseEntity, SDKPass_Pointer);
	
	if((g_hSDKCall_BumpWeapon = EndPrepSDKCall()) == INVALID_HANDLE)
		LogError("BumpWeapon Call Failed.");
	/********************************************************************************************************************************************************************************************************/
	StartPrepSDKCall(SDKCall_Player);
	PrepSDKCall_SetSignature(SDKLibrary_Server, "\x55\x8B\x2A\x8B\x2A\x2A\x01\x2A\x2A\x2A\x00\x00\x8B\x2A\x2A\x2A\x00\x00\x89\x2A\x3C", 21);
	PrepSDKCall_AddParameter(SDKType_PlainOldData, SDKPass_Plain);
	
	if((g_hSDKCall_IncrementFragCount = EndPrepSDKCall()) == INVALID_HANDLE)
		LogError("IncrementFragCount Call Failed.");
	/********************************************************************************************************************************************************************************************************/
	StartPrepSDKCall(SDKCall_Player);
	PrepSDKCall_SetSignature(SDKLibrary_Server, "\xC7\x81\xF8\x2A\x00\x00\x00\x00\x00\x00\xC7\x81\x3C", 13);
	
	if((g_hSDKCall_ResetFragCount = EndPrepSDKCall()) == INVALID_HANDLE)
		LogError("ResetFragCount Call Failed.");
	/********************************************************************************************************************************************************************************************************/
	StartPrepSDKCall(SDKCall_Player);//it start from bot entity
	PrepSDKCall_SetSignature(SDKLibrary_Server, "\x55\x8B\xEC\x8B\x2A\x2A\xD9\x2A\xD9\x2A\x2A\x2A\x2A\x2A\xD9\x2A\x2A\xD9\x2A\x2A\x2A\x2A\x2A\xD9\x2A\x2A\x8B", 27);
	PrepSDKCall_AddParameter(SDKType_Vector, SDKPass_ByRef);//position to move.
	PrepSDKCall_AddParameter(SDKType_PlainOldData, SDKPass_ByRef);//followed enum RouteType
	
	if((g_hSDKCall_BotMoveTo = EndPrepSDKCall()) == INVALID_HANDLE)
		LogError("Bot MoveTo Call Failed.");
	/********************************************************************************************************************************************************************************************************/
	StartPrepSDKCall(SDKCall_Player);
	PrepSDKCall_SetSignature(SDKLibrary_Server, "\x55\x8B\x2A\x8B\x2A\x2A\x01\x2A\x2A\x2A\x00\x00\x8B\x2A\x2A\x2A\x00\x00\x89\x2A\x40", 21);
	PrepSDKCall_AddParameter(SDKType_PlainOldData, SDKPass_Plain);
	
	if((g_hSDKCall_IncrementDeathCount = EndPrepSDKCall()) == INVALID_HANDLE)
		LogError("IncrementDeathCount Call Failed.");
	/********************************************************************************************************************************************************************************************************/
	StartPrepSDKCall(SDKCall_Player);
	PrepSDKCall_SetSignature(SDKLibrary_Server, "\xC7\x81\xFC\x2A\x00\x00\x00\x00\x00\x00\xC7\x81\x40", 13);
	
	if((g_hSDKCall_ResetDeathCount = EndPrepSDKCall()) == INVALID_HANDLE)
		LogError("ResetDeathCount Call Failed.");
	/********************************************************************************************************************************************************************************************************/
}

stock LoadOffset()
{
	//CCSPlayer::RemoveAllItems(bool)
	StartPrepSDKCall(SDKCall_Player);
	PrepSDKCall_SetVirtual(342);
	PrepSDKCall_AddParameter(SDKType_Bool, SDKPass_Plain);//suit
	g_hSDKCall_RemoveAllItems = EndPrepSDKCall();
	
	//CCSPlayer::CommitSuicide(bool, bool)
	StartPrepSDKCall(SDKCall_Player);
	PrepSDKCall_SetVirtual(440);
	PrepSDKCall_AddParameter(SDKType_Bool, SDKPass_Plain);//explode
	PrepSDKCall_AddParameter(SDKType_Bool, SDKPass_Plain);//force
	g_hSDKCall_CommitSuicide = EndPrepSDKCall();
}

stock CreateServerRagdoll(entity, forceBone, const info[TakeDamageInfo], collisionGroup, bool:useLRURetirement = false)
{
	if(g_hSDKCall_CreateServerRagdoll == INVALID_HANDLE)
	{
		return -1;
	}
	return SDKCall(g_hSDKCall_CreateServerRagdoll, entity, forceBone, info, collisionGroup, useLRURetirement);
}

stock BumpWeapon(player, weapon)
{
	if(g_hSDKCall_BumpWeapon == INVALID_HANDLE)
	{
		return;
	}
	SDKCall(g_hSDKCall_BumpWeapon, player, weapon);
}

stock IncrementFragCount(player, count)
{
	if(g_hSDKCall_IncrementFragCount == INVALID_HANDLE)
		return;
	
	SDKCall(g_hSDKCall_IncrementFragCount, player, count);
}

stock ResetFragCount(player)
{
	if(g_hSDKCall_ResetFragCount == INVALID_HANDLE)
		return;
	
	SDKCall(g_hSDKCall_ResetFragCount, player);
}

stock IncrementDeathCount(player, count)
{
	if(g_hSDKCall_IncrementDeathCount == INVALID_HANDLE)
		return;
	
	SDKCall(g_hSDKCall_IncrementDeathCount, player, count);
}

stock ResetDeathCount(player)
{
	if(g_hSDKCall_ResetDeathCount == INVALID_HANDLE)
		return;
	
	SDKCall(g_hSDKCall_ResetDeathCount, player);
}

stock Bot_MoveTo(bot, Float:vector[3], RouteType:route = SAFEST_ROUTE)
{
	if(!IsValidClient(bot) || !IsClientInGame(bot) || !IsFakeClient(bot) || !IsPlayerAlive(bot) || g_hSDKCall_BotMoveTo == INVALID_HANDLE) {
		return;
	}
	SDKCall(g_hSDKCall_BotMoveTo, bot, vector, route);
}

stock RemoveAllItems(Client, bool:suit=false)
{
	if(!IsValidClient(Client) || !IsClientInGame(Client) || !IsPlayerAlive(Client) || g_hSDKCall_RemoveAllItems == INVALID_HANDLE) {
		return;
	}
	SDKCall(g_hSDKCall_RemoveAllItems, Client, suit);
}

stock CommitSuicide(player, bool:force=true, bool:explode=false)
{
	if(g_hSDKCall_CommitSuicide == INVALID_HANDLE)
	{
		return;
	}
	SDKCall(g_hSDKCall_CommitSuicide, player, explode, force);
}