
#define WEAPONSLOT_PRIMARY_1		40
#define WEAPONSLOT_PRIMARY_2		41
#define WEAPONSLOT_SECONDARY_1	42
#define WEAPONSLOT_SECONDARY_2	43
#define WEAPONSLOT_KNIFE_1				44
#define WEAPONSLOT_KNIFE_2				45
#define WEAPONSLOT_C4_1						46
#define WEAPONSLOT_C4_2						47

enum weapon_model {
	v_crowbar,
	w_crowbar,
	
	v_stunbaton,
	w_stunbaton,
	
	v_goldengun,
	w_goldengun,
	
	v_knife,
	w_knife,
	
	v_teleporter,
	w_teleporter,
	
	v_dnascanner,
	w_dnascanner,
	
	v_crossbow,
	w_crossbow,
	
	v_jihadbomb,
	w_jihadbomb,
	
	v_rpg,
	w_rpg,
	
	v_tvirus,
	w_tvirus,
	
	v_flaregun,
	w_flaregun,
	
	v_newton,
	w_newton,
	
	v_station,
	
	v_disguise,
	w_disguise
};

new WeaponModel[weapon_model];
new g_weapon_model[2048][2]; // 0 = view, 1 = world

new bool:g_check_fire[MAXPLAYERS+1];

//#define _DEBUG_WEAPON

public Weapon_Initialize()
{
	WeaponModel[v_crowbar] = PrecacheModel("models/weapons/v_crowbar.mdl", true);
	WeaponModel[w_crowbar] = PrecacheModel("models/weapons/w_crowbar.mdl", true);
	WeaponModel[v_stunbaton] = PrecacheModel("models/weapons/v_stunbaton.mdl", true);
	WeaponModel[w_stunbaton] = PrecacheModel("models/weapons/w_stunbaton.mdl", true);
	WeaponModel[v_goldengun] = PrecacheModel("models/weapons/v_g_deagle.mdl", true);
	WeaponModel[w_goldengun] = PrecacheModel("models/weapons/w_g_deagle.mdl", true);
	WeaponModel[v_knife] = PrecacheModel("models/zombiex2/weapons/knife_v2/v_knife.mdl", true);
	WeaponModel[w_knife] = PrecacheModel("models/zombiex2/weapons/knife_v2/w_knife.mdl", true);
	//WeaponModel[v_knife] = PrecacheModel("models/weapons/v_dragn_t.mdl", true);
	//WeaponModel[w_knife] = PrecacheModel("models/weapons/w_dragn_t.mdl", true);
	WeaponModel[v_teleporter] = PrecacheModel("models/weapons/v_buddyfinder.mdl", true);
	WeaponModel[w_teleporter] = PrecacheModel("models/weapons/w_camphone.mdl", true);
	WeaponModel[v_dnascanner] = PrecacheModel("models/weapons/v_c4.mdl", true);
	WeaponModel[w_dnascanner] = PrecacheModel("models/weapons/w_c4.mdl", true);
	WeaponModel[v_crossbow] = PrecacheModel("models/weapons/v_crossbow.mdl", true);
	WeaponModel[w_crossbow] = PrecacheModel("models/weapons/w_crossbow2.mdl", true);
	WeaponModel[v_jihadbomb] = PrecacheModel("models/weapons/v_jb.mdl", true);
	WeaponModel[w_jihadbomb] = PrecacheModel("models/weapons/w_jb.mdl", true);
	WeaponModel[v_rpg] = PrecacheModel("models/weapons/v_rpg.mdl", true);
	WeaponModel[w_rpg] = PrecacheModel("models/weapons/w_rocket_launcher.mdl", true);
	WeaponModel[v_tvirus] = PrecacheModel("models/troubletown/weapons/tvirus/v_tvirus.mdl", true);
	WeaponModel[w_tvirus] = PrecacheModel("models/troubletown/weapons/tvirus/w_tvirus.mdl", true);
	WeaponModel[v_flaregun] = PrecacheModel("models/weapons/v_pist_casull.mdl", true);
	WeaponModel[w_flaregun] = PrecacheModel("models/weapons/w_pist_casull.mdl", true);
	WeaponModel[v_newton] = PrecacheModel("models/weapons/v_superphyscannon.mdl", true);
	WeaponModel[w_newton] = PrecacheModel("models/weapons/w_superphyscannon_v2.mdl", true);
	WeaponModel[v_station] = PrecacheModel("models/weapons/v_healthkit.mdl", true);
	WeaponModel[v_disguise] = PrecacheModel("models/weapons/v_disguise.mdl", true);
	WeaponModel[w_disguise] = PrecacheModel("models/weapons/w_disguise.mdl", true);
	
	g_weapon_trie = CreateTrie();
	SetTrieValue(g_weapon_trie, "weapon_knife", 0.75);
	SetTrieValue(g_weapon_trie, "weapon_fiveseven", 0.28);
	SetTrieValue(g_weapon_trie, "weapon_deagle", 0.44);
	SetTrieValue(g_weapon_trie, "weapon_elite", 0.18);
	SetTrieValue(g_weapon_trie, "weapon_usp", 0.23);
	SetTrieValue(g_weapon_trie, "weapon_glock", 0.25);
	SetTrieValue(g_weapon_trie, "weapon_p228", 0.12);
	SetTrieValue(g_weapon_trie, "weapon_ump45", 0.18);
	SetTrieValue(g_weapon_trie, "weapon_famas", 0.15);
}

public Weapon_Shutdown()
{
	CloseHandle(g_weapon_trie);
}

public Weapon_ResetModelData(weapons)
{
	g_weapon_model[weapons][0] = 0;
	g_weapon_model[weapons][1] = 0;
}

public Weapon_ResetModel(weapons)
{
	if(!IsWeapon(weapons)) {
		return;
	}
	
	SDKUnhook(weapons, SDKHook_Think, Hook_ThinkModel);
	Weapon_SetModel(weapons);
}

public Weapon_SetModel(weapons)
{
	if(!IsWeapon(weapons)) {
		return;
	}
	
	if(IsZMCarry(weapons))
	{
		g_weapon_model[weapons][0] = WeaponModel[v_stunbaton];
		g_weapon_model[weapons][1] = WeaponModel[w_stunbaton];
		WeaponData[weapons][IsUsingViewmodel] = true;
	}
	if(TTT_IsTester(weapons))
	{
		g_weapon_model[weapons][0] = WeaponModel[v_dnascanner];
		g_weapon_model[weapons][1] = WeaponModel[w_dnascanner];
		WeaponData[weapons][IsUsingViewmodel] = true;
	}
	if(TTT_IsJihad(weapons))
	{
		g_weapon_model[weapons][0] = WeaponModel[v_jihadbomb];
		g_weapon_model[weapons][1] = WeaponModel[w_jihadbomb];
		WeaponData[weapons][IsUsingViewmodel] = true;
	}
	if(IsCrossBow(weapons))
	{
		g_weapon_model[weapons][0] = WeaponModel[v_crossbow];
		g_weapon_model[weapons][1] = WeaponModel[w_crossbow];
		WeaponData[weapons][IsUsingViewmodel] = true;
	}
	if(IsTVirus(weapons))
	{
		g_weapon_model[weapons][0] = WeaponModel[v_tvirus];
		g_weapon_model[weapons][1] = WeaponModel[w_tvirus];
		WeaponData[weapons][IsUsingViewmodel] = true;
	}
	
	if(IsKnifeWeapon(weapons))
	{
		g_weapon_model[weapons][0] = WeaponModel[v_crowbar];
		g_weapon_model[weapons][1] = WeaponModel[w_crowbar];
		WeaponData[weapons][IsUsingViewmodel] = true;
	}
	
	if(WeaponData[weapons][IsGoldenGun])
	{
		g_weapon_model[weapons][0] = WeaponModel[v_goldengun];
		g_weapon_model[weapons][1] = WeaponModel[w_goldengun];
		WeaponData[weapons][IsUsingViewmodel] = true;
	}
	if(WeaponData[weapons][IsKnife])
	{
		g_weapon_model[weapons][0] = WeaponModel[v_knife];
		g_weapon_model[weapons][1] = WeaponModel[w_knife];
		WeaponData[weapons][IsUsingViewmodel] = true;
	}
	if(WeaponData[weapons][IsFlareGun])
	{
		g_weapon_model[weapons][0] = WeaponModel[v_flaregun];
		g_weapon_model[weapons][1] = WeaponModel[w_flaregun];
		WeaponData[weapons][IsUsingViewmodel] = true;
	}
	
	if(TTT_IsTeleporter(weapons))
	{
		g_weapon_model[weapons][0] = WeaponModel[v_teleporter];
		g_weapon_model[weapons][1] = WeaponModel[w_teleporter];
		WeaponData[weapons][IsUsingViewmodel] = true;
	}
	if(IsRPG(weapons))
	{
		g_weapon_model[weapons][0] = WeaponModel[v_rpg];
		g_weapon_model[weapons][1] = WeaponModel[w_rpg];
		WeaponData[weapons][IsUsingViewmodel] = true;
	}
	if(IsNewtonLauncher(weapons))
	{
		g_weapon_model[weapons][0] = WeaponModel[v_newton];
		g_weapon_model[weapons][1] = WeaponModel[w_newton];
		WeaponData[weapons][IsUsingViewmodel] = true;
	}
	if(IsHealthStation(weapons) || IsDeathStation(weapons))
	{
		g_weapon_model[weapons][0] = WeaponModel[v_station];
		WeaponData[weapons][IsUsingViewmodel] = true;
	}
	if(TTT_IsDisguise(weapons))
	{
		g_weapon_model[weapons][0] = WeaponModel[v_disguise];
		g_weapon_model[weapons][1] = WeaponModel[w_disguise];
		WeaponData[weapons][IsUsingViewmodel] = true;
	}
	SDKHook(weapons, SDKHook_Think, Hook_ThinkModel);
}

public Weapon_SetList(weapons)
{
	if(!IsWeapon(weapons)) {
		return;
	}
	
	if(IsC4Weapon(weapons)) {
		TTT_SetWeaponToList(weapons, WEAPONSLOT_C4_1, WEAPONSLOT_C4_2);
	}
	
	else if(IsKnifeWeapon(weapons)) {
		TTT_SetWeaponToList(weapons, WEAPONSLOT_KNIFE_1, WEAPONSLOT_KNIFE_2);
	}
	
	else if(IsSecondaryWeapon(weapons)) {
		TTT_SetWeaponToList(weapons, WEAPONSLOT_SECONDARY_1, WEAPONSLOT_SECONDARY_2);
	}
	
	else if(!IsGrenadeWeapon(weapons)) {
		TTT_SetWeaponToList(weapons, WEAPONSLOT_PRIMARY_1, WEAPONSLOT_PRIMARY_2);
	}
}

public Action:Hook_WeaponCanUse(Client, weapons)
{
	static Float:lastPrintTime[MAXPLAYERS+1];
	// traitor only can get traitor's weapon with press 'E'
	new bool:bFail = false;
	if(WeaponData[weapons][IsTraitorWeapon])
	{
		if(PlayerData[Client][Job] != traitor)
		{
			return Plugin_Handled;
		}
		new buttons = GetClientButtons(Client);
		bFail = !(buttons & IN_USE);
	}
	if(TTT_IsTester(weapons) || WeaponData[weapons][IsDetectiveWeapon])
	{
		new buttons = GetClientButtons(Client);
		bFail = !(buttons & IN_USE || PlayerData[Client][Job] == detective);
	}
	if(bFail)
	{
		new Float:diff = FloatSub(GetEngineTime(), lastPrintTime[Client]);
		if(diff > 1.0) { // block message when buying weapon
			lastPrintTime[Client] = GetEngineTime();
			return Plugin_Handled;
		}
		if(diff > 0.5)
		{
			lastPrintTime[Client] = GetEngineTime();
			SetGlobalTransTarget(Client);
			
			new String:sWeaponName[32];
			TTT_GetWeaponName(weapons, sWeaponName, sizeof(sWeaponName));
			HudMsg(Client, MSGTYPE_NOTICE, -1.0, 0.4, {255, 0, 0, 255}, {255, 0, 0, 255}, 2, 0.01, 0.5, 1.0, 0.0, "Press E to get %s", sWeaponName);
		}
		return Plugin_Handled;
	}
	if(TTT_CanUseWeapon(Client, weapons))
	{
		if(WeaponData[weapons][IsTraitorWeapon]) {
			SetBlockSwtich(Client, true);
		}
		
		if(!TTT_AutoPickupWeapon(Client, weapons)) {
			EquipPlayerWeapon(Client, weapons);
			BumpWeapon(Client, weapons);
		}
	}
	return Plugin_Continue;
}

public Action:Hook_WeaponSwitch(Client, weapons)
{
	if(GetBlockSwtich(Client)) {
		SetBlockSwtich(Client, false);
		return Plugin_Handled;
	}
	if(Jihad_CheckExecute(Client) && !TTT_IsTeleporter(weapons)) {
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public Hook_WeaponEquipPost(Client, weapons)
{
	Weapon_SetList(weapons);
	Weapon_SetModel(weapons);
	SetBlockSwtich(Client, false);
	SetAllowDrop(Client, false);
	
	new String:classname[32];
	GetEdictClassname(weapons, classname, sizeof(classname));
	if(StrEqual(classname, "weapon_usp"))
	{
		if(WeaponData[weapons][IsSilencedPistol]) {
			SetEntProp(weapons, Prop_Send, "m_bSilencerOn", 1);
			SetEntProp(weapons, Prop_Send, "m_weaponMode", 1);
		} else {
			SetEntProp(weapons, Prop_Send, "m_bSilencerOn", 0);
			SetEntProp(weapons, Prop_Send, "m_weaponMode", 0);
		}
	}
	if(StrEqual(classname, "weapon_knife"))
	{
		new weapon = GetClientWeaponIndex(Client);
		if(IsKnifeWeapon(weapon))
		{
			SetEntData(weapon, Offset[m_iWorldModelIndex], g_weapon_model[weapon][1], 4, true);
		}
	}
}

public Hook_SetWeaponFireratePre(Client)
{
	g_check_fire[Client] = false;
	
	new buttons = GetClientButtons(Client);
	if(buttons & IN_ATTACK)
	{
		g_check_fire[Client] = CanWeaponAttack(GetClientWeaponIndex(Client));
	}
}

public Hook_SetWeaponFirerate(Client)
{
	new weapons = GetClientWeaponIndex(Client);
	if(weapons == -1)
		return;
	
	new String:classname[32];
	GetEdictClassname(weapons, classname, sizeof(classname));
	
	new buttons = GetClientButtons(Client);
	if(buttons & IN_ATTACK)
	{
		if(g_check_fire[Client])
		{
			new Float:flRate = 1.0;
			if(GetTrieValue(g_weapon_trie, classname, flRate))
			{
				/*if(AutoFire[client] == false)
					SetEntProp(client, Prop_Send, "m_iShotsFired", 1);
				else
					SetEntProp(client, Prop_Send, "m_iShotsFired", 0);*/
				SetEntProp(Client, Prop_Send, "m_iShotsFired", 0);
				
				if(StrEqual(classname, "weapon_glock") && GetEntProp(weapons, Prop_Send, "m_bBurstMode"))
				{
					flRate *= 3.0; // increase rate for glock burst mode
				}
				SetWeaponNextAttack(weapons, flRate);
				
				/*static Float:flDelay[MAXPLAYERS+1];
				if(FloatSub(GetEngineTime(), flDelay[client]) > 20.0)
				{
					flDelay[client] = GetEngineTime();
					TitleCheck(client, TITLE_FIRE_WEAPON_DELAY_20s);
				}*/
			}
			g_check_fire[Client] = false;
		}
	}
}

public Action:Hook_WeaponDrop(Client, weapons)
{
	if(!IsWeapon(weapons)) {
		return Plugin_Continue;
	}
	
	//can't drop magneto-stick
	if(IsZMCarry(weapons))
	{
		if(GetClientHealth(Client) <= 0)
		{
			RemovePlayerItem(Client, weapons);
			DropTester(Client);
		}
		return Plugin_Handled;
	}
	
	if(TTT_IsJihad(weapons) && Jihad_CheckExecute(Client))
	{
		if(GetClientHealth(Client) <= 0)
		{
			RemovePlayerItem(Client, weapons);
		}
		return Plugin_Handled;
	}
	
	//check player can drop
	if(RoundType != RoundState_GameReady)
	{
		//disconnect
		if(IsClientInGame(Client) == false)
		{
			return Plugin_Continue;
		}
		//death
		if(GetClientHealth(Client) <= 0)
		{
			return Plugin_Continue;
		}
	}
	else if(!GetBuyDrop(Client)) // can't drop weapon before game start
	{
		return Plugin_Handled;
	}
	if(GetAllowDrop(Client))
	{
		SetBlockSwtich(Client, true);
		SetAllowDrop(Client, false);
		return Plugin_Continue;
	}
	return Plugin_Handled;
}

public Hook_ThinkModel(weapons)
{
	if(g_weapon_model[weapons][1] != 0)
	{
		SetEntData(weapons, Offset[m_iWorldModelIndex], g_weapon_model[weapons][1], 4, true);
		if(IsNewtonLauncher(weapons)) // changed color to blue
		{
			SetVariantInt(1);
			AcceptEntityInput(weapons, "skin");
		}
	}
	if(WeaponData[weapons][IsSuperCrowbar])
	{
		SetEntPropFloat(weapons, Prop_Send, "m_flModelScale", 1.3);
	}
}

public Hook_SetWeaponModel(Client)
{
	new viewmodel_1 = GetEntDataEnt2(Client, Offset[m_hViewModel]);
	new viewmodel_2 = GetEntDataEnt2(Client, Offset[m_hViewModel]+4);
	if(!IsValidEdict(viewmodel_1) || !IsValidEdict(viewmodel_2)) {
		return;
	}
	
	if(!IsPlayerAlive(Client))
	{
		if(HaveEffectFlags(viewmodel_1, EF_NODRAW))
		{
			RemoveEffectFlags(viewmodel_1, EF_NODRAW);
		}
		if(!HaveEffectFlags(viewmodel_2, EF_NODRAW))
		{
			AddEffectFlags(viewmodel_2, EF_NODRAW);
		}
		new spec = GetEntPropEnt(Client, Prop_Send, "m_hObserverTarget");
		if(spec != -1)
		{
			new weapons = GetClientWeaponIndex(spec);
			viewmodel_1 = GetEntDataEnt2(spec, Offset[m_hViewModel]);
			viewmodel_2 = GetEntDataEnt2(spec, Offset[m_hViewModel]+4);
			if(!IsWeapon(weapons) || !IsValidEdict(viewmodel_1) || !IsValidEdict(viewmodel_2)) {
				return;
			}
			if(WeaponData[weapons][IsUsingViewmodel])
			{
				AddEffectFlags(viewmodel_1, EF_NODRAW);
				RemoveEffectFlags(viewmodel_2, EF_NODRAW);
				SetEntData(viewmodel_2, Offset[m_nModelIndex], g_weapon_model[weapons][0], 4, true);
			}
			else
			{
				RemoveEffectFlags(viewmodel_1, EF_NODRAW);
				AddEffectFlags(viewmodel_2, EF_NODRAW);
			}
			//new model_index = Shop_GetViewModelIndex(spec);
			new model_index = PlayerData[spec][vmIndex];
			if(IsKnifeWeapon(weapons) && !WeaponData[weapons][IsKnife] && model_index)
			{
				SetEntData(viewmodel_2, Offset[m_nModelIndex], model_index, 4, true);
			}
		}
		return;
	}
	
	new weapons = GetClientWeaponIndex(Client);
	if(!IsWeapon(weapons))
	{
		if(!HaveEffectFlags(viewmodel_2, EF_NODRAW))
		{
			AddEffectFlags(viewmodel_2, EF_NODRAW);
		}
		return;
	}
	
	static hActiveWeapon[MAXPLAYERS+1], iResetCount[MAXPLAYERS+1];
	if(WeaponData[weapons][IsUsingViewmodel])
	{
		if(!HaveEffectFlags(viewmodel_1, EF_NODRAW))
		{
			AddEffectFlags(viewmodel_1, EF_NODRAW);
			RemoveEffectFlags(viewmodel_2, EF_NODRAW);
			SetEntData(viewmodel_2, Offset[m_nModelIndex], g_weapon_model[weapons][0], 4, true);
			SetEntData(weapons, Offset[m_iWorldModelIndex], g_weapon_model[weapons][1], 4, true);
			SetEntDataEnt2(viewmodel_2, Offset[m_hWeapon], GetEntDataEnt2(viewmodel_1, Offset[m_hWeapon]));
			iResetCount[Client] = 2;
			//Socket Shop Plugin
			//new model_index = Shop_GetViewModelIndex(Client);
			new model_index = PlayerData[Client][vmIndex];
			if(IsKnifeWeapon(weapons) && !WeaponData[weapons][IsKnife] && model_index)
			{
				SetEntData(viewmodel_2, Offset[m_nModelIndex], model_index, 4, true);
			}
		}
		
		new Float:playbackrate = GetEntDataFloat(viewmodel_1, Offset[m_flPlaybackRate]);
		new sequence = GetEntData(viewmodel_1, Offset[m_nSequence]);
		new sequence2 = GetEntData(viewmodel_2, Offset[m_nSequence]);
		new Float:cycle = GetEntDataFloat(viewmodel_1, Offset[m_flCycle]);
		new Float:cycle2 = GetEntDataFloat(viewmodel_2, Offset[m_flCycle]);
		
		Weapon_AdjustWeapon(Client, weapons, sequence, cycle, cycle2);
		SetEntDataFloat(viewmodel_2, Offset[m_flPlaybackRate], playbackrate, true);
		SetEntData(viewmodel_2, Offset[m_nSequence], sequence, 4, true);

		if((weapons == hActiveWeapon[Client] && sequence == sequence2 && cycle < cycle2) || iResetCount[Client])
		{
			SetEntData(viewmodel_2, Offset[m_nSequence], 0, 4, true);
			SetEntDataFloat(viewmodel_2, Offset[m_flPlaybackRate], 0.0, true);
			if(iResetCount[Client]) iResetCount[Client]--;
			else iResetCount[Client] = 9;
		}
		else
		{
			SetEntDataFloat(viewmodel_2, Offset[m_flCycle], cycle, true);
		}
	}
	else
	{
		if(!HaveEffectFlags(viewmodel_2, EF_NODRAW))
		{
			AddEffectFlags(viewmodel_2, EF_NODRAW);
			SetEntData(viewmodel_2, Offset[m_nModelIndex], 0, 4, true);
			SetEntData(viewmodel_2, Offset[m_nSequence], 0, 4, true);
			SetEntDataFloat(viewmodel_2, Offset[m_flPlaybackRate], 0.0, true);
			iResetCount[Client] = 0;
		}
	}
	hActiveWeapon[Client] = weapons;
}

public Weapon_AdjustWeapon(Client, weapons, &sequence, &Float:cycle, &Float:cycle2)
{
	new owner = GetOwnerEntity(weapons);
	if(!IsValidClient(owner) || !IsClientInGame(owner) || !IsPlayerAlive(owner)) {
		return;
	}
	
#if defined (_DEBUG_WEAPON)
	PrintToConsole(owner, "[DEBUG] adjust sequence : %d cycle : %f", sequence, cycle);
#endif
	
	if(IsCrossBow(weapons))
	{
		if(GetEntProp(weapons, Prop_Data, "m_bInReload"))
		{
			if(GetNextPrimaryFire(weapons) > 1.5)
			{
				SetNextPrimaryFire(weapons, 1.5);
				SetNextSecondaryFire(weapons, 1.5);
				SetEntPropFloat(owner, Prop_Send, "m_flNextAttack", GetGameTime()+1.5);
			}
		}
		if(2<= sequence <= 3) sequence = 1;
		else if(sequence == 4) sequence = 7;
		else if(sequence == 5) sequence = 4;
	}
	if(IsRPG(weapons))
	{
		if(sequence == 2) sequence = 1;
		else if(3<= sequence <= 5) sequence = 2;
	}
	if(IsNewtonLauncher(weapons))
	{
		if(sequence == 3) sequence = 4;
	}
	if(IsTVirus(weapons))
	{
		if(sequence == 1) sequence = 3;
		else if(sequence == 2) sequence = 1;
		else if(3<= sequence <= 4) sequence = 2;
		if(cycle < cycle2) {
			cycle = 0.01;
			cycle2 = 0.0;
		}
	}
	if(TTT_IsDisguise(weapons))
	{
		if(g_DisguiseTimer[Client] != INVALID_HANDLE || g_DisguiseTimer2[Client] != INVALID_HANDLE) sequence = 3;
	}
}

public Weapon_SendProxy(weapons)
{
	SendProxy_Hook(weapons, "m_iClip1", Prop_Int, SendProxy_Weapon);
	WeaponData[weapons][IsUsingSendProxy] = true;
}

public Action:Hook_BlockWeaponSwitch(Client, weapons)
{
	return Plugin_Handled;
}

public Action:Hook_BlockWeaponDrop(Client, weapons)
{
	return Plugin_Handled;
}