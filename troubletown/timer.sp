
public Action:g_ReadyTimer_Event(Handle:timer)
{
	if(RoundType != RoundState_GameReady) {
		g_ReadyTimer = INVALID_HANDLE;
		return Plugin_Stop;
	}
	new count = 0, total_clients[MAXPLAYERS+1];
	for(new i=1; i<=MaxClients; i++)
	{
		if(!IsClientInGame(i) || GetClientTeam(i) < 2) continue;
		total_clients[count] = i;
		if(IsPlayerAlive(i) == false)
		{
			CS_RespawnPlayer(i);
		}
		else
		{
			SetEntityHealth(i, 100+PlayerData[i][ExtraHealth]);
		}
		TTT_ResetPlayerData(i);
		count++;
	}
	
	if(count < 3)
	{
		PrintCenterTextAll("%t", "not enough player");
		if(GetReadyCount() != 20)
			SetReadyCount(20);
		return Plugin_Continue;
	}
	
	new Float:traitor_ratio = 0.25;
	new Float:detective_ratio = 0.125;
	
	new traitor_count = RoundToZero(traitor_ratio*count);
	if(traitor_count == 0) traitor_count = 1;
	
	new detective_count = RoundToZero(detective_ratio*count);
	if(count > 4 && detective_count == 0) detective_count = 1;
	
	new start_count = AddReadyCount(-1);
	if(start_count > 0)
	{
		PrintCenterTextAll("%t", "game will start", start_count, traitor_count, detective_count);
		return Plugin_Continue;
	}
	
	
	for(new i; i<traitor_count; i++)
	{
		new rand = GetRandomClients(total_clients,count);
		new player = total_clients[rand];
		
		TTT_SetPlayerJob(player, traitor);
		TTT_SetClientRemainCount(player, traitor_count);
		
		total_clients[rand] = 0;
		CS_SwitchTeam(player, 2);
	}
	for(new i; i<detective_count; i++)
	{
		new rand = GetRandomClients(total_clients,count);
		new player = total_clients[rand];
		
		TTT_SetPlayerJob(player, detective);
		TTT_SetClientRemainCount(player, traitor_count);
		
		total_clients[rand] = 0;
		CS_SwitchTeam(player, 3);
	}
	for(new i=1; i<=MaxClients; i++)
	{
		if(!IsClientInGame(i) || GetClientTeam(i) < 2) continue;
		
		new weapons = GetClientWeaponIndex(i);
		if(weapons != -1)
		{
			new Float:flStartDelay = 3.0;
			SetEntPropFloat(weapons, Prop_Send, "m_flNextPrimaryAttack", FloatAdd(GetGameTime(), flStartDelay));
		}
		ResetFragCount(i);
		ResetDeathCount(i);
		IncrementFragCount(i, 1);
		TTT_UpdateLog(i);
		GetClientModel(i, PlayerData[i][sOriginalModel], 128);
		for(new j; j<sizeof(g_skin_list); j++)
		{
			if(StrEqual(g_skin_list[j][1], PlayerData[i][sOriginalModel]))
			{
				PlayerData[i][iOriginalSkin] = StringToInt(g_skin_list[j][2]);
				break;
			}
		}
		
		if(g_isJailMode && PlayerData[i][Job] == detective) CS_RespawnPlayer(i);
		if(PlayerData[i][Job] != unselected) continue;
		
		TTT_SetPlayerJob(i, innocent);
		CS_SwitchTeam(i, 2);
	}
	ClearArray(g_damage_all_log);
	TTT_SetWeaponRemainCount(traitor_count, detective_count);
	TTT_ExecuteMapFunction("GameStart");
	
	//block respawn with playing game
	ChangeSourceTVTeam(3);
	
	SetGameCount(START_GAME_TIME);
	SetTraitorCount(0);
	
	CloseTimer(g_GameTimer);
	g_GameTimer = CreateTimer(1.0, g_GameTimer_Event,0,TIMER_REPEAT);
	RoundType = RoundState_GamePlay;
	
	g_ReadyTimer = INVALID_HANDLE;
	return Plugin_Stop;
}

public Action:g_GameTimer_Event(Handle:timer)
{
	if(RoundType != RoundState_GamePlay) {
		g_GameTimer = INVALID_HANDLE;
		return Plugin_Stop;
	}
	
	new count;
	if(GetGameCount() == 0)
		count = AddTraitorCount(-1);
	else
	{
		count = AddGameCount(-1);
		if(count == 0) // default time is ended. give credit them.
		{
			new players
			for(new i=1; i<=MaxClients; i++)
			{
				if(!IsClientInGame(i) || GetClientTeam(i) < 2 || !IsPlayerAlive(i)) continue;
				
				PlayerData[i][Credit]++;
				players++;
			}
		}
	}
	
	if(count == -1) // innocent win
	{
		CS_TerminateRound(10.0, CSRoundEnd_TargetSaved);
		g_GameTimer = INVALID_HANDLE;
		return Plugin_Stop;
	}
	return Plugin_Continue;
}


//called every 0.2s
public Action:g_HintTextTimer_Event(Handle:timer, any:Client)
{
	if(!IsClientConnected(Client) || !IsClientInGame(Client)) return Plugin_Stop;
	
	SetGlobalTransTarget(Client);
	
	new String:sMessage[255];
	new time = (RoundType == RoundState_GamePlay) ? GetGameCount() : GetReadyCount();
	new second = time%60, minute = time/60;
	new String:sTime[8], String:sExplodedString[2][4];
	Format(sExplodedString[0], 4, "%s%d", (minute<10)?"0":"", minute);
	Format(sExplodedString[1], 4, "%s%d", (second<10)?"0":"", second);
	ImplodeStrings(sExplodedString, 2, ":", sTime, sizeof(sTime));
	if(RoundType == RoundState_GamePlay)
	{
		new String:sExtendedTime[32];
		Format(sExtendedTime, sizeof(sExtendedTime), "%t", "extended time");
		Format(sMessage, sizeof(sMessage), "%t : %s\n", "time", time ? sTime : sExtendedTime);
		if(PlayerData[Client][Job] == traitor || !IsPlayerAlive(Client))
		{
			new traitor_time = GetGameCount()+GetTraitorCount();
			second = traitor_time%60;
			minute = traitor_time/60;
			new String:sTraitorTime[8];
			Format(sExplodedString[0], 4, "%s%d", (minute<10)?"0":"", minute);
			Format(sExplodedString[1], 4, "%s%d", (second<10)?"0":"", second);
			ImplodeStrings(sExplodedString, 2, ":", sTraitorTime, sizeof(sTraitorTime));
			Format(sMessage, sizeof(sMessage), "%s%t : %s\n", sMessage, "traitor's time", sTraitorTime);
		}
	}
	if(RoundType == RoundState_GameReady)
	{
		Format(sMessage, sizeof(sMessage), "%t : %s\n", "ready", sTime);
	}
	if(IsPlayerAlive(Client))
	{
		Format(sMessage, sizeof(sMessage), "%s%t : %t\n%t\n", sMessage, "job", JobName[PlayerData[Client][Job]], "you have credit", PlayerData[Client][Credit]);
		new weapons = GetClientWeaponIndex(Client);
		new weapon, String:sWeaponName[32], String:sKeyHintText[255];
		
		weapon = TTT_GetNextWeapon(Client, WEAPONTYPE_PRIMARY);
		TTT_GetWeaponName(weapon, sWeaponName, sizeof(sWeaponName));
		Format(sKeyHintText, sizeof(sKeyHintText), "Next Primary : %s\n", sWeaponName);
		
		weapon = TTT_GetNextWeapon(Client, WEAPONTYPE_SECONDARY);
		TTT_GetWeaponName(weapon, sWeaponName, sizeof(sWeaponName));
		Format(sKeyHintText, sizeof(sKeyHintText), "%sNext Secondary : %s\n", sKeyHintText, sWeaponName);
		
		weapon = TTT_GetNextWeapon(Client, WEAPONTYPE_KNIFE);
		TTT_GetWeaponName(weapon, sWeaponName, sizeof(sWeaponName));
		Format(sKeyHintText, sizeof(sKeyHintText), "%sNext Knife : %s\n", sKeyHintText, sWeaponName);
		
		weapon = TTT_GetNextWeapon(Client, WEAPONTYPE_C4);
		TTT_GetWeaponName(weapon, sWeaponName, sizeof(sWeaponName));
		Format(sKeyHintText, sizeof(sKeyHintText), "%sNext Tool : %s\n", sKeyHintText, sWeaponName);
		
		TTT_GetWeaponName(weapons, sWeaponName, sizeof(sWeaponName));
		Format(sKeyHintText, sizeof(sKeyHintText), "%s\nCurrent Weapon : %s\n", sKeyHintText, sWeaponName);
		if(PlayerData[Client][bExecuteDisguise] || TTT_IsDisguise(weapons)) {
			Format(sKeyHintText, sizeof(sKeyHintText), "%sRemain Energy : %d\n", sKeyHintText, Disguise_GetRemainEnergy(Client));
		}
		if(TTT_IsTeleporter(weapons)) {
			Format(sKeyHintText, sizeof(sKeyHintText), "%sRemain Amount : %d\n", sKeyHintText, Teleport_GetRemainCount(weapons));
		}
		
		KeyHintText(Client, sKeyHintText);
		
		static one_second[MAXPLAYERS+1];
		if(IsWeapon(weapons) && IsNewtonLauncher(weapons) && FloatSub(GetGameTime(), GetEntPropFloat(weapons, Prop_Send, "m_flNextPrimaryAttack")) > 2.0)
		{
			one_second[Client]++;
			if(one_second[Client] % 5 == 0)
			{
				new ammo = GetEntProp(weapons, Prop_Send, "m_iClip1") + 2;
				if (ammo > 120) ammo = 120;
				SetEntProp(weapons, Prop_Send, "m_iClip1", ammo);
				one_second[Client] = 0;
			}
		}
		else
		{
			one_second[Client] = 0;
		}
		if(g_DisguiseTimer[Client] != INVALID_HANDLE || g_DisguiseTimer2[Client] != INVALID_HANDLE)
		{
			Disguise_AddRemainEnergy(Client, -4);
		}
		else if(PlayerData[Client][bExecuteDisguise])
		{
			Disguise_AddRemainEnergy(Client, -1);
		}
		if(Disguise_GetRemainEnergy(Client) < 0) Disguise_AddRemainEnergy(Client, -Disguise_GetRemainEnergy(Client));
	}
	if(g_isJailMode)
	{
		Format(sMessage, sizeof(sMessage), "%s Jail Command : %s \n", sMessage, g_sJailCommand);
	}
	PrintHintText(Client, sMessage);
	
	//Flash for blocking radar
	if(IsPlayerAlive(Client) && !PlayerData[Client][blockFlash])
	{
		SetEntPropFloat(Client, Prop_Send, "m_flFlashDuration", GetRandomFloat(10.0, 10.1));
		SetEntPropFloat(Client, Prop_Send, "m_flFlashMaxAlpha", GetRandomFloat(0.0, 0.1));
	}
	//new String:sTag[16];
	//Shop_GetClientTag(Client, sTag, sizeof(sTag));
	//CS_SetClientClanTag(Client, sTag);
	
	return Plugin_Continue;
}

public Action:g_FastChatTimer_Event(Handle:timer, any:Client)
{
	if(IsClientInGame(Client))
	{
		CancelClientMenu(Client);
	}
	PlayerData[Client][openFastChat] = false;
	PlayerData[Client][playerFastChat] = 0;
	g_FastChatTimer[Client] = INVALID_HANDLE;
}

public Action:g_BlindTimer_Event(Handle:timer, any:Client)
{
	PlayerData[Client][blockFlash] = false;
	g_BlindTimer[Client] = INVALID_HANDLE;
}

public Action:g_WillTimer_Event(Handle:timer, Handle:datapack)
{
	ResetPack(datapack);
	new Client = ReadPackCell(datapack);
	new type = ReadPackCell(datapack);
	new String:sMsg[255], String:sTargetName[32];
	ReadPackString(datapack, sMsg, sizeof(sMsg));
	ReadPackString(datapack, sTargetName, sizeof(sTargetName));
	g_WillTimer[Client] = INVALID_HANDLE;
	if(RoundType == RoundState_GamePlay && IsClientInGame(Client) && !IsPlayerAlive(Client))
	{
		new String:sTag[20] = "\x07888888[Will] -";
		new String:buffer[200];
		if(type == WILLTYPE_CHAT)
		{
			Format(buffer, sizeof(buffer), "%s", sMsg);
		}
		if(type == WILLTYPE_FASTCHAT)
		{
			Format(buffer, sizeof(buffer), "%t", sMsg);
		}
		if(type == WILLTYPE_FASTCHAT_PLAYER || type == WILLTYPE_FASTCHAT_LONGDISTANCE)
		{
			Format(buffer, sizeof(buffer), "%t", sMsg, sTargetName);
		}
		SaveChatMessage(Client, 3, buffer);
		new String:sWill[255];
		Format(sWill, sizeof(sWill), "%s %N :  %s", sTag, Client, buffer);
		PrintToChatAll(sWill);
	}
}

public Action:g_TeleportRagdoll_Event(Handle:timer, any:ref)
{
	new entity = EntRefToEntIndex(ref);
	if(IsValidEntity(entity))
	{
		TeleportEntity(entity, Float:{10000.0, 10000.0, 10000.0}, NULL_VECTOR, NULL_VECTOR);
		SetEntityMoveType(entity, MOVETYPE_NONE);
	}
}

public Action:g_RemoveReference_Event(Handle:timer, any:ref)
{
	new entity = EntRefToEntIndex(ref);
	if(IsValidEntity(entity))
	{
		AcceptEntityInput(entity, "Kill");
	}
}

public Action:g_OpenTheDoor_PreEvent(Handle:timer)
{
	g_isOpendDoor = true;
	g_DoorTimer = CreateTimer(0.1, g_OpenTheDoor_Event, _, TIMER_FLAG_NO_MAPCHANGE);
	PrintToChatAll("\x04[TTT - Jail] \x01- 명령이 없으므로 문이 열립니다.");
}

public Action:g_OpenTheDoor_Event(Handle:timer)
{
	new ent;
	while((ent = FindEntityByClassname(ent, "func_door")) != -1)
	{
		AcceptEntityInput(ent, "Open");
	}
	while((ent = FindEntityByClassname(ent, "func_door_rotating")) != -1)
	{
		AcceptEntityInput(ent, "Open");
	}
	while((ent = FindEntityByClassname(ent, "prop_door_rotating")) != -1)
	{
		AcceptEntityInput(ent, "Open");
	}
	
	g_DoorTimer = INVALID_HANDLE;
}