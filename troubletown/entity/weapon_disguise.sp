

stock GetDisguise(Client)
{
	if(!IsValidClient(Client) || !IsClientInGame(Client) || !IsPlayerAlive(Client))
	{
		return -1;
	}
	new weapon = TTT_GetWeaponFromList(Client, WEAPONSLOT_C4_1);
	if(TTT_IsDisguise(weapon))
	{
		return weapon;
	}
	weapon = TTT_GetWeaponFromList(Client, WEAPONSLOT_C4_2);
	if(TTT_IsDisguise(weapon))
	{
		return weapon;
	}
	return -1;
}

stock Disguise_GetRemainEnergy(Client)
{
	new weapons = GetDisguise(Client);
	if(!IsWeapon(weapons) || !TTT_IsDisguise(weapons)) {
		return 0;
	}
	return Disguise[weapons][iRemainEnergy];
}

stock Disguise_AddRemainEnergy(Client, iCount)
{
	new weapons = GetDisguise(Client);
	if(!IsWeapon(weapons)) {
		Disguise_Off(Client);
		return;
	}
	Disguise[weapons][iRemainEnergy] += iCount;
	
	if(Disguise[weapons][iRemainEnergy] <= 0 && PlayerData[Client][bExecuteDisguise])
	{
		Disguise_Off(Client);
	}
}

stock Disguise_Off(Client)
{
	PlayerData[Client][bExecuteDisguise] = false;
	
	new String:sModelName[128], String:sSkinIndex[8];
	Format(sModelName, 128, PlayerData[Client][sOriginalModel]);
	IntToString(PlayerData[Client][iOriginalSkin], sSkinIndex, sizeof(sSkinIndex));
	
	SetEntityModel(Client, sModelName);
	ServerCommand("pointshop_adjust_hat %d %s", GetClientUserId(Client), sSkinIndex);
	SayText2Message(Client, Client, "\x04[TTT Disguise] \x01%T %T.", "Disguise", Client, "off", Client);
}

public PlayerRunCmd_Disguise(Client, buttons, oldbuttons)
{
	new weapons = GetClientWeaponIndex(Client);
	if(!TTT_IsDisguise(weapons)) {
		return;
	}
	
	if(g_DisguiseTimer[Client] != INVALID_HANDLE || g_DisguiseTimer2[Client] != INVALID_HANDLE)
	{
		return;
	}
	
	if(!(oldbuttons & IN_ATTACK) && buttons & IN_ATTACK && Disguise[weapons][iRemainEnergy] > 0)
	{
		TTT_SetProgressBar(Client, GetGameTime(), 1);
		SetEntityMoveType(Client, MOVETYPE_NONE);
		SetEntityRenderMode(Client, RENDER_TRANSCOLOR);
		
		SDKHook(Client, SDKHook_PostThinkPost, Hook_CheckDisguise);
		SDKHook(Client, SDKHook_WeaponSwitch, Hook_BlockWeaponSwitch);
		SDKHook(Client, SDKHook_WeaponDrop, Hook_BlockWeaponDrop);
		g_DisguiseTimer[Client] = CreateTimer(DISGUISE_ALL_DELAY/2, g_DisguiseTimer_Event, Client);
		Disguise[weapons][flDelay] = DISGUISE_ALL_DELAY;
		Disguise[weapons][fStartTime] = GetEngineTime();
	}
	if(!(oldbuttons & IN_ATTACK2) && buttons & IN_ATTACK2) {
		DisplayMenu(g_disguise_Menu, Client, MENU_TIME_FOREVER);
	}
}

public Hook_CheckDisguise(Client)
{
	if(g_DisguiseTimer[Client] != INVALID_HANDLE || g_DisguiseTimer2[Client] != INVALID_HANDLE)
	{
		if(!IsPlayerAlive(Client))
		{
			SDKUnhook(Client, SDKHook_PostThinkPost, Hook_CheckDisguise);
			SDKUnhook(Client, SDKHook_WeaponSwitch, Hook_BlockWeaponSwitch);
			SDKUnhook(Client, SDKHook_WeaponDrop, Hook_BlockWeaponDrop);
			CloseTimer(g_DisguiseTimer[Client]);
			CloseTimer(g_DisguiseTimer2[Client]);
			return;
		}
		
		new weapons = GetDisguise(Client);
		if(weapons == -1)
		{
			TTT_SetProgressBar(Client, 0.0, 0);
			SetEntityMoveType(Client, MOVETYPE_WALK);
			SetEntityRenderColor(Client, 255, 255, 255, 255);
			
			SDKUnhook(Client, SDKHook_PostThinkPost, Hook_CheckDisguise);
			SDKUnhook(Client, SDKHook_WeaponSwitch, Hook_BlockWeaponSwitch);
			SDKUnhook(Client, SDKHook_WeaponDrop, Hook_BlockWeaponDrop);
			return;
		}
		
		new alpha = RoundToNearest(FloatAbs(Disguise[weapons][flDelay]/2 - (GetEngineTime() - Disguise[weapons][fStartTime]))*(510.0/Disguise[weapons][flDelay]-1.0));
		if(alpha > 255) alpha = 255;
		SetEntityRenderColor(Client, 255, 255, 255, alpha);
	}
	else
	{
		SDKUnhook(Client, SDKHook_PostThinkPost, Hook_CheckDisguise);
		SDKUnhook(Client, SDKHook_WeaponSwitch, Hook_BlockWeaponSwitch);
		SDKUnhook(Client, SDKHook_WeaponDrop, Hook_BlockWeaponDrop);
	}
}

public Action:g_DisguiseTimer_Event(Handle:timer, any:Client)
{
	g_DisguiseTimer[Client] = INVALID_HANDLE;
	
	new weapons = GetClientWeaponIndex(Client);
	if(!TTT_IsDisguise(weapons))
	{
		return;
	}
	
	if(Disguise[weapons][flDelay] > 2.0)
	{
		PlayerData[Client][bExecuteDisguise] = !PlayerData[Client][bExecuteDisguise];
		
		new String:sModelName[128], String:sSkinIndex[8];
		Format(sModelName, 128, PlayerData[Client][sOriginalModel]);
		IntToString(PlayerData[Client][iOriginalSkin], sSkinIndex, sizeof(sSkinIndex));
		new skin_index = PlayerData[Client][iDisguisedSkin];
		SetEntityModel(Client, PlayerData[Client][bExecuteDisguise] ? g_skin_list[skin_index][1] : sModelName);
		ServerCommand("pointshop_adjust_hat %d %s", GetClientUserId(Client), PlayerData[Client][bExecuteDisguise] ? g_skin_list[skin_index][2] : sSkinIndex);
	}
	Format(PlayerData[Client][sDisguisedName], 32, PlayerData[Client][sDisguisePrepareName]);
	
	g_DisguiseTimer2[Client] = CreateTimer(Disguise[weapons][flDelay]/2, g_DisguiseTimer2_Event, Client);
}

public Action:g_DisguiseTimer2_Event(Handle:timer, any:Client)
{
	g_DisguiseTimer2[Client] = INVALID_HANDLE;
	
	new weapons = GetClientWeaponIndex(Client);
	if(!TTT_IsDisguise(weapons))
	{
		return;
	}
	
	TTT_SetProgressBar(Client, 0.0, 0);
	SetEntityMoveType(Client, MOVETYPE_WALK);
	SetEntityRenderColor(Client, 255, 255, 255, 255);
	SayText2Message(Client, Client, "\x04[TTT Disguise] \x01%T %T.", "Disguise", Client, PlayerData[Client][bExecuteDisguise]?"on":"off", Client);
	
	SDKUnhook(Client, SDKHook_PostThinkPost, Hook_CheckDisguise);
	SDKUnhook(Client, SDKHook_WeaponSwitch, Hook_BlockWeaponSwitch);
	SDKUnhook(Client, SDKHook_WeaponDrop, Hook_BlockWeaponDrop);
}