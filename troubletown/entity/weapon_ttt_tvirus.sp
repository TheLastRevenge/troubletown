
#define TVIRUS_DELAY 3

stock ActiveTVirus(Client)
{
	if(g_TVirusTimer[Client] == INVALID_HANDLE)
	{
		g_TVirusTimer[Client] = CreateTimer(15.0, g_TVirusTimer_Event, Client);
	}
}

public Action:g_TVirusTimer_Event(Handle:timer, any:Client)
{
	g_TVirusTimer[Client] = INVALID_HANDLE
	if(!IsClientInGame(Client) || !IsPlayerAlive(Client)) {
		return;
	}
	
	SetEntityMoveType(Client, MOVETYPE_NONE);
	PerformFade(Client, 15.0, 0, 255, 0, 255);
	
	PlayerData[Client][bIsInfected] = true;
	PlayerData[Client][flLastInfectTime] = GetGameTime();
	
	g_TVirusTimer[Client] = CreateTimer(float(TVIRUS_DELAY), g_PerformTVirus_Event, Client);
}

public Action:g_PerformTVirus_Event(Handle:timer, any:Client)
{
	g_TVirusTimer[Client] = INVALID_HANDLE
	if(!IsClientInGame(Client) || !IsPlayerAlive(Client)) {
		return;
	}
	
	// Makes Zombie
	new Float:vecOrigin[3];
	GetClientAbsOrigin(Client, vecOrigin);
	if(GetRandomInt(0, 1) == 1)
	{
		ServerCommand("spawn_fastzombie %f %f %f", vecOrigin[0], vecOrigin[1], vecOrigin[2]);
	}
	else
	{
		ServerCommand("spawn_poisonzombie %f %f %f", vecOrigin[0], vecOrigin[1], vecOrigin[2]);
	}
	EmitSoundToAll("troubletown/biohazard_detected.wav");
	
	TTT_CheckCreditAward(traitor, PlayerData[Client][Job]);
	AddTraitorCount(30);
	
	CommitSuicide(Client);
}