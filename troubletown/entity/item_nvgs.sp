
#define NIGHTVISION_DELAY	0.25

enum NVGS_DATA {
	hNightVisionBeam,
}
new NightVision[MAXPLAYERS+1][NVGS_DATA];

stock SpawnNightVision(Client)
{
	RemoveReference(NightVision[Client][hNightVisionBeam]);
	if(!PlayerData[Client][HasNightVision]) {
		return;
	}
	SetEntProp(Client, Prop_Send, "m_bHasNightVision", 1);
	SetEntProp(Client, Prop_Send, "m_bNightVisionOn", 1);
	
	new Float:Position[3];
	GetClientAbsOrigin(Client, Position);
	Position[2] += 32.0;
	new entity = CreateEntityByName("light_dynamic");
	DispatchKeyValue(entity, "_light", "10 10 10");
	DispatchKeyValue(entity, "brightness", "1");
	DispatchKeyValue(entity, "spotlight_radius", "0");
	DispatchKeyValue(entity, "pitch", "-90");
	DispatchKeyValue(entity, "distance", "4096");
	DispatchSpawn(entity);
	AcceptEntityInput(entity, "TurnOn");
	TeleportEntity(entity, Position, NULL_VECTOR, NULL_VECTOR);
	SetVariantString("!activator");
	AcceptEntityInput(entity, "SetParent", Client, entity, 0);
	SDKHook(entity, SDKHook_SetTransmit, Hook_SetTransmit_NightVision);
	NightVision[Client][hNightVisionBeam] = EntIndexToEntRef(entity);
}

public Action:Hook_SetTransmit_NightVision(entity, Client)
{
	if(!IsClientInGame(Client) || !IsPlayerAlive(Client)) {
		return Plugin_Continue;
	}
	if(EntRefToEntIndex(NightVision[Client][hNightVisionBeam]) == entity || IsClientSourceTV(Client))
	{
		return Plugin_Continue;
	}
	return Plugin_Handled;
}

public NightVision_Delay(Client)
{
	CreateTimer(NIGHTVISION_DELAY, NightVision_Timer_Event, Client, TIMER_FLAG_NO_MAPCHANGE);
}

public Action:NightVision_Timer_Event(Handle:timer, any:Client)
{
	if(!IsClientInGame(Client) || !IsPlayerAlive(Client)) {
		return;
	}
	
	if(PlayerData[Client][HasNightVision])
	{
		new bNightVisionOn = GetEntProp(Client, Prop_Send, "m_bNightVisionOn");
		new String:sInput[16];
		Format(sInput, sizeof(sInput), "%s", bNightVisionOn ? "TurnOn" : "TurnOff");
		AcceptEntityInput(NightVision[Client][hNightVisionBeam], sInput, Client);
	}
}

public NightVision_Remove(Client)
{
	RemoveReference(NightVision[Client][hNightVisionBeam]);
}