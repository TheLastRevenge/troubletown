
#define JIHAD_EXPLODE_DELAY	2.5

stock bool:Jihad_CheckExecute(Client)
{
	if(!IsValidClient(Client)) {
		return false;
	}
	
	return PlayerData[Client][bExecuteJihad];
}

stock PerformJihad(Client, weapons, bool:bSecondary = false)
{
	if(!IsClientInGame(Client) || !IsPlayerAlive(Client) || !TTT_IsJihad(weapons)) {
		return;
	}
	
	if(bSecondary)
	{
		if(GetNextSecondaryFire(weapons) < 0.0)
		{
			SetNextSecondaryFire(weapons, 3.0);
			TTT_EmitDeathSound(Client);
		}
		return;
	}
	if(GetNextPrimaryFire(weapons) < 0.0)
	{
		SetNextPrimaryFire(weapons, JIHAD_EXPLODE_DELAY+0.5);
		SendWeaponAnim(weapons, ACT_VM_PRIMARYATTACK);
		TTT_SetProgressBar(Client, GetGameTime()+0.5, 2);
		
		EmitSoundToAll("troubletown/jihad_fix.wav", Client, SNDCHAN_WEAPON, ATTN_TO_SNDLEVEL(0.7));
		CreateTimer(JIHAD_EXPLODE_DELAY, JihadTimer_Event, Client);
		PlayerData[Client][bExecuteJihad] = true;
	}
}

public Action:JihadTimer_Event(Handle:timer, any:Client)
{
	if(IsClientInGame(Client) && IsPlayerAlive(Client)) {
		new Float:vecOrigin[3];
		GetClientAbsOrigin(Client, vecOrigin);
		vecOrigin[2] += 10.0;
		new ent = makeExplosion(Client, GetClientWeaponIndex(Client), vecOrigin, "weapon_jihadbomb", 275, 0, 0.0, 2);
		EmitSoundToAll("troubletown/big_explosion.wav", ent, SNDCHAN_AUTO, ATTN_TO_SNDLEVEL(0.65));
	}
	PlayerData[Client][bExecuteJihad] = false;
}