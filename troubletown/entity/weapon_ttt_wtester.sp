
#define MAX_SAMPLES 9

#define CHARGE_DELAY 0.1

enum DNA_DATA {
	DNAScanIndex,
	Float:DNAScanStartTime,
	
	m_hSprite,
};
new DNAScanData[2048][DNA_DATA];

enum DNA_SAMPLE_DATA {
	m_hDNASearch,
	String:m_szDNAName[32],
};
new DNASampleData[2048][MAX_SAMPLES][DNA_SAMPLE_DATA];

public ShowDNAScanMenu(Client, weapons)
{
	new Handle:menuhandle = g_dnascan_Menu[Client];
	RemoveAllMenuItems(menuhandle);
	
	SetMenuTitle(menuhandle, "DNA Scan");
	for(new i; i<MAX_SAMPLES; i++)
	{
		new index = DNASampleData[weapons][i][m_hDNASearch];
		if(index) {
			AddMenuItem(menuhandle, "", DNASampleData[weapons][i][m_szDNAName], ITEMDRAW_DEFAULT);
		} else {
			AddMenuItem(menuhandle, "", "", ITEMDRAW_SPACER);
		}
	}
	SetMenuExitButton(menuhandle, true);
	DisplayMenu(menuhandle, Client, 10);
}

public PlayerRunCmd_DNAScanner(Client, buttons, oldbuttons)
{
	new weapons = GetClientWeaponIndex(Client);
	if(!TTT_IsTester(weapons)) {
		return;
	}
	
	if(!(oldbuttons & IN_ATTACK) && buttons & IN_ATTACK)
	{
		new ragdoll = TTT_GetEntityFromAim(Client, true), bool:have_dna;
		if(IsValidEntity(ragdoll)) have_dna = g_Entity_DNA[ragdoll] > 0;
		
		if(IsRagdollEntity(ragdoll) || have_dna)
		{
			new Float:vecPlayerOrigin[3], Float:vecRagdollOrigin[3];
			GetClientAbsOrigin(Client, vecPlayerOrigin);
			GetEntPropVector(ragdoll, Prop_Send, "m_vecOrigin", vecRagdollOrigin);
			
			if(GetVectorDistance(vecPlayerOrigin, vecRagdollOrigin) < RAGDOLL_SEARCH_DIST)
			{
				if(CollectDNA(Client, weapons, ragdoll))
				{
					if(have_dna)
					{
						new String:entityName[32];
						GetEdictClassname(ragdoll, entityName, sizeof(entityName));
						PrintToChat(Client, "\x04[TTT] \x01- You got DNA from %s", entityName);
					}
					else
					{
						new String:ragdollName[32];
						TTT_GetRagdollPlayerName(ragdoll, ragdollName, sizeof(ragdollName));
						PrintToChat(Client, "\x04[TTT] \x01- You got DNA from ragdoll %s", ragdollName);
					}
				}
			}
		}
	}
	
	if(!(oldbuttons & IN_ATTACK2) && buttons & IN_ATTACK2) {
		ShowDNAScanMenu(Client, weapons);
	}
}

stock bool:CollectDNA(Client, tester, entity)
{
	if(!IsClientInGame(Client) || !IsPlayerAlive(Client)) {
		return false;
	}
	
	new free_index = FindFreeDNAScanIndex(tester);
	if(free_index == -1) {
		PrintToChat(Client, "\x04[TTT] \x01- Storage limit reached");
		return false;
	}
	
	new dna = TTT_GetRagdollDNA(entity);
	if(dna) {
		new String:ragdollName[32];
		TTT_GetRagdollPlayerName(entity, ragdollName, sizeof(ragdollName));
		Format(DNASampleData[tester][free_index][m_szDNAName], 32, ragdollName);
		DNASampleData[tester][free_index][m_hDNASearch] = dna;
		TTT_RemoveRagdollDNA(entity);
		return true;
	}
	
	if(g_Entity_DNA[entity]) {
		new String:entityName[32];
		GetEdictClassname(entity, entityName, sizeof(entityName));
		TTT_GetWeaponName2(entity, DNASampleData[tester][free_index][m_szDNAName], 32);
		DNASampleData[tester][free_index][m_hDNASearch] = g_Entity_DNA[entity];
		g_Entity_DNA[entity] = 0;
		return true;
	}
	
	PrintToChat(Client, "\x04[TTT] \x01- Can't get DNA from this");
	return false;
}

stock FindFreeDNAScanIndex(tester)
{
	for(new i; i<MAX_SAMPLES; i++)
	{
		new index = DNASampleData[tester][i][m_hDNASearch];
		if(!index) {
			return i;
		}
	}
	return -1;
}

stock RemoveDNA(tester)
{
	for(new i; i<MAX_SAMPLES; i++)
	{
		DNASampleData[tester][i][m_hDNASearch] = 0;
	}
}

//TODO optimize this
stock DropTester(Client)
{
	new tester = GetTester(Client);
	if(tester != -1)
	{
		SetAllowDrop(Client, true);
		CS_DropWeapon(Client, tester, false);
	}
}

stock GetTester(Client)
{
	if(!IsValidClient(Client) || !IsClientInGame(Client) || !IsPlayerAlive(Client))
	{
		return -1;
	}
	new weapon = TTT_GetWeaponFromList(Client, WEAPONSLOT_C4_1);
	if(TTT_IsTester(weapon))
	{
		return weapon;
	}
	weapon = TTT_GetWeaponFromList(Client, WEAPONSLOT_C4_2);
	if(TTT_IsTester(weapon))
	{
		return weapon;
	}
	return -1;
}

public DNAScan(Client, Number)
{
	new weapons = GetClientWeaponIndex(Client);
	if(!TTT_IsTester(weapons)) {
		return;
	}
	
	if(Number < 0 || Number >= MAX_SAMPLES) {
		return;
	}
	
	CloseTimer(g_DNAScanTimer[Client]);
	g_DNAScanTimer[Client] = CreateTimer(CHARGE_DELAY, g_DNAScanTimer_Event, Client, TIMER_REPEAT);
	DNAScanData[weapons][DNAScanIndex] = Number;
	DNAScanData[weapons][DNAScanStartTime] = GetGameTime();
	TTT_SetProgressBar(Client, GetGameTime(), 15);
	PrintToChat(Client, "\x04[TTT] \x01- Start Scanning DNA \x07FFFFFF%s", DNASampleData[weapons][Number][m_szDNAName]);
}

public Action:g_DNAScanTimer_Event(Handle:timer, any:Client)
{
	static iAnimationCount[MAXPLAYERS+1];
	if(!IsClientInGame(Client) || !IsPlayerAlive(Client)) {
		g_DNAScanTimer[Client] = INVALID_HANDLE;
		iAnimationCount[Client] = 0;
		return Plugin_Stop;
	}
	
	new weapons = GetClientWeaponIndex(Client);
	if(!TTT_IsTester(weapons)) {
		g_DNAScanTimer[Client] = INVALID_HANDLE;
		TTT_SetProgressBar(Client, 0.0, 0);
		iAnimationCount[Client] = 0;
		return Plugin_Stop;
	}
	if(iAnimationCount[Client] == 0) {
		SendWeaponAnim(weapons, ACT_VM_PRIMARYATTACK);
	} else if(iAnimationCount[Client] >= 25) {
		SendWeaponAnim(weapons, ACT_VM_IDLE);
	}
	iAnimationCount[Client]++;
	
	new scan_index = DNAScanData[weapons][DNAScanIndex];
	
	new target, bool:success;
	target = DNASampleData[weapons][scan_index][m_hDNASearch];
	if(!IsValidClient(target) || !IsClientInGame(target)) {
		g_DNAScanTimer[Client] = INVALID_HANDLE;
		PrintToChat(Client, "\x04[TTT] \x01- DNA Scan Error : Can't get destination");
		TTT_SetProgressBar(Client, 0.0, 0);
		iAnimationCount[Client] = 0;
		SendWeaponAnim(weapons, ACT_VM_IDLE);
		return Plugin_Stop;
	}
	
	if(!IsPlayerAlive(target)) {
		new ragdoll = TTT_GetPlayerRagdoll(target);
		if(IsValidEdict(ragdoll))
		{
			target = ragdoll;
			success = true;
		}
	} else {
		success = true;
	}
	
	if(!success) {
		g_DNAScanTimer[Client] = INVALID_HANDLE;
		PrintToChat(Client, "\x04[TTT] \x01- DNA Scan Error : Can't get destination");
		TTT_SetProgressBar(Client, 0.0, 0);
		iAnimationCount[Client] = 0;
		SendWeaponAnim(weapons, ACT_VM_IDLE);
		return Plugin_Stop;
	}
	
	new Float:vecOrigin[3], Float:vecTargetOrigin[3];
	GetClientAbsOrigin(Client, vecOrigin);
	GetEntPropVector(target, Prop_Send, "m_vecOrigin", vecTargetOrigin);
	
	new Float:fValue = FloatSub(150.0, FloatDiv(GetVectorDistance(vecOrigin, vecTargetOrigin), 30.0))/1000.0;
	new Float:decreaseTime = fmax(-0.05, fValue);
	DNAScanData[weapons][DNAScanStartTime] -= decreaseTime;
	
	if(FloatSub(GetGameTime(), DNAScanData[weapons][DNAScanStartTime]) < 15.0) {
		TTT_SetProgressBar(Client, DNAScanData[weapons][DNAScanStartTime]);
		return Plugin_Continue;
	}
	
	new hSpriteEntity = EntRefToEntIndex(DNAScanData[weapons][m_hSprite]);
	if(!hSpriteEntity || !IsValidEdict(hSpriteEntity)) {
		new entity = CreateEntityByName("info_target");
		DispatchSpawn(entity);
		TeleportEntity(entity, vecTargetOrigin, NULL_VECTOR, NULL_VECTOR);
		DNAScanData[weapons][m_hSprite] = EntIndexToEntRef(entity);
		new sprite = CreateSprite(vecTargetOrigin, "materials/effects/strider_bulge_dudv_DX60.vmt", 0.25, true, entity);
		SDKHook(sprite, SDKHook_SetTransmit, Hook_SetTransmit_DNASprite);
		SetOwnerEntity(sprite, Client);
	} else {
		TeleportEntity(hSpriteEntity, vecTargetOrigin, NULL_VECTOR, NULL_VECTOR);
	}
	
	EmitSoundToClient(Client, "buttons/blip2.wav", Client);
	TTT_SetProgressBar(Client, GetGameTime());
	iAnimationCount[Client] = 0;
	DNAScanData[weapons][DNAScanStartTime] = GetGameTime();
	return Plugin_Continue;
}