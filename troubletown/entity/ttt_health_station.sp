
enum HealthStation {
	m_iHealth[2048],
	m_iRemainHealth[2048],
	Float:m_fLastSoundTime[2048],
	Float:m_fNextHeal[2048],
	m_hPlacer[2048],
	bool:Detectives[2048]
};

new TTT_HealthStation[HealthStation];

#define HealthStation_MaxHeal 25
#define HealthStation_MaxStored 200
#define HealthStation_RechargeRate 1.0
#define HealthStation_RechargeFreq 2
#define HealthStation_HealFreq 0.2

#define HealthStation_HealSound "items/medshot4.wav"
#define HealthStation_FailSound "items/medshotno1.wav"

stock SpawnHealthStation(Client)
{
	new Float:spawn_position[3], Float:player_angle[3], Float:player_velocity[3], Float:throw_velocity[3];
	GetClientEyePosition(Client, spawn_position);
	GetClientEyeAngles(Client, player_angle);
	GetEntPropVector(Client, Prop_Data, "m_vecVelocity", player_velocity);
	GetAngleVectors(player_angle, throw_velocity, NULL_VECTOR, NULL_VECTOR);
	ScaleVector(throw_velocity, 200.0+GetVectorLength(player_velocity));
	
	new entity = CreateEntityByName("prop_physics_override");
	DispatchKeyValue(entity, "model", "models/items/healthkit.mdl");
	DispatchKeyValue(entity, "classname", "ttt_health_station");
	DispatchSpawn(entity);
	TeleportEntity(entity, spawn_position, NULL_VECTOR, throw_velocity);
	SetEntProp(entity, Prop_Send, "m_CollisionGroup", 11);//COLLISION_GROUP_WEAPON
	SetEntPropFloat(entity, Prop_Send, "m_flModelScale", 2.0);
	SetPlayerCanPickup(entity, false);
	TTT_HealthStation[m_iHealth][entity] = 200;
	TTT_HealthStation[m_iRemainHealth][entity] = 200;
	TTT_HealthStation[m_fLastSoundTime][entity] = 0.0;
	TTT_HealthStation[m_fNextHeal][entity] = 0.0;
	TTT_HealthStation[m_hPlacer][entity] = EntIndexToEntRef(Client);
	TTT_HealthStation[Detectives][entity] = false;
	EmitSoundToAll("weapons/slam/throw.wav", entity, SNDCHAN_VOICE, _, _, 0.7);
	//HookSingleEntityOutput(entity, "OnPlayerUse", HealthStation_Use, false);//we can't use this function T.T
	SDKHook(entity, SDKHook_OnTakeDamage, HealthStation_OnTakeDamage);
	if(PlayerData[Client][Job] == detective)
	{
		TTT_HealthStation[Detectives][entity] = true;
		CreateTimer(HealthStation_RechargeRate, Timer_HealthStation_Recharge, entity, TIMER_REPEAT|TIMER_FLAG_NO_MAPCHANGE);
	}
}

public HealthStation_Use(const String:output[], entity, player, Float:delay)
{
	//Todo : entity == player, how to fix that?
	if(IsValidClient(player))
	{
		if(TTT_HealthStation[m_fNextHeal][entity] < GetEngineTime())
		{
			new healed = GiveHealth(entity, player);
			TTT_HealthStation[m_fNextHeal][entity] = GetEngineTime() + (HealthStation_HealFreq * healed);
		}
	}
}

public Action:HealthStation_OnTakeDamage(entity, &attacker, &inflictor, &Float:damage, &damagetype, &weapon, Float:damageForce[3], Float:damagePosition[3], damagecustom)
{
	new owner = EntRefToEntIndex(TTT_HealthStation[m_hPlacer][entity]);
	if(owner == attacker) return Plugin_Handled;
	
	TTT_HealthStation[m_iHealth][entity] -= RoundToNearest(damage);
	
	if(TTT_HealthStation[m_iHealth][entity] < 0)
	{
		SDKUnhook(entity, SDKHook_OnTakeDamage, HealthStation_OnTakeDamage);
		/* TODO : set achievement
		if(IsValidClient(attacker) && PlayerData[attacker][Job] == traitor)
		{
			AchievementCheck(attacker, ACHIEVEMENT_BREAK_HEALTHSTATION);
			if(TTT_HealthStation[Detectives][entity] == true)
			{
				AchievementCheck(attacker, ACHIEVEMENT_BREAK_HEALTHSTATION_DETECTIVE);
			}
		}*/
		AcceptEntityInput(entity, "Kill");
		if(IsValidClient(owner) && IsClientInGame(owner) && IsPlayerAlive(owner))
		{
			PrintHintText(owner, "Your Health Station has been destroyed!");
		}
	}
	return Plugin_Continue;
}

public Action:Timer_HealthStation_Recharge(Handle:timer, any:entity)
{
	if(IsHealthStationEntity(entity) == false)
	{
		return Plugin_Stop;
	}
	
	AddToStorage(entity, HealthStation_RechargeFreq);
	return Plugin_Continue;
}

stock GiveHealth(entity, player)
{
	if(GetStoredHealth(entity) == 0)
	{
		EmitSoundToAll(HealthStation_FailSound, entity, SNDCHAN_VOICE, ATTN_TO_SNDLEVEL(1.3), SND_CHANGEVOL);
		return 0;
	}
	
	new extra_health = PlayerData[player][ExtraHealth];
	new dmg = 100 + extra_health - GetClientHealth(player);
	if(dmg == 0)
	{
		EmitSoundToAll(HealthStation_FailSound, entity, SNDCHAN_VOICE, ATTN_TO_SNDLEVEL(1.3), SND_CHANGEVOL);
		return 0;
	}
	
	new healed = TakeFromStorage(entity, min(HealthStation_MaxHeal, dmg));
	new health = min(100+extra_health, GetClientHealth(player)+healed);
	SetEntityHealth(player, health);
	
	if (FloatAdd(TTT_HealthStation[m_fLastSoundTime][entity], 2.0) < GetEngineTime())
	{
		EmitSoundToAll(HealthStation_HealSound, entity, SNDCHAN_VOICE, ATTN_TO_SNDLEVEL(1.3));
		TTT_HealthStation[m_fLastSoundTime][entity] = GetEngineTime();
	}
	
	return healed;
	
	//Todo : we need to remain fingerprint
}

stock TakeFromStorage(entity, amount)
{
	amount = min(amount, TTT_HealthStation[m_iRemainHealth][entity]);
	SetStoredHealth(entity, max(0, GetStoredHealth(entity) - amount));
	return amount;
}

stock AddToStorage(entity, amount)
{
	SetStoredHealth(entity, min(HealthStation_MaxStored, GetStoredHealth(entity) + amount));
}

stock GetStoredHealth(entity)
{
	return TTT_HealthStation[m_iRemainHealth][entity];
}

stock SetStoredHealth(entity, amount)
{
	TTT_HealthStation[m_iRemainHealth][entity] = amount;
}

stock bool:IsHealthStationEntity(entity)
{
	if(IsValidEdict(entity) == false)
	{
		return false;
	}
	
	new String:classname[32];
	GetEdictClassname(entity, classname, sizeof(classname));
	if(StrEqual(classname, "ttt_health_station"))
	{
		return true;
	}
	return false;
}

stock bool:IsPlayerLookAtHealthStation(player, &entity)
{
	if(IsValidClient(player) == false || IsClientInGame(player) == false)
	{
		return false;
	}
	
	new Float:start[3], Float:angle[3];
	GetClientEyePosition(player, start);
	GetClientEyeAngles(player, angle);
	TR_TraceRayFilter(start, angle, MASK_SOLID, RayType_Infinite, TraceFilter_HealthStation_PassOneEntity, player);
	
	if(TR_DidHit() == false)
	{
		return false;
	}
	
	entity = TR_GetEntityIndex();
	if(IsValidEdict(entity) == false)
	{
		return false;
	}
	
	new String:classname[32];
	GetEdictClassname(entity, classname, sizeof(classname));
	if(StrEqual(classname, "ttt_health_station") == false)
	{
		return false;
	}
	
	new Float:position[3];
	GetEntPropVector(entity, Prop_Send, "m_vecOrigin", position);
	new Float:distance = GetVectorDistance(start, position);
	if(distance < 150.0)
	{
		return true;
	}
	return false;
}

public bool:TraceFilter_HealthStation_PassOneEntity(entity, mask, any:data)
{
	if(entity != data)
		return true;
	else
		return false;
}

public PlayerRunCmd_HealthStation(player, buttons)
{
	if(IsPlayerAlive(player) == false)
	{
		return;
	}
	
	if(buttons & IN_USE)
	{
		new entity;
		if(IsPlayerLookAtHealthStation(player, entity))
		{
			if(TTT_HealthStation[m_fNextHeal][entity] < GetEngineTime())
			{
				new healed = GiveHealth(entity, player);
				TTT_HealthStation[m_fNextHeal][entity] = GetEngineTime() + (HealthStation_HealFreq * healed);
			}
		}
	}
}

public HealthStation_ShowHud(entity, player)
{
	SetGlobalTransTarget(player);
	
	new String:Formatting[256];
	if(GetStoredHealth(entity) == 0) Format(Formatting, 256, "%t", "Empty Health Station");
	else Format(Formatting, 256, "%t\n%t : %i", "Health Station", "Remain Health Amount", GetStoredHealth(entity));
	HudMsg(player, MSGTYPE_PLAYERINFO, -1.0, 0.55, {255, 255, 255, 255}, {255, 255, 255, 255}, 2, 0.01, 1.0, 0.2, 0.0, Formatting);
}