
stock SpawnDeathStation(Client)
{
	new Float:spawn_position[3], Float:player_angle[3], Float:player_velocity[3], Float:throw_velocity[3];
	GetClientEyePosition(Client, spawn_position);
	GetClientEyeAngles(Client, player_angle);
	GetEntPropVector(Client, Prop_Data, "m_vecVelocity", player_velocity);
	GetAngleVectors(player_angle, throw_velocity, NULL_VECTOR, NULL_VECTOR);
	ScaleVector(throw_velocity, 200.0+GetVectorLength(player_velocity));
	
	new entity = CreateEntityByName("prop_physics_override");
	DispatchKeyValue(entity, "model", "models/items/healthkit.mdl");
	DispatchKeyValue(entity, "classname", "ttt_death_station");
	DispatchSpawn(entity);
	TeleportEntity(entity, spawn_position, NULL_VECTOR, throw_velocity);
	SetEntProp(entity, Prop_Send, "m_CollisionGroup", 11);//COLLISION_GROUP_WEAPON
	SetEntProp(entity, Prop_Data, "m_takedamage", 0, 1);//GOD Mode
	SetEntPropFloat(entity, Prop_Send, "m_flModelScale", 2.0);
	SetPlayerCanPickup(entity, false);
	//TTT_HealthStation[m_iHealth][entity] = GetRandomInt(200, 400);
	TTT_HealthStation[m_fLastSoundTime][entity] = 0.0;
	TTT_HealthStation[m_fNextHeal][entity] = 0.0;
	TTT_HealthStation[m_hPlacer][entity] = EntIndexToEntRef(Client);
	TTT_HealthStation[Detectives][entity] = false;
	EmitSoundToAll("weapons/slam/throw.wav", entity, SNDCHAN_VOICE, _, _, 0.7);
	//HookSingleEntityOutput(entity, "OnPlayerUse", HealthStation_Use, false);//we can't use this function T.T
	SDKHook(entity, SDKHook_OnTakeDamage, HealthStation_OnTakeDamage);
	TTT_RemainDNA(entity, Client);
}

stock bool:IsDeathStationEntity(entity)
{
	if(IsValidEdict(entity) == false)
	{
		return false;
	}
	
	new String:classname[32];
	GetEdictClassname(entity, classname, sizeof(classname));
	if(StrEqual(classname, "ttt_death_station"))
	{
		return true;
	}
	return false;
}

stock bool:IsPlayerLookAtDeathStation(player, &entity)
{
	if(IsValidClient(player) == false || IsClientInGame(player) == false)
	{
		return false;
	}
	
	new Float:start[3], Float:angle[3];
	GetClientEyePosition(player, start);
	GetClientEyeAngles(player, angle);
	TR_TraceRayFilter(start, angle, MASK_SOLID, RayType_Infinite, TraceFilter_HealthStation_PassOneEntity, player);
	
	if(TR_DidHit() == false)
	{
		return false;
	}
	
	entity = TR_GetEntityIndex();
	if(IsValidEdict(entity) == false)
	{
		return false;
	}
	
	new String:classname[32];
	GetEdictClassname(entity, classname, sizeof(classname));
	if(StrEqual(classname, "ttt_death_station") == false)
	{
		return false;
	}
	
	new Float:position[3];
	GetEntPropVector(entity, Prop_Send, "m_vecOrigin", position);
	new Float:distance = GetVectorDistance(start, position);
	if(distance < 150.0)
	{
		return true;
	}
	return false;
}

public PlayerRunCmd_DeathStation(player, buttons)
{
	if(IsPlayerAlive(player) == false)
	{
		return;
	}
	
	if(buttons & IN_USE)
	{
		new entity;
		if(IsPlayerLookAtDeathStation(player, entity))
		{
			if(TTT_HealthStation[m_fNextHeal][entity] < GetEngineTime())
			{
				TTT_HealthStation[m_fNextHeal][entity] = GetEngineTime() + (HealthStation_HealFreq * 6.25);
				new remainhealth = GetClientHealth(player) - 25;
				if(remainhealth <= 0)
				{
					new Float:position[3];
					GetEntPropVector(entity, Prop_Send, "m_vecOrigin", position);
					new owner = EntRefToEntIndex(TTT_HealthStation[m_hPlacer][entity]);
					makeDamage(owner != -1 ? owner : 0, player, 25, DMG_SHOCK, 0.0, position, "ttt_death_station");
					EmitSoundToAll(HealthStation_FailSound, entity, SNDCHAN_VOICE, ATTN_TO_SNDLEVEL(1.3), SND_CHANGEVOL);
					return;
				}
				else
				{
					SetEntityHealth(player, remainhealth);
					EmitSoundToAll(HealthStation_HealSound, entity, SNDCHAN_VOICE, ATTN_TO_SNDLEVEL(1.3));
				}
			}
		}
	}
}

public DeathStation_ShowHud(entity, player)
{
	new String:Formatting[256];
	Format(Formatting, 256, "%T\n%T : %i", (PlayerData[player][Job] == traitor ? "Death Station" : "Health Station"), player, "Remain Health Amount", player, 200);
	HudMsg(player, MSGTYPE_PLAYERINFO, -1.0, 0.55, {255, 255, 255, 255}, {255, 255, 255, 255}, 2, 0.01, 1.0, 0.2, 0.0, Formatting);
}