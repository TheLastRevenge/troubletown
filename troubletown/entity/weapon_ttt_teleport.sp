
stock Teleporter_ResetData(weapons)
{
	Teleporter[weapons][iRemainCount] = TELEPORT_MAX_COUNT;
	Teleporter[weapons][savedLocation] = false;
}

stock Teleporter_SetDelay(weapons)
{
	SetWeaponNextAttack(weapons, TELEPORT_ATTACKDELAY);
}

stock Teleport_GetRemainCount(weapons)
{
	if(!TTT_IsTeleporter(weapons)) {
		return -1;
	}
	return Teleporter[weapons][iRemainCount];
}

public PlayerRunCmd_Teleporter(Client, buttons, oldbuttons)
{
	new weapons = GetClientWeaponIndex(Client);
	if(!TTT_IsTeleporter(weapons)) {
		return;
	}
	
	if(!CanWeaponAttack(weapons)) {
		return;
	}
	
	if(!(oldbuttons & IN_ATTACK) && buttons & IN_ATTACK)
	{
		Teleporter_SetDelay(weapons);
		if(Teleporter[weapons][iRemainCount] && Teleporter[weapons][savedLocation] && g_TeleportTimer[Client] == INVALID_HANDLE)
		{
			new Float:vecOrigin[3];
			GetClientAbsOrigin(Client, vecOrigin);
			EmitSoundToAll("ambient/levels/labs/electric_explosion4.wav", 0, SNDCHAN_AUTO, ATTN_TO_SNDLEVEL(0.7), SND_CHANGEVOL, _, _, _, vecOrigin);
			
			for(new i; i<3; i++)
			{
				vecOrigin[i] = Teleporter[weapons][vecTeleportOrigin][i];
			}
			EmitSoundToAll("ambient/levels/labs/electric_explosion2.wav", 0, SNDCHAN_AUTO, ATTN_TO_SNDLEVEL(0.7), SND_CHANGEVOL, _, _, _, vecOrigin);
			
			TTT_SetProgressBar(Client, GetGameTime()-FloatSub(1.0, TELEPORT_DELAY), 1);
			SetEntityMoveType(Client, MOVETYPE_NONE);
			SetEntityRenderMode(Client, RENDER_TRANSCOLOR);
			SendWeaponAnim(weapons, ACT_VM_PRIMARYATTACK);
			
			SDKHook(Client, SDKHook_PostThinkPost, Hook_CheckTeleport);
			g_TeleportTimer[Client] = CreateTimer(TELEPORT_DELAY, g_TeleportTimer_Event, Client);
			Teleporter[weapons][fStartTime] = GetEngineTime();
		}
	}
	
	if(!(oldbuttons & IN_ATTACK2) && buttons & IN_ATTACK2)
	{
		Teleporter_SetDelay(weapons);
		if(Teleporter[weapons][iRemainCount] && CanStoreTeleport(Client, buttons))
		{
			ClientCommand(Client, "play buttons/combine_button7");
			
			new Float:vecOrigin[3], Float:vecAngles[3];
			GetClientAbsOrigin(Client, vecOrigin);
			GetClientEyeAngles(Client, vecAngles);
			for(new i; i<3; i++)
			{
				Teleporter[weapons][vecTeleportOrigin][i] = vecOrigin[i];
				Teleporter[weapons][vecTeleportAngle][i] = vecAngles[i];
			}
			Teleporter[weapons][savedLocation] = true;
		}
	}
}

stock bool:CanStoreTeleport(Client, buttons)
{
	if(!IsClientInGame(Client) || !IsPlayerAlive(Client)) {
		return false;
	}
	
	new ground_entity = GetGroundEntity(Client);
	if(ground_entity || GetEntityMoveType(Client) == MOVETYPE_NONE || buttons & IN_DUCK || g_TeleportTimer[Client] != INVALID_HANDLE) {
		return false;
	}
	return true;
}

public Hook_CheckTeleport(Client)
{
	if(g_TeleportTimer[Client] != INVALID_HANDLE)
	{
		if(!IsClientInGame(Client) || !IsPlayerAlive(Client))
		{
			SDKUnhook(Client, SDKHook_PostThinkPost, Hook_CheckTeleport);
			CloseTimer(g_TeleportTimer[Client]);
			return;
		}
		
		new weapons = GetClientWeaponIndex(Client);
		if(!TTT_IsTeleporter(weapons))
		{
			SDKUnhook(Client, SDKHook_PostThinkPost, Hook_CheckTeleport);
			CloseTimer(g_TeleportTimer[Client]);
			
			TTT_SetProgressBar(Client, 0.0, 0);
			SetEntityMoveType(Client, MOVETYPE_WALK);
			SetEntityRenderColor(Client, 255, 255, 255, 255);
		}
		else
		{
			SetEntityRenderColor(Client, 255, 255, 255, 255-RoundToNearest((GetEngineTime()-Teleporter[weapons][fStartTime])*250.0/TELEPORT_DELAY));
		}
	}
	else
	{
		SDKUnhook(Client, SDKHook_PostThinkPost, Hook_CheckTeleport);
	}
}

public Action:g_TeleportTimer_Event(Handle:timer, any:Client)
{
	g_TeleportTimer[Client] = INVALID_HANDLE;
	
	new weapons = GetClientWeaponIndex(Client);
	if(!TTT_IsTeleporter(weapons))
	{
		return;
	}
	
	TTT_SetProgressBar(Client, 0.0, 0);
	SetEntityMoveType(Client, MOVETYPE_WALK);
	SetEntityRenderColor(Client, 255, 255, 255, 255);
	SDKUnhook(Client, SDKHook_PostThinkPost, Hook_CheckTeleport);
	
	new Float:vecOrigin[3], Float:vecAngles[3];
	for(new i; i<3; i++)
	{
		vecOrigin[i] = Teleporter[weapons][vecTeleportOrigin][i];
		vecAngles[i] = Teleporter[weapons][vecTeleportAngle][i];
	}
	TeleportEntity(Client, vecOrigin, vecAngles, Float:{0.0,0.0,0.0});
	Teleporter[weapons][iRemainCount]--;
	
	vecOrigin[2] += 32.0;
	new sprite = CreateSprite(vecOrigin, "troubletown/teleport_track.vmt", 0.4);
	CreateTimer(60.0, RemoveEntity_Timer, EntIndexToEntRef(sprite));
}