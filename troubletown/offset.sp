
public Offset_Initialize()
{
	new offset = FindSendPropOffs("CCSPlayer","m_hMyWeapons");
	Offset[m_hMyWeapons] = offset;
	
	offset = FindSendPropOffs("CBasePlayer","m_hViewModel");
	Offset[m_hViewModel] = offset;
	
	offset = FindSendPropOffs("CBaseEntity","m_nModelIndex");
	Offset[m_nModelIndex] = offset;
	
	offset = FindSendPropOffs("CBaseCombatWeapon","m_iWorldModelIndex");
	Offset[m_iWorldModelIndex] = offset;
	
	offset = FindSendPropOffs("CBaseEntity","m_fEffects");
	Offset[m_fEffects] = offset;
	
	offset = FindSendPropOffs("CBaseAnimating","m_nSequence");
	Offset[m_nSequence] = offset;
	
	offset = FindSendPropOffs("CBaseAnimating","m_flPlaybackRate");
	Offset[m_flPlaybackRate] = offset;
	
	offset = FindSendPropOffs("CBaseAnimating","m_flCycle");
	Offset[m_flCycle] = offset;
	
	offset = FindSendPropOffs("CPredictedViewModel","m_hWeapon");
	Offset[m_hWeapon] = offset;
	
	offset = FindSendPropOffs("CBasePlayer","m_hGroundEntity");
	Offset[m_hGroundEntity] = offset;
	
	offset = FindSendPropOffs("CBaseEntity", "m_CollisionGroup");
	Offset[m_CollisionGroup] = offset;
	
	offset = FindSendPropInfo("CBasePlayer", "m_vecPunchAngle");
	Offset[m_vecPunchAngle] = offset;
}